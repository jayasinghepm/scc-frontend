import {Component, Inject, OnInit, Optional} from '@angular/core';
import {BackendService} from "../../@core/rest/bo_service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {MassterDataService} from "../../@core/service/masster-data.service";
import {UserState} from "../../@core/auth/UserState";
import {RequestGroup} from "../../@core/enums/request-group-enum";
import {RequestType} from "../../@core/enums/request-type-enums";
import {Router} from "@angular/router";
import {validateEmail, validateMobile, validateTelephone} from "../../@core/utils/validator";

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {

  public  showPassswordMatch = false;
  public showPasswordValid = false;
  public disableClose = false;
  public isFirstTime  = false;
  HNB:boolean = false;
  public settingModel = {
    userName: '',
    password: '',
    newPassword:'',
    confirmPassword: '',

  }

  private invalidFieldMessage = undefined;

  public clientModel = {
    email: '',
    telephoneNo: '',
    mobileNo: '',
    fname: '',
    lname: '',

  }

  public isClient = false;
  public isDataEntryUser = false;

  constructor(private boService:BackendService,
              public dialogRef: MatDialogRef<SettingComponent>,
              private  toastSerivce:  NbToastrService,
              private masterData: MassterDataService,
              private router: Router,
              @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any,) {

    console.log(this.popupData)
    this.isFirstTime = this.popupData.isFirstTime;
    //Special Requirement for HNB
    if( UserState.getInstance().companyId === 73){this.HNB = true}//73
    this.onLoadData()
    this.settingModel.userName = UserState.getInstance().username;
    this.settingModel.confirmPassword = ''
    this.settingModel.newPassword = ''
    this.isClient = UserState.getInstance().userType === 1;
    this.isDataEntryUser = UserState.getInstance().userType === 4;

  }

  ngOnInit() {
  }

  closeDialog() {
    if (!this.popupData.disableClose) {
      this.dialogRef.close();
    }
  }

  onPasswordChange($event: any) {
    if (this.validatePassword() === false) {
      return;
    }
    if (this.settingModel.newPassword !== this.settingModel.confirmPassword){
      this.showPassswordMatch = true;
      return;
    }
    this.showPassswordMatch = false;
    this.settingModel.password = this.settingModel.confirmPassword;
  }

  onClickSave() {
    if (!this.validatePassword()) {
      
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
        return;
    }
    
    switch(UserState.getInstance().userType) {
      case 1: {
        this.boService.sendRequestToBackend(
          RequestGroup.LoginUser,
          RequestType.ManageUserLogin,
          {
            DATA: {
              userId: UserState.getInstance().userId,
              userType: 1,
              loginStatus: 1,
              userName: this.settingModel.userName,
              password: this.settingModel.password,
              isFirstTime: 1,
            }
          }
        ).then(data  => {
          // this.isDisabledButton = false;
          console.log(data)
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.dto != undefined) {
              // this.toastSerivce.show('', 'Saved data successfully.', {
              //   status: 'success',
              //   destroyByClick: true,
              //   duration: 2000,
              //   hasIcon: false,
              //   position: NbGlobalPhysicalPosition.TOP_RIGHT,
              //   preventDuplicates: true,
              // });
              let jsonBody = {
                DATA: {
                  id: UserState.getInstance().clientId,
                  telephoneNo: this.clientModel.telephoneNo,
                  mobileNo: this.clientModel.mobileNo,
                  email: this.clientModel.email,
                  branchId: UserState.getInstance().branchId,
                  companyId: UserState.getInstance().companyId,
                  firstName: this.clientModel.fname,
                  lastName: this.clientModel.lname,
                }
              }
             if (this.isFirstTime) {
               if (!this.validate()) {
                 if (this.invalidFieldMessage !== undefined) {
                   this.toastSerivce.show('', this.invalidFieldMessage, {
                     status: 'warning',
                     destroyByClick: true,
                     duration: 2000,
                     hasIcon: false,
                     position: NbGlobalPhysicalPosition.TOP_RIGHT,
                     preventDuplicates: true,
                   });
                 } else {
                   this.toastSerivce.show('', 'Fill Empty Fields', {
                     status: 'warning',
                     destroyByClick: true,
                     duration: 2000,
                     hasIcon: false,
                     position: NbGlobalPhysicalPosition.TOP_RIGHT,
                     preventDuplicates: true,
                   });
                 }
                 return;
               }else {
                 this.boService.sendRequestToBackend(RequestGroup.BusinessUsers, RequestType.ManageClient,jsonBody).then( data => {
                   console.log(data)
                   // this.isDisabledButton = false;
                   if (data.HED != undefined && data.HED.RES_STS == 1) {
                     if (data.DAT != undefined && data.DAT.dto != undefined) {
                       this.toastSerivce.show('', 'Saved data successfully.', {
                         status: 'success',
                         destroyByClick: true,
                         duration: 2000,
                         hasIcon: false,
                         position: NbGlobalPhysicalPosition.TOP_RIGHT,
                         preventDuplicates: true,
                       });
                       this.router.navigateByUrl('home'); // ##login
                       this.disableClose = false;
                       this.dialogRef.close(true)
                       // this.userId = data.DAT.dto.userId;
                       // this.dialogRef.close(true);
                     } else {
                       this.toastSerivce.show('', 'Error in saving data.', {
                         status: 'danger',
                         destroyByClick: true,
                         duration: 2000,
                         hasIcon: false,
                         position: NbGlobalPhysicalPosition.TOP_RIGHT,
                         preventDuplicates: true,
                       })
                     }
                   }else {
                     this.toastSerivce.show('', 'Error in saving data.', {
                       status: 'danger',
                       destroyByClick: true,
                       duration: 2000,
                       hasIcon: false,
                       position: NbGlobalPhysicalPosition.TOP_RIGHT,
                       preventDuplicates: true,
                     })
                   }
                 })

                 this.disableClose = true;
               }
             }
             else {
               this.router.navigateByUrl('home');  // ##login
             }


              // console.log(data.DAT);
              // this.userId = data.DAT.dto.userId;
            } else {
              this.toastSerivce.show('Try with different user name', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          }else {
            this.toastSerivce.show('Try with different user name', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
        break;
      }
      case 2: {
        this.boService.sendRequestToBackend(
          RequestGroup.LoginUser,
          RequestType.ManageUserLogin,
          {
            DATA: {
              userId: UserState.getInstance().userId,
              userType: 2,
              loginStatus: 1,
              userName: this.settingModel.userName,
              password: this.settingModel.password,
              isFirstTime: 1,
            }
          }
        ).then(data  => {
          // this.isDisabledButton = false;
          console.log(data)
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.dto != undefined) {
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              });
              this.router.navigateByUrl('home');  // ##login
              // this.isClientActive = true;
              this.disableClose = true;
              this.dialogRef.close(true)
              console.log(data.DAT);
              // this.userId = data.DAT.dto.userId;
            } else {
              this.toastSerivce.show('Try with different user name', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          }else {
            this.toastSerivce.show('Try with different user name', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })

        break;
      }
      case 3: {
        this.boService.sendRequestToBackend(
          RequestGroup.LoginUser,
          RequestType.ManageUserLogin,
          {
            DATA: {
              userId: UserState.getInstance().userId,
              userType: 3,
              loginStatus: 1,
              userName: this.settingModel.userName,
              password: this.settingModel.password,
              isFirstTime: 1,
            }
          }
        ).then(data  => {
          // this.isDisabledButton = false;
          console.log(data)
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.dto != undefined) {
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              });
              this.router.navigateByUrl('home');  // ##login
              // this.isClientActive = true;
              this.disableClose = true;

              console.log(data.DAT);
              this.dialogRef.close(true)
              // this.userId = data.DAT.dto.userId;
            } else {
              this.toastSerivce.show('Try with different user name', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          }else {
            this.toastSerivce.show('Try with different user name', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
        break;
      }
      case 4: {
        if (!this.validate()) {
          if (this.invalidFieldMessage !== undefined) {
            this.toastSerivce.show('', this.invalidFieldMessage, {
              status: 'warning',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            });
          } else {
            this.toastSerivce.show('', 'Fill Empty Fields', {
              status: 'warning',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            });
          }
          return;
        }
        this.boService.sendRequestToBackend(
          RequestGroup.LoginUser,
          RequestType.ManageUserLogin,
          {
            DATA: {
              userId: UserState.getInstance().userId,
              userType: 4,
              loginStatus: 1,
              userName: this.settingModel.userName,
              password: this.settingModel.password,
              isFirstTime: 1,
            }
          }
        ).then(data  => {
          // this.isDisabledButton = false;
          console.log(data)
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.dto != undefined) {


              // this.toastSerivce.show('', 'Saved data successfully.', {
              //   status: 'success',
              //   destroyByClick: true,
              //   duration: 2000,
              //   hasIcon: false,
              //   position: NbGlobalPhysicalPosition.TOP_RIGHT,
              //   preventDuplicates: true,
              // });
              if (this.isFirstTime) {
                this.boService.sendRequestToBackend(
                  RequestGroup.BusinessUsers,
                  RequestType.ManageDataEntryUser,
                  {DATA: {
                      id: UserState.getInstance().dataEntryUserId,
                      userId: UserState.getInstance().userId,
                      email: this.clientModel.email,
                      telephoneNo: this.clientModel.telephoneNo,
                      mobileNo: this.clientModel.mobileNo
                    }}
                ).then(data => {
                  if (data.HED != undefined && data.HED.RES_STS == 1) {
                    if (data.DAT != undefined && data.DAT.dto != undefined) {
                      this.router.navigateByUrl('home');  // ##login
                      // this.isClientActive = true;
                      this.disableClose = true;
                      this.dialogRef.close(true)


                      this.toastSerivce.show('', 'Saved data successfully.', {
                        status: 'success',
                        destroyByClick: true,
                        duration: 2000,
                        hasIcon: false,
                        position: NbGlobalPhysicalPosition.TOP_RIGHT,
                        preventDuplicates: true,
                      });

                      // this.userId = data.DAT.dto.userId;
                      this.dialogRef.close(true);
                    } else {
                      this.toastSerivce.show('', 'Error in saving data.', {
                        status: 'danger',
                        destroyByClick: true,
                        duration: 2000,
                        hasIcon: false,
                        position: NbGlobalPhysicalPosition.TOP_RIGHT,
                        preventDuplicates: true,
                      })
                    }
                  }else {
                    this.toastSerivce.show('', 'Error in saving data.', {
                      status: 'danger',
                      destroyByClick: true,
                      duration: 2000,
                      hasIcon: false,
                      position: NbGlobalPhysicalPosition.TOP_RIGHT,
                      preventDuplicates: true,
                    })
                  }
                })
              }
              else {
                this.router.navigateByUrl('home');  // ##login
              }

              console.log(data.DAT);
              // this.userId = data.DAT.dto.userId;
            } else {
              this.toastSerivce.show('Try with different user name', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          }else {
            this.toastSerivce.show('Try with different user name', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })

        break;
      }
    }
  }

  private validatePassword():boolean {
    if (this.settingModel.confirmPassword === '' || this.settingModel.confirmPassword === undefined ||
      this.settingModel.confirmPassword === '' || this.settingModel.confirmPassword === undefined
    ) {
      return false;
    }
    if (this.settingModel.newPassword.length < 8) {
      this.showPasswordValid = true;
      this.showPassswordMatch = false;
      return false;
    }
     let rgularExp = {
      contains_alphaNumeric : '/^(?!-)(?!.*-)[A-Za-z0-9-]+(?<!-)$/',
      containsNumber : new RegExp('/\d+/') ,
      containsAlphabet : new RegExp('/[a-zA-Z]/'),

      onlyLetters : new RegExp('/^[A-Za-z]+$/'),
      onlyNumbers :new RegExp('/^[0-9]+$/') ,
      onlyMixOfAlphaNumeric : new RegExp('/^([0-9]+[a-zA-Z]+|[a-zA-Z]+[0-9]+)[0-9a-zA-Z]*$/')
    }

    let expMatch = {
      containsNumber: false,
      containsAlphabet: false,
      alphaNumeric: false,
    };
    // expMatch.containsNumber = ;
    // expMatch.containsAlphabet = rgularExp.containsAlphabet.test(this.settingModel.newPassword);
    // // expMatch.alphaNumeric = rgularExp.contains_alphaNumeric.test(this.settingModel.confirmPassword);

    // expMatch.onlyNumbers = rgularExp.onlyNumbers.test(str);
    // expMatch.onlyLetters = rgularExp.onlyLetters.test(str);
    // expMatch.mixOfAlphaNumeric = rgularExp.onlyMixOfAlphaNumeric.test(str);
    console.log(rgularExp.containsNumber.test(this.settingModel.newPassword))
    const numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8,9];
    for (let n of numbers) {
      if (this.settingModel.newPassword.indexOf(`${n}`) >= 0) {
        this.showPasswordValid = false;
        return true;
      }
    }
    this.showPasswordValid = true;
    this.showPassswordMatch = false;
    return false;;
    // if (rgularExp.containsNumber.test(this.settingModel.newPassword) === false) {
    //
    // }
    // this.showPasswordValid = false;
    // return true;

  }

  private onLoadData():void {
    switch(UserState.getInstance().userType) {
      case 1: {
        this.boService.sendRequestToBackend(
          RequestGroup.BusinessUsers,
          RequestType.ListClient,
          {
            FILTER_MODEL: {
                 id: { value: UserState.getInstance().clientId , type: 1, col: 1}
            },
          }
        ).then(data => {
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.list != undefined) {
              let list = [];
              console.log(data.DAT.list)
              data.DAT.list.forEach(val => {
                if (val != undefined) {
                    this.clientModel.email = val.email;
                  this.clientModel.telephoneNo = val.telephoneNo;
                  this.clientModel.mobileNo = val.mobileNo;
                }
              });

            } else {
              //  todo: show snack bar
              console.log('error data 1')
            }
          } else {
            //  todo: show snack bar
            console.log('error data 2')
          }

        });
        break;
          }
  }
  }

  private validate(): boolean {
    console.log(this.clientModel)
    this.invalidFieldMessage = undefined;
    if ((UserState.getInstance().userType ===1  || UserState.getInstance().userType === 4) && this.isFirstTime) {
      if (this.clientModel.email === '' || this.clientModel.email === undefined  ) {
        return false;
      } else if (!validateEmail(this.clientModel.email)){
        this.invalidFieldMessage = 'Enter Valid Email'
        return false;
      }
      if (this.clientModel.telephoneNo === '' || this.clientModel.telephoneNo === undefined  ) {
        return false;
      } else if (!validateTelephone(this.clientModel.telephoneNo)) {
        console.log(this.clientModel.telephoneNo)
        this.invalidFieldMessage = 'Enter Valid Telephone Number'
        return false;
      }
      if (this.clientModel.mobileNo === '' || this.clientModel.mobileNo === undefined ) {
        return false;
      } else if (!validateMobile(this.clientModel.mobileNo)) {
        this.invalidFieldMessage = 'Enter Valid Mobile Number'
        return false;
      }
    }
    if (this.settingModel.userName === '' || this.settingModel.userName === undefined) {
      return false;
    }
    if (this.settingModel.password ===  '' || this.settingModel.password === undefined) {
      return false;
    }
    
    return true;
  }
}
