import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransLocPurFormulaComponent } from './trans-loc-pur-formula.component';

describe('TransLocPurFormulaComponent', () => {
  let component: TransLocPurFormulaComponent;
  let fixture: ComponentFixture<TransLocPurFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransLocPurFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransLocPurFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
