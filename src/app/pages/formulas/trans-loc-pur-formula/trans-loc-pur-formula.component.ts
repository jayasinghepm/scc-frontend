import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-trans-loc-pur-formula',
  templateUrl: './trans-loc-pur-formula.component.html',
  styleUrls: ['./trans-loc-pur-formula.component.scss']
})
export class TransLocPurFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<TransLocPurFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

}
