import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaperWasteFormulaComponent } from './paper-waste-formula.component';

describe('PaperWasteFormulaComponent', () => {
  let component: PaperWasteFormulaComponent;
  let fixture: ComponentFixture<PaperWasteFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaperWasteFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaperWasteFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
