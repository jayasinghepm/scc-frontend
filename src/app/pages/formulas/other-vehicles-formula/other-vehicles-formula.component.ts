import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-other-vehicles-formula',
  templateUrl: './other-vehicles-formula.component.html',
  styleUrls: ['./other-vehicles-formula.component.scss']
})
export class OtherVehiclesFormulaComponent implements OnInit {

  public isPetrol : boolean = false;

  constructor( public dialogRef: MatDialogRef<OtherVehiclesFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)
    if (popupData.data.emissionDetails.isPetrol) {
      this.isPetrol = true;
    }

  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

}
