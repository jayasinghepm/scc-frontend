import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherVehiclesFormulaComponent } from './other-vehicles-formula.component';

describe('OtherVehiclesFormulaComponent', () => {
  let component: OtherVehiclesFormulaComponent;
  let fixture: ComponentFixture<OtherVehiclesFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherVehiclesFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherVehiclesFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
