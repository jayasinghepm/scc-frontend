import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-refri-formula',
  templateUrl: './refri-formula.component.html',
  styleUrls: ['./refri-formula.component.scss']
})
export class RefriFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<RefriFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

}
