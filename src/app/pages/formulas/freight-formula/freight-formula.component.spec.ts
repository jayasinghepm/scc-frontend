import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreightFormulaComponent } from './freight-formula.component';

describe('FreightFormulaComponent', () => {
  let component: FreightFormulaComponent;
  let fixture: ComponentFixture<FreightFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreightFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreightFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
