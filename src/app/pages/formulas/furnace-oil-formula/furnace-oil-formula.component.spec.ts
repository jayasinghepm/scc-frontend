import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FurnaceOilFormulaComponent } from './furnace-oil-formula.component';

describe('FurnaceOilFormulaComponent', () => {
  let component: FurnaceOilFormulaComponent;
  let fixture: ComponentFixture<FurnaceOilFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FurnaceOilFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FurnaceOilFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
