import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-mun-water-formula',
  templateUrl: './mun-water-formula.component.html',
  styleUrls: ['./mun-water-formula.component.scss']
})
export class MunWaterFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<MunWaterFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

}
