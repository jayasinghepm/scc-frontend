import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-oil-gas-trans-formula',
  templateUrl: './oil-gas-trans-formula.component.html',
  styleUrls: ['./oil-gas-trans-formula.component.scss']
})
export class OilGasTransFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<OilGasTransFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)


  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

}
