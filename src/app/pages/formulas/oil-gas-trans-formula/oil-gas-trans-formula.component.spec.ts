import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OilGasTransFormulaComponent } from './oil-gas-trans-formula.component';

describe('OilGasTransFormulaComponent', () => {
  let component: OilGasTransFormulaComponent;
  let fixture: ComponentFixture<OilGasTransFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OilGasTransFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OilGasTransFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
