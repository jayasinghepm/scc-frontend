import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneratorsFormulaComponent } from './generators-formula.component';

describe('GeneratorsFormulaComponent', () => {
  let component: GeneratorsFormulaComponent;
  let fixture: ComponentFixture<GeneratorsFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneratorsFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneratorsFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
