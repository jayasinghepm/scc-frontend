import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-wast-trans-formula',
  templateUrl: './wast-trans-formula.component.html',
  styleUrls: ['./wast-trans-formula.component.scss']
})
export class WastTransFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<WastTransFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }
}
