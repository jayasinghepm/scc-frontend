import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WastDisFormulaComponent } from './wast-dis-formula.component';

describe('WastDisFormulaComponent', () => {
  let component: WastDisFormulaComponent;
  let fixture: ComponentFixture<WastDisFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WastDisFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WastDisFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
