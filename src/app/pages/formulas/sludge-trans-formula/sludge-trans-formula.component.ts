import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-sludge-trans-formula',
  templateUrl: './sludge-trans-formula.component.html',
  styleUrls: ['./sludge-trans-formula.component.scss']
})
export class SludgeTransFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<SludgeTransFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)


  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

}
