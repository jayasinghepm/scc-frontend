import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-waste-water-treatment-formula',
  templateUrl: './waste-water-treatment-formula.component.html',
  styleUrls: ['./waste-water-treatment-formula.component.scss']
})
export class WasteWaterTreatmentFormulaComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<WasteWaterTreatmentFormulaComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) { }

  ngOnInit() {
  }
  closeDialog() {
    this.dialogRef.close();

  }
}
