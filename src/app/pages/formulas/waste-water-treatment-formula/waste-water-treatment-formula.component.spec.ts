import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WasteWaterTreatmentFormulaComponent } from './waste-water-treatment-formula.component';

describe('WasteWaterTreatmentFormulaComponent', () => {
  let component: WasteWaterTreatmentFormulaComponent;
  let fixture: ComponentFixture<WasteWaterTreatmentFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WasteWaterTreatmentFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WasteWaterTreatmentFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
