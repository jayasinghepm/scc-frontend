import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-saw-dust-trans-formula',
  templateUrl: './saw-dust-trans-formula.component.html',
  styleUrls: ['./saw-dust-trans-formula.component.scss']
})
export class SawDustTransFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<SawDustTransFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)


  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

}
