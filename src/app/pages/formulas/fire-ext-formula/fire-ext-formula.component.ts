import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-fire-ext-formula',
  templateUrl: './fire-ext-formula.component.html',
  styleUrls: ['./fire-ext-formula.component.scss']
})
export class FireExtFormulaComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<FireExtFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }
}
