import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BiomassFormulaComponent } from './biomass-formula.component';

describe('BiomassFormulaComponent', () => {
  let component: BiomassFormulaComponent;
  let fixture: ComponentFixture<BiomassFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BiomassFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BiomassFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
