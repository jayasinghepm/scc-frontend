import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaidManagerVehicleFormulaComponent } from './paid-manager-vehicle-formula.component';

describe('PaidManagerVehicleFormulaComponent', () => {
  let component: PaidManagerVehicleFormulaComponent;
  let fixture: ComponentFixture<PaidManagerVehicleFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaidManagerVehicleFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaidManagerVehicleFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
