import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-paid-manager-vehicle-formula',
  templateUrl: './paid-manager-vehicle-formula.component.html',
  styleUrls: ['./paid-manager-vehicle-formula.component.scss']
})
export class PaidManagerVehicleFormulaComponent implements OnInit {

  public isPetrol : boolean = false;

  constructor( public dialogRef: MatDialogRef<PaidManagerVehicleFormulaComponent>,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    console.log(this.popupData)

    if (popupData.data.emissionDetails.isPetrol) {
      this.isPetrol = true;
    }

  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

}
