import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForkliftsFormulaComponent } from './forklifts-formula.component';

describe('ForkliftsFormulaComponent', () => {
  let component: ForkliftsFormulaComponent;
  let fixture: ComponentFixture<ForkliftsFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForkliftsFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForkliftsFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
