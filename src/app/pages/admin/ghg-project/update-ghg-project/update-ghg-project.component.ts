import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from "@angular/material";
import {MassterDataService} from "../../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {BackendService} from "../../../../@core/rest/bo_service";
import * as moment from 'moment';
import {RequestGroup} from "../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../@core/enums/request-type-enums";

@Component({
  selector: 'app-update-ghg-project',
  templateUrl: './update-ghg-project.component.html',
  styleUrls: ['./update-ghg-project.component.scss']
})
export class UpdateGhgProjectComponent implements OnInit {

  public companies = [];
  public numPays = [ 2, 3];

  public jsonBody = {
    id: undefined,
    companyId: undefined,
    endDate: undefined,
    isExtended: undefined,
    signedWith: undefined,
    status: undefined,
    numberOfPayments: undefined,
    name: undefined,
    intiatedDate: undefined,
    signedDate: undefined,
    firstPayDate: undefined,
    secondPayDate: undefined,
    finalPayDate: undefined,
    extendedDate: undefined,
    isDeleted: undefined,
    company: undefined,
    statusStr: undefined,
    disableFormula: undefined,
  }

  public startDate: any;
  public endDate: any;
  public extendedDate: any;
  public initialPayDate:any;
  public secondPayDate: any;
  public finalPayDate: any;

  public paidInitial = false;
  public paidSecond = false;
  public paidFinal = false;



  constructor(
    public dialogRef: MatDialogRef<UpdateGhgProjectComponent>,
    private masterData: MassterDataService,
    private toastSerivce: NbToastrService,
    private boService: BackendService,
    @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any,
  ) { }

  ngOnInit() {
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companies = d;
      console.log(d);
    })
    this.jsonBody.id = -1;
    if (!this.popupData.isNew) {
      this.jsonBody.id = this.popupData.data.id;
      this.jsonBody.companyId = this.popupData.data.idCompany;
      this.jsonBody.name = this.popupData.data.name ;
      this.jsonBody.endDate = this.popupData.data. endDate ;
      this.jsonBody.isExtended = this.popupData.data.isExtended ;
      this.jsonBody.isExtended = this.popupData.data.isExtended ;
      this.jsonBody.signedWith = this.popupData.data.signedWith;
      this.jsonBody.status = this.popupData.data.status ;
      this.jsonBody.numberOfPayments = this.popupData.data.numsPays;
      this.jsonBody.intiatedDate = this.popupData.data.initiatedDate ;
      this.jsonBody.signedDate = this.popupData.data.signedDate ;
      this.jsonBody.firstPayDate = this.popupData.data.firstDate ;
      this.jsonBody.secondPayDate = this.popupData.data.secondDate ;
      this.jsonBody.finalPayDate = this.popupData.data.finalDate ;
      this.jsonBody.extendedDate = this.popupData.data.extendedDate;
      this.jsonBody.disableFormula = this.popupData.data.disableFormula;
      this.startDate = moment(this.popupData.data.initiatedDate, 'YYYY-MM-DD').toDate()
      this.endDate = moment(this.popupData.data.endDate, 'YYYY-MM-DD').toDate()
      this.extendedDate = moment(this.popupData.data.extendedDate, 'YYYY-MM-DD').toDate()
      this.initialPayDate = moment(this.popupData.data.initialPayDate, 'YYYY-MM-DD').toDate()
      this.secondPayDate = moment(this.popupData.data.secondPayDate, 'YYYY-MM-DD').toDate()
      this.finalPayDate = moment(this.popupData.data.finalPayDate, 'YYYY-MM-DD').toDate()
      console.log(this.startDate)
      console.log(this.endDate);
      this.paidInitial = (this.popupData.data.initiatedDate === undefined || this.popupData.data.initiatedDate === '') ? false: true;
      this.paidSecond = (this.popupData.data.secondPayDate === undefined || this.popupData.data.secondPayDate === '') ? false: true;
      this.paidFinal = (this.popupData.data.finalPayDate === undefined || this.popupData.data.finalPayDate === '') ? false: true;
      // this.sing = moment(this.popupData.data.endDate, 'YYYY-MM-DD')

    }
  }

  validate(isEdit:boolean):boolean {
    if(this.jsonBody.companyId === undefined || this.jsonBody.companyId == '') {
      return false;
    }
    if (isEdit && (this.jsonBody.id === undefined || this.jsonBody.id == '' )) {
      return false;
    }
    if (this.jsonBody.name === undefined || this.jsonBody.name === '') {
      return false;
    }
    if (this.jsonBody.numberOfPayments === undefined) {
      return false;
    }
    if (this.jsonBody.isExtended && (this.jsonBody.extendedDate === undefined || this.jsonBody.extendedDate == '')) {
      return false;
    }
    if (this.paidInitial && (this.jsonBody.intiatedDate === undefined || this.jsonBody.intiatedDate == '')) {
      return false;
    }
    return true;
  }


  onStartDateChange($event: any) {
    console.log(typeof(this.startDate))
    this.jsonBody.intiatedDate = this.convert(this.startDate);
  }

  onEndDateChange($event: any) {
    this.jsonBody.endDate = this.convert(this.endDate)
  }

  onExtendDateChange($event: any) {
    this.jsonBody.extendedDate = this.convert(this.extendedDate)
  }

  onInitialPayDateChange($event: any) {
    this.jsonBody.firstPayDate = this.convert(this.initialPayDate)
  }

  onSecondPayDateChange($event: any) {
    this.jsonBody.secondPayDate = this.convert(this.secondPayDate)
  }

  onFinalPayDateChange($event: any) {
    this.jsonBody.finalPayDate = this.convert(this.finalPayDate)
  }

  private convert(str) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }

  onClickSave() {
    if (this.validate(!this.popupData.isNew)) {
      this.jsonBody.statusStr = "New";
      this.boService.sendRequestToBackend(
        RequestGroup.GHGProject,
        RequestType.ManageProject,
        {DATA: this.jsonBody}
      ).then(data => {
        console.log(data)
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.DTO != undefined) {
            console.log(data.DAT.dto)
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }else if (data.DAT !== undefined ) {
            // ENTITY_ACTION_STATUS: -1
            // NARRATION: "Already active project exist."
            this.toastSerivce.show('', data.DAT.NARRATION, {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
            return;
          }
        }

      });
    } else {
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }

  }

  closeDialog() {
    this.dialogRef.close(true);
  }

  onChangeCompany($event: MatSelectChange) {
    this.jsonBody.company = this.companies.filter(c => c.id === $event.value)[0].name;
  }

  onClickDisableFormula(disable: boolean) {
    this.jsonBody.disableFormula = disable ? 1 : 0;
    this.onClickSave();
  }
}
