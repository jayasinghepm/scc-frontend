import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateGhgProjectComponent } from './update-ghg-project.component';

describe('UpdateGhgProjectComponent', () => {
  let component: UpdateGhgProjectComponent;
  let fixture: ComponentFixture<UpdateGhgProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateGhgProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateGhgProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
