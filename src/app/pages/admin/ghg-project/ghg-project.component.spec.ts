import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GhgProjectComponent } from './ghg-project.component';

describe('GhgProjectComponent', () => {
  let component: GhgProjectComponent;
  let fixture: ComponentFixture<GhgProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GhgProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GhgProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
