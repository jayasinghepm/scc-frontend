import { Component, OnInit } from '@angular/core';
import {BackendService} from "../../../@core/rest/bo_service";
import {MassterDataService} from "../../../@core/service/masster-data.service";
import {NbToastrService} from "@nebular/theme";
import {MatDialog} from "@angular/material";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {UserState} from "../../../@core/auth/UserState";
import {DeleteConfirmComponent} from "../../delete-confirm/delete-confirm.component";
// import {UpdateAirtravelComponent} from "../../cadmin/emission/client-air-travel/update-airtravel/update-airtravel.component";
import {ChangeStatusComponent} from "./change-status/change-status.component";
import {UpdateGhgProjectComponent} from "./update-ghg-project/update-ghg-project.component";
// import {UpdateWasteDisComponent} from "../emission/client-waste-disposal/update-waste-dis/update-waste-dis.component";
import * as moment from 'moment';
import {LocalDataSource} from "../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";
import {LoadingScreenService} from "../../../@core/service/loading-screen-service";



@Component({
  selector: 'app-ghg-project',
  templateUrl: './ghg-project.component.html',
  styleUrls: ['./ghg-project.component.scss']
})
export class GhgProjectComponent implements OnInit {
  public pg_current = 0;
  public fetchedCount =0;
  public disableNext = false;
  public disablePrev = true;
  public totalCount = 0;
  public pages = 0;

  private filterModel = {
    id: { type: 1, col: 1, value: 0},
    statusStr: { type: 4, col: 3, value: ''},
    numberOfPayments: { type: 1, col: 1, value: 0},
    name:{ value: '' , type: 4, col: 3},
    company: { value: '' , type: 4, col: 3},
  }
  private sortModel = {
    id: { dir: '' },
    status:{ dir: '' },
    numberOfPayments: { dir: '' },
    name:{ dir: '' },
    company:{ dir: '' },
  }


  settings = {
    mode: 'external',
    pager: {
      display: false,
    },
    actions: {
      delete: true,
      edit: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    editor: {
      type: 'completer',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: 'Name',
        editable: false,
        addable: false,
        type: 'string',
        filter: true,
        sort: true
      },
      company: {
        title: 'Company',
        editable: false,
        addable: false,
        type: 'string',
        filter: true,
        sort: true
      },
      extended: {
        title: 'Extended',
        type: 'string',
        filter: false,
        sort: false,

      },
      //Todo: have to change
      payment: {
        title: 'payment',
        type: 'string',
        editable: false,
        addable: false,
        filter: false,
        sort: false,

      },
      remdays: {
        title: 'Remaining Days',
        type: 'string',
        editable: false,
        addable: false,
        filter: false,
        sort: false,
      },
      status: {
        title: 'Status',
        type: 'custom',
        renderComponent: ChangeStatusComponent,
        onComponentInitFunction: (instance: any) => {
          instance.save.subscribe(d => {
            this.loadData();
          })
        },
        editable: false,
        addable: false,
        filter: true,
        sort: true
      },

    }
  };
  mySetting = JSON.parse(JSON.stringify(this.settings))

  source: LocalDataSource = new LocalDataSource();



  constructor( private boService: BackendService,
               private masterData: MassterDataService,
               private toastSerivce: NbToastrService,
               private dialog: MatDialog,
               private loadingService: LoadingScreenService
               ) {

    this.loadData();
  }

  ngOnInit() {

  }

  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.GHGProject,
      RequestType.ListProject,
      {
        PAGE_NUMBER: this.pg_current,
        // FILTER_MODEL: {
        //   // companyId: { value: UserState.getInstance().companyId , type: 1, col: 1}
        // }
      }
    ).then(data => {
      console.log(data)
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          let list = [];
          this.totalCount = data.DAT.TOTAL_COUNT;
          this.pages = Math.ceil(this.totalCount/20);
          this.fetchedCount = data.DAT.LIST.length;
          if (this.fetchedCount < 20) {
            this.disableNext = true;
          }
          data.DAT.LIST.forEach(val => {
            if (val != undefined) {
              list.push(
                this.fromListRequest(val)
              );
            }
          });
          this.source.load(list);
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  onUpdate(event: any, isNew:boolean) {
    console.log(event);
    const dialogRef = this.dialog.open(UpdateGhgProjectComponent,
      {
        data : {data: event.data , isNew: isNew, header: isNew ? 'New Project': 'Update Project' },
        width: '600px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
        this.loadData();
      if (d) {
        this.loadData();
      }
    });
  }

  public onDelete($event: any) {
    console.log($event)
    const dialogRef = this.dialog.open(DeleteConfirmComponent,
      {
        data : { },
        width: '300px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.boService.sendRequestToBackend(
          RequestGroup.GHGProject,
          RequestType.ManageProject,
          {
            DATA: {
              id: $event.data.id,
              isDeleted: 1,
            }
          }).then(data => {
          console.log(data)
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.DTO != undefined) {
              this.pg_current = 0;
              this.loadFiltered(0);
            }
          }
        });
      }
    });
  }

  onClickNextPage() {
    this.pg_current++;
    this.disablePrev = false;
    if (this.isFilterEnabled()) {
      return this.loadFiltered(this.pg_current)
    }
    if (this.isSorterEnabled()) {
      return this.loadSorted(this.pg_current)
    }
    this.loadData();
  }

  onClickPrevPage() {
    if (this.pg_current >0) {
      this.pg_current--;
      this.disablePrev = this.pg_current === 0 ? true : false;
      if (this.isFilterEnabled()) {
        return this.loadFiltered(this.pg_current)
      }
      if (this.isSorterEnabled()) {
        return this.loadSorted(this.pg_current)
      }
      this.loadData();
    }
  }

  private isFilterEnabled():boolean {
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (this.filterModel[k].value !== '' || this.filterModel[k].value !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  private isSorterEnabled():boolean {
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (this.sortModel[k].dir !== '' || this.sortModel[k].dir !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  onFilter($event) {
    console.log($event)
    switch($event.query.column.id) {
     case 'company':  {
        this.filterModel.company.value = $event.query.query;
        break;
      }  case 'name':  {
        this.filterModel.name.value = $event.query.query;
        break;
      }  case 'status':  {
        this.filterModel.statusStr.value = $event.query.query;
        break;
      }

    }
    this.loadFiltered(0);
  }

  onSort($event) {
    this.sortModel  = {
      id: { dir: '' },
      status:{ dir: '' },
      numberOfPayments: { dir: '' },
      name:{ dir: '' },
      company:{ dir: '' },
    }
    console.log($event)
    switch($event.id) {
      case 'company':  {
        this.sortModel.company.dir = $event.direction;
        break;
      }  case 'status':  {
        this.sortModel.status.dir = $event.direction;
        break;
      }  case 'name':  {
        this.sortModel.name.dir = $event.direction;
        break;
      }
    }
    this.pg_current = 0;
    this.loadSorted(0)
  }

  loadFiltered(pageNum: number) {
    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }

    if (this.isSorterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.GHGProject,
        RequestType.ListProject,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });

            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }else {
      this.boService.sendRequestToBackend(
        RequestGroup.GHGProject,
        RequestType.ListProject,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }


  }

  loadSorted(pageNum: number) {

    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }
    if(this.isFilterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.GHGProject,
        RequestType.ListProject,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }  else {
      this.boService.sendRequestToBackend(
        RequestGroup.GHGProject,
        RequestType.ListProject,
        {
          PAGE_NUMBER: pageNum,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }
  }

  fromListRequest(dto:any): any {
    let company;
    // this.masterData.getCompanyName(dto.companyId).subscribe( d => { company = d;});
    let payment = 'Not Paid'
    if (dto.firstPayDate !== undefined && dto.firstPayDate !== '') {
      payment = 'Initial'
    }
    if (dto.secondPayDate !== undefined && dto.secondPayDate !== '') {
      payment = 'Second'
    }
    if (dto.finalPayDate !== undefined && dto.finalPayDate !== '') {
      payment = 'Final'
    }
    let remdays;
    if (dto.isExtended) {
      const extend = moment(dto.extendedDate, 'YYYY-MM-DD');
      // const start = moment(dto.intiatedDate , 'YYYY-MM-DD');
      const today = moment().startOf('day');
      remdays = extend.diff(today, 'days');
    } else {
      // const start = moment(dto.intiatedDate , 'YYYY-MM-DD');
      const end = moment(dto.endDate , 'YYYY-MM-DD');
      const today = moment().startOf('day');
      const diff = end.diff(today, 'days');
      remdays = diff
      console.log(remdays);
    }
    return {
      id: dto.id ,
      company: dto.company,
      idCompany: dto.companyId,
      name: dto.name ,
      endDate: dto.endDate ,
      extended: dto.isExtended ? 'Yes': 'No',
      isExtended: dto.isExtended,
      signedWith: dto.signedWith ,
      status: dto.status ,
      numsPays: dto.numberOfPayments ,
      initiatedDate: dto.intiatedDate ,
      signedDate: dto.signedDate ,
      firstDate: dto.firstPayDate ,
      secondDate: dto.secondPayDate ,
      finalDate: dto.finalPayDate ,
      extendedDate: dto.extendedDate ,
      isDeleted: dto.isDeleted ,
      payment: payment,
      remdays: remdays,
      disableFormula: dto.disableFormula,
    }
  }

}
