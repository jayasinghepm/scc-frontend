import { Component, OnInit } from '@angular/core';
import {LocalDataSource} from "../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";
import {BackendService} from "../../../@core/rest/bo_service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";

@Component({
  selector: 'app-emission-categories',
  templateUrl: './emission-categories.component.html',
  styleUrls: ['./emission-categories.component.scss']
})
export class EmissionCategoriesComponent implements OnInit {

  settings = {
    mode: 'inline',
    pager: {
      display: false,
    },
    actions: {
      delete: true,
      add: true,
      edit: true
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // id: {
      //   title: 'Id',
      //   type: 'number',
      //   filter: false,
      //   sort: false,
      //
      // },
      name: {
        title: 'Name',
        type: 'string',
        filter: false,
        sort: false,
      },

    }
  };
  source: LocalDataSource = new LocalDataSource();

  public compaines:any [] = [];
  public selectedCompanyId = -1;

  constructor(private boService: BackendService, private toastSerivce: NbToastrService) {
    this.loadData()
  }


  onUpdate($event: any, action : number) {


    console.log($event)
    let jsonBody = {
       id: action == 1 ? 0 : $event.data.id,
      comId: this.selectedCompanyId,
      name: action != 3 ? $event.newData.name  : $event.data.name ,
    }

    console.log(jsonBody)


    if (action != 3 && jsonBody.name == ''
    ) {
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      })
      return;
    }
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ManageEmissionCategory,
      { DATA: jsonBody, action,}
    ).then(data => {

        if (data !== undefined && data.DAT !== undefined &&  data.DAT.dto !== undefined) {
          this.toastSerivce.show('', 'Saved data successfully.', {
            status: 'success',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
          $event.confirm.resolve($event.newData);
          this.loadData();
        } else {
          this.toastSerivce.show('', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      }

    )
  }



  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListCompany,
      {
        PAGE_NUMBER: -1,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
           console.log(data.DAT.list)
          this.compaines=data.DAT.list;
          this.selectedCompanyId =data.DAT.list.length > 0 ? data.DAT.list[0].id : undefined;
          this.source.load(data.DAT.list[0].emissionCategories)



        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }


  ngOnInit() {
  }


  onChangeCompany(value: any) {
    this.source.load(this.compaines.find(c => c.id == value).emissionCategories);
  }

}
