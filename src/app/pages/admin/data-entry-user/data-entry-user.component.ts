import { Component, OnInit } from '@angular/core';
import {LocalDataSource} from "../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";
import {BackendService} from "../../../@core/rest/bo_service";
import {MassterDataService} from "../../../@core/service/masster-data.service";
import {NbToastrService, NbWindowService} from "@nebular/theme";
import {MatDialog} from "@angular/material";
import {Router} from "@angular/router";
import {AddCadminComponent} from "../com-admins/add-cadmin/add-cadmin.component";
import {DeleteConfirmComponent} from "../../delete-confirm/delete-confirm.component";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {AddDataEntryUserComponent} from "../add-data-entry-user/add-data-entry-user.component";


@Component({
  selector: 'app-data-entry-user',
  templateUrl: './data-entry-user.component.html',
  styleUrls: ['./data-entry-user.component.scss']
})
export class DataEntryUserComponent implements OnInit {

  public pg_current = 0;
  public fetchedCount =0;
  public disableNext = false;
  public disablePrev = true;
  public totalCount = 0;
  public pages = 0;

  private filterModel = {
    id: { value:0, col: 1, type: 1},
    firstName:{ value:'', col: 3, type: 4},
    lastName: { value:'', col: 3, type: 4},
    title:{ value:'', col: 3, type: 4},
    email: { value:'', col: 3, type: 4},
    telephoneNo: { value:'', col: 3, type: 4},
    mobileNo:{ value:'', col: 3, type: 4},
    position: { value:'', col: 3, type: 4},
    company: { value:'', col: 3, type: 4},
  }

  private sortModel = {
    id: {dir: ''},
    firstName: {dir: ''},
    lastName:{dir: ''},
    title: {dir: ''},
    email: {dir: ''},
    telephoneNo: {dir: ''},
    mobileNo: {dir: ''},
    position:{dir: ''},
    company: {dir: ''},
  }
  settings = {
    mode: 'external',
    pager: {
      display: false,
    },
    actions: {
      delete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    editor: {
      type: 'completer',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      company: {
        title: 'Company',
        editable: false,
        addable: false,
        type: 'string',
        filter: true,
        sort: true,
      },
      firstName: {
        title: 'First Name',
        type: 'string',
        filter: true,
        sort: true,
      }, lastName: {
        title: 'Last Name',
        type: 'string',
        filter: true,
        sort: true,
      },
      email: {
        title: 'Email',
        type: 'string',
        filter: true,
        sort: true,
      },
      mobileNo: {
        title: 'Mobile No',
        type: 'string',
        filter: true,
        sort: true,

      },
      telephoneNo: {
        title: 'Telephone NO',
        type: 'string',
        filter: true,
        sort: true,

      },
      position: {
        title: 'Position',
        type: 'string',
        filter: true,
        sort: true,
      },
    }
  };
  mySetting = JSON.parse(JSON.stringify(this.settings))

  source: LocalDataSource = new LocalDataSource();

  constructor(
    private boService: BackendService,
    private masterData: MassterDataService,
    private  toastSerivce:  NbToastrService,
    private windowService: NbWindowService,
    private dialog: MatDialog,
    private router: Router
  ) {
    this.loadData();
  }

  ngOnInit() {
  }


  onUpdate($event: any, isEdit: boolean) {
    // console.log($event)
    const dialogRef = this.dialog.open(
      AddDataEntryUserComponent,
      {
        data : { isEdit,data: $event.data},
        width: '480px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      });
    dialogRef.afterClosed().subscribe(d => {
      // console.log(d);
      if (d) {
        this.loadData();
      }
    })
  }

  public onDelete($event: any) {
    const dialogRef = this.dialog.open(DeleteConfirmComponent,
      {
        data : { },
        width: '300px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.boService.sendRequestToBackend(
          RequestGroup.BusinessUsers,
          RequestType.ManageDataEntryUser,
          {
            DATA: {
              id: $event.data.id,
              isDeleted: 1,
            }
          }).then(data => {
          // console.log(data);
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.dto != undefined) {
              this.pg_current = 0;
              this.loadFiltered(0);
            }
          }
        });
      }
    });
  }

  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.BusinessUsers,
      RequestType.ListDataEntryUser,
      {
        PAGE_NUMBER: this.pg_current,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.totalCount = data.DAT.TOTAL_COUNT;
          this.pages = Math.ceil(this.totalCount/20);
          this.fetchedCount = data.DAT.list.length;
          if (this.fetchedCount < 20) {
            this.disableNext = true;
          } else {
            this.disableNext = false;
          }
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              list.push(this.fromListRequest(val));
            }
          });
          this.source.load(list);
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  private fromListRequest(dto: any): any {
    let company;
    // let branch;
    let id;
    let name;
    let email
    let mobileNo;
    let telephoneNo;
    let sector;
    let clientCode;
    let position;
    let userId;


    id = dto.id;
    name = dto.title + " " + dto.firstName + " " + dto.lastName;
    email = dto.email;
    mobileNo = dto.mobileNo;
    telephoneNo = dto.telephoneNo;
    sector = dto.sector;
    position = dto.position;
    userId = dto.userId;

    return {
      id,
      company: dto.company,
      firstName: dto.firstName,
      lastName: dto.lastName,
      title: dto.title,
      email,
      mobileNo,
      telephoneNo,
      position,
      userId,
      companyId: dto.companyId,

    };



  }


  onClickNextPage() {
    this.pg_current++;
    this.disablePrev = false;
    if (this.isFilterEnabled()) {
      return this.loadFiltered(this.pg_current)
    }
    if (this.isSorterEnabled()) {
      return this.loadSorted(this.pg_current)
    }
    this.loadData();
  }

  onClickPrevPage() {
    if (this.pg_current >0) {
      this.pg_current--;
      this.disablePrev = this.pg_current === 0 ? true : false;
      if (this.isFilterEnabled()) {
        return this.loadFiltered(this.pg_current)
      }
      if (this.isSorterEnabled()) {
        return this.loadSorted(this.pg_current)
      }
      this.loadData();
    }
  }

  private isFilterEnabled():boolean {
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (this.filterModel[k].value !== '' || this.filterModel[k].value !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  private isSorterEnabled():boolean {
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (this.sortModel[k].dir !== '' || this.sortModel[k].dir !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  onFilter($event) {
    switch($event.query.column.id) {
      case 'company':  {
        this.filterModel.company.value = $event.query.query;
        break;
      }  case 'firstName':  {
        this.filterModel.firstName.value = $event.query.query;
        break;
      }  case 'lastName':  {
        this.filterModel.lastName.value = $event.query.query;
        break;
      } case 'id':  {
        this.filterModel.id.value = $event.query.query;
        break;
      } case 'email':  {
        this.filterModel.email.value = $event.query.query;
        break;
      } case 'mobileNo':  {
        this.filterModel.mobileNo.value = $event.query.query;
        break;
      }case 'telephoneNo':  {
        this.filterModel.telephoneNo.value = $event.query.query;
        break;
      }case 'position':  {
        this.filterModel.position.value = $event.query.query;
        break;
      }
    }
    this.pg_current = 0;
    this.loadFiltered(0);
  }

  onSort($event) {
    this.sortModel  = {
      id: {dir: ''},
      firstName: {dir: ''},
      lastName:{dir: ''},
      title: {dir: ''},
      email: {dir: ''},
      telephoneNo: {dir: ''},
      mobileNo: {dir: ''},
      position:{dir: ''},
      company: {dir: ''},
    }
    // console.log($event)
    switch($event.id) {
      case 'company':  {
        this.sortModel.company.dir = $event.direction;
        break;
      }  case 'firstName':  {
        this.sortModel.firstName.dir = $event.direction;
        break;
      }  case 'lastName':  {
        this.sortModel.lastName.dir = $event.direction;
        break;
      } case 'id':  {
        this.sortModel.id.dir = $event.direction;
        break;
      } case 'email':  {
        this.sortModel.email.dir = $event.direction;
        break;
      } case 'mobileNo':  {
        this.sortModel.mobileNo.dir = $event.direction;
        break;
      }case 'telephoneNo':  {
        this.sortModel.telephoneNo.dir = $event.direction;
        break;
      }case 'position':  {
        this.sortModel.position.dir = $event.direction;
        break;
      }
    }
    this.loadSorted(0)
  }

  loadFiltered(pageNum: number) {
    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }

    if (this.isSorterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.BusinessUsers,
        RequestType.ListDataEntryUser,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }else {
      this.boService.sendRequestToBackend(
        RequestGroup.BusinessUsers,
        RequestType.ListDataEntryUser,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }


  }

  loadSorted(pageNum: number) {

    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }
    if(this.isFilterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.BusinessUsers,
        RequestType.ListDataEntryUser,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }  else {
      this.boService.sendRequestToBackend(
        RequestGroup.BusinessUsers,
        RequestType.ListDataEntryUser,
        {
          PAGE_NUMBER: pageNum,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.list.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }
  }


}
