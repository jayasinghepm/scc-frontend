import {Component, OnInit} from '@angular/core';
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {BackendService} from "../../../@core/rest/bo_service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";

@Component({
  selector: 'app-emission-factors',
  templateUrl: './emission-factors.component.html',
  styleUrls: ['./emission-factors.component.scss']
})
export class EmissionFactorsComponent implements OnInit {

  currentYear: number = new Date().getFullYear();
  py :number = this.currentYear-1;
  ppy :number = this.currentYear-2;
  pppy :number = this.currentYear-3;

  public emissionFactors = {
    values: {
      den_petrol: 0,
      den_diesel : 0,
      net_caloric_petrol: 0,
      net_caloric_diesel: 0,
      ef_co2_s_diesel: 0,
      ef_ch4_s_diesel: 0,
      ef_n2o_s_diesel: 0,
      ef_co2_s_petrol: 0,
      ef_ch4_s_petrol: 0,
      ef_n20_s_petrol: 0,
      gwp_co2: 0,
      gwp_ch4: 0,
      gwp_n2o: 0,
      gwp_r22 : 0,
      gwp_r407c_w_perc_ch2f2:0,
      gwp_r407c_w_perc_cf3chf2: 0,
      gwp_r407c_w_perc_cf3ch2f: 0,
      gwp_r407c_w_coff_cf3chf2: 0,
      gwp_r407c_w_coff_ch2f2:0,
      gwp_r407c_w_coff_cf3ch2f: 0,
      gwp_r410a_w_per_ch2f2: 0,
      gwp_r410a_w_per_chf2cf3: 0,
      gwp_r410a_w_coff_ch2f2: 0,
      gwp_r410a_w_coff_chf2cf3: 0,
      ef_co2_m_diesel: 0,
      ef_ch4_m_diesel: 0,
      ef_n20_m_diesel: 0,
      ef_co2_m_gasoline: 0,
      ef_ch4_m_gasoline: 0,
      ef_n20_m_gasoline: 0,
      ef_co2_o_diesel: 0,
      ef_ch4_o_diesel: 0,
      ef_n2o_o_diesel: 0,
      ef_co2_o_gasoline: 0,
      ef_ch4_o_gasoline: 0,
      ef_n20_o_gasoline: 0,
      
     
      
      grid_ef_sri_Lanka: 0,
      grid_ef_myanmar: 0,
      grid_ef_maldives: 0,
      grid_ef_indonesia: 0,
      grid_ef_india: 0,
      grid_ef_bangaladesh: 0,

      t_d_loss_perc_sri_Lanka: 0,
      t_d_loss_perc_myanmar: 0,
      t_d_loss_perc_maldives: 0,
      t_d_loss_perc_indonesia: 0,
      t_d_loss_perc_india: 0,
      t_d_loss_perc_bangaladesh: 0,

      cf_mw_sri_Lanka: 0,
      cf_mw_myanmar: 0,
      cf_mw_maldives: 0,
      cf_mw_indonesia: 0,
      cf_mw_india: 0,
      cf_mw_bangaladesh: 0,
      
      
   

      avg_pig_feed_rate: 0,

      // price_diesel_liter: 0,//fin lad FH   38
      // price_petrol_liter: 0,//fin lp95 FH  39
      // price_diesel_liter_cal: 0,// cal LAD_FH 40
      // price_petrol_liter_cal: 0,//cal lp95 FH 41

      // price_diesel_liter_new: 0,//fin LSD_FH 74
      // price_petrol_liter_new: 0,//fin LP92_FH 75
      // price_diesel_liter_cal_new: 0,//cal LSD FH
      // price_petrol_liter_cal_new: 0,//cal lp92 FH
     
      
      price_lad_fh_sri_lanka: 0,//fin lad FH   38
      price_lad_fh_myanmar: 0,
      price_lad_fh_maldives: 0,
      price_lad_fh_indonesia: 0,
      price_lad_fh_india: 0,
      price_lad_fh_bangladesh: 0,

      price_lp95_fh_sri_lanka: 0,//fin lp95 FH  39
      price_lp95_fh_myanmar: 0,
      price_lp95_fh_maldives: 0,
      price_lp95_fh_indonesia: 0,
      price_lp95_fh_india: 0,
      price_lp95_fh_bangladesh: 0,

      price_lad_fh_cal_sri_lanka: 0,// cal LAD_FH 40
      price_lad_fh_cal_myanmar: 0,
      price_lad_fh_cal_maldives: 0,
      price_lad_fh_cal_indonesia: 0,
      price_lad_fh_cal_india: 0,
      price_lad_fh_cal_bangladesh: 0,

      price_lp95_fh_cal_sri_lanka: 0,//cal lp95 FH 41
      price_lp95_fh_cal_myanmar: 0,
      price_lp95_fh_cal_maldives: 0,
      price_lp95_fh_cal_indonesia: 0,
      price_lp95_fh_cal_india: 0,
      price_lp95_fh_cal_bangladesh: 0,




      price_lsd_fh_sri_lanka: 0,//fin LSD_FH 74
      price_lsd_fh_myanmar: 0,
      price_lsd_fh_maldives: 0,
      price_lsd_fh_indonesia: 0,
      price_lsd_fh_india: 0,
      price_lsd_fh_bangladesh: 0,

      price_lp92_fh_sri_lanka: 0,//fin LP92_FH 75
      price_lp92_fh_myanmar: 0,
      price_lp92_fh_maldives: 0,
      price_lp92_fh_indonesia: 0,
      price_lp92_fh_india: 0,
      price_lp92_fh_bangladesh: 0,

      price_lsd_fh_cal_sri_lanka: 0,//cal LSD FH 76
      price_lsd_fh_cal_myanmar: 0,
      price_lsd_fh_cal_maldives: 0,
      price_lsd_fh_cal_indonesia: 0,
      price_lsd_fh_cal_india: 0,
      price_lsd_fh_cal_bangladesh: 0,

      price_lp92_fh_cal_sri_lanka: 0,//cal lp92 FH 77
      price_lp92_fh_cal_myanmar: 0,
      price_lp92_fh_cal_maldives: 0,
      price_lp92_fh_cal_indonesia: 0,
      price_lp92_fh_cal_india: 0,
      price_lp92_fh_cal_bangladesh: 0,

 
      price_lp95_sh_sri_lanka: 0,//fin lp95 SH  78
      price_lp95_sh_myanmar: 0,
      price_lp95_sh_maldives: 0,
      price_lp95_sh_indonesia: 0,
      price_lp95_sh_india: 0,
      price_lp95_sh_bangladesh: 0,

      price_lp92_sh_sri_lanka: 0,//fin LP92_SH 79
      price_lp92_sh_myanmar: 0,
      price_lp92_sh_maldives: 0,
      price_lp92_sh_indonesia: 0,
      price_lp92_sh_india: 0,
      price_lp92_sh_bangladesh: 0,

      price_lad_sh_sri_lanka: 0,//fin lad SH   80
      price_lad_sh_myanmar: 0,
      price_lad_sh_maldives: 0,
      price_lad_sh_indonesia: 0,
      price_lad_sh_india: 0,
      price_lad_sh_bangladesh: 0,

      price_lsd_sh_sri_lanka: 0,//fin LSD_SH 81
      price_lsd_sh_myanmar: 0,
      price_lsd_sh_maldives: 0,
      price_lsd_sh_indonesia: 0,
      price_lsd_sh_india: 0,
      price_lsd_sh_bangladesh: 0,

        
      price_lp95_sh_cal_sri_lanka: 0,//cal lp95 SH 82
      price_lp95_sh_cal_myanmar: 0,
      price_lp95_sh_cal_maldives: 0,
      price_lp95_sh_cal_indonesia: 0,
      price_lp95_sh_cal_india: 0,
      price_lp95_sh_cal_bangladesh: 0,

      price_lp92_sh_cal_sri_lanka: 0,//cal lp92 SH 83
      price_lp92_sh_cal_myanmar: 0,
      price_lp92_sh_cal_maldives: 0,
      price_lp92_sh_cal_indonesia: 0,
      price_lp92_sh_cal_india: 0,
      price_lp92_sh_cal_bangladesh: 0,

      price_lad_sh_cal_sri_lanka: 0,// cal LAD_SH 84
      price_lad_sh_cal_myanmar: 0,
      price_lad_sh_cal_maldives: 0,
      price_lad_sh_cal_indonesia: 0,
      price_lad_sh_cal_india: 0,
      price_lad_sh_cal_bangladesh: 0,
      
      price_lsd_sh_cal_sri_lanka: 0,//cal LSD SH 85
      price_lsd_sh_cal_myanmar: 0,
      price_lsd_sh_cal_maldives: 0,
      price_lsd_sh_cal_indonesia: 0,
      price_lsd_sh_cal_india: 0,
      price_lsd_sh_cal_bangladesh: 0,
   
       
      //pyear
      price_lp95_p_sri_lanka: 0,//fin lp95 SH  78
      price_lp95_p_myanmar: 0,
      price_lp95_p_maldives: 0,
      price_lp95_p_indonesia: 0,
      price_lp95_p_india: 0,
      price_lp95_p_bangladesh: 0,

      price_lp92_p_sri_lanka: 0,//fin LP92_SH 79
      price_lp92_p_myanmar: 0,
      price_lp92_p_maldives: 0,
      price_lp92_p_indonesia: 0,
      price_lp92_p_india: 0,
      price_lp92_p_bangladesh: 0,

      price_lad_p_sri_lanka: 0,//fin lad SH   80
      price_lad_p_myanmar: 0,
      price_lad_p_maldives: 0,
      price_lad_p_indonesia: 0,
      price_lad_p_india: 0,
      price_lad_p_bangladesh: 0,

      price_lsd_p_sri_lanka: 0,//fin LSD_SH 81
      price_lsd_p_myanmar: 0,
      price_lsd_p_maldives: 0,
      price_lsd_p_indonesia: 0,
      price_lsd_p_india: 0,
      price_lsd_p_bangladesh: 0,

     // _sri_lanka _myanmar _maldives _indonesia _india _bangladesh

      price_lp95_p_cal_sri_lanka: 0,//cal lp95 SH 82
      price_lp95_p_cal_myanmar: 0,
      price_lp95_p_cal_maldives: 0,
      price_lp95_p_cal_indonesia: 0,
      price_lp95_p_cal_india: 0,
      price_lp95_p_cal_bangladesh: 0,

      price_lp92_p_cal_sri_lanka: 0,//cal lp92 SH 83
      price_lp92_p_cal_myanmar: 0,
      price_lp92_p_cal_maldives: 0,
      price_lp92_p_cal_indonesia: 0,
      price_lp92_p_cal_india: 0,
      price_lp92_p_cal_bangladesh: 0,

      price_lad_p_cal_sri_lanka: 0,// cal LAD_SH 84
      price_lad_p_cal_myanmar: 0,
      price_lad_p_cal_maldives: 0,
      price_lad_p_cal_indonesia: 0,
      price_lad_p_cal_india: 0,
      price_lad_p_cal_bangladesh: 0,

      price_lsd_p_cal_sri_lanka: 0,//cal LSD SH 85
      price_lsd_p_cal_myanmar: 0,
      price_lsd_p_cal_maldives: 0,
      price_lsd_p_cal_indonesia: 0,
      price_lsd_p_cal_india: 0,
      price_lsd_p_cal_bangladesh: 0,


    
     //ppyear
      price_lp95_pp_sri_lanka: 0,//fin lp95 SH  78
      price_lp95_pp_myanmar: 0,
      price_lp95_pp_maldives: 0,
      price_lp95_pp_indonesia: 0,
      price_lp95_pp_india: 0,
      price_lp95_pp_bangladesh: 0,

      price_lp92_pp_sri_lanka: 0,//fin LP92_SH 79
      price_lp92_pp_myanmar: 0,
      price_lp92_pp_maldives: 0,
      price_lp92_pp_indonesia: 0,
      price_lp92_pp_india: 0,
      price_lp92_pp_bangladesh: 0,

      price_lad_pp_sri_lanka: 0,//fin lad SH   80
      price_lad_pp_myanmar: 0,
      price_lad_pp_maldives: 0,
      price_lad_pp_indonesia: 0,
      price_lad_pp_india: 0,
      price_lad_pp_bangladesh: 0,

      price_lsd_pp_sri_lanka: 0,//fin LSD_SH 81
      price_lsd_pp_myanmar: 0,
      price_lsd_pp_maldives: 0,
      price_lsd_pp_indonesia: 0,
      price_lsd_pp_india: 0,
      price_lsd_pp_bangladesh: 0,

     

      price_lp95_pp_cal_sri_lanka: 0,//cal lp95 SH 82
      price_lp95_pp_cal_myanmar: 0,
      price_lp95_pp_cal_maldives: 0,
      price_lp95_pp_cal_indonesia: 0,
      price_lp95_pp_cal_india: 0,
      price_lp95_pp_cal_bangladesh: 0,

      price_lp92_pp_cal_sri_lanka: 0,//cal lp92 SH 83
      price_lp92_pp_cal_myanmar: 0,
      price_lp92_pp_cal_maldives: 0,
      price_lp92_pp_cal_indonesia: 0,
      price_lp92_pp_cal_india: 0,
      price_lp92_pp_cal_bangladesh: 0,

      price_lad_pp_cal_sri_lanka: 0,// cal LAD_SH 84
      price_lad_pp_cal_myanmar: 0,
      price_lad_pp_cal_maldives: 0,
      price_lad_pp_cal_indonesia: 0,
      price_lad_pp_cal_india: 0,
      price_lad_pp_cal_bangladesh: 0,

      price_lsd_pp_cal_sri_lanka: 0,//cal LSD SH 85
      price_lsd_pp_cal_myanmar: 0,
      price_lsd_pp_cal_maldives: 0,
      price_lsd_pp_cal_indonesia: 0,
      price_lsd_pp_cal_india: 0,
      price_lsd_pp_cal_bangladesh: 0,
   
   
      

      em_intensity_finance: 0,
      em_intensity_tel: 0,
      em_intensity_apparel: 0,
      em_intensity_hospitality: 0,
      em_intensity_plant: 0,
      em_intensity_trans: 0,
      em_intensity_food: 0,
      em_intensity_manuf: 0,
      em_intensity_other: 0,
      cf_nautic_to_km : 0,
      ef_co2_air_freight_range1 : 0,
      ef_co2_air_freight_range2 :0,
      ef_co2_air_freight_range3 : 0,
      ef_co2_sea_freight : 0,
      ncv_biomass : 0,
      ef_co2_biomass: 0,
      ef_ch4_biomass: 0,
      ef_n20_biomass : 0,
      ncv_lpgas : 0,
      ef_co2_lpgas: 0,
      ef_ch4_lpgas: 0,
      ef_n20_lpgas : 0,
      ef_co2_paperwaste: 0,

    },
    refs: {
      den_petrol: '',
      den_diesel : '',
      net_caloric_petrol: '',
      net_caloric_diesel: '',
      ef_co2_s_diesel: '',
      ef_ch4_s_diesel: '',
      ef_n2o_s_diesel: '',
      ef_co2_s_petrol: '',
      ef_ch4_s_petrol: '',
      ef_n20_s_petrol: '',
      gwp_co2: '',
      gwp_ch4: '',
      gwp_n2o: '',
      gwp_r22 : '',
      gwp_r407c_w_perc_ch2f2:'',
      gwp_r407c_w_perc_cf3chf2: '',
      gwp_r407c_w_perc_cf3ch2f: '',
      gwp_r407c_w_coff_cf3chf2: '',
      gwp_r407c_w_coff_ch2f2:'',
      gwp_r407c_w_coff_cf3ch2f: '',
      gwp_r410a_w_per_ch2f2: '',
      gwp_r410a_w_per_chf2cf3: '',
      gwp_r410a_w_coff_ch2f2: '',
      gwp_r410a_w_coff_chf2cf3: '',
      ef_co2_m_diesel: '',
      ef_ch4_m_diesel: '',
      ef_n20_m_diesel: '',
      ef_co2_m_gasoline: '',
      ef_ch4_m_gasoline: '',
      ef_n20_m_gasoline: '',
      ef_co2_o_diesel: '',
      ef_ch4_o_diesel: '',
      ef_n2o_o_diesel: '',
      ef_co2_o_gasoline: '',
      ef_ch4_o_gasoline: '',
      ef_n20_o_gasoline: '',


       grid_ef_sri_Lanka:'',
      grid_ef_myanmar: '',
      grid_ef_maldives: '',
      grid_ef_indonesia: '',
      grid_ef_india: '',
      grid_ef_bangaladesh: '',

      t_d_loss_perc_sri_Lanka: '',
      t_d_loss_perc_myanmar: '',
      t_d_loss_perc_maldives: '',
      t_d_loss_perc_indonesia: '',
      t_d_loss_perc_india: '',
      t_d_loss_perc_bangaladesh: '',

      cf_mw_sri_Lanka: '',
      cf_mw_myanmar: '',
      cf_mw_maldives: '',
      cf_mw_indonesia: '',
      cf_mw_india: '',
      cf_mw_bangaladesh: '',

      avg_pig_feed_rate:'',

   


      // price_diesel_liter: '',
      // price_petrol_liter: '',
      // price_diesel_liter_cal: '',
      // price_petrol_liter_cal: '',

      // price_diesel_liter_new: '',
      // price_petrol_liter_new: '',
      // price_diesel_liter_cal_new: '',
      // price_petrol_liter_cal_new: '',

      price_lad_fh_sri_lanka: '',//fin lad FH   38
      price_lad_fh_myanmar: '',
      price_lad_fh_maldives: '',
      price_lad_fh_indonesia: '',
      price_lad_fh_india: '',
      price_lad_fh_bangladesh: '',

      price_lp95_fh_sri_lanka: '',//fin lp95 FH  39
      price_lp95_fh_myanmar: '',
      price_lp95_fh_maldives: '',
      price_lp95_fh_indonesia: '',
      price_lp95_fh_india: '',
      price_lp95_fh_bangladesh: '',

      price_lad_fh_cal_sri_lanka: '',// cal LAD_FH 40
      price_lad_fh_cal_myanmar: '',
      price_lad_fh_cal_maldives: '',
      price_lad_fh_cal_indonesia: '',
      price_lad_fh_cal_india: '',
      price_lad_fh_cal_bangladesh: '',

      price_lp95_fh_cal_sri_lanka: '',//cal lp95 FH 41
      price_lp95_fh_cal_myanmar: '',
      price_lp95_fh_cal_maldives: '',
      price_lp95_fh_cal_indonesia: '',
      price_lp95_fh_cal_india: '',
      price_lp95_fh_cal_bangladesh: '',




      price_lsd_fh_sri_lanka: '',//fin LSD_FH 74
      price_lsd_fh_myanmar: '',
      price_lsd_fh_maldives: '',
      price_lsd_fh_indonesia: '',
      price_lsd_fh_india: '',
      price_lsd_fh_bangladesh: '',

      price_lp92_fh_sri_lanka: '',//fin LP92_FH 75
      price_lp92_fh_myanmar: '',
      price_lp92_fh_maldives: '',
      price_lp92_fh_indonesia: '',
      price_lp92_fh_india: '',
      price_lp92_fh_bangladesh: '',

      price_lsd_fh_cal_sri_lanka: '',//cal LSD FH 76
      price_lsd_fh_cal_myanmar: '',
      price_lsd_fh_cal_maldives: '',
      price_lsd_fh_cal_indonesia: '',
      price_lsd_fh_cal_india: '',
      price_lsd_fh_cal_bangladesh: '',

      price_lp92_fh_cal_sri_lanka: '',//cal lp92 FH 77
      price_lp92_fh_cal_myanmar: '',
      price_lp92_fh_cal_maldives: '',
      price_lp92_fh_cal_indonesia: '',
      price_lp92_fh_cal_india: '',
      price_lp92_fh_cal_bangladesh: '',

 
      price_lp95_sh_sri_lanka: '',//fin lp95 SH  78
      price_lp95_sh_myanmar: '',
      price_lp95_sh_maldives: '',
      price_lp95_sh_indonesia: '',
      price_lp95_sh_india: '',
      price_lp95_sh_bangladesh: '',

      price_lp92_sh_sri_lanka: '',//fin LP92_SH 79
      price_lp92_sh_myanmar: '',
      price_lp92_sh_maldives: '',
      price_lp92_sh_indonesia: '',
      price_lp92_sh_india: '',
      price_lp92_sh_bangladesh: '',

      price_lad_sh_sri_lanka: '',//fin lad SH   80
      price_lad_sh_myanmar: '',
      price_lad_sh_maldives: '',
      price_lad_sh_indonesia: '',
      price_lad_sh_india: '',
      price_lad_sh_bangladesh: '',

      price_lsd_sh_sri_lanka: '',//fin LSD_SH 81
      price_lsd_sh_myanmar: '',
      price_lsd_sh_maldives: '',
      price_lsd_sh_indonesia: '',
      price_lsd_sh_india: '',
      price_lsd_sh_bangladesh: '',

        
      price_lp95_sh_cal_sri_lanka: '',//cal lp95 SH 82
      price_lp95_sh_cal_myanmar: '',
      price_lp95_sh_cal_maldives: '',
      price_lp95_sh_cal_indonesia: '',
      price_lp95_sh_cal_india: '',
      price_lp95_sh_cal_bangladesh: '',

      price_lp92_sh_cal_sri_lanka: '',//cal lp92 SH 83
      price_lp92_sh_cal_myanmar: '',
      price_lp92_sh_cal_maldives: '',
      price_lp92_sh_cal_indonesia: '',
      price_lp92_sh_cal_india: '',
      price_lp92_sh_cal_bangladesh: '',

      price_lad_sh_cal_sri_lanka: '',// cal LAD_SH 84
      price_lad_sh_cal_myanmar: '',
      price_lad_sh_cal_maldives: '',
      price_lad_sh_cal_indonesia: '',
      price_lad_sh_cal_india: '',
      price_lad_sh_cal_bangladesh: '',
      
      price_lsd_sh_cal_sri_lanka: '',//cal LSD SH 85
      price_lsd_sh_cal_myanmar: '',
      price_lsd_sh_cal_maldives: '',
      price_lsd_sh_cal_indonesia: '',
      price_lsd_sh_cal_india: '',
      price_lsd_sh_cal_bangladesh: '',
   
       
      //pyear
      price_lp95_p_sri_lanka: '',//fin lp95 SH  78
      price_lp95_p_myanmar: '',
      price_lp95_p_maldives: '',
      price_lp95_p_indonesia: '',
      price_lp95_p_india: '',
      price_lp95_p_bangladesh: '',

      price_lp92_p_sri_lanka: '',//fin LP92_SH 79
      price_lp92_p_myanmar: '',
      price_lp92_p_maldives: '',
      price_lp92_p_indonesia: '',
      price_lp92_p_india: '',
      price_lp92_p_bangladesh: '',

      price_lad_p_sri_lanka: '',//fin lad SH   80
      price_lad_p_myanmar: '',
      price_lad_p_maldives: '',
      price_lad_p_indonesia: '',
      price_lad_p_india: '',
      price_lad_p_bangladesh: '',

      price_lsd_p_sri_lanka: '',//fin LSD_SH 81
      price_lsd_p_myanmar: '',
      price_lsd_p_maldives: '',
      price_lsd_p_indonesia: '',
      price_lsd_p_india: '',
      price_lsd_p_bangladesh: '',

     // _sri_lanka _myanmar _maldives _indonesia _india _bangladesh

      price_lp95_p_cal_sri_lanka: '',//cal lp95 SH 82
      price_lp95_p_cal_myanmar: '',
      price_lp95_p_cal_maldives: '',
      price_lp95_p_cal_indonesia: '',
      price_lp95_p_cal_india: '',
      price_lp95_p_cal_bangladesh: '',

      price_lp92_p_cal_sri_lanka: '',//cal lp92 SH 83
      price_lp92_p_cal_myanmar: '',
      price_lp92_p_cal_maldives: '',
      price_lp92_p_cal_indonesia: '',
      price_lp92_p_cal_india: '',
      price_lp92_p_cal_bangladesh: '',

      price_lad_p_cal_sri_lanka: '',// cal LAD_SH 84
      price_lad_p_cal_myanmar: '',
      price_lad_p_cal_maldives: '',
      price_lad_p_cal_indonesia: '',
      price_lad_p_cal_india: '',
      price_lad_p_cal_bangladesh: '',

      price_lsd_p_cal_sri_lanka: '',//cal LSD SH 85
      price_lsd_p_cal_myanmar: '',
      price_lsd_p_cal_maldives: '',
      price_lsd_p_cal_indonesia: '',
      price_lsd_p_cal_india: '',
      price_lsd_p_cal_bangladesh: '',


    
     //ppyear
      price_lp95_pp_sri_lanka: '',//fin lp95 SH  78
      price_lp95_pp_myanmar: '',
      price_lp95_pp_maldives: '',
      price_lp95_pp_indonesia: '',
      price_lp95_pp_india: '',
      price_lp95_pp_bangladesh: '',

      price_lp92_pp_sri_lanka: '',//fin LP92_SH 79
      price_lp92_pp_myanmar: '',
      price_lp92_pp_maldives: '',
      price_lp92_pp_indonesia: '',
      price_lp92_pp_india: '',
      price_lp92_pp_bangladesh: '',

      price_lad_pp_sri_lanka: '',//fin lad SH   80
      price_lad_pp_myanmar: '',
      price_lad_pp_maldives: '',
      price_lad_pp_indonesia: '',
      price_lad_pp_india: '',
      price_lad_pp_bangladesh: '',

      price_lsd_pp_sri_lanka: '',//fin LSD_SH 81
      price_lsd_pp_myanmar: '',
      price_lsd_pp_maldives: '',
      price_lsd_pp_indonesia: '',
      price_lsd_pp_india: '',
      price_lsd_pp_bangladesh: '',

      // _sri_lanka _myanmar _maldives _indonesia _india _bangladesh

      price_lp95_pp_cal_sri_lanka: '',//cal lp95 SH 82
      price_lp95_pp_cal_myanmar: '',
      price_lp95_pp_cal_maldives: '',
      price_lp95_pp_cal_indonesia: '',
      price_lp95_pp_cal_india: '',
      price_lp95_pp_cal_bangladesh: '',

      price_lp92_pp_cal_sri_lanka: '',//cal lp92 SH 83
      price_lp92_pp_cal_myanmar: '',
      price_lp92_pp_cal_maldives: '',
      price_lp92_pp_cal_indonesia: '',
      price_lp92_pp_cal_india: '',
      price_lp92_pp_cal_bangladesh: '',

      price_lad_pp_cal_sri_lanka: '',// cal LAD_SH 84
      price_lad_pp_cal_myanmar: '',
      price_lad_pp_cal_maldives: '',
      price_lad_pp_cal_indonesia: '',
      price_lad_pp_cal_india: '',
      price_lad_pp_cal_bangladesh: '',

      price_lsd_pp_cal_sri_lanka: '',//cal LSD SH 85
      price_lsd_pp_cal_myanmar: '',
      price_lsd_pp_cal_maldives: '',
      price_lsd_pp_cal_indonesia: '',
      price_lsd_pp_cal_india: '',
      price_lsd_pp_cal_bangladesh: '',

      em_intensity_finance: '',
      em_intensity_tel: '',
      em_intensity_apparel: '',
      em_intensity_hospitality: '',
      em_intensity_plant: '',
      em_intensity_trans: '',
      em_intensity_food: '',
      em_intensity_manuf: '',
      em_intensity_other: '',
      cf_nautic_to_km : '',
      ef_co2_air_freight_range1 : '',
      ef_co2_air_freight_range2 :'',
      ef_co2_air_freight_range3 : '',
      ef_co2_sea_freight : '',
      ncv_biomass : '',
      ef_co2_biomass: '',
      ef_ch4_biomass: '',
      ef_n20_biomass : '',
      ncv_lpgas : '',
      ef_co2_lpgas: '',
      ef_ch4_lpgas: '',
      ef_n20_lpgas : '',
      ef_co2_paperwaste: '',

    }
  }


  constructor(private boService: BackendService, private toastSerivce: NbToastrService) {
    this.loadData()
  }

  ngOnInit() {

  }

  public onUpdate(id: number) {
    let jsonBody = {
      id: undefined,
      value: undefined,
      reference: undefined,
    }
    switch(id) {
      case 1: {
        jsonBody.value = this.emissionFactors.values.den_diesel;
        jsonBody.id = id ;
        jsonBody.reference = this.emissionFactors.refs.den_diesel;

        break;
      }
      case 2: {
        jsonBody.value = this.emissionFactors.values.den_petrol;
        jsonBody.id = id;
        jsonBody.reference =  this.emissionFactors.refs.den_petrol;
        break;
      }
      case 3: {
        jsonBody.value = this.emissionFactors.values.net_caloric_diesel ;
        jsonBody.id = id ;
        jsonBody.reference = this.emissionFactors.refs.net_caloric_diesel;
        break;
      }
      case 4: {
        jsonBody.value = this.emissionFactors.values.net_caloric_petrol;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.net_caloric_petrol;
        break;
      }
      case 5: {
        jsonBody.value = this.emissionFactors.values.ef_co2_s_diesel;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_s_diesel;
        break;
      }
      case 6: {
        jsonBody.value = this.emissionFactors.values.ef_ch4_s_diesel;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_ch4_s_diesel;
        break;
      }
      case 7: {
        jsonBody.value = this.emissionFactors.values.ef_n2o_s_diesel;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_n2o_s_diesel ;
        break;
      }
      case 8: {
        jsonBody.value = this.emissionFactors.values.gwp_co2;
        jsonBody.id = id;
        jsonBody.reference =this.emissionFactors.refs.gwp_co2 ;
        break;
      }
      case 9: {
        jsonBody.value =   this.emissionFactors.values.gwp_ch4;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_ch4;
        break;
      }
      case 10: {
        jsonBody.value = this.emissionFactors.values.gwp_n2o;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_n2o;
        break;
      }
      case 11: {
        jsonBody.value =this.emissionFactors.values.gwp_r22 ;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r22;
        break;
      }
      case 12: {
        jsonBody.value = this.emissionFactors.values.gwp_r407c_w_perc_ch2f2;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r407c_w_perc_ch2f2;
        break;
      }
      case 13: {
        jsonBody.value = this.emissionFactors.values.gwp_r407c_w_perc_cf3chf2;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r407c_w_perc_cf3chf2 ;
        break;
      }
      case 14: {
        jsonBody.value = this.emissionFactors.values.gwp_r407c_w_perc_cf3ch2f;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r407c_w_perc_cf3ch2f;

        break;
      }
      case 15: {
        jsonBody.value = this.emissionFactors.values.gwp_r407c_w_coff_cf3chf2;
        jsonBody.id =id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r407c_w_coff_cf3chf2;

        break;
      }
      case 16: {
        jsonBody.value = this.emissionFactors.values.gwp_r407c_w_coff_ch2f2 ;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r407c_w_coff_ch2f2;
        break;
      }
      case 17: {
        jsonBody.value = this.emissionFactors.values.gwp_r407c_w_coff_cf3ch2f;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r407c_w_coff_cf3ch2f;
        break;
      }
      case 18: {
        jsonBody.value = this.emissionFactors.values.gwp_r410a_w_per_ch2f2;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r410a_w_per_ch2f2;
        break;
      }
      case 19: {
        jsonBody.value = this.emissionFactors.values.gwp_r410a_w_per_chf2cf3;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r410a_w_per_chf2cf3;
        break;
      }
      case 20: {
        jsonBody.value = this.emissionFactors.values.gwp_r410a_w_coff_ch2f2;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r410a_w_coff_ch2f2;
        break;
      }
      case 21: {
        jsonBody.value = this.emissionFactors.values.gwp_r410a_w_coff_chf2cf3;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.gwp_r410a_w_coff_chf2cf3;
        break;
      }
      case 22: {
        jsonBody.value = this.emissionFactors.values.ef_co2_m_gasoline;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_m_gasoline;
        break;
      }
      case 23: {
        jsonBody.value = this.emissionFactors.values.ef_co2_m_diesel;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_m_diesel ;
        break;
      }
      case 24: {
        jsonBody.value = this.emissionFactors.values.ef_ch4_m_gasoline;
        jsonBody.id =id;
        jsonBody.reference = this.emissionFactors.refs.ef_ch4_m_gasoline;
        break;
      }
      case 25: {
        jsonBody.value = this.emissionFactors.values.ef_ch4_m_diesel;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_ch4_m_diesel;
        break;
      }
      case 26: {
        jsonBody.value = this.emissionFactors.values.ef_n20_m_gasoline;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_n20_m_gasoline;
        break;
      }
      case 27: {
        jsonBody.value =this.emissionFactors.values.ef_n20_m_diesel ;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_n20_m_diesel ;
        break;
      }
      case 28: {
        jsonBody.value = this.emissionFactors.values.ef_co2_o_gasoline;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_o_gasoline;
        break;
      }
      case 29: {
        jsonBody.value = this.emissionFactors.values.ef_co2_o_diesel
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_o_diesel
        break;
      }
      case 30: {
        jsonBody.value = this.emissionFactors.values.ef_ch4_o_gasoline;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_ch4_o_gasoline;
        break;
      }
      case 31: {
        jsonBody.value = this.emissionFactors.values.ef_ch4_o_diesel;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_ch4_o_diesel;
        break;
      }
      case 32: {
        jsonBody.value = this.emissionFactors.values.ef_n20_o_gasoline;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_n20_o_gasoline;
        break;
      }
      case 33: {
        jsonBody.value = this.emissionFactors.values.ef_n2o_o_diesel;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.ef_n2o_o_diesel;
        break;
      }
      case 34: {
        jsonBody.value = this.emissionFactors.values.grid_ef_sri_Lanka;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.grid_ef_sri_Lanka;
        break;
      }
      case 103: {
        jsonBody.value = this.emissionFactors.values.grid_ef_myanmar;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.grid_ef_myanmar;
        break;
      }
      case 106: {
        jsonBody.value = this.emissionFactors.values.grid_ef_maldives;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.grid_ef_maldives;
        break;
      }
      case 109: {
        jsonBody.value = this.emissionFactors.values.grid_ef_indonesia;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.grid_ef_indonesia;
        break;
      }
      case 112: {
        jsonBody.value = this.emissionFactors.values.grid_ef_india;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.grid_ef_india;
        break;
      }
      case 115: {
        jsonBody.value = this.emissionFactors.values.grid_ef_bangaladesh;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.grid_ef_bangaladesh;
        break;
      }


      case 35: {
        jsonBody.value = this.emissionFactors.values.t_d_loss_perc_sri_Lanka;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.t_d_loss_perc_sri_Lanka;
        break;
      }
      case 104: {
        jsonBody.value = this.emissionFactors.values.t_d_loss_perc_myanmar;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.t_d_loss_perc_myanmar;
        break;
      }
      case 107: {
        jsonBody.value = this.emissionFactors.values.t_d_loss_perc_maldives;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.t_d_loss_perc_maldives;
        break;
      }
      case 110: {
        jsonBody.value = this.emissionFactors.values.t_d_loss_perc_indonesia;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.t_d_loss_perc_indonesia;
        break;
      }
      case 113: {
        jsonBody.value = this.emissionFactors.values.t_d_loss_perc_india;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.t_d_loss_perc_india;
        break;
      }
      case 116: {
        jsonBody.value = this.emissionFactors.values.t_d_loss_perc_bangaladesh;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.t_d_loss_perc_bangaladesh;
        break;
      }

      
      case 36: {
        jsonBody.value = this.emissionFactors.values.cf_mw_sri_Lanka;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.cf_mw_sri_Lanka;

        break;
      }
      case 105: {
        jsonBody.value = this.emissionFactors.values.cf_mw_myanmar;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.cf_mw_myanmar;

        break;
      }
      case 108: {
        jsonBody.value = this.emissionFactors.values.cf_mw_maldives;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.cf_mw_maldives;

        break;
      }
      case 111: {
        jsonBody.value = this.emissionFactors.values.cf_mw_indonesia;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.cf_mw_indonesia;

        break;
      }
      case 114: {
        jsonBody.value = this.emissionFactors.values.cf_mw_india;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.cf_mw_india;

        break;
      }
      case 117: {
        jsonBody.value = this.emissionFactors.values.cf_mw_bangaladesh;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.cf_mw_bangaladesh;

        break;
      }

      case 37: {
        jsonBody.value = this.emissionFactors.values.avg_pig_feed_rate;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.avg_pig_feed_rate;
        break;
      }
      // case 38: {
      //   jsonBody.value = this.emissionFactors.values.price_diesel_liter;
      //   jsonBody.id = id;
      //   jsonBody.reference = this.emissionFactors.refs.price_diesel_liter;
      //   break;
      // }
      // case 39: {
      //   jsonBody.value = this.emissionFactors.values.price_petrol_liter;
      //   jsonBody.id = id;
      //   jsonBody.reference = this.emissionFactors.refs.price_petrol_liter;
      //   break;
      // }
      // case 40: {
      //   jsonBody.id = id;
      //   jsonBody.value = this.emissionFactors.values.price_diesel_liter_cal;
      //   jsonBody.reference = this.emissionFactors.refs.price_diesel_liter_cal;
      //   break;
      // }
      // case 41: {
      //   jsonBody.id = id;
      //   jsonBody.value = this.emissionFactors.values.price_petrol_liter_cal;
      //   jsonBody.reference = this.emissionFactors.refs.price_petrol_liter_cal;
      //   break;
      // }

      case 38: {
        jsonBody.value = this.emissionFactors.values.price_lad_fh_sri_lanka;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lad_fh_sri_lanka;
        break;
      }
      case 118: {
        jsonBody.value = this.emissionFactors.values.price_lad_fh_myanmar;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lad_fh_myanmar;
        break;
      }
      case 119: {
        jsonBody.value = this.emissionFactors.values.price_lad_fh_maldives;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lad_fh_maldives;
        break;
      }
      case 120: {
        jsonBody.value = this.emissionFactors.values.price_lad_fh_indonesia;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lad_fh_indonesia;
        break;
      }
      case 121: {
        jsonBody.value = this.emissionFactors.values.price_lad_fh_india;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lad_fh_india;
        break;
      }
      case 122: {
        jsonBody.value = this.emissionFactors.values.price_lad_fh_bangladesh;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lad_fh_bangladesh;
        break;
      }
       
      case 39: {
        jsonBody.value = this.emissionFactors.values.price_lp95_fh_sri_lanka;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lp95_fh_sri_lanka;
        break;
      }
      case 123: {
        jsonBody.value = this.emissionFactors.values.price_lp95_fh_myanmar;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lp95_fh_myanmar;
        break;
      }
      case 124: {
        jsonBody.value = this.emissionFactors.values.price_lp95_fh_maldives;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lp95_fh_maldives;
        break;
      }
      case 125: {
        jsonBody.value = this.emissionFactors.values.price_lp95_fh_indonesia;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lp95_fh_indonesia;
        break;
      }
      case 126: {
        jsonBody.value = this.emissionFactors.values.price_lp95_fh_india;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lp95_fh_india;
        break;
      }
      case 127: {
        jsonBody.value = this.emissionFactors.values.price_lp95_fh_bangladesh;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lp95_fh_bangladesh;
        break;
      }
      
      case 40: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lad_fh_cal_sri_lanka;
        jsonBody.reference = this.emissionFactors.refs.price_lad_fh_cal_sri_lanka;
        break;
      }
      case 138: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lad_fh_cal_myanmar;
        jsonBody.reference = this.emissionFactors.refs.price_lad_fh_cal_myanmar;
        break;
      }
      case 139: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lad_fh_cal_maldives;
        jsonBody.reference = this.emissionFactors.refs.price_lad_fh_cal_maldives;
        break;
      }
      case 140: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lad_fh_cal_indonesia;
        jsonBody.reference = this.emissionFactors.refs.price_lad_fh_cal_indonesia;
        break;
      }
      case 141: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lad_fh_cal_india;
        jsonBody.reference = this.emissionFactors.refs.price_lad_fh_cal_india;
        break;
      }
      case 142: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lad_fh_cal_bangladesh;
        jsonBody.reference = this.emissionFactors.refs.price_lad_fh_cal_bangladesh;
        break;
      }
      case 41: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lp95_fh_cal_sri_lanka;
        jsonBody.reference = this.emissionFactors.refs.price_lp95_fh_cal_sri_lanka;
        break;
      }
      case 143: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lp95_fh_cal_myanmar;
        jsonBody.reference = this.emissionFactors.refs.price_lp95_fh_cal_myanmar;
        break;
      }
      case 144: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lp95_fh_cal_maldives;
        jsonBody.reference = this.emissionFactors.refs.price_lp95_fh_cal_maldives;
        break;
      }
      case 145: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lp95_fh_cal_indonesia;
        jsonBody.reference = this.emissionFactors.refs.price_lp95_fh_cal_indonesia;
        break;
      }
      case 146: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lp95_fh_cal_india;
        jsonBody.reference = this.emissionFactors.refs.price_lp95_fh_cal_india;
        break;
      }
      case 147: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lp95_fh_cal_bangladesh;
        jsonBody.reference = this.emissionFactors.refs.price_lp95_fh_cal_bangladesh;
        break;
      }

      case 42: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_finance;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_finance;
        break;
      }
      case 43: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_tel;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_tel;
        break;
      }
      case 44: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_apparel;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_apparel;
        break;
      }
      case 45: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_hospitality;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_hospitality;
        break;
      }
      case 46: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_plant;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_plant;
        break;
      }
      case 47: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_trans;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_trans;
        break;
      }
      case 48: {
        jsonBody.id  = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_food;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_food;
        break;
      }
      case 49: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_manuf;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_manuf;
        break;
      }
      case 50: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.em_intensity_other;
        jsonBody.reference = this.emissionFactors.refs.em_intensity_other;
        break;

      }
      case 51: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.cf_nautic_to_km;
        jsonBody.reference = this.emissionFactors.refs.cf_nautic_to_km;
        break;

      }
      case 52: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_co2_air_freight_range1;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_air_freight_range1;
        break;

      }
      case 53: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_co2_air_freight_range2;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_air_freight_range2;
        break;

      }
      case 54: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_co2_air_freight_range3;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_air_freight_range3;
        break;

      }
      case 55: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_co2_sea_freight;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_sea_freight;
        break;

      }
      case 56: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ncv_biomass;
        jsonBody.reference = this.emissionFactors.refs.ncv_biomass;
        break;

      }
      case 57: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_ch4_biomass;
        jsonBody.reference = this.emissionFactors.refs.ef_ch4_biomass;
        break;

      }
      case 58: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_co2_biomass;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_biomass;
        break;

      }
      case 59: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_n20_biomass;
        jsonBody.reference = this.emissionFactors.refs.ef_n20_biomass;
        break;

      }

      case 60: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ncv_lpgas;
        jsonBody.reference = this.emissionFactors.refs.ncv_lpgas;
        break;

      }
      case 61: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_ch4_lpgas;
        jsonBody.reference = this.emissionFactors.refs.ef_ch4_lpgas;
        break;

      }
      case 62: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_co2_lpgas;
        jsonBody.reference = this.emissionFactors.refs.ef_co2_lpgas;
        break;

      }
      case 63: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.ef_n20_lpgas;
        jsonBody.reference = this.emissionFactors.refs.ef_n20_lpgas;
        break;

      }
    
      //new added
    
      case 74: {
        jsonBody.value = this.emissionFactors.values.price_lsd_fh_sri_lanka;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lsd_fh_sri_lanka;
        break;
      }
      case 128: {
        jsonBody.value = this.emissionFactors.values.price_lsd_fh_myanmar;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lsd_fh_myanmar;
        break;
      }
      case 129: {
        jsonBody.value = this.emissionFactors.values.price_lsd_fh_maldives;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lsd_fh_maldives;
        break;
      }
      case 130: {
        jsonBody.value = this.emissionFactors.values.price_lsd_fh_indonesia;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lsd_fh_indonesia;
        break;
      }
      case 131: {
        jsonBody.value = this.emissionFactors.values.price_lsd_fh_india;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lsd_fh_india;
        break;
      }
      case 132: {
        jsonBody.value = this.emissionFactors.values.price_lsd_fh_bangladesh;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lsd_fh_bangladesh;
        break;
      }
      
      case 75: {
        jsonBody.value = this.emissionFactors.values.price_lp92_fh_sri_lanka;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lp92_fh_sri_lanka;
        break;
      }
      case 133: {
        jsonBody.value = this.emissionFactors.values.price_lp92_fh_myanmar;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lp92_fh_myanmar;
        break;
      }
      case 134: {
        jsonBody.value = this.emissionFactors.values.price_lp92_fh_maldives;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lp92_fh_maldives;
        break;
      }
      case 135: {
        jsonBody.value = this.emissionFactors.values.price_lp92_fh_indonesia;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lp92_fh_indonesia;
        break;
      }
      case 136: {
        jsonBody.value = this.emissionFactors.values.price_lp92_fh_india;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lp92_fh_india;
        break;
      }
      case 137: {
        jsonBody.value = this.emissionFactors.values.price_lp92_fh_bangladesh;
        jsonBody.id = id;
        jsonBody.reference = this.emissionFactors.refs.price_lp92_fh_bangladesh;
        break;
      }
      case 76: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lsd_fh_cal_sri_lanka;
        jsonBody.reference = this.emissionFactors.refs.price_lsd_fh_cal_sri_lanka;
        break;
      }
      case 148: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lsd_fh_cal_myanmar;
        jsonBody.reference = this.emissionFactors.refs.price_lsd_fh_cal_myanmar;
        break;
      }
      case 149: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lsd_fh_cal_maldives;
        jsonBody.reference = this.emissionFactors.refs.price_lsd_fh_cal_maldives;
        break;
      }
      case 150: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lsd_fh_cal_indonesia;
        jsonBody.reference = this.emissionFactors.refs.price_lsd_fh_cal_indonesia;
        break;
      }
      case 151: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lsd_fh_cal_india;
        jsonBody.reference = this.emissionFactors.refs.price_lsd_fh_cal_india;
        break;
      }
      case 152: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lsd_fh_cal_bangladesh;
        jsonBody.reference = this.emissionFactors.refs.price_lsd_fh_cal_bangladesh;
        break;
      }
      case 77: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lp92_fh_cal_sri_lanka;
        jsonBody.reference = this.emissionFactors.refs.price_lp92_fh_cal_sri_lanka;
        break;
      }
      case 153: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lp92_fh_cal_myanmar;
        jsonBody.reference = this.emissionFactors.refs.price_lp92_fh_cal_myanmar;
        break;
      }
      case 154: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lp92_fh_cal_maldives;
        jsonBody.reference = this.emissionFactors.refs.price_lp92_fh_cal_maldives;
        break;
      }
      case 155: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lp92_fh_cal_indonesia;
        jsonBody.reference = this.emissionFactors.refs.price_lp92_fh_cal_indonesia;
        break;
      }
      case 156: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lp92_fh_cal_india;
        jsonBody.reference = this.emissionFactors.refs.price_lp92_fh_cal_india;
        break;
      }
      case 157: {
        jsonBody.id = id;
        jsonBody.value = this.emissionFactors.values.price_lp92_fh_cal_bangladesh;
        jsonBody.reference = this.emissionFactors.refs.price_lp92_fh_cal_bangladesh;
        break;
      }

    
    case 78: {
      jsonBody.value = this.emissionFactors.values.price_lp95_sh_sri_lanka;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_sh_sri_lanka;
      break;
    }
    case 158: {
      jsonBody.value = this.emissionFactors.values.price_lp95_sh_myanmar;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_sh_myanmar;
      break;
    }
    case 159: {
      jsonBody.value = this.emissionFactors.values.price_lp95_sh_maldives;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_sh_maldives;
      break;
    }
    case 160: {
      jsonBody.value = this.emissionFactors.values.price_lp95_sh_indonesia;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_sh_indonesia;
      break;
    }
    case 161: {
      jsonBody.value = this.emissionFactors.values.price_lp95_sh_india;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_sh_india;
      break;
    }
    case 162: {
      jsonBody.value = this.emissionFactors.values.price_lp95_sh_bangladesh;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_sh_bangladesh;
      break;
    }
    case 79: {
      jsonBody.value = this.emissionFactors.values.price_lp92_sh_sri_lanka;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_sh_sri_lanka;
      break;
    }
    case 163: {
      jsonBody.value = this.emissionFactors.values.price_lp92_sh_myanmar;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_sh_myanmar;
      break;
    }
    case 164: {
      jsonBody.value = this.emissionFactors.values.price_lp92_sh_maldives;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_sh_maldives;
      break;
    }
    case 165: {
      jsonBody.value = this.emissionFactors.values.price_lp92_sh_indonesia;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_sh_indonesia;
      break;
    }
    case 166: {
      jsonBody.value = this.emissionFactors.values.price_lp92_sh_india;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_sh_india;
      break;
    }
    case 167: {
      jsonBody.value = this.emissionFactors.values.price_lp92_sh_bangladesh;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_sh_bangladesh;
      break;
    }
    case 80: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_sh_sri_lanka;
      jsonBody.reference = this.emissionFactors.refs.price_lad_sh_sri_lanka;
      break;
    }
    case 168: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_sh_myanmar;
      jsonBody.reference = this.emissionFactors.refs.price_lad_sh_myanmar;
      break;
    }
    case 169: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_sh_maldives;
      jsonBody.reference = this.emissionFactors.refs.price_lad_sh_maldives;
      break;
    }
    case 170: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_sh_indonesia;
      jsonBody.reference = this.emissionFactors.refs.price_lad_sh_indonesia;
      break;
    }
    case 171: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_sh_india;
      jsonBody.reference = this.emissionFactors.refs.price_lad_sh_india;
      break;
    }
    case 172: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_sh_bangladesh;
      jsonBody.reference = this.emissionFactors.refs.price_lad_sh_bangladesh;
      break;
    }
    case 81: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_sh_sri_lanka;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_sh_sri_lanka;
      break;
    }
    case 173: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_sh_myanmar;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_sh_myanmar;
      break;
    }
    case 174: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_sh_maldives;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_sh_maldives;
      break;
    }
    case 175: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_sh_indonesia;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_sh_indonesia;
      break;
    }
    case 176: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_sh_india;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_sh_india;
      break;
    }
    case 177: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_sh_bangladesh;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_sh_bangladesh;
      break;
    }

    case 82: {
      jsonBody.value = this.emissionFactors.values.price_lp95_sh_cal_sri_lanka;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_sh_cal_sri_lanka;
      break;
    }
    case 178: {
      jsonBody.value = this.emissionFactors.values.price_lp95_sh_cal_myanmar;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_sh_cal_myanmar;
      break;
    }
    case 179: {
      jsonBody.value = this.emissionFactors.values.price_lp95_sh_cal_maldives;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_sh_cal_maldives;
      break;
    }
    case 180: {
      jsonBody.value = this.emissionFactors.values.price_lp95_sh_cal_indonesia;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_sh_cal_indonesia;
      break;
    }
    case 181: {
      jsonBody.value = this.emissionFactors.values.price_lp95_sh_cal_india;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_sh_cal_india;
      break;
    }
    case 182: {
      jsonBody.value = this.emissionFactors.values.price_lp95_sh_cal_bangladesh;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_sh_cal_bangladesh;
      break;
    }


    case 83: {
      jsonBody.value = this.emissionFactors.values.price_lp92_sh_cal_sri_lanka;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_sh_cal_sri_lanka;
      break;
    }
    case 183: {
      jsonBody.value = this.emissionFactors.values.price_lp92_sh_cal_myanmar;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_sh_cal_myanmar;
      break;
    }
    case 184: {
      jsonBody.value = this.emissionFactors.values.price_lp92_sh_cal_maldives;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_sh_cal_maldives;
      break;
    }
    case 185: {
      jsonBody.value = this.emissionFactors.values.price_lp92_sh_cal_indonesia;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_sh_cal_indonesia;
      break;
    }
    case 186: {
      jsonBody.value = this.emissionFactors.values.price_lp92_sh_cal_india;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_sh_cal_india;
      break;
    }
    case 187: {
      jsonBody.value = this.emissionFactors.values.price_lp92_sh_cal_bangladesh;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_sh_cal_bangladesh;
      break;
    }

    case 84: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_sh_cal_sri_lanka;
      jsonBody.reference = this.emissionFactors.refs.price_lad_sh_cal_sri_lanka;
      break;
    }
    case 188: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_sh_cal_myanmar;
      jsonBody.reference = this.emissionFactors.refs.price_lad_sh_cal_myanmar;
      break;
    }
    case 189: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_sh_cal_maldives;
      jsonBody.reference = this.emissionFactors.refs.price_lad_sh_cal_maldives;
      break;
    }
    case 190: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_sh_cal_indonesia;
      jsonBody.reference = this.emissionFactors.refs.price_lad_sh_cal_indonesia;
      break;
    }
    case 191: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_sh_cal_india;
      jsonBody.reference = this.emissionFactors.refs.price_lad_sh_cal_india;
      break;
    }
    case 192: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_sh_cal_bangladesh;
      jsonBody.reference = this.emissionFactors.refs.price_lad_sh_cal_bangladesh;
      break;
    }

    case 85: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_sh_cal_sri_lanka;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_sh_cal_sri_lanka;
      break;
    }
    case 193: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_sh_cal_myanmar;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_sh_cal_myanmar;
      break;
    }
    case 194: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_sh_cal_maldives;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_sh_cal_maldives;
      break;
    }
    case 195: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_sh_cal_indonesia;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_sh_cal_indonesia;
      break;
    }
    case 196: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_sh_cal_india;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_sh_cal_india;
      break;
    }
    case 197: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_sh_cal_bangladesh;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_sh_cal_bangladesh;
      break;
    }


    case 86: {
      jsonBody.value = this.emissionFactors.values.price_lp95_p_sri_lanka;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_p_sri_lanka;
      break;
    }
    case 198: {
      jsonBody.value = this.emissionFactors.values.price_lp95_p_myanmar;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_p_myanmar;
      break;
    }
    case 199: {
      jsonBody.value = this.emissionFactors.values.price_lp95_p_maldives;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_p_maldives;
      break;
    }
    case 200: {
      jsonBody.value = this.emissionFactors.values.price_lp95_p_indonesia;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_p_indonesia;
      break;
    }
    case 201: {
      jsonBody.value = this.emissionFactors.values.price_lp95_p_india;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_p_india;
      break;
    }
    case 202: {
      jsonBody.value = this.emissionFactors.values.price_lp95_p_bangladesh;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_p_bangladesh;
      break;
    }


    case 87: {
      jsonBody.value = this.emissionFactors.values.price_lp92_p_sri_lanka;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_p_sri_lanka;
      break;
    }
    case 203: {
      jsonBody.value = this.emissionFactors.values.price_lp92_p_myanmar;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_p_myanmar;
      break;
    }
    case 204: {
      jsonBody.value = this.emissionFactors.values.price_lp92_p_maldives;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_p_maldives;
      break;
    }
    case 205: {
      jsonBody.value = this.emissionFactors.values.price_lp92_p_indonesia;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_p_indonesia;
      break;
    }
    case 206: {
      jsonBody.value = this.emissionFactors.values.price_lp92_p_india;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_p_india;
      break;
    }
    case 207: {
      jsonBody.value = this.emissionFactors.values.price_lp92_p_bangladesh;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_p_bangladesh;
      break;
    }


    case 88: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_p_sri_lanka;
      jsonBody.reference = this.emissionFactors.refs.price_lad_p_sri_lanka;
      break;
    }
    case 208: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_p_myanmar;
      jsonBody.reference = this.emissionFactors.refs.price_lad_p_myanmar;
      break;
    }
    case 209: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_p_maldives;
      jsonBody.reference = this.emissionFactors.refs.price_lad_p_maldives;
      break;
    }
    case 210: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_p_indonesia;
      jsonBody.reference = this.emissionFactors.refs.price_lad_p_indonesia;
      break;
    }
    case 211: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_p_india;
      jsonBody.reference = this.emissionFactors.refs.price_lad_p_india;
      break;
    }
    case 212: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_p_bangladesh;
      jsonBody.reference = this.emissionFactors.refs.price_lad_p_bangladesh;
      break;
    }

    case 89: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_p_sri_lanka;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_p_sri_lanka;
      break;
    }
    case 213: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_p_myanmar;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_p_myanmar;
      break;
    }
    case 214: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_p_maldives;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_p_maldives;
      break;
    }
    case 215: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_p_indonesia;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_p_indonesia;
      break;
    }
    case 216: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_p_india;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_p_india;
      break;
    }
    case 217: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_p_bangladesh;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_p_bangladesh;
      break;
    }

    case 90: {
      jsonBody.value = this.emissionFactors.values.price_lp95_p_cal_sri_lanka;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_p_cal_sri_lanka;
      break;
    }
    case 218: {
      jsonBody.value = this.emissionFactors.values.price_lp95_p_cal_myanmar;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_p_cal_myanmar;
      break;
    }
    case 219: {
      jsonBody.value = this.emissionFactors.values.price_lp95_p_cal_maldives;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_p_cal_maldives;
      break;
    }
    case 220: {
      jsonBody.value = this.emissionFactors.values.price_lp95_p_cal_indonesia;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_p_cal_indonesia;
      break;
    }
    case 221: {
      jsonBody.value = this.emissionFactors.values.price_lp95_p_cal_india;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_p_cal_india;
      break;
    }
    case 222: {
      jsonBody.value = this.emissionFactors.values.price_lp95_p_cal_bangladesh;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp95_p_cal_bangladesh;
      break;
    }
    case 91: {
      jsonBody.value = this.emissionFactors.values.price_lp92_p_cal_sri_lanka;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_p_cal_sri_lanka;
      break;
    }
    case 223: {
      jsonBody.value = this.emissionFactors.values.price_lp92_p_cal_myanmar;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_p_cal_myanmar;
      break;
    }
    case 224: {
      jsonBody.value = this.emissionFactors.values.price_lp92_p_cal_maldives;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_p_cal_maldives;
      break;
    }
    case 225: {
      jsonBody.value = this.emissionFactors.values.price_lp92_p_cal_indonesia;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_p_cal_indonesia;
      break;
    }
    case 226: {
      jsonBody.value = this.emissionFactors.values.price_lp92_p_cal_india;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_p_cal_india;
      break;
    }
    case 227: {
      jsonBody.value = this.emissionFactors.values.price_lp92_p_cal_bangladesh;
      jsonBody.id = id;
      jsonBody.reference = this.emissionFactors.refs.price_lp92_p_cal_bangladesh;
      break;
    }
    case 92: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_p_cal_sri_lanka;
      jsonBody.reference = this.emissionFactors.refs.price_lad_p_cal_sri_lanka;
      break;
    }
    case 228: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_p_cal_myanmar;
      jsonBody.reference = this.emissionFactors.refs.price_lad_p_cal_myanmar;
      break;
    }
    case 229: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_p_cal_maldives;
      jsonBody.reference = this.emissionFactors.refs.price_lad_p_cal_maldives;
      break;
    }
    case 230: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_p_cal_indonesia;
      jsonBody.reference = this.emissionFactors.refs.price_lad_p_cal_indonesia;
      break;
    }
    case 231: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_p_cal_india;
      jsonBody.reference = this.emissionFactors.refs.price_lad_p_cal_india;
      break;
    }
    case 232: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lad_p_cal_bangladesh;
      jsonBody.reference = this.emissionFactors.refs.price_lad_p_cal_bangladesh;
      break;
    }
    case 93: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_p_cal_sri_lanka;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_p_cal_sri_lanka;
      break;
    }
    case 233: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_p_cal_myanmar;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_p_cal_myanmar;
      break;
    }
    case 234: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_p_cal_maldives;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_p_cal_maldives;
      break;
    }
    case 235: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_p_cal_indonesia;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_p_cal_indonesia;
      break;
    }
    case 236: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_p_cal_india;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_p_cal_india;
      break;
    }
    case 237: {
      jsonBody.id = id;
      jsonBody.value = this.emissionFactors.values.price_lsd_p_cal_bangladesh;
      jsonBody.reference = this.emissionFactors.refs.price_lsd_p_cal_bangladesh;
      break;
    }
      
        case 94: {
          jsonBody.value = this.emissionFactors.values.price_lp95_pp_sri_lanka;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp95_pp_sri_lanka;
          break;
        }
        case 238: {
          jsonBody.value = this.emissionFactors.values.price_lp95_pp_myanmar;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp95_pp_myanmar;
          break;
        }
        case 239: {
          jsonBody.value = this.emissionFactors.values.price_lp95_pp_maldives;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp95_pp_maldives;
          break;
        }
        case 240: {
          jsonBody.value = this.emissionFactors.values.price_lp95_pp_indonesia;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp95_pp_indonesia;
          break;
        }
        case 241: {
          jsonBody.value = this.emissionFactors.values.price_lp95_pp_india;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp95_pp_india;
          break;
        }
        case 242: {
          jsonBody.value = this.emissionFactors.values.price_lp95_pp_bangladesh;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp95_pp_bangladesh;
          break;
        }
        case 95: {
          jsonBody.value = this.emissionFactors.values.price_lp92_pp_sri_lanka;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp92_pp_sri_lanka;
          break;
        }
        case 318: {
          jsonBody.value = this.emissionFactors.values.price_lp92_pp_myanmar;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp92_pp_myanmar;
          break;
        }
        case 319: {
          jsonBody.value = this.emissionFactors.values.price_lp92_pp_maldives;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp92_pp_maldives;
          break;
        }
        case 320: {
          jsonBody.value = this.emissionFactors.values.price_lp92_pp_indonesia;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp92_pp_indonesia;
          break;
        }
        case 321: {
          jsonBody.value = this.emissionFactors.values.price_lp92_pp_india;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp92_pp_india;
          break;
        }
        case 322: {
          jsonBody.value = this.emissionFactors.values.price_lp92_pp_bangladesh;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp92_pp_bangladesh;
          break;
        }
        case 96: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lad_pp_sri_lanka;
          jsonBody.reference = this.emissionFactors.refs.price_lad_pp_sri_lanka;
          break;
        }
        case 323: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lad_pp_myanmar;
          jsonBody.reference = this.emissionFactors.refs.price_lad_pp_myanmar;
          break;
        }
        case 324: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lad_pp_maldives;
          jsonBody.reference = this.emissionFactors.refs.price_lad_pp_maldives;
          break;
        }
        case 325: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lad_pp_indonesia;
          jsonBody.reference = this.emissionFactors.refs.price_lad_pp_indonesia;
          break;
        }
        case 326: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lad_pp_india;
          jsonBody.reference = this.emissionFactors.refs.price_lad_pp_india;
          break;
        }
        case 327: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lad_pp_bangladesh;
          jsonBody.reference = this.emissionFactors.refs.price_lad_pp_bangladesh;
          break;
        }
        case 97: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lsd_pp_sri_lanka;
          jsonBody.reference = this.emissionFactors.refs.price_lsd_pp_sri_lanka;
          break;
        }
        case 328: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lsd_pp_myanmar;
          jsonBody.reference = this.emissionFactors.refs.price_lsd_pp_myanmar;
          break;
        }
        case 329: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lsd_pp_maldives;
          jsonBody.reference = this.emissionFactors.refs.price_lsd_pp_maldives;
          break;
        }
        case 330: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lsd_pp_indonesia;
          jsonBody.reference = this.emissionFactors.refs.price_lsd_pp_indonesia;
          break;
        }
        case 331: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lsd_pp_india;
          jsonBody.reference = this.emissionFactors.refs.price_lsd_pp_india;
          break;
        }
        case 332: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lsd_pp_bangladesh;
          jsonBody.reference = this.emissionFactors.refs.price_lsd_pp_bangladesh;
          break;
        }
    
        case 98: {
          jsonBody.value = this.emissionFactors.values.price_lp95_pp_cal_sri_lanka;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp95_pp_cal_sri_lanka;
          break;
        }
        case 333: {
          jsonBody.value = this.emissionFactors.values.price_lp95_pp_cal_myanmar;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp95_pp_cal_myanmar;
          break;
        }
        case 334: {
          jsonBody.value = this.emissionFactors.values.price_lp95_pp_cal_maldives;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp95_pp_cal_maldives;
          break;
        }
        case 335: {
          jsonBody.value = this.emissionFactors.values.price_lp95_pp_cal_indonesia;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp95_pp_cal_indonesia;
          break;
        }
        case 336: {
          jsonBody.value = this.emissionFactors.values.price_lp95_pp_cal_india;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp95_pp_cal_india;
          break;
        }
        case 337: {
          jsonBody.value = this.emissionFactors.values.price_lp95_pp_cal_bangladesh;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp95_pp_cal_bangladesh;
          break;
        }
        case 99: {
          jsonBody.value = this.emissionFactors.values.price_lp92_pp_cal_sri_lanka;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp92_pp_cal_sri_lanka;
          break;
        }
        case 338: {
          jsonBody.value = this.emissionFactors.values.price_lp92_pp_cal_myanmar;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp92_pp_cal_myanmar;
          break;
        }
        case 339: {
          jsonBody.value = this.emissionFactors.values.price_lp92_pp_cal_maldives;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp92_pp_cal_maldives;
          break;
        }
        case 340: {
          jsonBody.value = this.emissionFactors.values.price_lp92_pp_cal_indonesia;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp92_pp_cal_indonesia;
          break;
        }
        case 341: {
          jsonBody.value = this.emissionFactors.values.price_lp92_pp_cal_india;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp92_pp_cal_india;
          break;
        }
        case 342: {
          jsonBody.value = this.emissionFactors.values.price_lp92_pp_cal_bangladesh;
          jsonBody.id = id;
          jsonBody.reference = this.emissionFactors.refs.price_lp92_pp_cal_bangladesh;
          break;
        }
        case 100: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lad_pp_cal_sri_lanka;
          jsonBody.reference = this.emissionFactors.refs.price_lad_pp_cal_sri_lanka;
          break;
        }
        case 343: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lad_pp_cal_myanmar;
          jsonBody.reference = this.emissionFactors.refs.price_lad_pp_cal_myanmar;
          break;
        }
        case 344: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lad_pp_cal_maldives;
          jsonBody.reference = this.emissionFactors.refs.price_lad_pp_cal_maldives;
          break;
        }
        case 345: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lad_pp_cal_indonesia;
          jsonBody.reference = this.emissionFactors.refs.price_lad_pp_cal_indonesia;
          break;
        }
        case 346: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lad_pp_cal_india;
          jsonBody.reference = this.emissionFactors.refs.price_lad_pp_cal_india;
          break;
        }
        case 347: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lad_pp_cal_bangladesh;
          jsonBody.reference = this.emissionFactors.refs.price_lad_pp_cal_bangladesh;
          break;
        }
        case 101: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lsd_pp_cal_sri_lanka;
          jsonBody.reference = this.emissionFactors.refs.price_lsd_pp_cal_sri_lanka;
          break;
        }
        case 348: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lsd_pp_cal_myanmar;
          jsonBody.reference = this.emissionFactors.refs.price_lsd_pp_cal_myanmar;
          break;
        }
        case 349: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lsd_pp_cal_maldives;
          jsonBody.reference = this.emissionFactors.refs.price_lsd_pp_cal_maldives;
          break;
        }
        case 350: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lsd_pp_cal_indonesia;
          jsonBody.reference = this.emissionFactors.refs.price_lsd_pp_cal_indonesia;
          break;
        }
        case 351: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lsd_pp_cal_india;
          jsonBody.reference = this.emissionFactors.refs.price_lsd_pp_cal_india;
          break;
        }
        case 352: {
          jsonBody.id = id;
          jsonBody.value = this.emissionFactors.values.price_lsd_pp_cal_bangladesh;
          jsonBody.reference = this.emissionFactors.refs.price_lsd_pp_cal_bangladesh;
          break;
        }


    }

    if (jsonBody.value === '' || jsonBody.value === undefined) {
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      })
      return;
    }
    this.boService.sendRequestToBackend(
      RequestGroup.EmissionFactors,
      RequestType.ManageEmissionFactors,
      { DATA: jsonBody}
      ).then(data => {

      if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
        this.toastSerivce.show('', 'Saved data successfully.', {
          status: 'success',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
        // event.confirm.resolve(event.newData);
        // this.loadData();
      } else {
        this.toastSerivce.show('', 'Error in saving data.', {
          status: 'danger',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
      }

    )
  }

  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.EmissionFactors,
      RequestType.ListEmFactors,
      {
        PAGE_NUMBER: -1,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          let list = [];
           console.log(data.DAT.LIST)
          data.DAT.LIST.forEach(dto => {
            if (dto === undefined) return;
            switch(dto.id) {
              case 1: {
                this.emissionFactors.values.den_diesel = dto.value;
                this.emissionFactors.refs.den_diesel = dto.reference;
                break;
              }
              case 2: {
                this.emissionFactors.values.den_petrol = dto.value;
                this.emissionFactors.refs.den_petrol = dto.reference;
                break;
              }
              case 3: {
                this.emissionFactors.values.net_caloric_diesel = dto.value;
                this.emissionFactors.refs.net_caloric_diesel = dto.reference;
                break;
              }
              case 4: {
                this.emissionFactors.values.net_caloric_petrol = dto.value;
                this.emissionFactors.refs.net_caloric_petrol = dto.reference;
                break;
              }
              case 5: {
                this.emissionFactors.values.ef_co2_s_diesel = dto.value;
                this.emissionFactors.refs.ef_co2_s_diesel = dto.reference;
                break;
              }
              case 6: {
                this.emissionFactors.values.ef_ch4_s_diesel = dto.value;
                this.emissionFactors.refs.ef_ch4_s_diesel = dto.reference;
                break;
              }
              case 7: {
                this.emissionFactors.values.ef_n2o_s_diesel = dto.value;
                this.emissionFactors.refs.ef_n2o_s_diesel = dto.reference;
                break;
              }
              case 8: {
                this.emissionFactors.values.gwp_co2 = dto.value;
                this.emissionFactors.refs.gwp_co2 = dto.reference;
                break;
              }
              case 9: {
                this.emissionFactors.values.gwp_ch4 = dto.value;
                this.emissionFactors.refs.gwp_ch4  = dto.reference;
                break;
              }
              case 10: {
                this.emissionFactors.values.gwp_n2o = dto.value;
                this.emissionFactors.refs.gwp_n2o = dto.reference;
                break;
              }
              case 11: {
                this.emissionFactors.values.gwp_r22 = dto.value;
                this.emissionFactors.refs.gwp_r22 = dto.reference;
                break;
              }
              case 12: {
                this.emissionFactors.values.gwp_r407c_w_perc_ch2f2 = dto.value;
                this.emissionFactors.refs.gwp_r407c_w_perc_ch2f2 = dto.reference;
                break;
              }
              case 13: {
                this.emissionFactors.values.gwp_r407c_w_perc_cf3chf2 = dto.value;
                this.emissionFactors.refs.gwp_r407c_w_perc_cf3chf2 = dto.reference;
                break;
              }
              case 14: {
                this.emissionFactors.values.gwp_r407c_w_perc_cf3ch2f = dto.value;
                this.emissionFactors.refs.gwp_r407c_w_perc_cf3ch2f = dto.reference;
                break;
              }
              case 15: {
                this.emissionFactors.values.gwp_r407c_w_coff_cf3chf2 = dto.value;
                this.emissionFactors.refs.gwp_r407c_w_coff_cf3chf2 = dto.reference;
                break;
              }
              case 16: {
                this.emissionFactors.values.gwp_r407c_w_coff_ch2f2 = dto.value;
                this.emissionFactors.refs.gwp_r407c_w_coff_ch2f2 = dto.reference;
                break;
              }
              case 17: {
                this.emissionFactors.values.gwp_r407c_w_coff_cf3ch2f = dto.value;
                this.emissionFactors.refs.gwp_r407c_w_coff_cf3ch2f = dto.reference;
                break;
              }
              case 18: {
                this.emissionFactors.values.gwp_r410a_w_per_ch2f2 = dto.value;
                this.emissionFactors.refs.gwp_r410a_w_per_ch2f2 = dto.reference;
                break;
              }
              case 19: {
                this.emissionFactors.values.gwp_r410a_w_per_chf2cf3 = dto.value;
                this.emissionFactors.refs.gwp_r410a_w_per_chf2cf3 = dto.reference;
                break;
              }
              case 20: {
                this.emissionFactors.values.gwp_r410a_w_coff_ch2f2 = dto.value;
                this.emissionFactors.refs.gwp_r410a_w_coff_ch2f2 = dto.reference;
                break;
              }
              case 21: {
                this.emissionFactors.values.gwp_r410a_w_coff_chf2cf3 = dto.value;
                this.emissionFactors.refs.gwp_r410a_w_coff_chf2cf3 = dto.reference;
                break;
              }
              case 22: {
                this.emissionFactors.values.ef_co2_m_gasoline = dto.value;
                this.emissionFactors.refs.ef_co2_m_gasoline = dto.reference;
                break;
              }
              case 23: {
                this.emissionFactors.values.ef_co2_m_diesel = dto.value;
                this.emissionFactors.refs.ef_co2_m_diesel = dto.reference;
                break;
              }
              case 24: {
                this.emissionFactors.values.ef_ch4_m_gasoline = dto.value;
                this.emissionFactors.refs.ef_ch4_m_gasoline = dto.reference;
                break;
              }
              case 25: {
                this.emissionFactors.values.ef_ch4_m_diesel = dto.value;
                this.emissionFactors.refs.ef_ch4_m_diesel = dto.reference;
                break;
              }
              case 26: {
                this.emissionFactors.values.ef_n20_m_gasoline = dto.value;
                this.emissionFactors.refs.ef_n20_m_gasoline = dto.reference;
                break;
              }
              case 27: {
                this.emissionFactors.values.ef_n20_m_diesel = dto.value;
                this.emissionFactors.refs.ef_n20_m_diesel = dto.reference;
                break;
              }
              case 28: {
                this.emissionFactors.values.ef_co2_o_gasoline = dto.value;
                this.emissionFactors.refs.ef_co2_o_gasoline = dto.reference;
                break;
              }
              case 29: {
                this.emissionFactors.values.ef_co2_o_diesel = dto.value;
                this.emissionFactors.refs.ef_co2_o_diesel = dto.reference;
                break;
              }
              case 30: {
                this.emissionFactors.values.ef_ch4_o_gasoline = dto.value;
                this.emissionFactors.refs.ef_ch4_o_gasoline = dto.reference;
                break;
              }
              case 31: {
                this.emissionFactors.values.ef_ch4_o_diesel = dto.value;
                this.emissionFactors.refs.ef_ch4_o_diesel = dto.reference;
                break;
              }
              case 32: {
                this.emissionFactors.values.ef_n20_o_gasoline = dto.value;
                this.emissionFactors.refs.ef_n20_o_gasoline = dto.reference;
                break;
              }
              case 33: {
                this.emissionFactors.values.ef_n2o_o_diesel = dto.value;
                this.emissionFactors.refs.ef_n2o_o_diesel = dto.reference;
                break;
              }

              case 34: {
                this.emissionFactors.values.grid_ef_sri_Lanka = dto.value;
                this.emissionFactors.refs.grid_ef_sri_Lanka = dto.reference;
                break;
              }
              case 103: {
                this.emissionFactors.values.grid_ef_myanmar = dto.value;
                this.emissionFactors.refs.grid_ef_myanmar = dto.reference;
                break;
              }
              case 106: {
                this.emissionFactors.values.grid_ef_maldives = dto.value;
                this.emissionFactors.refs.grid_ef_maldives = dto.reference;
                break;
              }
              case 109: {
                this.emissionFactors.values.grid_ef_indonesia = dto.value;
                this.emissionFactors.refs.grid_ef_indonesia = dto.reference;
                break;
              }
              case 112: {
                this.emissionFactors.values.grid_ef_india = dto.value;
                this.emissionFactors.refs.grid_ef_india = dto.reference;
                break;
              }
              case 115: {
                this.emissionFactors.values.grid_ef_bangaladesh = dto.value;
                this.emissionFactors.refs.grid_ef_bangaladesh = dto.reference;
                break;
              }



              case 35: {
                this.emissionFactors.values.t_d_loss_perc_sri_Lanka = dto.value;
                this.emissionFactors.refs.t_d_loss_perc_sri_Lanka = dto.reference;
                break;
              }
              case 104: {
                this.emissionFactors.values.t_d_loss_perc_myanmar = dto.value;
                this.emissionFactors.refs.t_d_loss_perc_myanmar = dto.reference;
                break;
              }
              case 107: {
                this.emissionFactors.values.t_d_loss_perc_maldives = dto.value;
                this.emissionFactors.refs.t_d_loss_perc_maldives = dto.reference;
                break;
              }
              case 110: {
                this.emissionFactors.values.t_d_loss_perc_indonesia = dto.value;
                this.emissionFactors.refs.t_d_loss_perc_indonesia = dto.reference;
                break;
              }
              case 113: {
                this.emissionFactors.values.t_d_loss_perc_india = dto.value;
                this.emissionFactors.refs.t_d_loss_perc_india = dto.reference;
                break;
              }
              case 116: {
                this.emissionFactors.values.t_d_loss_perc_bangaladesh = dto.value;
                this.emissionFactors.refs.t_d_loss_perc_bangaladesh = dto.reference;
                break;
              }


              case 36: {
                this.emissionFactors.values.cf_mw_sri_Lanka = dto.value;
                this.emissionFactors.refs.cf_mw_sri_Lanka = dto.reference;
                break;
              }
              case 105: {
                this.emissionFactors.values.cf_mw_myanmar = dto.value;
                this.emissionFactors.refs.cf_mw_myanmar = dto.reference;
                break;
              }
              case 108: {
                this.emissionFactors.values.cf_mw_maldives = dto.value;
                this.emissionFactors.refs.cf_mw_maldives = dto.reference;
                break;
              }
              case 111: {
                this.emissionFactors.values.cf_mw_indonesia = dto.value;
                this.emissionFactors.refs.cf_mw_indonesia = dto.reference;
                break;
              }
              case 114: {
                this.emissionFactors.values.cf_mw_india = dto.value;
                this.emissionFactors.refs.cf_mw_india = dto.reference;
                break;
              }
              case 117: {
                this.emissionFactors.values.cf_mw_bangaladesh = dto.value;
                this.emissionFactors.refs.cf_mw_bangaladesh = dto.reference;
                break;
              }
              case 37: {
                this.emissionFactors.values.avg_pig_feed_rate = dto.value;
                this.emissionFactors.refs.avg_pig_feed_rate = dto.reference;
                break;
              }
              // case 38: {
              //   this.emissionFactors.values.price_diesel_liter = dto.value;
              //   this.emissionFactors.refs.price_diesel_liter = dto.reference;
              //   break;
              // }
              // case 39: {
              //   this.emissionFactors.values.price_petrol_liter = dto.value;
              //   this.emissionFactors.refs.price_petrol_liter  = dto.reference;
              //   break;
              // }
              // case 40: {
              //   this.emissionFactors.values.price_diesel_liter_cal = dto.value;
              //   this.emissionFactors.refs.price_diesel_liter_cal = dto.reference;
              //   break;
              // }
              // case 41: {
              //   this.emissionFactors.values.price_petrol_liter_cal = dto.value;
              //   this.emissionFactors.refs.price_petrol_liter_cal = dto.reference;
              //   break;
              // }

            
      
              case 38: {
               this.emissionFactors.values.price_lad_fh_sri_lanka = dto.value;
               this.emissionFactors.refs.price_lad_fh_sri_lanka;
                break;
              }
              case 118: {
               this.emissionFactors.values.price_lad_fh_myanmar = dto.value;
               this.emissionFactors.refs.price_lad_fh_myanmar;
                break;
              }
              case 119: {
                this.emissionFactors.values.price_lad_fh_maldives = dto.value;
               this.emissionFactors.refs.price_lad_fh_maldives;
                break;
              }
              case 120: {
                this.emissionFactors.values.price_lad_fh_indonesia = dto.value;
                this.emissionFactors.refs.price_lad_fh_indonesia;
                break;
              }
              case 121: {
                 this.emissionFactors.values.price_lad_fh_india = dto.value;
                this.emissionFactors.refs.price_lad_fh_india;
                break;
              }
              case 122: {
              this.emissionFactors.values.price_lad_fh_bangladesh = dto.value;
               this.emissionFactors.refs.price_lad_fh_bangladesh;
                break;
              }
               
              case 39: {
                 this.emissionFactors.values.price_lp95_fh_sri_lanka = dto.value;
                this.emissionFactors.refs.price_lp95_fh_sri_lanka;
                break;
              }
              case 123: {
                 this.emissionFactors.values.price_lp95_fh_myanmar = dto.value;
                 this.emissionFactors.refs.price_lp95_fh_myanmar;
                break;
              }
              case 124: {
                this.emissionFactors.values.price_lp95_fh_maldives = dto.value;
                 this.emissionFactors.refs.price_lp95_fh_maldives;
                break;
              }
              case 125: {
                 this.emissionFactors.values.price_lp95_fh_indonesia = dto.value;
                this.emissionFactors.refs.price_lp95_fh_indonesia;
                break;
              }
              case 126: {
                 this.emissionFactors.values.price_lp95_fh_india = dto.value;
                this.emissionFactors.refs.price_lp95_fh_india;
                break;
              }
              case 127: {
                 this.emissionFactors.values.price_lp95_fh_bangladesh = dto.value;
                this.emissionFactors.refs.price_lp95_fh_bangladesh;
                break;
              }
              
              case 40: {
                this.emissionFactors.values.price_lad_fh_cal_sri_lanka = dto.value;
                 this.emissionFactors.refs.price_lad_fh_cal_sri_lanka;
                break;
              }
              case 138: {
               this.emissionFactors.values.price_lad_fh_cal_myanmar = dto.value;
                this.emissionFactors.refs.price_lad_fh_cal_myanmar;
                break;
              }
              case 139: {
                this.emissionFactors.values.price_lad_fh_cal_maldives = dto.value;
                this.emissionFactors.refs.price_lad_fh_cal_maldives;
                break;
              }
              case 140: {
                this.emissionFactors.values.price_lad_fh_cal_indonesia = dto.value;
                this.emissionFactors.refs.price_lad_fh_cal_indonesia;
                break;
              }
              case 141: {
                 this.emissionFactors.values.price_lad_fh_cal_india = dto.value;
                 this.emissionFactors.refs.price_lad_fh_cal_india;
                break;
              }
              case 142: {
                this.emissionFactors.values.price_lad_fh_cal_bangladesh = dto.value;
                 this.emissionFactors.refs.price_lad_fh_cal_bangladesh;
                break;
              }
              case 41: {
                 this.emissionFactors.values.price_lp95_fh_cal_sri_lanka = dto.value;
                 this.emissionFactors.refs.price_lp95_fh_cal_sri_lanka;
                break;
              }
              case 143: {
               this.emissionFactors.values.price_lp95_fh_cal_myanmar = dto.value;
                this.emissionFactors.refs.price_lp95_fh_cal_myanmar;
                break;
              }
              case 144: {
                this.emissionFactors.values.price_lp95_fh_cal_maldives = dto.value;
                 this.emissionFactors.refs.price_lp95_fh_cal_maldives;
                break;
              }
              case 145: {
                this.emissionFactors.values.price_lp95_fh_cal_indonesia = dto.value;
                 this.emissionFactors.refs.price_lp95_fh_cal_indonesia;
                break;
              }
              case 146: {
               
                 this.emissionFactors.values.price_lp95_fh_cal_india = dto.value;
                this.emissionFactors.refs.price_lp95_fh_cal_india;
                break;
              }
              case 147: {
                
                 this.emissionFactors.values.price_lp95_fh_cal_bangladesh = dto.value;
                this.emissionFactors.refs.price_lp95_fh_cal_bangladesh;
                break;
              }

              
              case 42: {
                this.emissionFactors.values.em_intensity_finance = dto.value;
                this.emissionFactors.refs.em_intensity_finance  = dto.reference;
                break;
              }
              case 43: {
                this.emissionFactors.values.em_intensity_tel = dto.value;
                this.emissionFactors.refs.em_intensity_tel = dto.reference;
                break;
              }
              case 44: {
                this.emissionFactors.values.em_intensity_apparel = dto.value;
                this.emissionFactors.refs.em_intensity_apparel = dto.reference;
                break;
              }
              case 45: {
                this.emissionFactors.values.em_intensity_hospitality = dto.value;
                this.emissionFactors.refs.em_intensity_hospitality = dto.reference;
                break;
              }
              case 46:{
                this.emissionFactors.values.em_intensity_plant = dto.value;
                this.emissionFactors.refs.em_intensity_plant = dto.reference;
                break;
              }
              case 47: {
                this.emissionFactors.values.em_intensity_trans = dto.value;
                this.emissionFactors.refs.em_intensity_trans= dto.reference;
                break;
              }
              case 48: {
                this.emissionFactors.values.em_intensity_food = dto.value;
                this.emissionFactors.refs.em_intensity_food = dto.reference;
                break;
              }
              case 49: {
                this.emissionFactors.values.em_intensity_manuf = dto.value;
                this.emissionFactors.refs.em_intensity_manuf = dto.reference;
                break;
              }
              case 50: {
                this.emissionFactors.values.em_intensity_other = dto.value;
                this.emissionFactors.refs.em_intensity_other = dto.reference;
                break;
              }
              case 51: {
                this.emissionFactors.values.cf_nautic_to_km = dto.value;
                this.emissionFactors.refs.cf_nautic_to_km = dto.reference;
                break;
              }
              case 52: {
                this.emissionFactors.values.ef_co2_air_freight_range1 = dto.value;
                this.emissionFactors.refs.ef_co2_air_freight_range1 = dto.reference;
                break;
              }
              case 53: {
                this.emissionFactors.values.ef_co2_air_freight_range2 = dto.value;
                this.emissionFactors.refs.ef_co2_air_freight_range2 = dto.reference;
                break;
              }
              case 54: {
                this.emissionFactors.values.ef_co2_air_freight_range3 = dto.value;
                this.emissionFactors.refs.ef_co2_air_freight_range3 = dto.reference;
                break;
              }
              case 55: {
                this.emissionFactors.values.ef_co2_sea_freight = dto.value;
                this.emissionFactors.refs.ef_co2_sea_freight = dto.reference;
                break;
              }
              case 56: {
                this.emissionFactors.values.ncv_biomass = dto.value;
                this.emissionFactors.refs.ncv_biomass = dto.reference;
                break;
              }

              case 57: {
                this.emissionFactors.values.ef_ch4_biomass = dto.value;
                this.emissionFactors.refs.ef_ch4_biomass = dto.reference;
                break;
              }
              case 58: {
                this.emissionFactors.values.ef_co2_biomass = dto.value;
                this.emissionFactors.refs.ef_co2_biomass = dto.reference;
                break;
              }
              case 59: {
                this.emissionFactors.values.ef_n20_biomass = dto.value;
                this.emissionFactors.refs.ef_n20_biomass = dto.reference;
                break;
              }
              case 60: {
                this.emissionFactors.values.ncv_lpgas = dto.value;
                this.emissionFactors.refs.ncv_lpgas = dto.reference;
                break;
              }
              case 61: {
                this.emissionFactors.values.ef_ch4_lpgas = dto.value;
                this.emissionFactors.refs.ef_ch4_lpgas = dto.reference;
                break;
              }
              case 62: {
                this.emissionFactors.values.ef_co2_lpgas = dto.value;
                this.emissionFactors.refs.ef_co2_lpgas = dto.reference;
                break;
              }
              case 63: {
                this.emissionFactors.values.ef_n20_lpgas = dto.value;
                this.emissionFactors.refs.ef_n20_lpgas = dto.reference;
                break;
              }
              //new added
          
              case 74: {
               this.emissionFactors.values.price_lsd_fh_sri_lanka = dto.value;
               this.emissionFactors.refs.price_lsd_fh_sri_lanka = dto.reference;
                break;
              }
              case 128: {
               this.emissionFactors.values.price_lsd_fh_myanmar = dto.value;
              this.emissionFactors.refs.price_lsd_fh_myanmar = dto.reference;
                break;
              }
              case 129: {
              this.emissionFactors.values.price_lsd_fh_maldives = dto.value;
                this.emissionFactors.refs.price_lsd_fh_maldives = dto.reference;
                break;
              }
              case 130: {
               this.emissionFactors.values.price_lsd_fh_indonesia = dto.value;
               this.emissionFactors.refs.price_lsd_fh_indonesia = dto.reference;
                break;
              }
              case 131: {
               this.emissionFactors.values.price_lsd_fh_india = dto.value;
                this.emissionFactors.refs.price_lsd_fh_india = dto.reference;
                break;
              }
              case 132: {
                 this.emissionFactors.values.price_lsd_fh_bangladesh = dto.value;
               this.emissionFactors.refs.price_lsd_fh_bangladesh = dto.reference;
                break;
              }
              
              case 75: {
              this.emissionFactors.values.price_lp92_fh_sri_lanka = dto.value;
               this.emissionFactors.refs.price_lp92_fh_sri_lanka = dto.reference;
                break;
              }
              case 133: {
                this.emissionFactors.values.price_lp92_fh_myanmar = dto.value;
                 this.emissionFactors.refs.price_lp92_fh_myanmar = dto.reference;
                break;
              }
              case 134: {
               this.emissionFactors.values.price_lp92_fh_maldives = dto.value;
               
               this.emissionFactors.refs.price_lp92_fh_maldives = dto.reference;
                break;
              }
              case 135: {
              this.emissionFactors.values.price_lp92_fh_indonesia = dto.value;
              
              this.emissionFactors.refs.price_lp92_fh_indonesia = dto.reference;
                break;
              }
              case 136: {
             this.emissionFactors.values.price_lp92_fh_india = dto.value;
              this.emissionFactors.refs.price_lp92_fh_india = dto.reference;
                break;
              }
              case 137: {
              this.emissionFactors.values.price_lp92_fh_bangladesh = dto.value;
               this.emissionFactors.refs.price_lp92_fh_bangladesh = dto.reference;
                break;
              }
              case 76: {
             this.emissionFactors.values.price_lsd_fh_cal_sri_lanka = dto.value;
               this.emissionFactors.refs.price_lsd_fh_cal_sri_lanka = dto.reference;
                break;
              }
              case 148: {
               this.emissionFactors.values.price_lsd_fh_cal_myanmar = dto.value;
             this.emissionFactors.refs.price_lsd_fh_cal_myanmar = dto.reference;
                break;
              }
              case 149: {
                this.emissionFactors.values.price_lsd_fh_cal_maldives = dto.value;
              this.emissionFactors.refs.price_lsd_fh_cal_maldives = dto.reference;
                break;
              }
              case 150: {
               this.emissionFactors.values.price_lsd_fh_cal_indonesia = dto.value;
               this.emissionFactors.refs.price_lsd_fh_cal_indonesia = dto.reference;
                break;
              }
              case 151: {
               this.emissionFactors.values.price_lsd_fh_cal_india = dto.value;
               this.emissionFactors.refs.price_lsd_fh_cal_india = dto.reference;
                break;
              }
              case 152: {
                this.emissionFactors.values.price_lsd_fh_cal_bangladesh = dto.value;
               this.emissionFactors.refs.price_lsd_fh_cal_bangladesh = dto.reference;
                break;
              }
              case 77: {
                this.emissionFactors.values.price_lp92_fh_cal_sri_lanka = dto.value;
                this.emissionFactors.refs.price_lp92_fh_cal_sri_lanka = dto.reference;
                break;
              }
              case 153: {
             this.emissionFactors.values.price_lp92_fh_cal_myanmar= dto.value;
                this.emissionFactors.refs.price_lp92_fh_cal_myanmar = dto.reference;
                break;
              }
              case 154: {
               this.emissionFactors.values.price_lp92_fh_cal_maldives = dto.value;
               this.emissionFactors.refs.price_lp92_fh_cal_maldives= dto.reference;
                break;
              }
              case 155: {
               this.emissionFactors.values.price_lp92_fh_cal_indonesia = dto.value;
               this.emissionFactors.refs.price_lp92_fh_cal_indonesia= dto.reference;
                break;
              }
              case 156: {
              this.emissionFactors.values.price_lp92_fh_cal_india = dto.value;
              this.emissionFactors.refs.price_lp92_fh_cal_india= dto.reference;
                break;
              }
              case 157: {
              this.emissionFactors.values.price_lp92_fh_cal_bangladesh = dto.value;
              this.emissionFactors.refs.price_lp92_fh_cal_bangladesh= dto.reference;
                break;
              }
        
            
            case 78: {
             this.emissionFactors.values.price_lp95_sh_sri_lanka = dto.value;
              this.emissionFactors.refs.price_lp95_sh_sri_lanka;
              break;
            }
            case 158: {
               this.emissionFactors.values.price_lp95_sh_myanmar = dto.value;
             this.emissionFactors.refs.price_lp95_sh_myanmar= dto.reference;
              break;
            }
            case 159: {
             this.emissionFactors.values.price_lp95_sh_maldives = dto.value;
             this.emissionFactors.refs.price_lp95_sh_maldives= dto.reference;
              break;
            }
            case 160: {
             this.emissionFactors.values.price_lp95_sh_indonesia = dto.value;
             this.emissionFactors.refs.price_lp95_sh_indonesia= dto.reference;
              break;
            }
            case 161: {
           this.emissionFactors.values.price_lp95_sh_india = dto.value;
           this.emissionFactors.refs.price_lp95_sh_india= dto.reference;
              break;
            }
            case 162: {
            this.emissionFactors.values.price_lp95_sh_bangladesh = dto.value;
           this.emissionFactors.refs.price_lp95_sh_bangladesh= dto.reference;
              break;
            }
            case 79: {
             this.emissionFactors.values.price_lp92_sh_sri_lanka = dto.value;
             this.emissionFactors.refs.price_lp92_sh_sri_lanka= dto.reference;
              break;
            }
            case 163: {
           this.emissionFactors.values.price_lp92_sh_myanmar = dto.value;
            this.emissionFactors.refs.price_lp92_sh_myanmar= dto.reference;
              break;
            }
            case 164: {
             this.emissionFactors.values.price_lp92_sh_maldives = dto.value;
             this.emissionFactors.refs.price_lp92_sh_maldives= dto.reference;
              break;
            }
            case 165: {
             this.emissionFactors.values.price_lp92_sh_indonesia = dto.value;
              this.emissionFactors.refs.price_lp92_sh_indonesia= dto.reference;
              break;
            }
            case 166: {
              this.emissionFactors.values.price_lp92_sh_india = dto.value;
              this.emissionFactors.refs.price_lp92_sh_india= dto.reference;
              break;
            }
            case 167: {
             this.emissionFactors.values.price_lp92_sh_bangladesh = dto.value;
             this.emissionFactors.refs.price_lp92_sh_bangladesh= dto.reference;
              break;
            }
            case 80: {
           this.emissionFactors.values.price_lad_sh_sri_lanka = dto.value;
             this.emissionFactors.refs.price_lad_sh_sri_lanka= dto.reference;
              break;
            }
            case 168: {
             this.emissionFactors.values.price_lad_sh_myanmar = dto.value;
              this.emissionFactors.refs.price_lad_sh_myanmar= dto.reference;
              break;
            }
            case 169: {
            this.emissionFactors.values.price_lad_sh_maldives = dto.value;
             this.emissionFactors.refs.price_lad_sh_maldives= dto.reference;
              break;
            }
            case 170: {
            this.emissionFactors.values.price_lad_sh_indonesia = dto.value;
            this.emissionFactors.refs.price_lad_sh_indonesia= dto.reference;
              break;
            }
            case 171: {
            this.emissionFactors.values.price_lad_sh_india = dto.value;
             this.emissionFactors.refs.price_lad_sh_india= dto.reference;
              break;
            }
            case 172: {
             this.emissionFactors.values.price_lad_sh_bangladesh = dto.value;
        this.emissionFactors.refs.price_lad_sh_bangladesh= dto.reference;
              break;
            }
            case 81: {
             this.emissionFactors.values.price_lsd_sh_sri_lanka = dto.value;
           this.emissionFactors.refs.price_lsd_sh_sri_lanka= dto.reference;
              break;
            }
            case 173: {
         this.emissionFactors.values.price_lsd_sh_myanmar = dto.value;
              this.emissionFactors.refs.price_lsd_sh_myanmar= dto.reference;
              break;
            }
            case 174: {
           this.emissionFactors.values.price_lsd_sh_maldives = dto.value;
             this.emissionFactors.refs.price_lsd_sh_maldives= dto.reference;
              break;
            }
            case 175: {
          this.emissionFactors.values.price_lsd_sh_indonesia = dto.value;
             this.emissionFactors.refs.price_lsd_sh_indonesia= dto.reference;
              break;
            }
            case 176: {
           this.emissionFactors.values.price_lsd_sh_india = dto.value;
             this.emissionFactors.refs.price_lsd_sh_india= dto.reference;
              break;
            }
            case 177: {
          this.emissionFactors.values.price_lsd_sh_bangladesh = dto.value;
             this.emissionFactors.refs.price_lsd_sh_bangladesh= dto.reference;
              break;
            }
        
            case 82: {
            this.emissionFactors.values.price_lp95_sh_cal_sri_lanka = dto.value;
             this.emissionFactors.refs.price_lp95_sh_cal_sri_lanka= dto.reference;
              break;
            }
            case 178: {
             this.emissionFactors.values.price_lp95_sh_cal_myanmar = dto.value;
           this.emissionFactors.refs.price_lp95_sh_cal_myanmar= dto.reference;
              break;
            }
            case 179: {
             this.emissionFactors.values.price_lp95_sh_cal_maldives = dto.value;
              this.emissionFactors.refs.price_lp95_sh_cal_maldives= dto.reference;
              break;
            }
            case 180: {
              this.emissionFactors.values.price_lp95_sh_cal_indonesia = dto.value;
            this.emissionFactors.refs.price_lp95_sh_cal_indonesia= dto.reference;
              break;
            }
            case 181: {
             this.emissionFactors.values.price_lp95_sh_cal_india = dto.value;
            this.emissionFactors.refs.price_lp95_sh_cal_india= dto.reference;
              break;
            }
            case 182: {
             this.emissionFactors.values.price_lp95_sh_cal_bangladesh = dto.value;
             this.emissionFactors.refs.price_lp95_sh_cal_bangladesh= dto.reference;
              break;
            }
        
        
            case 83: {
              this.emissionFactors.values.price_lp92_sh_cal_sri_lanka = dto.value;
              this.emissionFactors.refs.price_lp92_sh_cal_sri_lanka= dto.reference;
              break;
            }
            case 183: {
            this.emissionFactors.values.price_lp92_sh_cal_myanmar = dto.value;
              this.emissionFactors.refs.price_lp92_sh_cal_myanmar= dto.reference;
              break;
            }
            case 184: {
              this.emissionFactors.values.price_lp92_sh_cal_maldives = dto.value;
            
             this.emissionFactors.refs.price_lp92_sh_cal_maldives= dto.reference;
              break;
            }
            case 185: {
             this.emissionFactors.values.price_lp92_sh_cal_indonesia = dto.value;
             this.emissionFactors.refs.price_lp92_sh_cal_indonesia= dto.reference;
              break;
            }
            case 186: {
             this.emissionFactors.values.price_lp92_sh_cal_india = dto.value;
              this.emissionFactors.refs.price_lp92_sh_cal_india= dto.reference;
              break;
            }
            case 187: {
              this.emissionFactors.values.price_lp92_sh_cal_bangladesh = dto.value;
              this.emissionFactors.refs.price_lp92_sh_cal_bangladesh= dto.reference;
              break;
            }
        
            case 84: {
              this.emissionFactors.values.price_lad_sh_cal_sri_lanka = dto.value;
             this.emissionFactors.refs.price_lad_sh_cal_sri_lanka= dto.reference;
              break;
            }
            case 188: {
            this.emissionFactors.values.price_lad_sh_cal_myanmar = dto.value;
              this.emissionFactors.refs.price_lad_sh_cal_myanmar= dto.reference;
              break;
            }
            case 189: {
            this.emissionFactors.values.price_lad_sh_cal_maldives = dto.value;
             this.emissionFactors.refs.price_lad_sh_cal_maldives= dto.reference;
              break;
            }
            case 190: {
             this.emissionFactors.values.price_lad_sh_cal_indonesia = dto.value;
              this.emissionFactors.refs.price_lad_sh_cal_indonesia= dto.reference;
              break;
            }
            case 191: {
             this.emissionFactors.values.price_lad_sh_cal_india = dto.value;
             this.emissionFactors.refs.price_lad_sh_cal_india= dto.reference;
              break;
            }
            case 192: {
             this.emissionFactors.values.price_lad_sh_cal_bangladesh = dto.value;
             this.emissionFactors.refs.price_lad_sh_cal_bangladesh= dto.reference;
              break;
            }
        
            case 85: {
             this.emissionFactors.values.price_lsd_sh_cal_sri_lanka = dto.value;
              this.emissionFactors.refs.price_lsd_sh_cal_sri_lanka;
              break;
            }
            case 193: {
             this.emissionFactors.values.price_lsd_sh_cal_myanmar = dto.value;
             this.emissionFactors.refs.price_lsd_sh_cal_myanmar= dto.reference;
              break;
            }
            case 194: {
             this.emissionFactors.values.price_lsd_sh_cal_maldives = dto.value;
               this.emissionFactors.refs.price_lsd_sh_cal_maldives= dto.reference;
              break;
            }
            case 195: {
              this.emissionFactors.values.price_lsd_sh_cal_indonesia = dto.value;
              this.emissionFactors.refs.price_lsd_sh_cal_indonesia= dto.reference;
              break;
            }
            case 196: {
              this.emissionFactors.values.price_lsd_sh_cal_india = dto.value;
             this.emissionFactors.refs.price_lsd_sh_cal_india= dto.reference;
              break;
            }
            case 197: {
           this.emissionFactors.values.price_lsd_sh_cal_bangladesh = dto.value;
              this.emissionFactors.refs.price_lsd_sh_cal_bangladesh= dto.reference;
              break;
            }
        
        
            case 86: {
              this.emissionFactors.values.price_lp95_p_sri_lanka = dto.value;
              this.emissionFactors.refs.price_lp95_p_sri_lanka= dto.reference;
              break;
            }
            case 198: {
             this.emissionFactors.values.price_lp95_p_myanmar = dto.value;
             this.emissionFactors.refs.price_lp95_p_myanmar= dto.reference;
              break;
            }
            case 199: {
              this.emissionFactors.values.price_lp95_p_maldives = dto.value;
             this.emissionFactors.refs.price_lp95_p_maldives= dto.reference;
              break;
            }
            case 200: {
              this.emissionFactors.values.price_lp95_p_indonesia = dto.value;
              this.emissionFactors.refs.price_lp95_p_indonesia= dto.reference;
              break;
            }
            case 201: {
             this.emissionFactors.values.price_lp95_p_india = dto.value;
              this.emissionFactors.refs.price_lp95_p_india= dto.reference;
              break;
            }
            case 202: {
              this.emissionFactors.values.price_lp95_p_bangladesh = dto.value;
              this.emissionFactors.refs.price_lp95_p_bangladesh= dto.reference;
              break;
            }
        
        
            case 87: {
            this.emissionFactors.values.price_lp92_p_sri_lanka = dto.value;
             this.emissionFactors.refs.price_lp92_p_sri_lanka= dto.reference;
              break;
            }
            case 203: {
              this.emissionFactors.values.price_lp92_p_myanmar = dto.value;
           this.emissionFactors.refs.price_lp92_p_myanmar= dto.reference;
              break;
            }
            case 204: {
            this.emissionFactors.values.price_lp92_p_maldives = dto.value;
             
              this.emissionFactors.refs.price_lp92_p_maldives= dto.reference;
              break;
            }
            case 205: {
              this.emissionFactors.values.price_lp92_p_indonesia = dto.value;
            this.emissionFactors.refs.price_lp92_p_indonesia= dto.reference;
              break;
            }
            case 206: {
              this.emissionFactors.values.price_lp92_p_india = dto.value;
            this.emissionFactors.refs.price_lp92_p_india= dto.reference;
              break;
            }
            case 207: {
             this.emissionFactors.values.price_lp92_p_bangladesh = dto.value;
             this.emissionFactors.refs.price_lp92_p_bangladesh= dto.reference;
              break;
            }
        
        
            case 88: {
             this.emissionFactors.values.price_lad_p_sri_lanka = dto.value;
             this.emissionFactors.refs.price_lad_p_sri_lanka= dto.reference;
              break;
            }
            case 208: {
             this.emissionFactors.values.price_lad_p_myanmar = dto.value;
             this.emissionFactors.refs.price_lad_p_myanmar= dto.reference;
              break;
            }
            case 209: {
             this.emissionFactors.values.price_lad_p_maldives = dto.value;
              this.emissionFactors.refs.price_lad_p_maldives= dto.reference;
              break;
            }
            case 210: {
              this.emissionFactors.values.price_lad_p_indonesia = dto.value;
            this.emissionFactors.refs.price_lad_p_indonesia= dto.reference;
              break;
            }
            case 211: {
             this.emissionFactors.values.price_lad_p_india = dto.value;
             this.emissionFactors.refs.price_lad_p_india= dto.reference;
              break;
            }
            case 212: {
            this.emissionFactors.values.price_lad_p_bangladesh = dto.value;
              this.emissionFactors.refs.price_lad_p_bangladesh= dto.reference;
              break;
            }
        
            case 89: {
             this.emissionFactors.values.price_lsd_p_sri_lanka = dto.value;
             this.emissionFactors.refs.price_lsd_p_sri_lanka= dto.reference;
              break;
            }
            case 213: {
             this.emissionFactors.values.price_lsd_p_myanmar = dto.value;
              this.emissionFactors.refs.price_lsd_p_myanmar= dto.reference;
              break;
            }
            case 214: {
            this.emissionFactors.values.price_lsd_p_maldives = dto.value;
             this.emissionFactors.refs.price_lsd_p_maldives= dto.reference;
              break;
            }
            case 215: {
           this.emissionFactors.values.price_lsd_p_indonesia = dto.value;
             this.emissionFactors.refs.price_lsd_p_indonesia= dto.reference;
              break;
            }
            case 216: {
            this.emissionFactors.values.price_lsd_p_india = dto.value;
              this.emissionFactors.refs.price_lsd_p_india= dto.reference;
              break;
            }
            case 217: {
             this.emissionFactors.values.price_lsd_p_bangladesh = dto.value;
             this.emissionFactors.refs.price_lsd_p_bangladesh= dto.reference;
              break;
            }
        
            case 90: {
            this.emissionFactors.values.price_lp95_p_cal_sri_lanka = dto.value;
             this.emissionFactors.refs.price_lp95_p_cal_sri_lanka= dto.reference;
              break;
            }
            case 218: {
             this.emissionFactors.values.price_lp95_p_cal_myanmar = dto.value;
             this.emissionFactors.refs.price_lp95_p_cal_myanmar= dto.reference;
              break;
            }
            case 219: {
             this.emissionFactors.values.price_lp95_p_cal_maldives = dto.value;
            this.emissionFactors.refs.price_lp95_p_cal_maldives= dto.reference;
              break;
            }
            case 220: {
          this.emissionFactors.values.price_lp95_p_cal_indonesia = dto.value;
           this.emissionFactors.refs.price_lp95_p_cal_indonesia= dto.reference;
              break;
            }
            case 221: {
            this.emissionFactors.values.price_lp95_p_cal_india = dto.value;
            this.emissionFactors.refs.price_lp95_p_cal_india= dto.reference;
              break;
            }
            case 222: {
             this.emissionFactors.values.price_lp95_p_cal_bangladesh = dto.value;
             this.emissionFactors.refs.price_lp95_p_cal_bangladesh= dto.reference;
              break;
            }
            case 91: {
             this.emissionFactors.values.price_lp92_p_cal_sri_lanka = dto.value;
           this.emissionFactors.refs.price_lp92_p_cal_sri_lanka= dto.reference;
              break;
            }
            case 223: {
            this.emissionFactors.values.price_lp92_p_cal_myanmar = dto.value;
             this.emissionFactors.refs.price_lp92_p_cal_myanmar= dto.reference;
              break;
            }
            case 224: {
             this.emissionFactors.values.price_lp92_p_cal_maldives = dto.value;
              this.emissionFactors.refs.price_lp92_p_cal_maldives= dto.reference;
              break;
            }
            case 225: {
               this.emissionFactors.values.price_lp92_p_cal_indonesia = dto.value;
             this.emissionFactors.refs.price_lp92_p_cal_indonesia= dto.reference;
              break;
            }
            case 226: {
              this.emissionFactors.values.price_lp92_p_cal_india = dto.value;
              this.emissionFactors.refs.price_lp92_p_cal_india= dto.reference;
              break;
            }
            case 227: {
              this.emissionFactors.values.price_lp92_p_cal_bangladesh = dto.value;
              this.emissionFactors.refs.price_lp92_p_cal_bangladesh= dto.reference;
              break;
            }
            case 92: {
              this.emissionFactors.values.price_lad_p_cal_sri_lanka = dto.value;
               this.emissionFactors.refs.price_lad_p_cal_sri_lanka= dto.reference;
              break;
            }
            case 228: {
             this.emissionFactors.values.price_lad_p_cal_myanmar = dto.value;
               this.emissionFactors.refs.price_lad_p_cal_myanmar= dto.reference;
              break;
            }
            case 229: {
              this.emissionFactors.values.price_lad_p_cal_maldives = dto.value;
               this.emissionFactors.refs.price_lad_p_cal_maldives= dto.reference;
              break;
            }
            case 230: {
           this.emissionFactors.values.price_lad_p_cal_indonesia = dto.value;
              this.emissionFactors.refs.price_lad_p_cal_indonesia= dto.reference;
              break;
            }
            case 231: {
             this.emissionFactors.values.price_lad_p_cal_india = dto.value;
              this.emissionFactors.refs.price_lad_p_cal_india= dto.reference;
              break;
            }
            case 232: {
             this.emissionFactors.values.price_lad_p_cal_bangladesh = dto.value;
              this.emissionFactors.refs.price_lad_p_cal_bangladesh= dto.reference;
              break;
            }
            case 93: {
            this.emissionFactors.values.price_lsd_p_cal_sri_lanka = dto.value;
             this.emissionFactors.refs.price_lsd_p_cal_sri_lanka= dto.reference;
              break;
            }
            case 233: {
           this.emissionFactors.values.price_lsd_p_cal_myanmar = dto.value;
              this.emissionFactors.refs.price_lsd_p_cal_myanmar= dto.reference;
              break;
            }
            case 234: {
             this.emissionFactors.values.price_lsd_p_cal_maldives = dto.value;
              this.emissionFactors.refs.price_lsd_p_cal_maldives= dto.reference;
              break;
            }
            case 235: {
          this.emissionFactors.values.price_lsd_p_cal_indonesia = dto.value;
              this.emissionFactors.refs.price_lsd_p_cal_indonesia= dto.reference;
              break;
            }
            case 236: {
             this.emissionFactors.values.price_lsd_p_cal_india = dto.value;
              this.emissionFactors.refs.price_lsd_p_cal_india= dto.reference;
              break;
            }
            case 237: {
              this.emissionFactors.values.price_lsd_p_cal_bangladesh = dto.value;
               this.emissionFactors.refs.price_lsd_p_cal_bangladesh= dto.reference;
              break;
            }
              
                case 94: {
                   this.emissionFactors.values.price_lp95_pp_sri_lanka = dto.value;
                  this.emissionFactors.refs.price_lp95_pp_sri_lanka= dto.reference;
                  break;
                }
                case 313: {
                   this.emissionFactors.values.price_lp95_pp_myanmar = dto.value;
                  this.emissionFactors.refs.price_lp95_pp_myanmar= dto.reference;
                  break;
                }
                case 314: {
                this.emissionFactors.values.price_lp95_pp_maldives = dto.value;
                 this.emissionFactors.refs.price_lp95_pp_maldives= dto.reference;
                  break;
                }
                case 315: {
                 this.emissionFactors.values.price_lp95_pp_indonesia = dto.value;
                  this.emissionFactors.refs.price_lp95_pp_indonesia= dto.reference;
                  break;
                }
                case 316: {
                 this.emissionFactors.values.price_lp95_pp_india = dto.value;
                  this.emissionFactors.refs.price_lp95_pp_india= dto.reference;
                  break;
                }
                case 317: {
                   this.emissionFactors.values.price_lp95_pp_bangladesh = dto.value;
                  this.emissionFactors.refs.price_lp95_pp_bangladesh= dto.reference;
                  break;
                }
                case 95: {
                 this.emissionFactors.values.price_lp92_pp_sri_lanka = dto.value;
                 
                this.emissionFactors.refs.price_lp92_pp_sri_lanka= dto.reference;
                  break;
                }
                case 318: {
                 this.emissionFactors.values.price_lp92_pp_myanmar = dto.value;
                 this.emissionFactors.refs.price_lp92_pp_myanmar= dto.reference;
                  break;
                }
                case 319: {
                  this.emissionFactors.values.price_lp92_pp_maldives = dto.value;
                  this.emissionFactors.refs.price_lp92_pp_maldives= dto.reference;
                  break;
                }
                case 320: {
                  this.emissionFactors.values.price_lp92_pp_indonesia = dto.value;
                 this.emissionFactors.refs.price_lp92_pp_indonesia= dto.reference;
                  break;
                }
                case 321: {
                  this.emissionFactors.values.price_lp92_pp_india = dto.value;
                 this.emissionFactors.refs.price_lp92_pp_india= dto.reference;
                  break;
                }
                case 322: {
                 this.emissionFactors.values.price_lp92_pp_bangladesh = dto.value;
                  this.emissionFactors.refs.price_lp92_pp_bangladesh= dto.reference;
                  break;
                }
                case 96: {
                  this.emissionFactors.values.price_lad_pp_sri_lanka = dto.value;
                  this.emissionFactors.refs.price_lad_pp_sri_lanka= dto.reference;
                  break;
                }
                case 323: {
                   this.emissionFactors.values.price_lad_pp_myanmar = dto.value;
               this.emissionFactors.refs.price_lad_pp_myanmar= dto.reference;
                  break;
                }
                case 324: {
                  this.emissionFactors.values.price_lad_pp_maldives = dto.value;
                  this.emissionFactors.refs.price_lad_pp_maldives= dto.reference;
                  break;
                }
                case 325: {
                 this.emissionFactors.values.price_lad_pp_indonesia = dto.value;
                this.emissionFactors.refs.price_lad_pp_indonesia= dto.reference;
                  break;
                }
                case 326: {
                  this.emissionFactors.values.price_lad_pp_india = dto.value;
                   this.emissionFactors.refs.price_lad_pp_india= dto.reference;
                  break;
                }
                case 327: {
                 this.emissionFactors.values.price_lad_pp_bangladesh = dto.value;
                 this.emissionFactors.refs.price_lad_pp_bangladesh= dto.reference;
                  break;
                }
                case 97: {
                  this.emissionFactors.values.price_lsd_pp_sri_lanka = dto.value;
                  this.emissionFactors.refs.price_lsd_pp_sri_lanka= dto.reference;
                  break;
                }
                case 328: {
                 this.emissionFactors.values.price_lsd_pp_myanmar = dto.value;
              this.emissionFactors.refs.price_lsd_pp_myanmar= dto.reference;
                  break;
                }
                case 329: {
                  this.emissionFactors.values.price_lsd_pp_maldives = dto.value;
                  this.emissionFactors.refs.price_lsd_pp_maldives= dto.reference;
                  break;
                }
                case 330: {
                  this.emissionFactors.values.price_lsd_pp_indonesia = dto.value;
                   this.emissionFactors.refs.price_lsd_pp_indonesia= dto.reference;
                  break;
                }
                case 331: {
                this.emissionFactors.values.price_lsd_pp_india = dto.value;
                 this.emissionFactors.refs.price_lsd_pp_india= dto.reference;
                  break;
                }
                case 332: {
                  this.emissionFactors.values.price_lsd_pp_bangladesh = dto.value;
                  this.emissionFactors.refs.price_lsd_pp_bangladesh= dto.reference;
                  break;
                }
            
                case 98: {
                 this.emissionFactors.values.price_lp95_pp_cal_sri_lanka = dto.value;
                 this.emissionFactors.refs.price_lp95_pp_cal_sri_lanka= dto.reference;
                  break;
                }
                case 333: {
                  this.emissionFactors.values.price_lp95_pp_cal_myanmar = dto.value;
                 this.emissionFactors.refs.price_lp95_pp_cal_myanmar= dto.reference;
                  break;
                }
                case 334: {
                 this.emissionFactors.values.price_lp95_pp_cal_maldives = dto.value;
                  this.emissionFactors.refs.price_lp95_pp_cal_maldives= dto.reference;
                  break;
                }
                case 335: {
                  this.emissionFactors.values.price_lp95_pp_cal_indonesia = dto.value;
                 this.emissionFactors.refs.price_lp95_pp_cal_indonesia= dto.reference;
                  break;
                }
                case 336: {
                 this.emissionFactors.values.price_lp95_pp_cal_india = dto.value;
                  this.emissionFactors.refs.price_lp95_pp_cal_india= dto.reference;
                  break;
                }
                case 337: {
                  this.emissionFactors.values.price_lp95_pp_cal_bangladesh = dto.value;
                 this.emissionFactors.refs.price_lp95_pp_cal_bangladesh= dto.reference;
                  break;
                }
                case 99: {
                   this.emissionFactors.values.price_lp92_pp_cal_sri_lanka = dto.value;
                   this.emissionFactors.refs.price_lp92_pp_cal_sri_lanka= dto.reference;
                  break;
                }
                case 338: {
                  this.emissionFactors.values.price_lp92_pp_cal_myanmar = dto.value;
                 this.emissionFactors.refs.price_lp92_pp_cal_myanmar= dto.reference;
                  break;
                }
                case 339: {
                 this.emissionFactors.values.price_lp92_pp_cal_maldives = dto.value;
                  this.emissionFactors.refs.price_lp92_pp_cal_maldives= dto.reference;
                  break;
                }
                case 340: {
                  this.emissionFactors.values.price_lp92_pp_cal_indonesia = dto.value;
                 this.emissionFactors.refs.price_lp92_pp_cal_indonesia= dto.reference;
                  break;
                }
                case 341: {
                  this.emissionFactors.values.price_lp92_pp_cal_india = dto.value;
                  this.emissionFactors.refs.price_lp92_pp_cal_india= dto.reference;
                  break;
                }
                case 342: {
                  this.emissionFactors.values.price_lp92_pp_cal_bangladesh = dto.value;
                 
                  this.emissionFactors.refs.price_lp92_pp_cal_bangladesh= dto.reference;
                  break;
                }
                case 100: {
                  this.emissionFactors.values.price_lad_pp_cal_sri_lanka = dto.value;
                 this.emissionFactors.refs.price_lad_pp_cal_sri_lanka= dto.reference;
                  break;
                }
                case 343: {
                   this.emissionFactors.values.price_lad_pp_cal_myanmar = dto.value;
                 this.emissionFactors.refs.price_lad_pp_cal_myanmar= dto.reference;
                  break;
                }
                case 344: {
                  this.emissionFactors.values.price_lad_pp_cal_maldives = dto.value;
                   this.emissionFactors.refs.price_lad_pp_cal_maldives= dto.reference;
                  break;
                }
                case 345: {
                 this.emissionFactors.values.price_lad_pp_cal_indonesia = dto.value;
                  this.emissionFactors.refs.price_lad_pp_cal_indonesia= dto.reference;
                  break;
                }
                case 346: {
                  this.emissionFactors.values.price_lad_pp_cal_india = dto.value;
                  this.emissionFactors.refs.price_lad_pp_cal_india= dto.reference;
                  break;
                }
                case 347: {
                 this.emissionFactors.values.price_lad_pp_cal_bangladesh = dto.value;
                 this.emissionFactors.refs.price_lad_pp_cal_bangladesh= dto.reference;
                  break;
                }
                case 101: {
                  this.emissionFactors.values.price_lsd_pp_cal_sri_lanka = dto.value;
                  this.emissionFactors.refs.price_lsd_pp_cal_sri_lanka= dto.reference;
                  break;
                }
                case 348: {
                 this.emissionFactors.values.price_lsd_pp_cal_myanmar = dto.value;
                this.emissionFactors.refs.price_lsd_pp_cal_myanmar= dto.reference;
                  break;
                }
                case 349: {
                 this.emissionFactors.values.price_lsd_pp_cal_maldives = dto.value;
                 this.emissionFactors.refs.price_lsd_pp_cal_maldives= dto.reference;
                  break;
                }
                case 350: {
                 this.emissionFactors.values.price_lsd_pp_cal_indonesia = dto.value;
                 this.emissionFactors.refs.price_lsd_pp_cal_indonesia= dto.reference;
                  break;
                }
                case 351: {
                  this.emissionFactors.values.price_lsd_pp_cal_india = dto.value;
                 this.emissionFactors.refs.price_lsd_pp_cal_india= dto.reference;
                  break;
                }
                case 352: {
                  this.emissionFactors.values.price_lsd_pp_cal_bangladesh = dto.value;
                 this.emissionFactors.refs.price_lsd_pp_cal_bangladesh= dto.reference;
                  break;
                }

            }
          })
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })


  }


}
