import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from "@angular/material";
import {MassterDataService} from "../../../../../@core/service/masster-data.service";
import {BackendService} from "../../../../../@core/rest/bo_service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";

@Component({
  selector: 'app-update-mun-water',
  templateUrl: './update-mun-water.component.html',
  styleUrls: ['./update-mun-water.component.scss']
})
export class UpdateMunWaterComponent implements OnInit {
  private isSaved = false;

  public jsonBody = {
    MUNICIPAL_WATER_ENTRY_ID:  -1,
    BRANCH_ID: undefined,
    COMPANY_ID: undefined, // todo:
    UNITS: undefined, // todo
    YEAR: undefined,
    MONTH: undefined,
    WATER_CONSUMPTION: undefined,
    // NUMS_OF_METERS: numOfMeters,
    METER_NO: undefined,
    company: undefined,
    branch: undefined,
    mon: undefined,
  }

  public companies = [];
  public branches = [];
  public months = [];
  public years = [];
  public units = [{ id: 1, name: 'm3'}, { id: 6, name: 'LKR'}];



  constructor( public dialogRef: MatDialogRef<UpdateMunWaterComponent>,
               private masterData: MassterDataService,
               private boService: BackendService,
               private toastSerivce: NbToastrService,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    // console.log(this.popupData)
    this.init(this.popupData.data, this.popupData.isNew);
    if(popupData.ismulti){
      this.onClickSave();
      this.closeDialog
    }

  }

  private init(data:any, isNew:boolean) {
    this.masterData.loadCompaninesForDataEnry().subscribe(d => {
      this.companies = d;
    })
    this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
      this.years = d;
    })
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });
    //----------------------------branch search ---------------------
    this.branches.push({
      id: 0, name: 'Search Branch'
    })
    //---------------------------------------------------------------
    if (!isNew) {
      this.jsonBody.MUNICIPAL_WATER_ENTRY_ID = data.id ;
      this.jsonBody.COMPANY_ID = data.idCom ;
      this.jsonBody.BRANCH_ID = data.idBran ;
      this.jsonBody.METER_NO = data.meterNo ;
      this.jsonBody.WATER_CONSUMPTION = data.consumption ;
      this.jsonBody.YEAR = data.year ;
      this.jsonBody.MONTH = data.idMon;
      this.jsonBody.UNITS = data.idUnit;
      // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => this.branches = d)
      this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
        // console.log(d);
        this.years = d;
      })

      //----------------branch search-------------------------
      this.branches.push({
        id: data.idBran, name: data.branch
      })
      this.selectedComId =  this.jsonBody.COMPANY_ID;

      // ----------------------------------------------------
    }
  }

  private validateEntry(onEdit: boolean): boolean {
    // if (this.jsonBody.BRANCH_ID === undefined || this.jsonBody.BRANCH_ID === '' || this.jsonBody.BRANCH_ID === 0 ){
    //   return false;
    // }
    if (onEdit && (this.jsonBody.COMPANY_ID === undefined || this.jsonBody.COMPANY_ID === '') ){
      return false;
    }
    if (this.jsonBody.METER_NO === undefined || this.jsonBody.METER_NO === '' ){
      return false;
    }
    if (this.jsonBody.WATER_CONSUMPTION === undefined || this.jsonBody.WATER_CONSUMPTION === '' ){
      return false;
    }
    if (this.jsonBody.UNITS === undefined || this.jsonBody.UNITS === '' ){
      return false;
    }
    if (this.jsonBody.YEAR === undefined || this.jsonBody.YEAR == "") {
      //show snack bar
      return false;
    }
    if (this.jsonBody.MONTH === undefined || this.jsonBody.MONTH < 0) {
      //show snack bar
      return false;
    }
    if (onEdit && (this.jsonBody.MUNICIPAL_WATER_ENTRY_ID === undefined || this.jsonBody.MUNICIPAL_WATER_ENTRY_ID <= 0)) {
      //show snack bar
      return false;
    }
    return true;
  }

  ngOnInit() {
  }

  public selectedComId = 0;
  onChangeCompany(value: any) {
    this.selectedComId = value;
    this.branches = [];
    this.branches.push({
      id: 0, name: 'Search Branch'
    })
    this.jsonBody.company = this.companies.filter(c => c.id === value)[0].name
    // this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)
    this.masterData.getYearsForCompany(value).subscribe(d => {
      // console.log(d);
      this.years = d;
    })
  }

  closeDialog() {
    this.dialogRef.close(this.isSaved);

  }

  public onClickSave() {
    // update
    if (!this.popupData.isNew) {
        if (this.validateEntry(true)) {
          this.boService.sendRequestToBackend(
            RequestGroup.Emission,
            RequestType.ManageMunWaterEntry,
            { DATA: this.jsonBody}
          ).then( data => {
            if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
              this.isSaved = true;
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            } else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          })
        } else {
          this.toastSerivce.show('', 'Fill empty fields', {
            status: 'warning',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }

    }
    // new
    else {
      if (this.validateEntry( false)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageMunWaterEntry,
          { DATA: this.jsonBody}
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }

  }

  onSearchBranch(value: any) {
    if (value === '') {
      return;
    }
    let queryReq = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        name: { value: value , type: 4, col: 3},

      },
    }
    if (this.selectedComId != 0) {
      queryReq.FILTER_MODEL[ 'companyId'] = { value: this.selectedComId, type: 1, col: 1}
    }
    this.branches = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      queryReq
    ).then(data => {
      // console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.branches = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.branches.unshift({id: val.id, name: val.name});
            }
          });
          if (this.branches.length === 0){
            this.branches.push({id: 0, name: 'No Matches Found'})
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  onChangeBranch($event: MatSelectChange) {
    this.jsonBody.branch = this.branches.filter(b => b.id === $event)[0].name;
  }

  onChangeMonth($event: MatSelectChange) {
    this.jsonBody.mon = this.months.filter(m => m.id === $event.value)[0].name;
  }


}
