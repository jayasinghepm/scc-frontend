import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFurnaceOilComponent } from './admin-furnace-oil.component';

describe('AdminFurnaceOilComponent', () => {
  let component: AdminFurnaceOilComponent;
  let fixture: ComponentFixture<AdminFurnaceOilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFurnaceOilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFurnaceOilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
