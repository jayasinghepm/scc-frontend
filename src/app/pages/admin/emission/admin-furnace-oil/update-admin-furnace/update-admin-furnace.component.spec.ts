import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAdminFurnaceComponent } from './update-admin-furnace.component';

describe('UpdateAdminFurnaceComponent', () => {
  let component: UpdateAdminFurnaceComponent;
  let fixture: ComponentFixture<UpdateAdminFurnaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAdminFurnaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAdminFurnaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
