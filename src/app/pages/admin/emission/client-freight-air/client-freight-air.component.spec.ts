import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ClientFreightAirComponent } from './client-freight-air.component';


describe('ClientAirTravelComponent', () => {
  let component: ClientFreightAirComponent;
  let fixture: ComponentFixture<ClientFreightAirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientFreightAirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientFreightAirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
