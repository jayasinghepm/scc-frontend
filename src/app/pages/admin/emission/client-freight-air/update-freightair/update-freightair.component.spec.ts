import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {  UpdateFreightAirComponent } from './update-freightair.component';

describe('UpdateAirtravelComponent', () => {
  let component: UpdateFreightAirComponent;
  let fixture: ComponentFixture<UpdateFreightAirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFreightAirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFreightAirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
