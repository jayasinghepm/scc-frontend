import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUpdateSeaAirFreightComponent } from './admin-update-sea-air-freight.component';

describe('AdminUpdateSeaAirFreightComponent', () => {
  let component: AdminUpdateSeaAirFreightComponent;
  let fixture: ComponentFixture<AdminUpdateSeaAirFreightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUpdateSeaAirFreightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUpdateSeaAirFreightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
