import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAdminSawDustTransComponent } from './update-admin-saw-dust-trans.component';

describe('UpdateAdminSawDustTransComponent', () => {
  let component: UpdateAdminSawDustTransComponent;
  let fixture: ComponentFixture<UpdateAdminSawDustTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAdminSawDustTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAdminSawDustTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
