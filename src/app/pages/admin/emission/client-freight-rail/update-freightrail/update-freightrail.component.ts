import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from "@angular/material";
import {MassterDataService} from "../../../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {BackendService} from "../../../../../@core/rest/bo_service";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import {MatCheckboxChange} from "@angular/material/typings/esm5/checkbox";
import { UserState } from 'src/app/@core/auth/UserState';

@Component({
  selector: 'app-update-freightrail',
  templateUrl: './update-freightrail.component.html',
  styleUrls: ['./update-freightrail.component.scss']
})
export class UpdateFreightRailComponent implements OnInit {

  private isSaved = false;
  public twoWay = false;
  public isnotAvailable = false;
  public isPaid = false;
  public jsonBody = {
    entryId: -1,
    branchId: undefined,
    companyId: undefined, // todo:
    EMP_ID: undefined, // todo
    year: undefined,
    month: undefined,

    DEP_AIRPORT: undefined,
    DEP_COUNTRY: undefined,
    TRANSIST_1: undefined,
    TRANSIST_2: undefined,
    TRANSIST_3: undefined,
    AIR_TRAVEL_WAY: undefined,
    DEST_AIRPORT: undefined,
    DEST_COUNTRY: undefined,
    ACTIVITY:undefined,
    TYPE:undefined,
    SIZE:undefined,

    company: undefined,
    branch: undefined,
    mon: undefined,
    deptCountry: undefined,
    destCountry: undefined,
    destPort: undefined,
    deptPort: undefined,
    t1Port: undefined,
    t2Port: undefined,
    t3Port: undefined,


   

    ownerShip:undefined,
    weight:undefined,
    totalDistance:undefined,
    distanceUnit:undefined,
    vehicleNumber:undefined,
    method:undefined,
    freightMode:undefined,   
    activity: undefined,
    type:undefined,
    size:undefined,
    isTwoWay: undefined,
    isNotAvailable:undefined,
    isFuelPaiByCompany:undefined,

    fuelConsumption:undefined,
    fuelDensity:undefined,
    ncv:undefined,
    fuelType:undefined,
    vessel:undefined,


  
  }

  public countries = [];
  public airports = [];
  public months = [];
  public years = [];
  public branches = [];
  public companies = [];
  public seatClasses = [];
  public freightTypes = [];
  public owenership = [];
  public method = [];
  public activities = [];
  public types = [];
  public sizes = [];
  public fuels = [];
  public units = [];




  public countriesBackup = [];
  private portsAdded = [];
  private countyAdded =  [];


  constructor(
                public dialogRef: MatDialogRef<UpdateFreightRailComponent>,
                private masterData: MassterDataService,
                private toastSerivce: NbToastrService,
                private boService: BackendService,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any,
                ) {
    // console.log(this.popupData)
    this.init(this.popupData.data, this.popupData.isNew);
  }

  private init(data:any, isNew: boolean) {
    this.masterData.loadCompaninesForDataEnry().subscribe(d => {
      this.companies = d;
      if(UserState.getInstance().adminId == 3){
        this.companies = this.companies.slice(1,5);


      }
    })
    this.masterData.getYearsForCompany(this.jsonBody.companyId).subscribe(d => {
      // console.log(d);
      this.years = d;
    })
    this.masterData.getAirportsFull().subscribe(data => {
      // @ts-ignore
      this.airports.push(...data);
    });
    this.masterData.getCountriesFull().subscribe(data => {
      // @ts-ignore
      this.countries  = data;
     this.countriesBackup = data;
    });
    this.masterData.getSeatClassesFull().subscribe(data => {
     this.seatClasses = data;
    });
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });

    this.masterData.getFreightTypesFull().subscribe(data => {
      this.freightTypes = data;
    });

    this.masterData.getVehicleCategoriesFull().subscribe(d => {
      this.owenership = d;
    })

    this.masterData.getMethod().subscribe(d => {
      this.method= d;
    });
    
    this.masterData.getActivity().subscribe(d => {
      this.activities= d;
    })
    this.masterData.getFuelTypesVehicleFull().subscribe(d => {
      this.fuels = d;
    })

    this.masterData.getUnitDistance().subscribe(d => {
      this.units = d;
    })

   
    //----------------------------branch search ---------------------
    this.branches.push({
      id: 0, name: 'Search Branch'
    })
    //---------------------------------------------------------------
    if (!isNew) 
    
    {
      this.jsonBody.entryId = data.id;
      this.jsonBody.branchId = data.idBran ;
      this.jsonBody.companyId = data.idCom ;
      this.jsonBody.year = data.year;
      this.jsonBody.month = data.idMon ;
      this.jsonBody.DEP_AIRPORT = data. idDepPort ;
      this.jsonBody.DEP_COUNTRY = data.idDepCou ;
      this.jsonBody.DEST_AIRPORT = data.idDesPort ;
      this.jsonBody.DEST_COUNTRY = data.idDesCou ;
      this.jsonBody.TRANSIST_1 = data.idTran1 ;
      this.jsonBody.TRANSIST_2 = data.idTrans2 ;
      this.jsonBody.TRANSIST_3 = data.idTrans3 ;
      this.jsonBody.AIR_TRAVEL_WAY = data.isTwoWay === 'No'? 1: 2 ;
      this.twoWay = this.jsonBody.AIR_TRAVEL_WAY == 2? true: false;
      // this.jsonBody.SEAT_CLASS = data.idSeat ;
      // this.jsonBody.EMISSION_INFO.tco2  = data.emissionDetails.tco2 ;
      // this.jsonBody.EMISSION_INFO.co2  = data.emissionDetails.co2;
      // this.jsonBody.EMISSION_INFO.ch4  = data.emissionDetails.ch4;
      // this.jsonBody.EMISSION_INFO.n2o  = data.emissionDetails.n2o;
      // this.jsonBody.EMISSION_INFO.km  = data.emissionDetails.km;
      // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => this.branches = d)
      this.masterData.getYearsForCompany(this.jsonBody.companyId).subscribe(d => {
        // console.log(d);
        this.years = d;
      })
      //----------------branch search-------------------------
      this.branches.push({
        id: data.idBran, name: data.branch
      })
      this.selectedComId =  this.jsonBody.companyId;

      // ----------------------------------------------------
      this.jsonBody.t1Port = data.trans1;
      this.jsonBody.t2Port = data.trans2;
      this.jsonBody.t3Port = data.trans3;
      this.jsonBody.destCountry = data.destCoun;
      this.jsonBody.deptCountry = data.depCoun;
      this.jsonBody.deptPort = data.depPort;
      this.jsonBody.destPort = data.destPort;


      this.portsAdded.push(...[
        { id: this.jsonBody.DEP_AIRPORT, name: this.jsonBody.deptPort},
        { id: this.jsonBody.DEST_AIRPORT, name: this.jsonBody.destPort},
      ])
      if (this.jsonBody.t1Port !== undefined) {
        this.portsAdded.push({ id: this.jsonBody.TRANSIST_1 , name: this.jsonBody.t1Port})
      }
      if (this.jsonBody.t2Port !== undefined) {
        this.portsAdded.push({ id: this.jsonBody.TRANSIST_2, name: this.jsonBody.t2Port})
      }
      if (this.jsonBody.t3Port !== undefined) {
        this.portsAdded.push({ id: this.jsonBody.TRANSIST_3, name: this.jsonBody.t3Port})
      }
      this.airports.push(...this.portsAdded);

      // console.log(this.jsonBody);
      // console.log(this.countries);
      // console.log(this.airports);
    }


  }

  private validateEntry(onEdit: boolean): boolean {
    if(this.twoWay) { this.jsonBody.AIR_TRAVEL_WAY = 2} else { this.jsonBody.AIR_TRAVEL_WAY = 1;}
    // if (this.jsonBody.BRANCH_ID === undefined || this.jsonBody.BRANCH_ID == ""  || this.jsonBody.BRANCH_ID === 0) {
    //   //show snack bar
    //   return false;
    // }

    if (this.jsonBody.AIR_TRAVEL_WAY === undefined || this.jsonBody.AIR_TRAVEL_WAY == "") {
      //show snack bar
      return false;
    }
    if (this.jsonBody.DEP_COUNTRY === undefined || this.jsonBody.DEP_COUNTRY == "") {
      //show snack bar
      return false;
    }

    if (this.jsonBody.DEST_COUNTRY === undefined || this.jsonBody.DEST_COUNTRY == "") {
      //show snack bar
      return false;
    }
    // if (this.jsonBody.NO_OF_EMPLOYEES === undefined || this.jsonBody.NO_OF_EMPLOYEES == "") {
    //   //show snack bar
    //   return false;
    // }

    // if (this.jsonBody.SEAT_CLASS === undefined || this.jsonBody.SEAT_CLASS == "") {
    //   //show snack bar
    //   return false;
    // }
    if(this.jsonBody.TRANSIST_1 === -1) {
      return false;
    }
    if(this.jsonBody.TRANSIST_2 === -1) {
      return false;
    }
    if(this.jsonBody.TRANSIST_3 === -1) {
      return false;
    }
    if (this.jsonBody.DEP_AIRPORT === undefined || this.jsonBody.DEP_AIRPORT == "" || this.jsonBody.DEP_AIRPORT === -1) {
      //show snack bar
      return false;
    }
    if (this.jsonBody.DEST_AIRPORT === undefined || this.jsonBody.DEST_AIRPORT == "" || this.jsonBody.DEST_AIRPORT === -1) {
      //show snack bar
      return false;
    }
    // if (this.jsonBody.transit2 === undefined || this.jsonBody.transit2 == "") {
    //   //show snack bar
    //   return false;
    // }
    // if (this.jsonBody.transit3 === undefined || this.jsonBody.transit3 == "") {
    //   //show snack bar
    //   return false;
    // }
    if (this.jsonBody.year === undefined || this.jsonBody.year == "") {
      //show snack bar
      return false;
    }
    if (this.jsonBody.month === undefined || this.jsonBody.month < 0 ){
      //show snack bar
      return false;
    }
    // if (this.jsonBody.transit1 === undefined || this.jsonBody.transit1 == "") {
    //   //show snack bar
    //   return false;
    // }
    if (onEdit && (this.jsonBody.entryId === undefined || this.jsonBody.entryId <=0)) {
      //show snack bar
      return false;
    }
    if (onEdit && (this.jsonBody.companyId === undefined || this.jsonBody.companyId <= 0) ){
      return false;
    }
    return true;

  }


  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close(this.isSaved);

  }

  public selectedComId = 0;
  onChangeCompany(value: any) {
    this.selectedComId = value;
    this.branches = [];
    this.branches.push({
      id: 0, name: 'Search Branch'
    })
    this.jsonBody.company = this.companies.filter(d => {return d.id === value})[0].name;
    // this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)

    this.masterData.getYearsForCompany(value).subscribe(d => {
      this.years = d;
    })
  }

  onChangeActivity(value: any) {
     console.log("val---",value);
    this.jsonBody.activity = this.activities.filter(d => d.id === value)[0].name;
    this.masterData.getType(value).subscribe(d => {
      this.types = d;

    })
  }

  onChangeType(value: any) {
    console.log("val---",value);
   this.jsonBody.type = this.types.filter(d => d.id === value)[0].name;
   this.masterData.getSize(value).subscribe(d => {
     this.sizes = d;

   })
 }

  public onClickSave(){

    console.log("sendingBody--",this.jsonBody)
    // update
    if (!this.popupData.isNew) {

        if (this.validateEntry( true)) {
            this.boService.sendRequestToBackend(
              RequestGroup.Emission,
               RequestType.ManageAirtravelEntry,
              { DATA: this.jsonBody}
               ).then( data => {
                if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
                  this.isSaved = true;
                  this.toastSerivce.show('', 'Saved data successfully.', {
                    status: 'success',
                    destroyByClick: true,
                    duration: 2000,
                    hasIcon: false,
                    position: NbGlobalPhysicalPosition.TOP_RIGHT,
                    preventDuplicates: true,
                  })
                } else {
                  this.toastSerivce.show('', 'Error in saving data.', {
                    status: 'danger',
                    destroyByClick: true,
                    duration: 2000,
                    hasIcon: false,
                    position: NbGlobalPhysicalPosition.TOP_RIGHT,
                    preventDuplicates: true,
                  })
                }
               })
        } else {
          this.toastSerivce.show('', 'Fill empty fields', {
            status: 'warning',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
    }
    // new
    else {
      console.log("add neww")
      if (this.validateEntry( false)) {
        console.log("sending Jsn after validate--",this.jsonBody)

        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
           RequestType.ManageAirtravelEntry,
          { DATA: this.jsonBody}
           ).then( data => {
            if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
              this.isSaved = true;
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            } else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
           })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }
  }



  onSearchAirport(value: any) {
    // console.log(value)
    if (value === '') {
      // console.log(true)
      return;
    }
    this.airports = []
    this.airports.push(...this.portsAdded)
    this.boService.sendRequestToBackend(
      RequestGroup.MasterData,
      RequestType.ListAirports,
      {
        PAGE_NUMBER: -1,
        FILTER_MODEL: {
          searchName: { value: value , type: 4, col: 3}
        },
      }
    ).then(data => {
      // console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
                      if (val != undefined) {
                        this.airports.unshift({id: val.id, name: val.name});
                      }
                    });
          if (this.airports.length === 0) {
            if (this.portsAdded.length === 0) {
              this.airports.push({
                id: -1, name: 'No Matches Found'
              })
            }else {
              this.airports.push(...JSON.parse(JSON.stringify(this.portsAdded)));
            }
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  onChangePort($event: MatSelectChange, field: number) {
    switch(field) {
      case 1: {
        // this.portsAdded .push({id: this.jsonBody.DEP_AIRPORT , name:$event.value })
        this.jsonBody.DEP_AIRPORT = this.airports.filter(a => a.name ===  $event.value)[0].id;
        this.portsAdded .push({id: this.jsonBody.DEP_AIRPORT , name:$event.value })
        // this.jsonBody.depPort = $event.value.name;
        break;
      }
      case 2: {
        this.jsonBody.DEST_AIRPORT = this.airports.filter(a => a.name ===  $event.value)[0].id;
        // this.jsonBody.desPort  = $event.value.name;
        this.portsAdded .push({id: this.jsonBody.DEST_AIRPORT , name:$event.value })
        break;
      }
      case 3: {
        this.jsonBody.TRANSIST_1 = this.airports.filter(a => a.name ===  $event.value)[0].id;
        // this.jsonBody.trans1 = $event.value.name;
        this.portsAdded .push({id: this.jsonBody.TRANSIST_1 , name:$event.value })
        break;
      }
      case 4: {
        this.jsonBody.TRANSIST_2 = this.airports.filter(a => a.name ===  $event.value)[0].id;
        // this.jsonBody.trans2 = $event.value.name;
        this.portsAdded .push({id: this.jsonBody.TRANSIST_2 , name:$event.value })
        break;
      }
      case 5: {
        this.jsonBody.TRANSIST_3 = this.airports.filter(a => a.name ===  $event.value)[0].id;
        // this.jsonBody.trans3 = $event.value.name;
        this.portsAdded .push({id: this.jsonBody.TRANSIST_3 , name:$event.value })
        break;
      }
    }
    // console.log(this.jsonBody)
  }


  onSearchBranch(value: any) {
    if (value === '') {
      return;
    }
    let queryReq = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        name: { value: value , type: 4, col: 3},

      },
    }
    if (this.selectedComId != 0) {
      queryReq.FILTER_MODEL[ 'companyId'] = { value: this.selectedComId, type: 1, col: 1}
    }
    this.branches = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      queryReq
    ).then(data => {
      // console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.branches = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.branches.unshift({id: val.id, name: val.name});
            }
          });
          if (this.branches.length === 0){
            this.branches.push({id: 0, name: 'No Matches Found'})
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  onChangeBranch($event: MatSelectChange) {
    this.jsonBody.branch = this.branches.filter(b => b.id === $event)[0].name;

  }

  onChangeMonth($event: MatSelectChange) {
    this.jsonBody.mon = this.months.filter(m => m.id === $event.value)[0].name;
  }

  public country;
  onSearchCountry(value: any) {
    if (value === '') {
      this.countries = JSON.parse(JSON.stringify(this.countriesBackup));
    }
    this.countries = [];
    this.countries.push(... JSON.parse(JSON.stringify(this.countyAdded)))
    this.countries.push(...JSON.parse(JSON.stringify(this.countriesBackup.filter(c => c.name.toLowerCase().indexOf(value.toLowerCase()) > -1))));
    if (this.countries.length === 0) {
      this.countries = JSON.parse(JSON.stringify(this.countriesBackup));
      // this.country = ''
    }
    // console.log(this.countyAdded)
  }
  onChangeCountry($event: MatSelectChange, field: number) {
    switch(field) {
      case 1: {
        this.jsonBody.DEP_COUNTRY = this.countriesBackup.filter(c => c.name === $event.value)[0].id;
        // this.jsonBody.depCoun = $event.value.name;
        this.countyAdded.push({id: this.jsonBody.DEP_COUNTRY, name: $event.value});
        break;
      }
      case 2: {
        this.jsonBody.DEST_COUNTRY = this.countriesBackup.filter(c => c.name === $event.value)[0].id;
        // this.jsonBody.destCoun = $event.value.name;
        this.countyAdded.push({id: this.jsonBody.DEST_COUNTRY, name: $event.value});
        break;
      }
    }
  }

  // onChangeSeat($event: MatSelectChange) {
  //   this.jsonBody.seat = this.seatClasses.filter(c => c.id == $event.value)[0].name;
  // }
  onChangeWay($event: MatCheckboxChange) {
    this.jsonBody.isTwoWay = $event.checked ? true:false;//onChangePaid
  }

  onChangePaid($event: MatCheckboxChange) {
    this.jsonBody.isFuelPaiByCompany = $event.checked ? true:false;//onChangePaid
  }
  onChangeAvailabe($event: MatCheckboxChange) {
    this.jsonBody.isNotAvailable = $event.checked ? true:false;//onChangePaid
  }


  onCloseSearch($event: KeyboardEvent) {
    this.countries = JSON.parse(JSON.stringify(this.countriesBackup));
  }

  onSearchPortClose($event: KeyboardEvent) {
    if (this.airports.length === 0) {
      this.airports.push(...this.portsAdded);
    }
  }

  onChangeSize($event: MatSelectChange) {
    this.jsonBody.size = this.sizes.filter(w => w.id === $event.value)[0].name;
  }
}
