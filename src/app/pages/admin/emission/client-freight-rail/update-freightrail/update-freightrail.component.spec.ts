import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UpdateFreightRailComponent } from './update-freightrail.component';


describe('UpdateAirtravelComponent', () => {
  let component: UpdateFreightRailComponent;
  let fixture: ComponentFixture<UpdateFreightRailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFreightRailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFreightRailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
