import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ClientFreightRailComponent } from './client-freight-rail.component';


describe('ClientAirTravelComponent', () => {
  let component: ClientFreightRailComponent;
  let fixture: ComponentFixture<ClientFreightRailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientFreightRailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientFreightRailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
