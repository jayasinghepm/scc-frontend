import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLorryTransComponent } from './admin-lorry-trans.component';

describe('AdminLorryTransComponent', () => {
  let component: AdminLorryTransComponent;
  let fixture: ComponentFixture<AdminLorryTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminLorryTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLorryTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
