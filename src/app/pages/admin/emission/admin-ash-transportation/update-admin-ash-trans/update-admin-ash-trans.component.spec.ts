import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAdminAshTransComponent } from './update-admin-ash-trans.component';

describe('UpdateAdminAshTransComponent', () => {
  let component: UpdateAdminAshTransComponent;
  let fixture: ComponentFixture<UpdateAdminAshTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAdminAshTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAdminAshTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
