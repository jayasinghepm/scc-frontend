import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {MassterDataService} from "../../../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {BackendService} from "../../../../../@core/rest/bo_service";
import {UserState} from "../../../../../@core/auth/UserState";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import {MatSelectChange} from "@angular/material/select";

@Component({
  selector: 'app-update-admin-ash-trans',
  templateUrl: './update-admin-ash-trans.component.html',
  styleUrls: ['./update-admin-ash-trans.component.scss']
})
export class UpdateAdminAshTransComponent implements OnInit {


  private isSaved = false;

  public jsonBody = {
    entryId : -1,
    branchId : undefined,
    companyId : undefined,
    month : undefined,
    year : undefined,
    fuleType: undefined,//pasindu
    distance : undefined,
    fuelEconomy : undefined,
    quantity : undefined,//kg
    company : undefined,
    branch : undefined,
    mon : undefined,
    noOfTrips: 1,
    fuelConsumption: 0,

  }

  public months = [];
  public companies = []
  public years = [];
  public branches = [];
  public fuelTypes = [{id: 1, name: 'Petrol' },
  {id: 2, name: 'Diesel',}];//pasindu




  constructor(
    public dialogRef: MatDialogRef<UpdateAdminAshTransComponent>,
    private masterData: MassterDataService,
    private toastSerivce: NbToastrService,
    private boService: BackendService,
    @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any,

  ) {
    console.log(this.popupData)
    this.init(this.popupData.data, this.popupData.isNew)
  }

  private init(data:any, isNew:boolean) {
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companies = d;
      // this.jsonBody.company = this.companies.filter(c => c.id === this.jsonBody.companyId)[0].name

    })
    // this.masterData.getYearsForCompany(this.jsonBody.companyId).subscribe(d => {
    //   console.log(d);
    //   this.years = d;
    // })
    // this.masterData.getBranchesForCompany(this.jsonBody.companyId).subscribe(d => {
    //   this.branches = d;
    //   console.log(d);
    // })
    // this.masterData.getYearsForCompany(this.jsonBody.companyId).subscribe(d => {
    //   this.years = d;
    // })
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });
    this.branches.push({
      id: 0, name: 'Search Branch'
    })

    if(!isNew)  {
      this.jsonBody.entryId = data.id;
      this.jsonBody.companyId = data.idCom ;
      this.jsonBody.branchId= data.idBran;
      this.jsonBody.fuelEconomy = data.fuelEconomy ;
      this.jsonBody.distance = data.distance ;
      this.jsonBody.year = data.year ;
      this.jsonBody.month = data.idMon ;
      this.jsonBody.fuleType = data.fuleType;//pasindu
      this.jsonBody.fuelConsumption = data.fuelConsumption;
      this.jsonBody.noOfTrips = data.noOfTrips;
      // this.masterData.getBranchesForCompany(this.jsonBody.companyId).subscribe(d => this.branches = d)
      this.masterData.getYearsForCompany(this.jsonBody.companyId).subscribe(d => {
        console.log(d);
        this.years = d;
      })
      this.branches.push({
        id: data.idBran, name: data.branch
      })
    }
  }

  onSearchBranch(value: any) {
    if (value === '') {
      return;
    }
    let queryReq = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        name: { value: value , type: 4, col: 3},
        companyId: { value: this.jsonBody.companyId, type: 1, col: 1},
      },
    }
    // if (this.selectedComId != 0) {
    //   queryReq.FILTER_MODEL[ 'companyId'] = { value: this.selectedComId, type: 1, col: 1}
    // }
    this.branches = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      queryReq
    ).then(data => {
      console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.branches = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.branches.unshift({id: val.id, name: val.name});
            }
          });
          if (this.branches.length === 0){
            this.branches.push({id: 0, name: 'No Matches Found'})
          }
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  private validateEntry(onEdit: boolean): boolean {
    // if (this.jsonBody.b === undefined || this.jsonBody.BRANCH_ID === '' || this.jsonBody.BRANCH_ID === 0 ){
    //   return false;
    // }
    if (this.jsonBody.distance === undefined || this.jsonBody.distance === '' ){
      return false;
    }
    if (this.jsonBody.fuelEconomy === undefined || this.jsonBody.fuelEconomy === '' ){
      return false;
    }
    // if (this.jsonBody.UNITS === undefined || this.jsonBody.UNITS === '' ){
    //   return false;
    // }


    if (this.jsonBody.year === undefined || this.jsonBody.year == "") {
      //show snack bar
      return false;
    }
    if (this.jsonBody.month === undefined || this.jsonBody.month < 0) {
      //show snack bar
      return false;
    }
    if (onEdit && (this.jsonBody.entryId === undefined || this.jsonBody.entryId <= 0)) {
      //show snack bar
      return false;
    }
    if (onEdit && (this.jsonBody.companyId === undefined || this.jsonBody.companyId === '') ){
      return false;
    }
    if (!((this.jsonBody.fuelConsumption > 0) || (this.jsonBody.distance > 0 &&
      this.jsonBody.fuelEconomy  > 0 && this.jsonBody.noOfTrips > 0))) {
      return false;
    }
    return true;
  }

  ngOnInit() {
  }

  onChangeCompany(value: any) {
    this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)
    this.masterData.getYearsForCompany(value).subscribe(d => {
      console.log(d);
      this.years = d;
    })

    this.jsonBody.company = this.companies.filter(c => c.id === value)[0].name
  }

  closeDialog() {
    this.dialogRef.close(this.isSaved);

  }

  changeFuelConsumption ($event) {
    if (this.jsonBody.fuelEconomy == 0) {
      this.jsonBody.fuelConsumption = 0;
      return;
    }
    this.jsonBody.fuelConsumption = (this.jsonBody.distance / this.jsonBody.fuelEconomy) * this.jsonBody.noOfTrips
  }

  public onClickSave() {
    // update
    if (!this.popupData.isNew) {
      if (this.validateEntry( true)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageAshTransportation,
          { DATA: this.jsonBody}
        ).then( data =>
        {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }
    // new
    else {
      if (this.validateEntry( false)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageAshTransportation,
          { DATA: this.jsonBody}
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            });
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }
  }


  onChangeBranch($event: MatSelectChange) {
    this.jsonBody.branch = this.branches.filter(b => b.id === $event)[0].name;
  }

  onChangeMonth($event: MatSelectChange) {
    this.jsonBody.mon = this.months.filter(m => m.id === $event.value)[0].name;
  }


}
