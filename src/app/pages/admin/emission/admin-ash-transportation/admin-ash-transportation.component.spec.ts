import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAshTransportationComponent } from './admin-ash-transportation.component';

describe('AdminAshTransportationComponent', () => {
  let component: AdminAshTransportationComponent;
  let fixture: ComponentFixture<AdminAshTransportationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAshTransportationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAshTransportationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
