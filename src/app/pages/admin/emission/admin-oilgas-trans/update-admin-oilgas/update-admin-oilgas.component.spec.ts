import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAdminOilgasComponent } from './update-admin-oilgas.component';

describe('UpdateAdminOilgasComponent', () => {
  let component: UpdateAdminOilgasComponent;
  let fixture: ComponentFixture<UpdateAdminOilgasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAdminOilgasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAdminOilgasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
