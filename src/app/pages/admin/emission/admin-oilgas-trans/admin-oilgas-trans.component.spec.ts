import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminOilgasTransComponent } from './admin-oilgas-trans.component';

describe('AdminOilgasTransComponent', () => {
  let component: AdminOilgasTransComponent;
  let fixture: ComponentFixture<AdminOilgasTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminOilgasTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminOilgasTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
