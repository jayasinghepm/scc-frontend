import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AirtravelFormulaComponent } from './airtravel-formula.component';

describe('AirtravelFormulaComponent', () => {
  let component: AirtravelFormulaComponent;
  let fixture: ComponentFixture<AirtravelFormulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AirtravelFormulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AirtravelFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
