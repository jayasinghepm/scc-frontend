import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAdminRawMaterialTransComponent } from './update-admin-raw-material-trans.component';

describe('UpdateAdminRawMaterialTransComponent', () => {
  let component: UpdateAdminRawMaterialTransComponent;
  let fixture: ComponentFixture<UpdateAdminRawMaterialTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAdminRawMaterialTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAdminRawMaterialTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
