import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRawMaterialTransComponent } from './admin-raw-material-trans.component';

describe('AdminRawMaterialTransComponent', () => {
  let component: AdminRawMaterialTransComponent;
  let fixture: ComponentFixture<AdminRawMaterialTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRawMaterialTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRawMaterialTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
