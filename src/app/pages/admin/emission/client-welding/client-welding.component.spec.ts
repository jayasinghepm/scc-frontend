import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientWeldingComponent } from './client-welding.component';

describe('ClientElectricityComponent', () => {
  let component: ClientWeldingComponent;
  let fixture: ComponentFixture<ClientWeldingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientWeldingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientWeldingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
