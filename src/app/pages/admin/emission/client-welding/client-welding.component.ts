import {AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {SmartTableService} from "../../../../@core/service/smart-table.service";
import {BackendService} from "../../../../@core/rest/bo_service";
import {MassterDataService} from "../../../../@core/service/masster-data.service";
import {NbDialogService, NbGlobalPhysicalPosition, NbMenuService, NbToastrService} from "@nebular/theme";
import {RequestGroup} from "../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../@core/enums/request-type-enums";
import {DOCUMENT} from "@angular/common";
import {ElecFormulaComponent} from "../../../formulas/elec-formula/elec-formula.component";
import {MatDialog} from "@angular/material";
import {AddClientComponent} from "../../admin-clients/add-client/add-client.component";
import {UpdateAirtravelComponent} from "../client-air-travel/update-airtravel/update-airtravel.component";
import {UpdateWeldingComponent} from "./update-welding/update-welding.component";
import {DeleteConfirmComponent} from "../../../delete-confirm/delete-confirm.component";
import {LocalDataSource} from "../../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";
import {FireExtFormulaComponent} from "../../../formulas/fire-ext-formula/fire-ext-formula.component";
import { UserState } from 'src/app/@core/auth/UserState';

@Component({
  selector: 'app-client-welding',
  templateUrl: './client-welding.component.html',
  styleUrls: ['./client-welding.component.scss']
})
export class ClientWeldingComponent implements OnInit {
  public pg_current = 0;
  public fetchedCount =0;
  public disableNext = false;
  public disablePrev = true;
  public totalCount = 0;
  public pages = 0;
  public ismasterAdmin: boolean = false;

  settings = {

    selectMode: 'multi',

    mode: 'external',
    pager: {
      display: false,
    },
    actions: {

      delete: true,
      add: true,
      edit: true,
      select: true,
      // delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    editor: {
      type: 'completer',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // id: {
      //   title: 'ID',
      //   type: 'number',
      //   editable: false,
      //   addable: false,
      //   filter: true,
      //   sort: true,
      // },
      company: {
        title: 'Company',
        type: 'string',
        filter: true,
        sort: true,

      },
      //Todo: have to change
      branch: {
        title: 'Branch',
        type: 'string',
        filter: true,
        sort: true,
      },
      acetylene: {
        title: 'Acetylene',
        type: 'number',
        filter: false,
        sort: false,
      },
      liquidCO2: {
        title: 'Liquid CO2',
        type: 'number',
        filter: false,
        sort: false,
      },
      month: {
        title: 'Month',
        type: 'string',
        filter: true,
        sort: true,
      },
      year: {
        title: 'Year',
        type: 'string',
        filter: true,
        sort: true,
      },
      acetyleneEmi: {
        title: 'Emission Acetylene (tCO2e)',
        type: 'number',
        addable: false,
        editable: false,
        filter: false,
        sort: false,
      },
      liquidCO2Emi: {
        title: 'Emission Liquid CO2 (tCO2e)',
        type: 'number',
        addable: false,
        editable: false,
        filter: false,
        sort: false,
      },
      totalEmi: {
        title: 'Total Emission (tCO2e)',
        type: 'number',
        addable: false,
        editable: false,
        filter: false,
        sort: false,
      },
    },
  };
  mySetting = JSON.parse(JSON.stringify(this.settings))


  private filterModel = {
    // company: { value: '' , type: 4, col: 3},
    // branch: { value: '' , type: 4, col: 3},
    mon: { value: '' , type: 4, col: 3},
    //todo: float
    consumption: { value: '' , type: 1, col:5},
    meterNo: { value: '' , type: 4, col: 3},
    entryId: { value: 0 , type: 1, col: 1},
    year: { value: '' , type: 4, col: 3},
    // companyId: { value: 0 , type: 1, col: 1},
    companyId: { value: UserState.getInstance().companyId , type: 1, col: 1},
    branch: { value: UserState.getInstance().branchId , type: 4, col: 3},
  }


  private sortModel = {
    company: { dir: '' },
    branch: { dir: '' },
    mon: { dir: '' },
    consumption: { dir: '' },
    meterNo: { dir: '' },
    entryId: { dir: '' },
    year: { dir: '' },

  }


  source: LocalDataSource = new LocalDataSource();
  constructor(private boService: BackendService,
              private masterData: MassterDataService,
              private  toastSerivce:  NbToastrService,
              private nbMenuService: NbMenuService,
              private dialogService: NbDialogService,
              private dialog: MatDialog,
              @Inject(DOCUMENT) private document: Document
              ) {
    this.selectedCompany = 0;
    // this.loadFiltered(this.pg_current);
    this.loadFiltered(this.pg_current)
    // this.source.load(data);
  }

  public  companines = [];
  public selectedCompany: number;

  ngOnInit(): void {
    this.ismasterAdmin = UserState.getInstance().ismasterAdmin;
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companines.push({
        id: 0,
        name: 'All'
      })
      if (d !== undefined && d.length !== 0) {
        d.forEach(c => {
          this.companines.push({
            id: c.id,
            name: c.name
          });
        })
      }
    })
    this.mySetting.actions = this.initActions()
    this.settings = Object.assign({}, this.mySetting);
  }

  initActions():any {

    if (UserState.getInstance().projectStatus == 4) { //data entry
      const entitle = UserState.getInstance().pageRoutes.filter(v => {
        let page = v.split('-')[0];
        if (+page === 23) {
          return v;
        }
      })[0];
      if (entitle !== undefined) {
        const add = entitle.split('-')[1] === '1'? true: false;
        const edit = entitle.split('-')[2] === '1'? true: false;
        const del = entitle.split('-')[3] === '1'? true: false;
        return {
          add: add,
          edit: edit,
          delete: del,
        }

      }
    }else {
      return {
        add: false,
        edit: false,
        delete: false,
      }
    }



  }

  onChangeCompany(value: any) {
    this.filterModel.companyId.value = value;
    this.loadFiltered(this.pg_current);
  }






  public onUpdate(event: any, isNew: boolean) {
    // console.log(event);
    const dialogRef = this.dialog.open(UpdateWeldingComponent,
      {
        data : {data: event.data , isNew: isNew, header: isNew ? 'New Entry': 'Edit Entry' },
        width: '400px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.loadFiltered(this.pg_current);
      }
    });


  }
  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.Emission,
      RequestType.ListWeldingEntry,
      {
        PAGE_NUMBER: this.pg_current,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          let list = [];
          this.totalCount = data.DAT.TOTAL_COUNT;
          this.pages = Math.ceil(this.totalCount/20);
          this.fetchedCount = data.DAT.LIST.length;
          if (this.fetchedCount < 20) {
            this.disableNext = true;
          } else {
            this.disableNext = false;
          }
          data.DAT.LIST.forEach(val => {
            if (val != undefined) {
              list.push(this.fromListRequest(val));
            }
          });
          this.source.load(list);
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  private validateEntry(data: any, onEdit: boolean): boolean {
    if (data.branch === undefined || data.branch === '' ){
      return false;
    }
    if (data.meterNo === undefined || data.meterNo === '' ){
      return false;
    }
    if (data.consumption === undefined || data.consumption === '' ){
      return false;
    }
    if (data.units === undefined || data.units === '' ){
      return false;
    }


    if (data.year === undefined || data.year == "") {
      //show snack bar
      return false;
    }
    if (data.month === undefined || data.month == "") {
      //show snack bar
      return false;
    }
    if (onEdit && (data.id === undefined || data.id == "")) {
      //show snack bar
      return false;
    }
    if (onEdit && (data.company === undefined || data.company === '') ){
      return false;
    }
    return true;
  }

  private fromListRequest(dto: any): any {
    let company;
    let branch;
    let numOfMeters;
    let meterNo;
    let units
    let consumption;
    let month;
    let year;
    let id;


    this.masterData.getMonthName(dto.month).subscribe(data => { month = data; }).unsubscribe();
    year = dto.year;
    id = dto.entryId;

    let emissionDetails = dto.emissionDetails;
    let acetyleneEmi = emissionDetails['acetylene'] ? parseFloat(emissionDetails['acetylene']): 0;
    let liquidCO2Emi = emissionDetails['liquidCO2'] ? parseFloat(emissionDetails['liquidCO2']): 0;

    return {
      id,
      idCom: dto.companyId,
      idBran: dto.branchId,
      idMon: dto.month,
      company: dto.company,
      branch: dto.branch,
      consumption,
      year: year,
      month: month,
      liquidCO2: dto.liquidCO2,
      acetylene: dto.acetylene,
      acetyleneEmi: acetyleneEmi.toFixed(5),
      liquidCO2Emi: liquidCO2Emi.toFixed(5),
      totalEmi: (liquidCO2Emi + acetyleneEmi).toFixed(5)
    };



  }



  public onShowcontextMenu($event: any) {
    const dialogRef = this.dialog.open(ElecFormulaComponent,
      {
        data : {data: $event.data},
        width: '500px',
        maxHeight: '650px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      })
  }

  public onDelete($event: any) {
    const dialogRef = this.dialog.open(DeleteConfirmComponent,
      {
        data : { },
        width: '300px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageWelding,
          {
            DATA: {
              entryId: $event.data.id,
              isDeleted: 1,
            }
          }).then(data=> {
            // console.log(data)
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.DTO != undefined) {
              this.loadFiltered(this.pg_current);
            }
          }
        })


        // });
      }
    });
  }
  onClickNextPage() {
    this.pg_current++;
    this.disablePrev = false;
    if (this.isFilterEnabled()) {
      return this.loadFiltered(this.pg_current)
    }
    if (this.isSorterEnabled()) {
      return this.loadSorted(this.pg_current)
    }
    this.loadFiltered(this.pg_current);
  }

  onClickPrevPage() {
    if (this.pg_current >0) {
      this.pg_current--;
      this.disablePrev = this.pg_current === 0 ? true : false;
      if (this.isFilterEnabled()) {
        return this.loadFiltered(this.pg_current)
      }
      if (this.isSorterEnabled()) {
        return this.loadSorted(this.pg_current)
      }
      this.loadFiltered(this.pg_current);
    }
  }

  private isFilterEnabled():boolean {
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (this.filterModel[k].value !== '' || this.filterModel[k].value !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  private isSorterEnabled():boolean {
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (this.sortModel[k].dir !== '' || this.sortModel[k].dir !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  onFilter($event) {
    // console.log($event)
    switch($event.query.column.id) {
      case 'id':  {
        this.filterModel.entryId.value = +$event.query.query;
        break;
      } 
      //  case 'company':  {
      //   this.filterModel.company.value = $event.query.query;
      //   break;
      // } 
       case 'branch':  {
        this.filterModel.branch.value = $event.query.query;
        break;
      }  case 'meterNo':  {
        this.filterModel.meterNo.value = $event.query.query;
        break;
      } case 'month':  {
        this.filterModel.mon.value = $event.query.query;
        break;
      }  case 'year':  {
        this.filterModel.year.value = $event.query.query;
        break;
      }
      case 'consumption': {
        this.filterModel.consumption.value = $event.query.query;
        break;
      }

    }
    this.pg_current = 0;
    this.loadFiltered(0);
  }

  onSort($event) {
    this.sortModel  =  {
      company: { dir: '' },
      branch: { dir: '' },
      mon: { dir: '' },
      consumption: { dir: '' },
      meterNo: { dir: '' },
      entryId: { dir: '' },
      year: { dir: '' },

    };
    // console.log($event)
    switch($event.id) {
      case 'id':  {
        this.sortModel.entryId.dir = $event.direction;
        break;
      }  case 'company':  {
        this.sortModel.company.dir = $event.direction;
        break;
      }  case 'branch':  {
        this.sortModel.branch.dir = $event.direction;
        break;
      }  case 'meterNo':  {
        this.sortModel.meterNo.dir = $event.direction;
        break;
      }  case 'consumption':  {
        this.sortModel.consumption.dir = $event.direction;
        break;
      }  case 'month':  {
        this.sortModel.mon.dir = $event.direction;
        break;
      }  case 'year':  {
        this.sortModel.year.dir = $event.direction;
        break;
      }
    }
    this.loadSorted(0)
  }

  loadFiltered(pageNum: number) {
    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }

    if (this.isSorterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListWeldingEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            // if ((list.length === 0 || list.length <= 20) && this.pg_current > 0) {
            //   this.pg_current = 0;
            //   this.loadFiltered(this.pg_current);
            //
            // }
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListWeldingEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }

    // this.boService.sendRequestToBackend(
    //   RequestGroup.Emission,
    //   RequestType.ListWeldingEntry,
    //   {
    //     PAGE_NUMBER: pageNum,
    //     FILTER_MODEL: effectiveFilter,
    //   }
    // ).then(data => {
    //   if (data.HED != undefined && data.HED.RES_STS == 1) {
    //     if (data.DAT != undefined && data.DAT.LIST != undefined) {
    //       let list = [];
    //       this.totalCount = data.DAT.TOTAL_COUNT;
    //       this.pages = Math.ceil(this.totalCount/20);
    //       this.fetchedCount = data.DAT.LIST.length;
    //       if (this.fetchedCount < 20) {
    //         this.disableNext = true;
    //       } else {
    //         this.disableNext = false;
    //       }
    //       data.DAT.LIST.forEach(val => {
    //         if (val != undefined) {
    //           list.push(this.fromListRequest(val));
    //         }
    //       });
    //       this.source.load(list);
    //     } else {
    //       //  todo: show snack bar
    //       console.log('error data 1')
    //     }
    //   } else {
    //     //  todo: show snack bar
    //     console.log('error data 2')
    //   }
    // })
  }

  loadSorted(pageNum: number) {

    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }
    if(this.isFilterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListWeldingEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }  else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListWeldingEntry,
        {
          PAGE_NUMBER: pageNum,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }
  }

  multiselectraw(event:any,isNew:boolean,ismulti:boolean){
    let  data1 = event.selected
    console.log("sele===========",data1) 
    for(let i = 0 ; i < data1.length; i++){

    const dialogRef = this.dialog.open(UpdateWeldingComponent,
      {
        data : {data: data1[i] , isNew: isNew, ismulti:ismulti ,header: isNew ? 'New Entry': 'Edit Entry' },
        width: '400px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.loadFiltered(this.pg_current);
      }
    });
     this.dialog.closeAll();
  }
  
  // console.log("datta 5---",data1[4])
}

  onShowFormula($event: any) {
    const dialogRef = this.dialog.open(ElecFormulaComponent,
      {
        data : {data: $event.data},
        width: '500px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      })
  }
}
