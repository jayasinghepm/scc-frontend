import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateElecComponent } from './update-welding.component';

describe('UpdateElecComponent', () => {
  let component: UpdateElecComponent;
  let fixture: ComponentFixture<UpdateElecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateElecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateElecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
