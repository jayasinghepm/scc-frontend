import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from "@angular/material";
import {MassterDataService} from "../../../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {BackendService} from "../../../../../@core/rest/bo_service";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import { UserState } from 'src/app/@core/auth/UserState';

@Component({
  selector: 'app-update-welding',
  templateUrl: './update-welding.component.html',
  styleUrls: ['./update-welding.component.scss']
})
export class UpdateWeldingComponent implements OnInit {

  private isSaved = false;

  public jsonBody = {

    entryId: -1,
    isDeleted: 0,
    addedBy: 0,
    branchId: 0,
    companyId: 0,
    month: 0,
    year: "",
    acetylene: 0,
    liquidCO2: 0,
    company: undefined,
    branch: undefined,
    mon: undefined,
  }

  public months = [];
  public companies = []
  public years = [];
  public branches = [];



  constructor(
               public dialogRef: MatDialogRef<UpdateWeldingComponent>,
               private masterData: MassterDataService,
               private toastSerivce: NbToastrService,
               private boService: BackendService,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any,

  ) {
    // console.log(this.popupData)
    this.init(this.popupData.data, this.popupData.isNew);

    if(popupData.ismulti){
      this.onClickSave();
      this.closeDialog
    }

  }

  private init(data:any, isNew:boolean) {
    this.jsonBody.companyId = UserState.getInstance().companyId;

    this.masterData.loadCompaninesForDataEnry().subscribe(d => {
      this.companies = d;
    })
    this.masterData.getYearsForCompany(this.jsonBody.companyId).subscribe(d => {
      this.years = d;
    })
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });
    //----------------------------branch search ---------------------
    this.branches.push({
      id: 0, name: 'Search Branch'
    })
    //---------------------------------------------------------------
    if(!isNew)  {

      this.jsonBody.liquidCO2 = data.liquidCO2;
      this.jsonBody.acetylene = data.acetylene;
      this.jsonBody.entryId = data.id;

      this.jsonBody.companyId = data.idCom ;
      this.jsonBody.branchId = data.idBran;
      this.jsonBody.year = data.year ;
      this.jsonBody.month = data.idMon ;

      // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => this.branches = d)
      this.masterData.getYearsForCompany(this.jsonBody.companyId).subscribe(d => {
        // console.log(d);
        this.years = d;
      })

      //----------------branch search-------------------------
      this.branches.push({
        id: data.branchId, name: data.branch
      })
      this.selectedComId =  this.jsonBody.companyId;

      // ----------------------------------------------------
    }
  }

  private validateEntry(onEdit: boolean): boolean {
    console.log(onEdit, this.jsonBody)
    // if (this.jsonBody.BRANCH_ID === undefined || this.jsonBody.BRANCH_ID === '' || this.jsonBody.BRANCH_ID === 0 ){
    //   return false;
    // }
    if (this.jsonBody.acetylene === undefined ){
      return false;
    }
    if (this.jsonBody.liquidCO2 === undefined ){
      return false;
    }
    // if (this.jsonBody.UNITS === undefined || this.jsonBody.UNITS === '' ){
    //   return false;
    // }


    if (this.jsonBody.year === undefined || this.jsonBody.year == "") {
      //show snack bar
      return false;
    }
    if (this.jsonBody.month === undefined || this.jsonBody.month < 0) {
      //show snack bar
      return false;
    }
    if (onEdit && (this.jsonBody.entryId === undefined || this.jsonBody.entryId <= 0)) {
      //show snack bar
      return false;
    }
    if (onEdit && (this.jsonBody.companyId === undefined) ){
      return false;
    }
    return true;
  }

  ngOnInit() {
  }

  public selectedComId = 0;
  onChangeCompany(value: any) {
    this.selectedComId = value;
    this.branches = [];
    this.branches.push({
      id: 0, name: 'Search Branch'
    })
    this.jsonBody.company = this.companies.filter(c => c.id === value)[0].name
    // this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)
    this.masterData.getYearsForCompany(value).subscribe(d => {
      // console.log(d);
      this.years = d;
    })
  }

  closeDialog() {
    this.dialogRef.close(this.isSaved);

  }

  public onClickSave() {
    // update
    if (!this.popupData.isNew) {
        if (this.validateEntry( true)) {
          this.boService.sendRequestToBackend(
            RequestGroup.Emission,
            RequestType.ManageWelding,
            { DATA: this.jsonBody}
          ).then( data =>
          {
            if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
              this.isSaved = true;
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            } else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          })
        } else {
          this.toastSerivce.show('', 'Fill empty fields', {
            status: 'warning',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
    }
    // new
    else {
      if (this.validateEntry( false)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageWelding,
          { DATA: this.jsonBody}
        ).then( data => {        
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.popupData.isNew = false;
            this.jsonBody.entryId = data.DAT.DTO.entryId;           
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            });
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }
  }

  onSearchBranch(value: any) {
    if (value === '') {
      return;
    }
    let queryReq = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        name: { value: value , type: 4, col: 3},

      },
    }
    if (this.selectedComId != 0) {
      queryReq.FILTER_MODEL[ 'companyId'] = { value: this.selectedComId, type: 1, col: 1}
    }
    this.branches = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      queryReq
    ).then(data => {
      // console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.branches = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.branches.unshift({id: val.id, name: val.name});
            }
          });
          if (this.branches.length === 0){
            this.branches.push({id: 0, name: 'No Matches Found'})
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  onChangeBranch($event: MatSelectChange) {
    this.jsonBody.branch = this.branches.filter(b => b.id === $event)[0].name;
  }

  onChangeMonth($event: MatSelectChange) {
    this.jsonBody.mon = this.months.filter(m => m.id === $event.value)[0].name;
  }


}
