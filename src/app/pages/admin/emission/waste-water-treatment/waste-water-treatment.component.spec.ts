import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WasteWaterTreatmentComponent } from './waste-water-treatment.component';

describe('WasteWaterTreatmentComponent', () => {
  let component: WasteWaterTreatmentComponent;
  let fixture: ComponentFixture<WasteWaterTreatmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WasteWaterTreatmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WasteWaterTreatmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
