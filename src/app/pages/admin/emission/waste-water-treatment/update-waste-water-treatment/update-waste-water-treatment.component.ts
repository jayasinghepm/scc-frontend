import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MatSelectChange, MAT_DIALOG_DATA } from '@angular/material';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { RequestGroup } from 'src/app/@core/enums/request-group-enum';
import { RequestType } from 'src/app/@core/enums/request-type-enums';
import { BackendService } from 'src/app/@core/rest/bo_service';
import { MassterDataService } from 'src/app/@core/service/masster-data.service';

@Component({
  selector: 'app-update-waste-water-treatment',
  templateUrl: './update-waste-water-treatment.component.html',
  styleUrls: ['./update-waste-water-treatment.component.scss']
})
export class UpdateWasteWaterTreatmentComponent implements OnInit {
  private isSaved = false;

  public companies = [];
  public branches = [];
  public months = [];
  public years = [];

  public selectedComId = 0;
  public jsonBody = {
    WASTE_WATER_TREATMENT_ENTRY_ID:  -1,
    BRANCH_ID: undefined,
    COMPANY_ID: undefined, // todo:
    YEAR: undefined,
    MONTH: undefined,
    TOTAL_INDUSTRY_PRODUCT: undefined,
    WASTE_GENERATED: undefined,
    CHEMICAL_OXYGEN_DEMAND: undefined,
    TYPE_OF_TREATMENT: undefined,
    SLUDGE_REMOVED: undefined,
    RECOVERED_CH4: undefined,
    company: undefined,
    branch: undefined,
   month:undefined
  }

  public typeOfTreatment:string[]=["Sea, rever and lake discharge",
                                   "Aerobic treatment plant with well managed",
                                   "Aerobic treatment plant without well managed",
                                   "Anaerobic digester for sludge",
                                   "Anaerobic reactor",
                                   "Anaerobic shallow lagoon",
                                   "Anaerobic deep lagoon"
                                  ]



  constructor(public dialogRef: MatDialogRef<UpdateWasteWaterTreatmentComponent>,
    private masterData: MassterDataService,
    private toastSerivce: NbToastrService,
    private boService: BackendService,
    @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any){
      this.init(this.popupData.data, this.popupData.isNew)
      if(popupData.ismulti){
        this.onClickSave();
        this.closeDialog()
      }
      // console.log(this.popupData)
    }

  ngOnInit() {
  }
  private init(data: any, isNew: boolean) {
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });
    this.masterData.loadCompaninesForDataEnry().subscribe(d => {
      this.companies = d;
    })
    //----------------------------branch search ---------------------
    this.branches.push({
      id: 0, name: 'Search Branch'
    })
    //---------------------------------------------------------------
   
    if (!isNew) {
      this.jsonBody.WASTE_WATER_TREATMENT_ENTRY_ID = data.id;
      this.jsonBody.COMPANY_ID = data.idCom ;
      this.jsonBody.BRANCH_ID = data.idBran ;
      this.jsonBody.TOTAL_INDUSTRY_PRODUCT = data.TotalIndustryProduct ;
      this.jsonBody.WASTE_GENERATED = data.WasteGenerated ;
      this.jsonBody.CHEMICAL_OXYGEN_DEMAND = data.ChemicalOxygenDemand ;
      this.jsonBody.TYPE_OF_TREATMENT = data.TypeOfTreatment ;
      this.jsonBody.SLUDGE_REMOVED = data.SludgeRemoved ;
      this.jsonBody.RECOVERED_CH4 = data.RecoveredCH4InEachIndustrySector ;
     
      // this.jsonBody.MONTH = data.idMon ;
      this.jsonBody.MONTH = data.idMon;
      this.jsonBody.month = data.month
      this.jsonBody.YEAR = data.year;
      

      // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => this.branches = d)
      this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
        // console.log(d);
        this.years = d;
      })

      //----------------branch search-------------------------
      this.branches.push({
        id: data.idBran, name: data.branch
      })
      this.selectedComId =  this.jsonBody.COMPANY_ID;

      // ----------------------------------------------------
    }


  }



  private validateEntry(onEdit: boolean): boolean {
    // if (this.jsonBody.BRANCH_ID === undefined || this.jsonBody.BRANCH_ID === '' || this.jsonBody.BRANCH_ID === 0 ){
    //   // console.log(1)
    //   return false;
    // }
    if (onEdit && (this.jsonBody.COMPANY_ID === undefined || this.jsonBody.COMPANY_ID === '') ){
      // console.log(2)
      return false;
    }
   
    if (this.jsonBody.YEAR === undefined || this.jsonBody.YEAR == "") {
      // console.log(1)
      return false;
    }
    if (this.jsonBody.MONTH === undefined || this.jsonBody.MONTH < 0) {
      // console.log(1)
      return false;
    }
    if (this.jsonBody.MONTH === undefined || this.jsonBody.MONTH < 0) {
      // console.log(1)
      return false;
    } if (this.jsonBody.TOTAL_INDUSTRY_PRODUCT === undefined || this.jsonBody.TOTAL_INDUSTRY_PRODUCT < 0) {
      // console.log(1)
      return false;
    } if (this.jsonBody.WASTE_GENERATED === undefined || this.jsonBody.WASTE_GENERATED < 0) {
      // console.log(1)
      return false;
    } if (this.jsonBody.CHEMICAL_OXYGEN_DEMAND === undefined || this.jsonBody.CHEMICAL_OXYGEN_DEMAND < 0) {
      // console.log(1)
      return false;
    } if (this.jsonBody.SLUDGE_REMOVED === undefined || this.jsonBody.SLUDGE_REMOVED < 0) {
      // console.log(1)
      return false;
    } if (this.jsonBody.RECOVERED_CH4 === undefined || this.jsonBody.RECOVERED_CH4 < 0) {
      // console.log(1)
      return false;
    }
    if (this.jsonBody.TYPE_OF_TREATMENT === undefined || this.jsonBody.TYPE_OF_TREATMENT =="") {
      // console.log(1)
      return false;
    }

    return true;
  }


  closeDialog() {
    this.dialogRef.close(this.isSaved);

  }


  onChangeCompany(value: any) {
    this.selectedComId = value;
    this.branches = [];
    this.branches.push({
      id: 0, name: 'Search Branch'
    })
    this.jsonBody.company = this.companies.filter(c => c.id === value)[0].name

    // this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)
    this.masterData.getYearsForCompany(value).subscribe(d => {
      console.log(d);
      this.years = d;
    })
  }



  onClickSave() {
    console.log("jsonbody-----",this.jsonBody)
    if (!this.popupData.isNew) {
      // update
      if (this.validateEntry(!this.popupData.isNew)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageWasteWaterTreatmentEntry,
          {DATA: this.jsonBody}
        ).then( data => {
          console.log("ress---",data)
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
            // event.confirm.resolve(event.newData);
            // this.loadData();
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }
    else {
      if (this.validateEntry( false)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageWasteWaterTreatmentEntry,
          {DATA: this.jsonBody}
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
            // event.confirm.resolve(event.newData);
            // this.loadData();
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }

  }
  onChangeBranch($event: MatSelectChange) {
    this.jsonBody.branch = this.branches.filter(b => b.id === $event)[0].name;
  }

  onSearchBranch(value: any) {
    if (value === '') {
      return;
    }
    let queryReq = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        name: { value: value , type: 4, col: 3},

      },
    }
    if (this.selectedComId != 0) {
      queryReq.FILTER_MODEL[ 'companyId'] = { value: this.selectedComId, type: 1, col: 1}
    }
    this.branches = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      queryReq
    ).then(data => {
      console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.branches = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.branches.unshift({id: val.id, name: val.name});
            }
          });
          if (this.branches.length === 0){
            this.branches.push({id: 0, name: 'No Matches Found'})
          }
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  onChangeMonth($event: MatSelectChange) {
    this.jsonBody.MONTH = this.months.filter(m => m.name === $event.value)[0].id;
    // this.jsonBody.MONTH = $event.value;
  }
}
