import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateWasteWaterTreatmentComponent } from './update-waste-water-treatment.component';

describe('UpdateWasteWaterTreatmentComponent', () => {
  let component: UpdateWasteWaterTreatmentComponent;
  let fixture: ComponentFixture<UpdateWasteWaterTreatmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateWasteWaterTreatmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateWasteWaterTreatmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
