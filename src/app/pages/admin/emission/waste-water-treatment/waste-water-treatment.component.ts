import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { NbToastrService, NbMenuService, NbDialogService } from '@nebular/theme';
import { UserState } from 'src/app/@core/auth/UserState';
import { RequestGroup } from 'src/app/@core/enums/request-group-enum';
import { RequestType } from 'src/app/@core/enums/request-type-enums';
import { BackendService } from 'src/app/@core/rest/bo_service';
import { MassterDataService } from 'src/app/@core/service/masster-data.service';
import { LocalDataSource } from 'src/app/ng2-smart-table/src/public-api';
import { DeleteConfirmComponent } from 'src/app/pages/delete-confirm/delete-confirm.component';
import { ElecFormulaComponent } from 'src/app/pages/formulas/elec-formula/elec-formula.component';
import { WasteWaterTreatmentFormulaComponent } from 'src/app/pages/formulas/waste-water-treatment-formula/waste-water-treatment-formula.component';
import { UpdateAdminPaperWasteComponent } from '../admin-paper-waste/update-admin-paper-waste/update-admin-paper-waste.component';
import { UpdateWasteWaterTreatmentComponent } from './update-waste-water-treatment/update-waste-water-treatment.component';

@Component({
  selector: 'app-waste-water-treatment',
  templateUrl: './waste-water-treatment.component.html',
  styleUrls: ['./waste-water-treatment.component.scss']
})
export class WasteWaterTreatmentComponent implements OnInit {


  public pg_current = 0;
  public fetchedCount =0;
  public disableNext = false;
  public disablePrev = true;
  public totalCount = 0;
  public pages = 0;

  public months=[]
  settings = {
    mode: 'external',
    pager: {
      display: false,
    },
    actions: {
      // delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    editor: {
      type: 'completer',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {

      company: {
        title: 'Company',
        editable: false,
        addable: false,
        type: 'string',
        filter: false,
        sort: false,
      },
      //Todo: have to change
      branch: {
        title: 'Branch',
        type: 'string',
        filter: true,
        sort: true,
      },
      TotalIndustryProduct: {
        title: 'Total industry product (t/yr)',
        type: 'number',
        filter: true,
        sort: true,
      },
      WasteGenerated: {
        title: 'Waste generated (m3/t)',
        type: 'number',
        filter: true,
        sort: true,
      },
      ChemicalOxygenDemand: {
        title: 'Chemical Oxygen Demand (kg COD/m3)',
        type: 'number',
        filter: true,
        sort: true,
      },
      TypeOfTreatment: {
        title: 'Type of treatment',
        type: 'string',
        filter: true,
        sort: true,
      },
      SludgeRemoved: {
        title: 'Sludge removed in each industry sector (kg COD/yr)',
        type: 'number',
        filter: true,
        sort: true,
      },
      RecoveredCH4InEachIndustrySector: {
        title: 'Recovered CH4 in each industry sector (kg CH4/yr)',
        type: 'number',
        filter: true,
        sort: true,
      },
      month: {
        title: 'Month',
        type: 'string',
        filter: true,
        sort: true,
      },
      year: {
        title: 'Year',
        type: 'string',
        filter: true,
        sort: true,
      },
      emission: {
        title: 'Emission (tCO2e)',
        type: 'number',
        addable: false,
        editable: false,
        filter: false,
        sort: false,
      },
    },
  };
  mySetting = JSON.parse(JSON.stringify(this.settings))

  public ismasterAdmin: boolean = false;

  private filterModel = {
    // companyId: { value: '' , type: 1, col: 1},
    // branch: { value: '' , type: 4, col: 3},
    companyId: { value: UserState.getInstance().companyId , type: 1, col: 1},
    branch: { value: UserState.getInstance().branchId , type: 4, col: 3},
    mon: { value: '' , type: 4, col: 3},
    //todo: float
    quantity: { value: '' , type: 1, col:5},
    entryId: { value: 0 , type: 1, col: 1},
    year: { value: '' , type: 4, col: 3},
  }


  private sortModel = {
    company: { dir: '' },
    branch: { dir: '' },
    mon: { dir: '' },
    quantity: { dir: '' },
    entryId: { dir: '' },
    year: { dir: '' },

  }



  source: LocalDataSource = new LocalDataSource();
  constructor(private boService: BackendService,
              private masterData: MassterDataService,
              private  toastSerivce:  NbToastrService,
              private nbMenuService: NbMenuService,
              private dialogService: NbDialogService,
              private dialog: MatDialog,
              @Inject(DOCUMENT) private document: Document
  ) {
    this.selectedCompany = 0;
    // this.loadFiltered(this.pg_current);
    this.loadFiltered(this.pg_current)
    // this.source.load(data);
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });
  }

  public  companines = [];
  public selectedCompany: number;


  ngOnInit() {
    this.ismasterAdmin = UserState.getInstance().ismasterAdmin;

    this.masterData.getCompaninesFull().subscribe(d => {
      this.companines.push({
        id: 0,
        name: 'All'
      })
      if (d !== undefined && d.length !== 0) {
       d.forEach(c => {
         this.companines.push({
           id: c.id,
           name: c.name
         });
       })
      }
      if(UserState.getInstance().adminId == 3){
        console.log("cccccccc====",this.companines.slice(0,5))
        this.companines = this.companines.slice(1,5);


      }
    })
    this.mySetting.actions = this.initActions()
    this.settings = Object.assign({}, this.mySetting);
  }

  initActions():any {

    if (UserState.getInstance().projectStatus == 4) { //data entry
      const entitle = UserState.getInstance().pageRoutes.filter(v => {
        let page = v.split('-')[0];
        if (+page === 23) {
          return v;
        }
      })[0];
      if (entitle !== undefined) {
        const add = entitle.split('-')[1] === '1'? true: false;
        const edit = entitle.split('-')[2] === '1'? true: false;
        const del = entitle.split('-')[3] === '1'? true: false;
        return {
          add: add,
          edit: edit,
          delete: del,
        }

      }
    }else {
      return {
        add: false,
        edit: false,
        delete: false,
      }
    }



  }


  onChangeCompany(value: any) {
    this.filterModel.companyId.value = value;
    this.loadFiltered(this.pg_current);
  }
  public onUpdate(event: any, isNew: boolean) {
    // console.log(event);
    // console.log(event);
    const dialogRef = this.dialog.open(UpdateWasteWaterTreatmentComponent,
      {
        data : {data: event.data , isNew: isNew, header: isNew ? 'New Entry': 'Edit Entry' },
        width: '400px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.loadFiltered(this.pg_current);
      }
    });


  }
  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.Emission,
      RequestType.ListWastWaterTreatmentEntry,
      {
        PAGE_NUMBER: this.pg_current,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          let list = [];
          this.totalCount = data.DAT.TOTAL_COUNT;
          this.pages = Math.ceil(this.totalCount/20);
          this.fetchedCount = data.DAT.LIST.length;
          if (this.fetchedCount < 20) {
            this.disableNext = true;
          } else {
            this.disableNext = false;
          }
          data.DAT.LIST.forEach(val => {
            if (val != undefined) {
              list.push(this.fromListRequest(val));
            }
          });
          this.source.load(list);
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }
  private validateEntry(data: any, onEdit: boolean): boolean {
    if (data.branch === undefined || data.branch === '' ){
      return false;
    }
    if (data.TotalIndustryProduct === undefined || data.TotalIndustryProduct === '' ){
      return false;
    }
    if (data.WasteGenerated === undefined || data.WasteGenerated === '' ){
      return false;
    }
    if (data.ChemicalOxygenDemand === undefined || data.ChemicalOxygenDemand === '' ){
      return false;
    }

    if (data.TypeOfTreatment === undefined || data.TypeOfTreatment === '' ){
      return false;
    }
    if (data.SludgeRemoved === undefined || data.SludgeRemoved === '' ){
      return false;
    }
    if (data.RecoveredCH4InEachIndustrySector === undefined || data.RecoveredCH4InEachIndustrySector === '' ){
      return false;
    }



    if (data.year === undefined || data.year == "") {
      //show snack bar
      return false;
    }
    if (data.month === undefined || data.month == "") {
      //show snack bar
      return false;
    }
    if (onEdit && (data.id === undefined || data.id == "")) {
      //show snack bar
      return false;
    }
    if (onEdit && (data.company === undefined || data.company === '') ){
      return false;
    }
    return true;
  }

  private fromListRequest(dto: any): any {
    
    console.log("dto",dto)

    return {
      id : dto.WASTE_WATER_TREATMENT_ENTRY_ID,
      idCom: dto.COMPANY_ID,
      idBran: dto.BRANCH_ID,
      idMon: dto.MONTH,
      company:dto.company,
      branch:dto.branch,
      month : this.months.filter(m => m.id === dto.MONTH)[0].name,
      year:dto.YEAR,
      TotalIndustryProduct:isNaN(dto.TOTAL_INDUSTRY_PRODUCT)?0.0:dto.TOTAL_INDUSTRY_PRODUCT,
      WasteGenerated: isNaN(dto.WASTE_GENERATED)?0.0:dto.WASTE_GENERATED,
      ChemicalOxygenDemand:isNaN(dto.CHEMICAL_OXYGEN_DEMAND)?0.0:dto.CHEMICAL_OXYGEN_DEMAND,
      TypeOfTreatment:dto.TYPE_OF_TREATMENT?dto.TYPE_OF_TREATMENT:"",
      SludgeRemoved: isNaN(dto.SLUDGE_REMOVED)?0.0:dto.SLUDGE_REMOVED,
      RecoveredCH4InEachIndustrySector:isNaN(dto.RECOVERED_CH4)?0.0:dto.RECOVERED_CH4,
      emission: isNaN(dto.EMISSION_INFO.tco2e_wwt)? 0.0 : (+dto.EMISSION_INFO.tco2e_wwt).toFixed(5)
    };
  }


 public onShowcontextMenu($event: any) {
    const dialogRef = this.dialog.open(WasteWaterTreatmentFormulaComponent,
      {
        data : {data: $event.data},
        width: '500px',
        maxHeight: '650px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      })
  }

  public onDelete($event: any) {
    const dialogRef = this.dialog.open(DeleteConfirmComponent,
      {
        data : { },
        width: '300px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageWasteWaterTreatmentEntry,
          {
            DATA: {
             entryId: $event.data.id,
              isDeleted: 1,
            }
          }).then(data=> {
          // console.log(data)
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.DTO != undefined) {
              this.loadFiltered(this.pg_current);
            }
          }
        })


        // });
      }
    });
  }
  onClickNextPage() {
    this.pg_current++;
    this.disablePrev = false;
    if (this.isFilterEnabled()) {
      return this.loadFiltered(this.pg_current)
    }
    if (this.isSorterEnabled()) {
      return this.loadSorted(this.pg_current)
    }
    this.loadFiltered(this.pg_current);
  }

  onClickPrevPage() {
    if (this.pg_current >0) {
      this.pg_current--;
      this.disablePrev = this.pg_current === 0 ? true : false;
      if (this.isFilterEnabled()) {
        return this.loadFiltered(this.pg_current)
      }
      if (this.isSorterEnabled()) {
        return this.loadSorted(this.pg_current)
      }
      this.loadFiltered(this.pg_current);
    }
  }

  private isFilterEnabled():boolean {
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (this.filterModel[k].value !== '' || this.filterModel[k].value !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  private isSorterEnabled():boolean {
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (this.sortModel[k].dir !== '' || this.sortModel[k].dir !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  onFilter($event) {
    console.log($event)
    switch($event.query.column.id) {
      case 'id':  {
        this.filterModel.entryId.value = +$event.query.query;
        break;
      }   case 'branch':  {
        this.filterModel.branch.value = $event.query.query;
        break;
      } case 'month':  {
        this.filterModel.mon.value = $event.query.query;
        break;
      }  case 'year':  {
        this.filterModel.year.value = $event.query.query;
        break;
      }
      // case 'quantity': {
      //   this.filterModel.quantity.value = $event.query.query;
      //   break;
      // }

    }
    this.pg_current = 0;
    this.loadFiltered(0);
  }

  onSort($event) {
    this.sortModel  =  {
      company: { dir: '' },
      branch: { dir: '' },
      mon: { dir: '' },
      quantity: { dir: '' },

      entryId: { dir: '' },
      year: { dir: '' },

    };
    console.log($event)
    switch($event.id) {
      case 'id':  {
        this.sortModel.entryId.dir = $event.direction;
        break;
      }  case 'company':  {
        this.sortModel.company.dir = $event.direction;
        break;
      }  case 'branch':  {
        this.sortModel.branch.dir = $event.direction;
        break;
      }   case 'quantity':  {
        this.sortModel.quantity.dir = $event.direction;
        break;
      }  case 'month':  {
        this.sortModel.mon.dir = $event.direction;
        break;
      }  case 'year':  {
        this.sortModel.year.dir = $event.direction;
        break;
      }
    }
    this.loadSorted(0)
  }

  loadFiltered(pageNum: number) {
    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }

    if (this.isSorterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListWastWaterTreatmentEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            // if ((list.length === 0 || list.length <= 20) && this.pg_current > 0) {
            //   this.pg_current = 0;
            //   this.loadFiltered(this.pg_current);
            //
            // }
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListWastWaterTreatmentEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }

    // this.boService.sendRequestToBackend(
    //   RequestGroup.Emission,
    //   RequestType.ListElectricityEntry,
    //   {
    //     PAGE_NUMBER: pageNum,
    //     FILTER_MODEL: effectiveFilter,
    //   }
    // ).then(data => {
    //   if (data.HED != undefined && data.HED.RES_STS == 1) {
    //     if (data.DAT != undefined && data.DAT.LIST != undefined) {
    //       let list = [];
    //       this.totalCount = data.DAT.TOTAL_COUNT;
    //       this.pages = Math.ceil(this.totalCount/20);
    //       this.fetchedCount = data.DAT.LIST.length;
    //       if (this.fetchedCount < 20) {
    //         this.disableNext = true;
    //       } else {
    //         this.disableNext = false;
    //       }
    //       data.DAT.LIST.forEach(val => {
    //         if (val != undefined) {
    //           list.push(this.fromListRequest(val));
    //         }
    //       });
    //       this.source.load(list);
    //     } else {
    //       //  todo: show snack bar
    //       console.log('error data 1')
    //     }
    //   } else {
    //     //  todo: show snack bar
    //     console.log('error data 2')
    //   }
    // })
  }

  loadSorted(pageNum: number) {

    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }
    if(this.isFilterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListWastWaterTreatmentEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }  else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListWastWaterTreatmentEntry,
        {
          PAGE_NUMBER: pageNum,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }
  }

  onShowFormula($event: any) {
    const dialogRef = this.dialog.open(WasteWaterTreatmentFormulaComponent,
      {
        data : {data: $event.data},
        width: '600px',
        maxHeight: '500px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      })
  }





}
