import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAdminStaffTransComponent } from './update-admin-staff-trans.component';

describe('UpdateAdminStaffTransComponent', () => {
  let component: UpdateAdminStaffTransComponent;
  let fixture: ComponentFixture<UpdateAdminStaffTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAdminStaffTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAdminStaffTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
