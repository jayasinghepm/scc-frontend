import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminStaffTransComponent } from './admin-staff-trans.component';

describe('AdminStaffTransComponent', () => {
  let component: AdminStaffTransComponent;
  let fixture: ComponentFixture<AdminStaffTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminStaffTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminStaffTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
