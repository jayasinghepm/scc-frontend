import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UpdateFreightRoadComponent } from './update-freightroad.component';


describe('UpdateAirtravelComponent', () => {
  let component: UpdateFreightRoadComponent;
  let fixture: ComponentFixture<UpdateFreightRoadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFreightRoadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFreightRoadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
