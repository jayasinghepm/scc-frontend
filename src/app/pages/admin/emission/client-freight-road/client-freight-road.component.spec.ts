import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ClientFreightRoadComponent } from './client-freight-road.component';


describe('ClientAirTravelComponent', () => {
  let component: ClientFreightRoadComponent;
  let fixture: ComponentFixture<ClientFreightRoadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientFreightRoadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientFreightRoadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
