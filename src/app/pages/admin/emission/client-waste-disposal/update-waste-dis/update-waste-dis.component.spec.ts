import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateWasteDisComponent } from './update-waste-dis.component';

describe('UpdateWasteDisComponent', () => {
  let component: UpdateWasteDisComponent;
  let fixture: ComponentFixture<UpdateWasteDisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateWasteDisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateWasteDisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
