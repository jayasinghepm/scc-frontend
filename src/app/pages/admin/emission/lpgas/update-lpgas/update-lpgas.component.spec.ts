import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateLpgasComponent } from './update-lpgas.component';

describe('UpdateLpgasComponent', () => {
  let component: UpdateLpgasComponent;
  let fixture: ComponentFixture<UpdateLpgasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateLpgasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateLpgasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
