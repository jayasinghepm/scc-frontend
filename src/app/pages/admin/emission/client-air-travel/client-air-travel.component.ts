import { Component, OnInit } from '@angular/core';
import { SmartTableService } from "../../../../@core/service/smart-table.service";
import { BackendService } from "../../../../@core/rest/bo_service";
import { RequestGroup } from "../../../../@core/enums/request-group-enum";
import { RequestType } from "../../../../@core/enums/request-type-enums";
import { MassterDataService } from "../../../../@core/service/masster-data.service";
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
} from '@nebular/theme';
import {MatDialog} from "@angular/material";
import {ElecFormulaComponent} from "../../../formulas/elec-formula/elec-formula.component";
import {UpdateAirtravelComponent} from "./update-airtravel/update-airtravel.component";
import {DeleteConfirmComponent} from "../../../delete-confirm/delete-confirm.component";
import {LocalDataSource} from "../../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";
import { UserState } from 'src/app/@core/auth/UserState';


@Component({
  selector: 'app-client-air-travel',
  templateUrl: './client-air-travel.component.html',
  styleUrls: ['./client-air-travel.component.scss']
})
export class ClientAirTravelComponent implements OnInit {
  public fetchedCount =0;
  public pg_current = 0;
  public disableNext = false;
  public disablePrev = true;
  public totalCount = 0;
  public pages = 0;

  settings = {
    mode: 'external',
    pager: {
      display: false,
    },
    actions: {
      // delete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    editor: {
      type: 'completer',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // id: {
      //   title: 'ID',
      //   type: 'number',
      //   editable: false,
      //   addable: false,
      //   filter: true,
      //   sort: true,
      //
      // },
      company: {
        title: 'Company',
        type: 'string',
        filter: true,
        sort: true,
      },
      //Todo: have to change
      branch: {
        title: 'Branch',
        type: 'string',
        filter: true,
        sort: true,

      },
      numEmps: {
        title: 'No of Employees',
        type: 'string',
        filter: true,
        sort: true,
      },
      depCountry: {
        title: 'Dept Country',
        type: 'string',
        filter: true,
        sort: true,
      },
      depAirport: {
        title: 'Dept Airport',
        type: 'string',
        filter: true,
        sort: true,
      },
      destCountry: {
        title: 'Dest Country',
        type: 'string',
        filter: true,
        sort: true,
      },
      destAirport: {
        title: 'Dept Airport',
        type: 'string',
        filter: true,
        sort: true,
      },
      transit1: {
        title: 'Transit 1',
        type: 'string',
        filter: true,
        sort: true,
      },
      transit2: {
        title: 'Transit 2',
        type: 'string',
        filter: true,
        sort: true,
      },
      transit3: {
        title: 'Transit 3',
        type: 'string',
        filter: true,
        sort: true,

      },
      isTwoWay: {
        title: 'Two way',
        type: 'string',
        filter: true,
        sort: false,
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          }
        }
      },
      seatClass: {
        title: 'Seat Class',
        type: 'string',
        filter: true,
        sort: true,
      },
      month: {
        title: 'Month',
        type: 'string',
        filter: true,
        sort: true,
      },
      year: {
        title: 'Year',
        type: 'number',
        filter: true,
        sort: true,
      },
      emission: {
        title: 'Emission (tCO2e)',
        type: 'number',
        addable: false,
        editable: false,
        filter: false,
        sort: false,
      },
    },
  };
  mySetting = JSON.parse(JSON.stringify(this.settings))




  source: LocalDataSource = new LocalDataSource();

  private filterModel = {
    company: { value: '' , type: 4, col: 3},
    branch: { value: '' , type: 4, col: 3},
    mon: { value: '' , type: 4, col: 3},
    depCoun: { value: '' , type: 4, col:3},
    destCoun: { value: '' , type: 4, col: 3},
    desPort: { value: '' , type: 4, col: 3},
    depPort: { value: '' , type: 4, col: 3},
    trans1: { value: '' , type: 4, col: 3},
    trans2: { value: '' , type: 4, col: 3},
    trans3: { value: '' , type: 4, col: 3},
    seat: { value: '' , type: 4, col: 3},
    travelWay: { value: '' , type: 4, col: 3},
    airTravelEntryId: { value: 0 , type: 1, col: 1},
    travelYear: { value: '' , type: 4, col: 3},
    noOfEmps: { value: 0 , type: 1, col: 1},
    companyId: { value: 0 , type: 1, col: 1},
  }


  private sortModel = {
    company: { dir: '' },
    branch: { dir: '' },
    mon: { dir: '' },
    depCoun: { dir: '' },
    destCoun: { dir: '' },
    desPort: { dir: '' },
    depPort: { dir: '' },
    trans1: { dir: '' },
    trans2: { dir: '' },
    trans3: { dir: '' },
    seat: { dir: '' },
    travelWay: { dir: '' },
    airTravelEntryId: { dir: '' },
    travelYear: { dir: '' },
    noOfEmps: { dir: '' },
  }



  constructor(
    private boService: BackendService,
    private masterData: MassterDataService,
    private  toastSerivce:  NbToastrService,
    private dialog: MatDialog,
  ) {
    this.selectedCompany = 0;
    // this.loadFiltered(this.pg_current);
    this.loadFiltered(this.pg_current)
    // this.source.load(data);
  }

  public  companines = [];
  public selectedCompany: number;

  ngOnInit(): void {
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companines.push({
        id: 0,
        name: 'All'
      })
      if (d !== undefined && d.length !== 0) {
       d.forEach(c => {
         this.companines.push({
           id: c.id,
           name: c.name
         });
       })
      }
      if(UserState.getInstance().adminId == 3){
        console.log("cccccccc====",this.companines.slice(0,5))
        this.companines = this.companines.slice(1,5);


      }
    })

  }
  onChangeCompany(value: any) {
    this.filterModel.companyId.value = value;
    this.loadFiltered(this.pg_current);
  }


  onUpdate(event: any, isNew:boolean) {
    // console.log(event);
    const dialogRef = this.dialog.open(UpdateAirtravelComponent,
      {
        data : {data: event.data , isNew: isNew, header: isNew ? 'New Entry': 'Edit Entry' },
        width: '700px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
        if (d) {
          this.loadFiltered(this.pg_current);
        }
    });


  }


  private loadData() {
    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    this.boService.sendRequestToBackend(
      RequestGroup.Emission,
      RequestType.ListAirtravelEntry,
      {
        PAGE_NUMBER: this.pg_current,
        FILTER_MODEL: effectiveFilter,
        SORT_MODEL: effectiveSorter,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          // console.log( data.DAT.LIST)
          this.totalCount = data.DAT.TOTAL_COUNT;
          this.pages = Math.ceil(this.totalCount/20);
          let list = [];
          this.fetchedCount = data.DAT.LIST.length;
          if (this.fetchedCount < 20) {
            this.disableNext = true;
          } else {
            this.disableNext = false;
          }
          data.DAT.LIST.forEach(val => {
            if (val != undefined) {
              list.push(this.fromListRequest(val));
            }
          });
          this.source.load(list);
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  private fromListRequest(dto: any): any {
    let company;
    let branch;
    let empIDorName;
    let depCountry;
    let depAirport
    let destCountry;
    let destAirport;
    let transit1;
    let transit2;
    let transit3;
    let isTwoWay;
    let seatClass;
    let month;
    let year;
    let id;
    // this.masterData.getBranchName(dto.BRANCH_ID).subscribe(data => { branch = data; }).unsubscribe();
    // this.masterData.getA
    // this.masterData.getCompanyName(dto.COMPANY_ID).subscribe(data => { company = data; }).unsubscribe();
    this.masterData.getCountryName(dto.DEP_COUNTRY).subscribe(data => { depCountry = data; }).unsubscribe();
    this.masterData.getCountryName(dto.DEST_COUNTRY).subscribe(data => { destCountry = data; }).unsubscribe();
    //
    // this.masterData.getAirportName(dto.DEP_AIRPORT).subscribe(data => { depAirport = data; }).unsubscribe();
    // this.masterData.getAirportName(dto.DEST_AIRPORT).subscribe(data => { destAirport = data; }).unsubscribe();
    // this.masterData.getAirportName(dto.TRANSIST_1).subscribe(data => { transit1 = data; }).unsubscribe();
    // this.masterData.getAirportName(dto.TRANSIST_2).subscribe(data => { transit2 = data; }).unsubscribe();
    // this.masterData.getAirportName(dto.TRANSIST_3).subscribe(data => { transit3 = data; }).unsubscribe();
    this.masterData.getAirTravelWay(dto.AIR_TRAVEL_WAY).subscribe(data => { isTwoWay = data }).unsubscribe();

    this.masterData.getSeatClassName(dto.SEAT_CLASS).subscribe(data => { seatClass = data; }).unsubscribe();
    this.masterData.getMonthName(dto.TRAVEL_MONTH).subscribe(data => { month = data; }).unsubscribe();
    year = dto.TRAVEL_YEAR;
    id = dto.AIR_TRAVEL_ENTRY_ID;
    empIDorName = dto.EMP_ID;
    return {
      id,
      idCom: dto.COMPANY_ID,
      idBran:dto.BRANCH_ID ,
      idDepPort: dto.DEP_AIRPORT ,
      idDesPort: dto.DEST_AIRPORT ,
      idDepCou:dto.DEP_COUNTRY ,
      idDesCou: dto.DEST_COUNTRY,
      idTran1: dto.TRANSIST_1,
      idTrans2: dto.TRANSIST_2,
      idTrans3: dto.TRANSIST_3,
      idSeat: dto.SEAT_CLASS,
      idMon: dto.TRAVEL_MONTH,
      company: dto.company,
      branch: dto.branch,
      empIDorName,
      depCountry: dto.depCoun,
      destAirport : dto.desPort,
      depAirport : dto.depPort,
      destCountry: dto.destCoun,
      transit1 : dto.trans1,
      transit2 : dto.trans2,
      transit3 : dto.trans3,
      isTwoWay,
      seatClass,
      year,
      month,
      numEmps: dto.NO_OF_EMPLOYEES,
      mon: dto.mon,
      depCoun: dto.depCoun,
      destCoun: dto.destCoun,
      destPort: dto.desPort,
      depPort: dto.depPort,
      trans1: dto.trans1,
      trans2: dto.trans2,
      trans3: dto.trans3,
      emissionDetails: dto.EMISSION_INFO,
      
      emission: isNaN(+dto.EMISSION_INFO.co2) ? 0.0 : (+dto.EMISSION_INFO.co2).toFixed(5),
      emission_info: dto.EMISSION_INFO,
      tco2: dto.EMISSION_INFO.tco2,
      co2: dto.EMISSION_INFO.co2,
   
    };


  }



  public onDelete($event: any) {
    // console.log($event)
    const dialogRef = this.dialog.open(DeleteConfirmComponent,
      {
        data : { },
        width: '300px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageAirtravelEntry,
          {
            DATA: {
              AIR_TRAVEL_ENTRY_ID: $event.data.id,
              isDeleted: 1,
            }
          }).then(data => {
            // console.log(data)
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.DTO != undefined) {
              this.loadFiltered(this.pg_current);
            }
          }
        });
      }
    });
  }

  onClickNextPage() {
    this.pg_current++;
    this.disablePrev = false;
    if (this.isFilterEnabled()) {
      return this.loadFiltered(this.pg_current)
    }
    if (this.isSorterEnabled()) {
      return this.loadSorted(this.pg_current)
    }
    this.loadFiltered(this.pg_current);
  }

  onClickPrevPage() {
    if (this.pg_current >0) {
      this.pg_current--;
      this.disablePrev = this.pg_current === 0 ? true : false;
      if (this.isFilterEnabled()) {
        return this.loadFiltered(this.pg_current)
      }
      if (this.isSorterEnabled()) {
        return this.loadSorted(this.pg_current)
      }
      this.loadFiltered(this.pg_current);
    }
  }

  private isFilterEnabled():boolean {
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (this.filterModel[k].value !== '' || this.filterModel[k].value !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  private isSorterEnabled():boolean {
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (this.sortModel[k].dir !== '' || this.sortModel[k].dir !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  onFilter($event) {
    // console.log($event)
    switch($event.query.column.id) {
      case 'id':  {
        this.filterModel.airTravelEntryId.value = +$event.query.query;
        break;
      }  case 'company':  {
        this.filterModel.company.value = $event.query.query;
        break;
      }  case 'branch':  {
        this.filterModel.branch.value = $event.query.query;
        break;
      }  case 'numEmps':  {
        this.filterModel.noOfEmps.value = +$event.query.query;
        break;
      }  case 'depCountry':  {
        this.filterModel.depCoun.value = $event.query.query;
        break;
      }  case 'depAirport':  {
        this.filterModel.depPort.value = $event.query.query;
        break;
      }  case 'destCountry':  {
        this.filterModel.destCoun.value = $event.query.query;
        break;
      }  case 'destAirport':  {
        this.filterModel.desPort.value = $event.query.query;
        break;
      }  case 'transit1':  {
        this.filterModel.trans1.value = $event.query.query;
        break;
      }  case 'transit2':  {
        this.filterModel.trans2.value = $event.query.query;
        break;
      }  case 'transit3':  {
        this.filterModel.trans3.value = $event.query.query;
        break;
      }  case 'seatClass':  {
        this.filterModel.seat.value = $event.query.query;
        break;
      }  case 'month':  {
        this.filterModel.mon.value = $event.query.query;
        break;
      }  case 'year':  {
        this.filterModel.travelYear.value = $event.query.query;
        break;
      }
      case 'isTwoWay': {
        this.filterModel.travelWay.value = $event.query.query;
        break;
      }

    }
    this.pg_current = 0;
    this.loadFiltered(0);
  }

  onSort($event) {
    this.sortModel  = {
      company: { dir: '' },
      branch: { dir: '' },
      mon: { dir: '' },
      depCoun: { dir: '' },
      destCoun: { dir: '' },
      desPort: { dir: '' },
      depPort: { dir: '' },
      trans1: { dir: '' },
      trans2: { dir: '' },
      trans3: { dir: '' },
      seat: { dir: '' },
      travelWay: { dir: '' },
      airTravelEntryId: { dir: '' },
      travelYear: { dir: '' },
      noOfEmps: { dir: '' },
    }
    // console.log($event)
    switch($event.id) {
      case 'id':  {
        this.sortModel.airTravelEntryId.dir = $event.direction;
        break;
      }  case 'company':  {
        this.sortModel.company.dir = $event.direction;
        break;
      }  case 'branch':  {
        this.sortModel.branch.dir = $event.direction;
        break;
      }  case 'numEmps':  {
        this.sortModel.noOfEmps.dir = $event.direction;
        break;
      }  case 'depCountry':  {
        this.sortModel.depCoun.dir = $event.direction;
        break;
      }  case 'depAirport':  {
        this.sortModel.depPort.dir = $event.direction;
        break;
      }  case 'destCountry':  {
        this.sortModel.destCoun.dir = $event.direction;
        break;
      }  case 'destAirport':  {
        this.sortModel.desPort.dir = $event.direction;
        break;
      }  case 'transit1':  {
        this.sortModel.trans1.dir = $event.direction;
        break;
      }  case 'transit2':  {
        this.sortModel.trans2.dir = $event.direction;
        break;
      }  case 'transit3':  {
        this.sortModel.trans3.dir = $event.direction;
        break;
      }  case 'seatClass':  {
        this.sortModel.seat.dir = $event.direction;
        break;
      }  case 'month':  {
        this.sortModel.mon.dir = $event.direction;
        break;
      }  case 'year':  {
        this.sortModel.travelYear.dir = $event.direction;
        break;
      }
      case 'isTwoWay': {
        this.sortModel.travelWay.dir = $event.direction;
        break;
      }
    }
    this.loadSorted(0)
  }

  loadFiltered(pageNum: number) {
    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }

    if (this.isSorterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListAirtravelEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });

            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListAirtravelEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }

    // this.boService.sendRequestToBackend(
    //   RequestGroup.Emission,
    //   RequestType.ListAirtravelEntry,
    //   {
    //     PAGE_NUMBER: pageNum,
    //     FILTER_MODEL: effectiveFilter,
    //   }
    // ).then(data => {
    //   if (data.HED != undefined && data.HED.RES_STS == 1) {
    //     if (data.DAT != undefined && data.DAT.LIST != undefined) {
    //       let list = [];
    //       this.totalCount = data.DAT.TOTAL_COUNT;
    //       this.pages = Math.ceil(this.totalCount/20);
    //       this.fetchedCount = data.DAT.LIST.length;
    //       if (this.fetchedCount < 20) {
    //         this.disableNext = true;
    //       } else {
    //         this.disableNext = false;
    //       }
    //       data.DAT.LIST.forEach(val => {
    //         if (val != undefined) {
    //           list.push(this.fromListRequest(val));
    //         }
    //       });
    //       this.source.load(list);
    //     } else {
    //       //  todo: show snack bar
    //       console.log('error data 1')
    //     }
    //   } else {
    //     //  todo: show snack bar
    //     console.log('error data 2')
    //   }
    // })
  }

  loadSorted(pageNum: number) {

    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    // console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    // console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }
    if(this.isFilterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListAirtravelEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }  else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListAirtravelEntry,
        {
          PAGE_NUMBER: pageNum,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            // console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 2')
        }
      })
    }
  }


}
