import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateBiogasComponent } from './update-biogas.component';

describe('UpdateBiogasComponent', () => {
  let component: UpdateBiogasComponent;
  let fixture: ComponentFixture<UpdateBiogasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateBiogasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBiogasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
