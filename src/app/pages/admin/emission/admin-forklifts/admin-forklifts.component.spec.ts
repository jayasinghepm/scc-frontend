import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminForkliftsComponent } from './admin-forklifts.component';

describe('AdminForkliftsComponent', () => {
  let component: AdminForkliftsComponent;
  let fixture: ComponentFixture<AdminForkliftsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminForkliftsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminForkliftsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
