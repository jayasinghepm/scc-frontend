import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAdminForkliftsComponent } from './update-admin-forklifts.component';

describe('UpdateAdminForkliftsComponent', () => {
  let component: UpdateAdminForkliftsComponent;
  let fixture: ComponentFixture<UpdateAdminForkliftsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAdminForkliftsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAdminForkliftsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
