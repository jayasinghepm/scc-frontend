import { Component, OnInit } from '@angular/core';
import {BackendService} from "../../../@core/rest/bo_service";
import {MassterDataService} from "../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {MatCheckboxChange} from "@angular/material/checkbox";

@Component({
  selector: 'app-manage-menu',
  templateUrl: './manage-menu.component.html',
  styleUrls: ['./manage-menu.component.scss']
})
export class ManageMenuComponent implements OnInit {
  public pages = [
    false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,
    false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false
  ]

  public emissionSources = [
    false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,
    false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false
  ]

  public allowedEmissionSources = {};

  public emissionCategories = [];

  public directOptions = [
    {id: 1, name: 'Direct'},
    {id: 2, name: 'Indirect'},
    {id: 3, name: 'None'}
  ]



  public companines: {
    id: number,
    name: '',
    pages: [],
    emissionCategories: [],
    allowedEmissionSources: any,
    emSources: []
  } [] = [];
  public companyId  = 0;
  public scopes :{id: number, name:string}[] = [{
    id: 1, name: 'Scope 1'
  }, {id: 2, name: 'Scope 2'}, {id: 3, name: 'Scope 3'}]



  
  freightKeys = {
    Air_Hired_Not_Paid:42,
    Air_Hired_Paid:43,
    Air_Rented_Not_Paid:44,
    Air_Rented_Paid:45,
    Air_Company_Own_Not_Paid:46,
    Air_Company_Own_Paid:47,
    Water_Hired_Not_Paid:48,
    Water_Hired_Paid:49,
    Water_Rented_Not_Paid:50,
    Water_Rented_Paid:51,
    Water_Company_Own_Not_Paid:52,
    Water_Company_Own_Paid:53,
    Road_Hired_Not_Paid:54,
    Road_Hired_Paid:55,
    Road_Rented_Not_Paid:56,
    Road_Rented_Paid:57,
    Road_Company_Own_Not_Paid:58,
    Road_Company_Own_Paid:59,
    Rail_Hired_Not_Paid:60,
    Rail_Hired_Paid:61,
    Rail_Rented_Not_Paid:62,
    Rail_Rented_Paid:63,
    Rail_Company_Own_Not_Paid:64,
    Rail_Company_Own_Paid:65,
    Offroad_Hired_Not_Paid:66,
    Offroad_Hired_Paid:67,
    Offroad_Rented_Not_Paid:68,
    Offroad_Rented_Paid:69,
    Offroad_Company_Own_Not_Paid:70,
    Offroad_Company_Own_Paid:71,
  }
  generatorsKeys = {
    Generator_Hired_Not_Paid: 72,
    Generator_Hired_Paid: 73,
    Generator_Rented_Not_Paid: 74,
    Generator_Rented_Paid: 75,
    Generator_Company_Own_Not_Paid: 76,
    Generator_Company_Own_Paid: 77
  }

  generatorsAccessList: {name:string, src: number, aSrc: number}[] = [ ]
  freightAccessList: {name:string, src: number, aSrc: number}[] = [];

  ngOnInit() {
    Array.from({length: 30}, () => false).forEach(a => this.emissionSources.push(a))   //  for freight Assesslist
    Array.from({length: 6}, () => false).forEach(a => this.emissionSources.push(a)) // generatorsAccessList

    Object.keys(this.freightKeys).forEach((key, index) => {
      this.freightAccessList.push({
        name: key.split('_').join(" "), 
        src: this.freightKeys[key],
        aSrc: this.freightKeys[key]
      })
    })

    Object.keys(this.generatorsKeys).forEach((key, index) => {
      this.generatorsAccessList.push({
        name: key.split('_').join(" "), 
        src: this.generatorsKeys[key], 
        aSrc: this.generatorsKeys[key]
      })
    })
  }
  

  constructor(private boService: BackendService,
              private masterData: MassterDataService,
              private toastSerivce: NbToastrService) {

    // this.companines.push({id: 0, name: 'Not Selected', valu})

    this.masterData.loadCompaniesDtos().subscribe(d => {
      this.companines = d;
      // this.companines.unshift({id: 0, name: 'Not Selected'})
    });
  }

  onChangeCompany(value: any) {
    const company = this.companines.find(com => com.id == value);
    if (company !== undefined) {
      this.fromMenusData(value, company);
      this.emissionCategories = company.emissionCategories;
      this.allowedEmissionSources = company.allowedEmissionSources;
      console.log("a-es----",this.allowedEmissionSources)
    }
  }

  onChangeCategory(value: any, srcId: number) {
     this.allowedEmissionSources[srcId].catId = value;
  }

  onChangeClassification(value: any, srcId: number) {
    this.allowedEmissionSources[srcId].direct = value ;
  }

  fromMenusData(comId: number, company) {
    if (company.pages && company.pages.length === this.pages.length) {
      this.pages = company.pages.map(menu => Boolean(menu));
    }else {

    }
    if(company. emSources && company. emSources.length === this.emissionSources.length) {
      this.emissionSources = company. emSources.map(src => Boolean(src));
    }else {

    }


  }

  toMenusData(company) {
    company.pages = this.pages.map( menu => menu ? 1: 0);
    company.emSources = this.emissionSources.map(src => src ? 1 : 0);   
  }

  onChangePageMenu($event: MatCheckboxChange, pageId: number) {
    // console.log("pid---",pageId)
    //enable and disable respective
    if (pageId == 7) { //air travel
      this.emissionSources[13] = $event.checked;

      if ($event.checked) {
        this.allowedEmissionSources[14] = {
          id: this.companyId +"-" + 14,
          srcId: 14,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,

        };
        console.log(this.allowedEmissionSources)
      }else {
        console.log(this.allowedEmissionSources)
        delete this.allowedEmissionSources[14];
        console.log(this.allowedEmissionSources)
      }
    }
    if (pageId == 8) { //fire ext
      this.emissionSources[7] = $event.checked;

      if ($event.checked) {
        this.allowedEmissionSources[8] = {
          id: this.companyId +"-" + 8,
          srcId: 8,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[8];
      }
    }
    if (pageId == 9) { //refri
      this.emissionSources[6] = $event.checked;

      if ($event.checked) {
        this.allowedEmissionSources[7] = {
          id: this.companyId +"-" + 7,
          srcId: 7,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[7];
      }
    }
    if (pageId == 10) { //generator
      this.emissionSources[5] = $event.checked;

      if ($event.checked) {

        Object.keys(this.generatorsKeys).forEach((key, index) => {
          this.allowedEmissionSources[this.generatorsKeys[key]] = {
            id: this.companyId +"-" + this.generatorsKeys[key],
            srcId: this.generatorsKeys[key],
            srcName: key.split("_").join(" "),
            comId: this.companyId,
            direct: undefined,
            catId: undefined,
            scopeId: undefined,
          };
        })   

        this.allowedEmissionSources[6] = {
          id: this.companyId +"-" + 6,
          srcId: 6,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[6];
      }
    }
    if (pageId == 11) { //elec
      this.emissionSources[12] = $event.checked;

      if ($event.checked) {
        this.allowedEmissionSources[13] = {
          id: this.companyId +"-" + 13,
          srcId: 13,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[13];
      }
    }
    if (pageId == 12) { //waste dis
      this.emissionSources[16] = $event.checked;

      if ($event.checked) {
        this.allowedEmissionSources[17] = {
          id: this.companyId +"-" + 17,
          srcId: 17,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[17];
      }
    }
    if (pageId == 13) { //mun water
      this.emissionSources[13] = $event.checked;

      if ($event.checked) {
        this.allowedEmissionSources[16] = {

          id: this.companyId +"-" + 16,
          srcId: 16,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[16];
      }
    }
    if (pageId == 17) { //tran loc pur
      this.emissionSources[2] = $event.checked;

      if ($event.checked) {
        this.allowedEmissionSources[3] = {
          id: this.companyId +"-" + 3,
          srcId: 3,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[3];
      }
    }
    if (pageId == 16) {
      this.emissionSources[18] = $event.checked;

      if ($event.checked) {
        this.allowedEmissionSources[19] = {
          id: this.companyId +"-" + 19,
          srcId: 19,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[19];
      }
    }

    if (pageId == 19) { //bio mass
      this.emissionSources[20] = $event.checked;

      if ($event.checked) {
        this.allowedEmissionSources[21] = {
          id: this.companyId +"-" + 21,
          srcId: 21,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[21];
      }
    }
    if (pageId == 20) { //lp gas
      this.emissionSources[19] = $event.checked;

      if ($event.checked) {
        this.allowedEmissionSources[20] = {
          id: this.companyId +"-" + 20,
          srcId: 20,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[20];
      }
    }
    //---------------------------------------
    if (pageId == 14) { //emp comm
      this.emissionSources[14] = $event.checked;
      this.emissionSources[10] = $event.checked;

      if ($event.checked) {
        this.allowedEmissionSources[11] = {
          id: this.companyId +"-" + 11,
          srcId: 11,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
        this.allowedEmissionSources[15] = {
          id: this.companyId +"-" + 15,
          srcId: 15,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[15];
        delete this.allowedEmissionSources[11];
      }
    }
    if (pageId == 18) { //freight

      console.log("18selected")
      this.emissionSources[0] = $event.checked;
      this.emissionSources[1] = $event.checked;

      Object.keys(this.freightKeys).forEach((key, index) => {
        this.emissionSources[this.freightKeys[key]] = $event.checked;
       })

      if ($event.checked) {
        this.allowedEmissionSources[2] = {
          id: this.companyId +"-" + 2,
          srcId: 2,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
        this.allowedEmissionSources[1] = {
          id: this.companyId +"-" + 1,
          srcId: 1,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
        Object.keys(this.freightKeys).forEach((key, index) => {
          this.allowedEmissionSources[this.freightKeys[key]] = {
            id: this.companyId +"-" + this.freightKeys[key],
            srcId: this.freightKeys[key],
            srcName: key.split("_").join(" "),
            comId: this.companyId,
            direct: undefined,
            catId: undefined,
            scopeId: undefined,
          };
        })        
      }else {
        delete this.allowedEmissionSources[1];
        delete this.allowedEmissionSources[2];
        Object.keys(this.freightKeys).forEach((key, index) => {
         delete this.allowedEmissionSources[this.freightKeys[key]] 
        })
      }

      console.log('allowedEmissionSources----',this.allowedEmissionSources)
    }
    if (pageId == 15) { //transport
      this.emissionSources[3] = $event.checked;
      this.emissionSources[4] = $event.checked;
      this.emissionSources[11] = $event.checked;
      this.emissionSources[8] = $event.checked;
      this.emissionSources[9] = $event.checked;

      if ($event.checked) {
        this.allowedEmissionSources[4] = {
          id: this.companyId +"-" + 4,
          srcId: 4,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
        this.allowedEmissionSources[5] = {
          id: this.companyId +"-" + 5,
          srcId: 5,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
        this.allowedEmissionSources[12] = {
          id: this.companyId +"-" + 12,
          srcId: 12,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
        this.allowedEmissionSources[9] = {
          id: this.companyId +"-" + 9,
          srcId: 9,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
        this.allowedEmissionSources[10] = {
          id: this.companyId +"-" + 10,
          srcId: 10,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[4];
        delete this.allowedEmissionSources[5];
        delete this.allowedEmissionSources[12];
        delete this.allowedEmissionSources[9];
        delete this.allowedEmissionSources[10];
      }
    }
   
    if (pageId == 19) //bio mass
    {
      this.emissionSources[20] = $event.checked;

      if ($event.checked) {
        this.allowedEmissionSources[21] = {
          id: this.companyId +"-" + 21,
          srcId: 21,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[21];
      }
    }
    if (pageId == 20) { //lp gas
      this.emissionSources[19] = $event.checked;

      if ($event.checked) {
        this.allowedEmissionSources[20] = {
          id: this.companyId +"-" + 20,
          srcId: 20,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[20];
      }
    }
    //biogas
    if (pageId == 21) { //bio gas
      this.emissionSources[37] = $event.checked;

      if ($event.checked) {
        this.allowedEmissionSources[38] = {
          id: this.companyId +"-" + 38,
          srcId: 38,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[38];
      }
    }
    //biogas

    if (pageId == 21) { //ash trans #emsrc 22

      if ($event.checked) {
        this.allowedEmissionSources[22] = {
          id: this.companyId +"-" + 22,
          srcId: 22,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
       delete this.allowedEmissionSources[22];
      }

    }

    if (pageId == 38) { //awaste water
      console.log('waster-water-')

      this.emissionSources[41] = $event.checked;

      if ($event.checked) {
        this.allowedEmissionSources[41] = {
          id: this.companyId +"-" + 41,
          srcId: 41,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
       delete this.allowedEmissionSources[41];
      }

    }

    
    if (pageId == 39) { //welding

      this.emissionSources[39] = $event.checked;

      if ($event.checked) {
        this.allowedEmissionSources[40] = {
          id: this.companyId +"-" + 40,
          srcId: 40,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
       delete this.allowedEmissionSources[40];
      }

    }
    if (pageId == 22) { //forklifts  #emsrc 23, 24
      if ($event.checked) {
        this.allowedEmissionSources[23] = {
          id: this.companyId +"-" + 23,
          srcId: 23,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };

        this.allowedEmissionSources[24] = {
          id: this.companyId +"-" + 24,
          srcId: 24,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[23];
        delete this.allowedEmissionSources[24];
      }
    }
    if (pageId == 23) {  //furnace oil  #emsrc 25
      if ($event.checked) {
        this.allowedEmissionSources[25] = {
          id: this.companyId +"-" + 25,
          srcId: 25,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[25];
      }
    }
    if (pageId == 24) {  // lorry trans #emsrc  26, 27
      if ($event.checked) {
        this.allowedEmissionSources[26] = {
          id: this.companyId +"-" + 26,
          srcId: 26,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
        this.allowedEmissionSources[27] = {
          id: this.companyId +"-" + 27,
          srcId: 27,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[26];
        delete this.allowedEmissionSources[27];
      }
    }
    if (pageId == 25) {  //oil gas #emsrc 30
      if ($event.checked) {
        this.allowedEmissionSources[30] = {
          id: this.companyId +"-" + 30,
          srcId: 30,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[30];
      }
    }
    if (pageId == 26) { //paid manger vehicle #emsrc 28
      if ($event.checked) {
        this.allowedEmissionSources[28] = {
          id: this.companyId +"-" + 28,
          srcId: 28,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[28];
      }
    }
    if (pageId == 27) { //paper waste #emsrc 29
      if ($event.checked) {
        this.allowedEmissionSources[29] = {
          id: this.companyId +"-" + 29,
          srcId: 29,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[29];
      }
    }
    if (pageId == 28) { //raw mat trans #emsrc 31
      if ($event.checked) {
        this.allowedEmissionSources[31] = {
          id: this.companyId +"-" + 31,
          srcId: 31,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[31];
      }
    }
    if (pageId == 29) { //saw dust trans #emsrc 34
      if ($event.checked) {
        this.allowedEmissionSources[34] = {
          id: this.companyId +"-" + 34,
          srcId: 34,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[34];
      }
    }
    if (pageId == 30) { //sludge trans #emsrc 32
      if ($event.checked) {
        this.allowedEmissionSources[32] = {
          id: this.companyId +"-" + 32,
          srcId: 32,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[32];
      }
    }
    if (pageId == 31) { //staff trans #emsrc 33
      if ($event.checked) {
        this.allowedEmissionSources[33] = {
          id: this.companyId +"-" + 33,
          srcId: 33,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[33];
      }
    }
    if (pageId == 32) { //vehicle others #emsrc 35
      if ($event.checked) {
        this.allowedEmissionSources[35] = {
          id: this.companyId +"-" + 35,
          srcId: 35,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[35];
      }
    }

  }

  onChangeEmissionSrc($event: MatCheckboxChange, srcId: number) {
     console.log(srcId)
     console.log(this.emissionSources[srcId])
     console.log(this.freightAccessList)
     console.log( Object.keys(this.freightKeys).find(key => this.freightKeys[key] === srcId))
      if ($event.checked) {
        this.allowedEmissionSources[srcId] = {
          id: this.companyId +"-" + srcId,
          srcId,
          srcName: '',
          comId: this.companyId,
          direct: undefined,
          catId: undefined,
          scopeId: undefined,
        };
      }else {
        delete this.allowedEmissionSources[srcId];
      }
  }

  updateMenus() {

    const company = this.companines.find(com =>  com.id == this.companyId);
    if (!(company !== undefined)) {
      return;
    }
    const dto = {
      id : company.id,
      pages:  this.pages.map(pg => pg ? 1 : 0),
      emSources: this.emissionSources.map(src => src ? 1 : 0),
      allowedEmissionSources : this.allowedEmissionSources,
    }
    console.log("savedto--",dto)

    this.toMenusData(company);
    console.log(company)
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ManageMenuCompany,
      {"DATA" : dto},
    ).then( data => {
      if (data !== undefined && data.HED !== undefined &&  data.HED.RES_STS!== undefined) {
        this.toastSerivce.show('', 'Updated data successfully.', {
          status: 'success',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })


        this.masterData.loadCompanies();
      } else {
        this.toastSerivce.show('', 'Error in updating.', {
          status: 'danger',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    })
  }




}
