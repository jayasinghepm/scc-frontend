import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateWasteDisposalComponent } from './update-waste-disposal.component';

describe('UpdateWasteDisposalComponent', () => {
  let component: UpdateWasteDisposalComponent;
  let fixture: ComponentFixture<UpdateWasteDisposalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateWasteDisposalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateWasteDisposalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
