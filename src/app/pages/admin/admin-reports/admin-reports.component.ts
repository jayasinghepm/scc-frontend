import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MassterDataService} from "../../../@core/service/masster-data.service";
import {BackendService} from "../../../@core/rest/bo_service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {validateEmail, validateTelephone, validateYear} from "../../../@core/utils/validator";
import {MatSelectChange, MatStepper} from "@angular/material";
import { UserState } from 'src/app/@core/auth/UserState';

class ImageSnippet {
  constructor(public src: string, public file: File) {
  }
}



@Component({
  selector: 'app-admin-reports',
  templateUrl: './admin-reports.component.html',
  styleUrls: ['./admin-reports.component.scss']
})
export class AdminReportsComponent implements OnInit {

  public companies: {id : number, name: string, allowedEmissionSources : any}[] = [];
  public reports: {name: string, url: string, type: string}[] = [];
  public reportsfilter: {name: string, url: string, type: string}[] = [];

  public projectsForReports: { id: number, name: string, status: number } [] = [];
  public projects: { id: number, name: string, status: number, comId: number } [] = [];


  public selectedCompany: any = {}

  //previous year info
  public isSelectedFy = false;
  public year = '';
  // public  previousYearEmission = {};
  // public previousYearEfs = {}
  public previousYears = [];

  public comIdMetaData = -1;
  public adminid;
  public fyCurrent = undefined;
  public emissionInfoCurrent = {
    prod: 0,
    total: 0,
    direct: 0,
    indirect: 0,
    per_capita:0,
    intensity: 0,
    waste_transport: 0,
    t_d: 0,
    waste_disposal: 0,
    mun_water: 0,
    emp_comm_not_paid: 0,
    emp_comm_paid: 0,
    air_travel: 0,
    electricity: 0,
    company_owned: 0,
    offroad: 0,
    fire_ext : 0,
    refri_leakage: 0,
    diesel_generators: 0,
    transport_loc_pur: 0,
    transport_hired_paid: 0,
    transport_hired_not_paid: 0,
    transport_rented: 0
  }


  public breakpoint = 4;

  public metaData = {
    id: -1,
    projectId: -1,
    comName_header: '',
    comName_address: '',
    addr1: '',
    addr2: '',
    district: '',
    comRegNo: '',
    email: '',
    telphone: '',
    fax: '',
    climateSi_Name: '',
    climateSi_addr1: '',
    climateSi_addr2: '',
    climateSi_district: '',
    climateSi_email: '',
    climaeSi_telephone: '',
    climatesi_fax: '',
    year: '',
    yearStartDate: '',
    yearEndDate: '',
    attempt: 0,
    proposedDate: '',
    desCompany: '',
    desResUnit: '',
    desEnvironmentalMgmntFramework: '',
    isoStandard: '',
    previousYearISOStandard: '',
    suggestions: [],
    desGHGMitigationActions: '',
    nextSteps: [],
    products: [],
    excludedSrcReasons: {
      elec: {
        elec: false,
        reason: ''
      },
      td: {
        td: false,
        reason: ''
      },
      mw: {
        mw: false,
        reason: ''
      },
      wasTrans: {
        wasTrans: false,
        reason: ''
      },
      wasteDis: {
        wastDis: false,
        reason: ''
      },
      gen: {
        gen: false,
        reason: ''
      },
      refri: {
        refri: false,
        reason: ''
      },
      fire: {
        fire: false,
        reason: ''
      },
      offRoad: {
        offRoad: false,
        reason: ''
      },
      comOwn: {
        comOwn: false,
        reason: ''
      },
      hired_paid: {
        hired_paid: false,
        reason: ''
      },
      hired_notpaid: {
        hired_notpaid: false,
        reason: ''
      },
      rented: {
        rented: false,
        reason: ''
      },
      empComm_paid: {
        empComm_paid: false,
        reason: ''
      },
      empComm_notpaid: {
        empComm_notpaid: false,
        reason: ''
      },
      air: {
        air: false,
        reason: ''
      },
      transLoc: {
        transLoc: false,
        reason: ''
      },
      airFreight: {
        airFreight: false,
        reason: ''
      },
      seaFreight: {
        seaFreight: false,
        reason: ''
      },
      bioMass: {
        bioMass: false,
        reason: ''
      },
      lpgGas: {
        lpgGas: false,
        reason: ''
      },
      ashTrans: {
        ashTrans: false,
        reason: ''
      },
      forkliftsPetrol: {
        forkliftsPetrol: false,
        reason: ''
      },
      forkliftsDiesel: {
        forkliftsDiesel: false,
        reason: ''
      },
      furnaceOil: {
        furnaceOil: false,
        reason: ''
      },

      lorryTransInternal: {
        lorryTransInternal: false,
        reason: ''
      },
      lorryTransExternal: {
        lorryTransExternal: false,
        reason: ''
      },
      paidManagerVehicle: {
        paidManagerVehicle: false,
        reason: ''
      },
      paperWaste: {
        paperWaste: false,
        reason: ''
      },
      rawMatTrans: {
        rawMatTrans: false,
        reason: ''
      },
      sawDustTrans: {
        sawDustTrans: false,
        reason: ''
      },
      sludgeTrans: {
        sludgeTrans: false,
        reason: ''
      },
      vehicleOthers: {
        vehicleOthers: false,
        reason: ''
      },
      oil_gas_trans: {
        oil_gas_trans: false,
        reason: ''
      },

    },
    figEnvMgmtFramework: '',
    figResUnit: '',
    srcEnvMgmtFramework: '',
    srcResUnit: '',
    previousYearEmission:{},
    previousYearEfs: {},
    previousYearStats: {},
    gr_dir_v_indir: '',
    gr_result_by_src : '',
    gr_pie_direct: '',
    gr_pie_indirect: '',
    gr_com_ghg: '',
    gr_com_direct_by_src: '',
    gr_com_indirect_by_src: '',
    gr_percaptia: '',
    gr_intensity: '',
    gr_per_production: '',
    num_emps: 0,
    revenue: 0,
    prod_ton: 0,
    efDTOS: [],
  }


  public efNameModal;
  public efValueModal;


  public suggestion = '';
  public product = '';
  public nextStep = '';

  public districts: string [] = [];

  private toastMsg = undefined;

  public projectStatus = -1;

  private reportZipReq = {
    projectId: -1,
    indirect_v_direct: '',
    all_src: '',
    pie_direct: '',
    pie_indirect: '',
    com_ghg_overyear: '',
    direct_over_year: '',
    indirect_over_year: '',
    capita: '',
    intensity: '',
    per_prod: '',
    metadata: this.metaData,
  }

  public jsonbodyUploadReport = {
    projectId: undefined,
    comId: undefined,
    pdf: undefined,
    reportType: -1,
  }

  public jsonBodyInitCharts = {
    num_emps: 0,
    revenue: 0,
    prod_ton: 0,
  }

  public disableNext = false;



  //charts options
  public chart1_options = undefined;
  public chart2_options = undefined;
  public chart3_options = undefined;
  public chart4_options = undefined;
  public chart5_options = undefined;
  public chart6_options = undefined;
  public chart7_options = undefined;
  public chart8_options = undefined;
  public chart9_options = undefined;
  public chart10_options = undefined;


  @ViewChild('stepper', {static: false}) private myStepper: MatStepper;
  @ViewChild('chart1', {static: false}) private chart1: ElementRef;
  @ViewChild('chart2', {static: false}) private chart2: ElementRef;
  @ViewChild('chart3', {static: false}) private chart3: ElementRef;
  @ViewChild('chart4', {static: false}) private chart4: ElementRef;
  @ViewChild('chart5', {static: false}) private chart5: ElementRef;
  @ViewChild('chart6', {static: false}) private chart6: ElementRef;
  @ViewChild('chart7', {static: false}) private chart7: ElementRef;
  @ViewChild('chart8', {static: false}) private chart8: ElementRef;
  @ViewChild('chart9', {static: false}) private chart9: ElementRef;
  @ViewChild('chart10', {static: false}) private chart10: ElementRef;

  public completed: boolean[] = [false, false, false, false, false, false, false, false];

  public totSteps = 11;
  public selctedIndex = 0;



  constructor(private masterData: MassterDataService,
              private boService: BackendService,
              private toastSerivce: NbToastrService) {
    this.masterData.loadProjects().subscribe(d => {
      // console.log(d);
      this.projects = d;
    })
    this.masterData.getDistricts().subscribe(d => {
      this.districts = d;
    });
    this.initCharts();
    this.initReports();
  }

  ngOnInit() {

    this.adminid =   UserState.getInstance().adminId;
    console.log("adminid--",this.adminid)

    this.breakpoint = (window.innerWidth <= 400) ? 1 : 6;
    // console.log(this.chart1)
  }

  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 400) ? 1 : 6;
  }

  onAddSuggestion($event) {
    if (this.suggestion == undefined || this.suggestion === '') {
      return;
    }
    this.metaData.suggestions.push(this.suggestion);
  }

  onAddNextStep($event) {
    // console.log(this.nextStep)
    if (this.nextStep == undefined || this.nextStep === '') {
      return;
    }
    this.metaData.nextSteps.push(this.nextStep)
  }

  onAddEmissionFactor($event) {
    // console.log(this.nextStep)
    if (this.efNameModal == undefined || this.efValueModal === '' || this.efValueModal == undefined
    || this.efValueModal === '') {
      return;
    }
    this.metaData.efDTOS.push({nameWithUnit: this.efNameModal , value: this.efValueModal})
  }

  onAddProduct($event) {
    if (this.product == undefined || this.product === '') {
      return;
    }
    this.metaData.products.push(this.product)
  }


  onSave(download: boolean) {
    const jsonBody = {DATA: this.metaData}

    if (this.validate()) {
      this.boService.sendRequestToBackend(RequestGroup.Report, RequestType.ManageReportMeta, jsonBody).then(
        data => {
          if (data !== undefined && data.DAT !== undefined && data.DAT.DATA !== undefined) {
            //todo:

          } else {
            //todo show error
            // console.log("Error in Report manage Datat")
          }
        }
      )
    } else {
      if (this.toastMsg === undefined) {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
        return;
      } else {
        this.toastSerivce.show('', this.toastMsg, {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
        return;
      }
    }

  }


  onLoad() {
    const jsonBody = {
      FILTER_MODEL: {
        projectId: {value: this.metaData.projectId, type: 1, col: 1}
      }
    }

    this.boService.sendRequestToBackend(RequestGroup.Report, RequestType.ListReportMeta, jsonBody).then(
      data => {

        if (data !== undefined && data.DAT !== undefined && data.DAT.LIST !== undefined) {
          const dto = data.DAT.LIST[0];
          if (dto !== undefined) {
            for (let [key, value] of Object.entries(dto)) {
              this.metaData[key] = value;
            }
          }


        }
      }
    )
  }

  onSelectProject($event) {

    const comId = this.projects.filter(p => p.id === $event.value)[0].comId;
    this.comIdMetaData = comId;
    this.selectedCompany = this.companies.find(com => com.id == comId);

    this.masterData.isFy(comId).subscribe(d =>
      {
        this.isSelectedFy = d;
      }
    );

    console.log(this.projects.filter(v => {
      return v.id === $event.value
    }))
    this.projectStatus = this.projects.filter(v => {
      return v.id === $event.value
    })[0].status;
    this.metaData = {
      id: -1,
      projectId: this.metaData.projectId,
      comName_header: '',
      comName_address: '',
      addr1: '',
      addr2: '',
      district: '',
      comRegNo: '',
      email: '',
      telphone: '',
      fax: '',
      climateSi_Name: '',
      climateSi_addr1: '',
      climateSi_addr2: '',
      climateSi_district: '',
      climateSi_email: '',
      climaeSi_telephone: '',
      climatesi_fax: '',
      year: '',
      yearStartDate: '',
      yearEndDate: '',
      attempt: 0,
      proposedDate: '',
      desCompany: '',
      desResUnit: '',
      desEnvironmentalMgmntFramework: '',
      isoStandard: '',
      previousYearISOStandard: '',
      suggestions: [],
      desGHGMitigationActions: '',
      nextSteps: [],
      products: [],
      excludedSrcReasons: {
        elec: {
          elec: false,
          reason: ''
        },
        td: {
          td: false,
          reason: ''
        },
        mw: {
          mw: false,
          reason: ''
        },
        wasTrans: {
          wasTrans: false,
          reason: ''
        },
        wasteDis: {
          wastDis: false,
          reason: ''
        },
        gen: {
          gen: false,
          reason: ''
        },
        refri: {
          refri: false,
          reason: ''
        },
        fire: {
          fire: false,
          reason: ''
        },
        offRoad: {
          offRoad: false,
          reason: ''
        },
        comOwn: {
          comOwn: false,
          reason: ''
        },
        hired_paid: {
          hired_paid: false,
          reason: ''
        },
        hired_notpaid: {
          hired_notpaid: false,
          reason: ''
        },
        rented: {
          rented: false,
          reason: ''
        },
        empComm_paid: {
          empComm_paid: false,
          reason: ''
        },
        empComm_notpaid: {
          empComm_notpaid: false,
          reason: ''
        },
        air: {
          air: false,
          reason: ''
        },
        transLoc: {
          transLoc: false,
          reason: ''
        },
        airFreight: {
          airFreight: false,
          reason: ''
        },
        seaFreight: {
          seaFreight: false,
          reason: ''
        },
        bioMass: {
          bioMass: false,
          reason: ''
        },
        lpgGas: {
          lpgGas: false,
          reason: ''
        },
        ashTrans: {
          ashTrans: false,
          reason: ''
        },
        forkliftsPetrol: {
          forkliftsPetrol: false,
          reason: ''
        },
        forkliftsDiesel: {
          forkliftsDiesel: false,
          reason: ''
        },
        furnaceOil: {
          furnaceOil: false,
          reason: ''
        },

        lorryTransInternal: {
          lorryTransInternal: false,
          reason: ''
        },
        lorryTransExternal: {
          lorryTransExternal: false,
          reason: ''
        },
        paidManagerVehicle: {
          paidManagerVehicle: false,
          reason: ''
        },
        paperWaste: {
          paperWaste: false,
          reason: ''
        },
        rawMatTrans: {
          rawMatTrans: false,
          reason: ''
        },
        sawDustTrans: {
          sawDustTrans: false,
          reason: ''
        },
        sludgeTrans: {
          sludgeTrans: false,
          reason: ''
        },
        vehicleOthers: {
          vehicleOthers: false,
          reason: ''
        },
        oil_gas_trans: {
          oil_gas_trans: false,
          reason: ''
        },
      },
      figEnvMgmtFramework: '',
      figResUnit: '',
      srcEnvMgmtFramework: '',
      srcResUnit: '',
      previousYearEmission:{},
      previousYearEfs: {},
      previousYearStats: {},
      gr_dir_v_indir: '',
      gr_result_by_src : '',
      gr_pie_direct: '',
      gr_pie_indirect: '',
      gr_com_ghg: '',
      gr_com_direct_by_src: '',
      gr_com_indirect_by_src: '',
      gr_percaptia: '',
      gr_intensity: '',
      gr_per_production: '',
      num_emps: 0,
      revenue: 0,
      prod_ton: 0,
      efDTOS: [],
    }
    this.onLoad()
  }

  validate(): boolean {
    this.toastMsg = undefined;
    // console.log(this.metaData)

    //todo id  validate with edit flag

    if (this.metaData.projectId == undefined || this.metaData.projectId === -1) {
      this.toastMsg = "Select Project First";
      return false;
    }
    if (this.metaData.comName_header == undefined || this.metaData.comName_header === '') {
      this.toastMsg = "Fill Empty Fields."
      return false;

    }
    if (this.metaData.comName_header == undefined || this.metaData.comName_header === '') {
      this.toastMsg = "Fill Empty Fields";
      return false;
    }
    if (this.metaData.addr1 == undefined || this.metaData.addr1 == '') {
      this.toastMsg = 'Fill Empty Fields'
      return false;
    }
    if (this.metaData.addr2 == undefined || this.metaData.addr2 == '') {
      this.toastMsg = 'Fill Empty Fields'
      return false;
    }
    if (this.metaData.district == undefined || this.metaData.district === '') {
      this.toastMsg = 'Fill Empty Fields'
      return false;
    }
    if (this.metaData.comRegNo == undefined || this.metaData.comRegNo === '') {
      this.toastMsg = "Fill Empty Fields"
      return false;
    }
    // Todo: phone, fax, email
    if (this.metaData.email === '' || this.metaData.email == undefined) {
      return false;
    } else if (!validateEmail(this.metaData.email)) {
      this.toastMsg = 'Enter Valid Email';
      return false;
    }
    if (this.metaData.telphone === '' || this.metaData.telphone == undefined) {
      return false;
    } else if (!validateTelephone(this.metaData.telphone)) {
      this.toastMsg = 'Enter Valid Telephone';
      return false;
    }
    if (this.metaData.fax === '' || this.metaData.fax == undefined) {
      return false;
    } else if (!validateTelephone(this.metaData.fax)) {
      this.toastMsg = 'Enter Valid Fax';
      return false;
    }


    if (this.metaData.climateSi_Name === undefined || this.metaData.climateSi_Name === '') {
      this.toastMsg = "Fill Empty Fields"
      return false;
    }

    if (this.metaData.climateSi_addr1 === undefined || this.metaData.climateSi_addr1 === '') {
      this.toastMsg = "Fill Empty Fields"
      return false;
    }

    if (this.metaData.climateSi_addr2 === undefined || this.metaData.climateSi_addr2 === '') {
      this.toastMsg = "Fill Empty Fields"
      return false;
    }

    // todo: telephone, fax, email
    if (this.metaData.climateSi_email === '' || this.metaData.climateSi_email == undefined) {
      return false;
    } else if (!validateEmail(this.metaData.climateSi_email)) {
      this.toastMsg = 'Enter Valid Email';
      return false;
    }
    if (this.metaData.climaeSi_telephone === '' || this.metaData.climaeSi_telephone == undefined) {
      return false;
    } else if (!validateTelephone(this.metaData.climaeSi_telephone)) {
      this.toastMsg = 'Enter Valid Telephone';
      return false;
    }
    if (this.metaData.climatesi_fax === '' || this.metaData.climatesi_fax == undefined) {
      return false;
    } else if (!validateTelephone(this.metaData.climatesi_fax)) {
      this.toastMsg = 'Enter Valid Fax';
      return false;
    }

    if (this.metaData.year === undefined || this.metaData.year === '') {
      this.toastMsg = "Fill Empty Fields"
      return false;
    }

    // if (this.metaData.yearStartDate === undefined || this.metaData.yearStartDate === '') {
    //   this.toastMsg = "Fill Empty Fields"
    //   return false;
    // }
    // if (this.metaData.yearEndDate === undefined || this.metaData.yearEndDate === '') {
    //   this.toastMsg = "Fill Empty Fields"
    //   return false;
    // }
    if (this.metaData.proposedDate === undefined || this.metaData.proposedDate === '') {
      this.toastMsg = "Fill Empty Fields"
      return false;
    }
    if (this.metaData.desCompany === undefined || this.metaData.desCompany === '') {
      this.toastMsg = "Fill Empty Fields"
      return false;
    }
    if (this.metaData.desResUnit === undefined || this.metaData.desResUnit === '') {
      this.toastMsg = "Fill Empty Fields"
      return false;
    }

    if (this.metaData.desEnvironmentalMgmntFramework === undefined || this.metaData.desEnvironmentalMgmntFramework == '') {
      this.toastMsg = "Fill Empty Fields"
      return false;
    }
    if (this.metaData.isoStandard === undefined || this.metaData.isoStandard === '') {
      this.toastMsg = "Fill Empty Fields"
      return false;
    }
    if (this.metaData.previousYearISOStandard === undefined || this.metaData.previousYearISOStandard === '') {
      this.toastMsg = "Fill Empty Fields"

      return false;
    }
    if (this.metaData.suggestions === undefined || this.metaData.suggestions.length === 0) {
      this.toastMsg = 'Add Suggestion'
      return false;
    }
    if (this.metaData.desGHGMitigationActions === undefined || this.metaData.desGHGMitigationActions === '') {

      return false;
    }
    if (this.metaData.nextSteps === undefined || this.metaData.nextSteps.length === 0) {
      this.toastMsg = 'Add Next Steps'
      return false;
      ;
    }
    if (this.metaData.excludedSrcReasons.elec.elec && (this.metaData.excludedSrcReasons.elec.reason == undefined || this.metaData.excludedSrcReasons.elec.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.td.td && (this.metaData.excludedSrcReasons.td.reason == undefined || this.metaData.excludedSrcReasons.td.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.mw.mw && (this.metaData.excludedSrcReasons.mw.reason == undefined || this.metaData.excludedSrcReasons.mw.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.wasTrans.wasTrans && (this.metaData.excludedSrcReasons.wasTrans.reason == undefined || this.metaData.excludedSrcReasons.wasTrans.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.wasteDis.wastDis && (this.metaData.excludedSrcReasons.wasteDis.reason == undefined || this.metaData.excludedSrcReasons.wasteDis.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.gen.gen && (this.metaData.excludedSrcReasons.gen.reason == undefined || this.metaData.excludedSrcReasons.gen.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.refri.refri && (this.metaData.excludedSrcReasons.refri.reason == undefined || this.metaData.excludedSrcReasons.refri.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.fire.fire && (this.metaData.excludedSrcReasons.fire.reason == undefined || this.metaData.excludedSrcReasons.fire.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.offRoad.offRoad && (this.metaData.excludedSrcReasons.offRoad.reason == undefined || this.metaData.excludedSrcReasons.offRoad.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.comOwn.comOwn && (this.metaData.excludedSrcReasons.comOwn.reason == undefined || this.metaData.excludedSrcReasons.comOwn.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.hired_paid.hired_paid && (this.metaData.excludedSrcReasons.hired_paid.reason == undefined || this.metaData.excludedSrcReasons.hired_paid.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.hired_notpaid.hired_notpaid && (this.metaData.excludedSrcReasons.hired_notpaid.reason == undefined || this.metaData.excludedSrcReasons.hired_notpaid.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.rented.rented && (this.metaData.excludedSrcReasons.rented.reason == undefined || this.metaData.excludedSrcReasons.rented.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.empComm_paid.empComm_paid && (this.metaData.excludedSrcReasons.empComm_paid.reason == undefined || this.metaData.excludedSrcReasons.empComm_paid.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.empComm_notpaid.empComm_notpaid && (this.metaData.excludedSrcReasons.empComm_notpaid.reason == undefined || this.metaData.excludedSrcReasons.empComm_notpaid.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.air.air && (this.metaData.excludedSrcReasons.air.reason == undefined || this.metaData.excludedSrcReasons.air.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.transLoc.transLoc && (this.metaData.excludedSrcReasons.transLoc.reason == undefined || this.metaData.excludedSrcReasons.transLoc.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }

    if (this.metaData.excludedSrcReasons.seaFreight.seaFreight && (this.metaData.excludedSrcReasons.seaFreight.reason == undefined || this.metaData.excludedSrcReasons.seaFreight.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.airFreight.airFreight && (this.metaData.excludedSrcReasons.airFreight.reason == undefined || this.metaData.excludedSrcReasons.airFreight.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.bioMass.bioMass && (this.metaData.excludedSrcReasons.bioMass.reason == undefined || this.metaData.excludedSrcReasons.bioMass.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.lpgGas.lpgGas && (this.metaData.excludedSrcReasons.lpgGas.reason == undefined || this.metaData.excludedSrcReasons.lpgGas.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.ashTrans.ashTrans && (this.metaData.excludedSrcReasons.ashTrans.reason == undefined || this.metaData.excludedSrcReasons.ashTrans.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.forkliftsPetrol.forkliftsPetrol && (this.metaData.excludedSrcReasons.forkliftsPetrol.reason == undefined || this.metaData.excludedSrcReasons.forkliftsPetrol.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.forkliftsDiesel.forkliftsDiesel && (this.metaData.excludedSrcReasons.forkliftsDiesel.reason == undefined || this.metaData.excludedSrcReasons.forkliftsDiesel.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.furnaceOil.furnaceOil && (this.metaData.excludedSrcReasons.furnaceOil.reason == undefined || this.metaData.excludedSrcReasons.furnaceOil.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.lorryTransInternal.lorryTransInternal && (this.metaData.excludedSrcReasons.lorryTransInternal.reason == undefined || this.metaData.excludedSrcReasons.lorryTransInternal.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.lorryTransExternal.lorryTransExternal && (this.metaData.excludedSrcReasons.lorryTransExternal.reason == undefined || this.metaData.excludedSrcReasons.lorryTransExternal.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.paidManagerVehicle.paidManagerVehicle && (this.metaData.excludedSrcReasons.paidManagerVehicle.reason == undefined || this.metaData.excludedSrcReasons.paidManagerVehicle.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.paperWaste.paperWaste && (this.metaData.excludedSrcReasons.paperWaste.reason == undefined || this.metaData.excludedSrcReasons.paperWaste.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.rawMatTrans.rawMatTrans && (this.metaData.excludedSrcReasons.rawMatTrans.reason == undefined || this.metaData.excludedSrcReasons.rawMatTrans.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.sawDustTrans.sawDustTrans && (this.metaData.excludedSrcReasons.sawDustTrans.reason == undefined || this.metaData.excludedSrcReasons.sawDustTrans.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.sludgeTrans.sludgeTrans && (this.metaData.excludedSrcReasons.sludgeTrans.reason == undefined || this.metaData.excludedSrcReasons.sludgeTrans.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.vehicleOthers.vehicleOthers && (this.metaData.excludedSrcReasons.vehicleOthers.reason == undefined || this.metaData.excludedSrcReasons.vehicleOthers.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }
    if (this.metaData.excludedSrcReasons.oil_gas_trans.oil_gas_trans && (this.metaData.excludedSrcReasons.oil_gas_trans.reason == undefined || this.metaData.excludedSrcReasons.oil_gas_trans.reason === '')) {
      this.toastMsg = 'Add Reasons to  Excludes Emission Sources '
      return false;
    }

    if (this.metaData.figResUnit === undefined || this.metaData.figResUnit === '') {
      this.toastMsg = 'Add Responsible Unit Figure';
      return false;
    }
    if (this.metaData.figEnvMgmtFramework === undefined || this.metaData.figEnvMgmtFramework === ''){
      this.toastMsg = 'Add Environmental Management Framework'
      return false;
    }
    if (this.metaData.srcEnvMgmtFramework === undefined || this.metaData.srcEnvMgmtFramework === '') {
      this.toastMsg = 'Enter Source: Environmental Management Framework'
      return false;
    }
    if (this.metaData.srcResUnit === undefined || this.metaData.srcResUnit === '') {
      this.toastMsg = 'Enter Source: Responsible Unit'
      return false;
    }
    if (this.metaData.num_emps === 0 ) {
      this.toastMsg = 'Enter Valid Number of Employees';
      return false;
    }
    if (this.metaData.revenue === 0) {
      this.toastMsg = 'Enter Valid Revenue';
      return false;
    }


    if (this.metaData.efDTOS.length === 0) {
      this.toastMsg = 'Enter Emission Factors';
      return false;
    }
    return true;

  }


  initCharts(): void {

    // all src
    this.chart1_options = {
      title: {
        text: 'GHG emissions of Financial year 2018/19',
        left: '25%',
        right: 'auto'
      },
      yAxis: {
        type: 'category',
        name: 'Source of Emission',
        nameLocation: 'center',
        nameGap: 320,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'left',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },

        data: [
          'Waste Transportation',
          'T & D Loss',
          'Waste Disposal',
          'Municipal Water',
          'Employee Commuting Not Paid by the Company',
          'Business Air Travels',
          'Grid Connected Electricity',
          'Transport Locally Purchased',
          'Vehicle Hired, Not Paid',
          'Employee Commuting, Paid by the Company',
          'Company Owned Vehicles',
          'Off-Road Mobile Sources and Machinery-Forklifts',
          'Fire Extinguishers',
          'Refirgerant Leakage',
          'Onsite Diesel Generators',
          'Vehicle Hired, Paid',
          'Vehicle Rented'
        ]
      },
      xAxis: {
        type: 'value',
        name: 'GHG Emission (tCO2e)',
        nameGap: 30,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
        },
        nameLocation: 'center',
        axisLine: {
          show: false
        },
        axisTick: {
          show: false
        },
        axisLabel: {
          textStyle: {
            color: '#999'
          }
        }
      },
      grid: {
        left: '40%',
      },
      dataZoom: [
        {
          type: 'inside'
        }
      ],
      series: [
        { // For shadow
          type: 'bar',
          itemStyle: {
            normal: {color: 'rgba(0,0,0,0.05)'}
          },
          barGap: '-100%',
          barCategoryGap: '40%',
          data: [],
          animation: false
        },
        {
          type: 'bar',
          itemStyle: {
            normal: {
              color: new echarts.graphic.LinearGradient(
                0, 0, 0, 1,
                [
                  {offset: 0, color: '#83bff6'},
                  {offset: 0.5, color: '#188df0'},
                  {offset: 1, color: '#188df0'}
                ]
              )
            },
            emphasis: {
              color: new echarts.graphic.LinearGradient(
                0, 0, 0, 1,
                [
                  {offset: 0, color: '#2378f7'},
                  {offset: 0.7, color: '#2378f7'},
                  {offset: 1, color: '#83bff6'}
                ]
              )
            }
          },
          data: [220, 182, 191, 234, 290, 330, 310, 123, 442, 321, 90, 149, 210, 122, 133, 123, 12],
        }
      ]
    };
    //direct indirect
    this.chart2_options = {
      title: {
        text: 'Orgnaizational GHG emissions',
        left: '40%',
        right: '10%'
      },
      xAxis: {


        axisTick: {
          show: false
        },
        axisLine: {
          show: false
        },
        type: 'category',
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
        data: ['Direct', 'Indirect']
      },
      yAxis: {
        axisLine: {
          show: false
        },
        axisTick: {
          show: false
        },
        axisLabel: {
          textStyle: {
            color: '#999'
          }
        },
        name: 'GHG Emission(tCO2e)',
        nameGap: 50,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
        },
        nameLocation: 'center',
        type: 'value'
      },
      dataZoom: [
        {
          type: 'inside'
        }
      ],
      series: [
        { // For shadow
          type: 'bar',
          itemStyle: {
            normal: {color: 'rgba(0,0,0,0.05)'}
          },
          barGap: '-100%',
          barCategoryGap: '40%',
          data: [],
          animation: false
        },
        {
          type: 'bar',
          itemStyle: {
            normal: {
              color: new echarts.graphic.LinearGradient(
                0, 0, 0, 1,
                [
                  {offset: 0, color: '#83bff6'},
                  {offset: 0.5, color: '#188df0'},
                  {offset: 1, color: '#188df0'}
                ]
              )
            },
            emphasis: {
              color: new echarts.graphic.LinearGradient(
                0, 0, 0, 1,
                [
                  {offset: 0, color: '#2378f7'},
                  {offset: 0.7, color: '#2378f7'},
                  {offset: 1, color: '#83bff6'}
                ]
              )
            }
          },
          data: [{
            value: 200,
            itemStyle: {color: '#1abc9c'},
          },
            {
              value: 11,
              itemStyle: {color: '#2980b9'},
            },]
        }
      ]
    };
    //pie direct src
    this.chart3_options = {
      title: {
        text: 'Direct GHG emissions over the Financial Year 2018/1',

        x: 'center'
      },
      tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
      },
      legend: {
        // type: 'scroll',
        orient: 'vertical',
        right: 10,
        top: 20,
        bottom: 20,
        data: ['Transport Hired (Paid)',
          'Transport Rented',
          'Transport Locally Purchased',
          'Transport Company Owned Vehicles',
          'Onsite Diesel Generator',
          'Refrigerant Leakage',
          'Fire Extinguishers',
          'Off-road Vehicles',
          'Employee Commuting (Paid)',

        ],


      },
      series: [
        {
          name: '',
          type: 'pie',
          radius: '55%',
          center: ['40%', '50%'],
          data: [
            {
              name: 'Transport Hired (Paid)',
              value: 33
            },
            {
              name: 'Transport Rented',
              value: 23
            },
            {
              name: 'Transport Locally Purchased',
              value: 22
            },
            {
              name: 'Transport Company Owned Vehicles',
              value: 25
            },
            {
              name: 'Onsite Diesel Generator',
              value: 22
            },
            {
              name: 'Refrigerant Leakage',
              value: 26
            },
            {
              name: 'Fire Extinguishers',
              value: 12
            },
            {
              name: 'Off-road Vehicles',
              value: 11
            },
            {
              name: 'Employee Commuting (Paid)',
              value: 63
            },

          ],
          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
          }
        }
      ]
    };
    //pie indirect src
    this.chart4_options = {
      title: {
        text: 'Indirect GHG emissions over the Financial Year 2018/1',

        x: 'center'
      },
      tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
      },
      legend: {
        // type: 'scroll',
        orient: 'vertical',
        right: 10,
        top: 20,
        bottom: 20,
        data: ['Transport Hired (Not Paid)',
          'Grid Connected Electricity',
          'T and D Loss',
          'Business Air Travel',
          'Employee Commuting (Not Paid)',
          'Waste Disposal',
          'Waste Transport',
          'Municipal Water',

        ],


      },
      series: [
        {
          name: '',
          type: 'pie',
          radius: '55%',
          center: ['40%', '50%'],
          data: [
            {
              name: 'Transport Hired (Paid)',
              value: 33
            },
            {
              name: 'Grid Connected Electricity',
              value: 23
            },
            {
              name: 'T and D Loss',
              value: 22
            },
            {
              name: 'Business Air Travel',
              value: 25
            },
            {
              name: 'Employee Commuting (Not Paid)',
              value: 22
            },
            {
              name: 'Waste Disposal',
              value: 26
            },
            {
              name: 'Waste Transport',
              value: 12
            },
            {
              name: 'Municipal Water',
              value: 11
            },
          ],
          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
          }
        }
      ]
    };
    //com years di vs indire
    this.chart5_options = {
      title: {
        text: 'Comparison of GHG emissions over the years',
        left: '20%'
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      legend: {
        data: ['Direct', 'Indirect'],
        right: '10%',
        top: '5%',
        bottom: '5%',
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      yAxis: {
        type: 'value',
        name: 'GHG Emission (tCO2e)',

        nameLocation: 'center',
        nameGap: 60,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'center',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
        boundaryGap: [0, 0.01]
      },
      xAxis: {
        nameGap: 20,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
        },
        nameLocation: 'center',
        name: 'Year',
        type: 'category',
        data: ['2015/16', '2016/17', '2017/18', '2018/19',]
      },
      series: [
        {
          name: 'Direct',
          type: 'bar',
          data: [18203, 23489, 29034, 104970,]
        },
        {
          name: 'Indirect',
          type: 'bar',
          data: [19325, 23438, 31000, 121594,]
        }
      ]
    };
    //direct com
    this.chart6_options = {
      title: {
        text: 'Direct GHG emissions over the financial years',
        left: '20%',
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      legend: {
        data: ['2015/16', '2017/18', '2019/20'],
        right: '10%',
        top: '5%'
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '13%',
        containLabel: true
      },
      yAxis: {
        name: 'GHG Emission (tCO2e)',

        nameLocation: 'center',
        nameGap: 40,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'center',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
      },
      xAxis: {
        name: 'Source of Emission',
        type: 'category',

        nameGap: 230,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
        },
        nameLocation: 'center',
        axisLabel: {
          rotate: -90,
        },
        data: ['Transport Hired (Paid)'
          , 'Transport Rented',
          'Transport Locally Purchased',
          'Transport Company Owned Vehicles',
          'Onsite Diesel Generato',
          'Refrigerant Leakage',
          'Fire Extinguishers',
          'Off-road Vehicles',
          'Employee Commuting (Paid)',

        ]
      },
      series: [
        {
          name: '2015/16',
          type: 'bar',
          data: [18, 23, 29, 10, 131, 63, 20, 20, 20]
        },
        {
          name: '2017/18',
          type: 'bar',
          data: [19, 238, 31, 12, 13, 68, 20, 15, 15]
        },
        {
          name: '2019/20',
          type: 'bar',
          data: [19, 23, 31, 12, 13, 68, 20, 17, 10]
        }
      ]
    };
    //indirect compar
    this.chart7_options = {
      title: {
        text: 'Indirect GHG emissions over the financial years',
        left: '20%',
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      legend: {
        data: ['2015/16', '2017/18', '2019/20'],
        right: '10%',
        top: '5%'
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '20%',
        containLabel: true
      },
      yAxis: {
        name: 'GHG Emission (tCO2e)',

        nameLocation: 'center',
        nameGap: 40,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'center',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
      },
      xAxis: {
        name: 'Source of Emission',
        type: 'category',

        nameGap: 230,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
        },
        nameLocation: 'center',
        axisLabel: {
          rotate: -90,
        },
        data: ['Transport Hired (Not Paid)'
          , 'Grid Connected Electricity',
          'T and D Loss}',
          'Business Air Travel',
          'Employee Commuting (Not Paid)',
          'Waste Disposal',
          'Waste Transport',
          'Municipal Water',


        ]
      },
      series: [
        {
          name: '2015/16',
          type: 'bar',
          data: [18, 23, 29, 10, 131, 63, 20, 20]
        },
        {
          name: '2017/18',
          type: 'bar',
          data: [19, 238, 31, 12, 13, 68, 20, 15]
        },
        {
          name: '2019/20',
          type: 'bar',
          data: [19, 23, 31, 12, 13, 68, 20, 17]
        }
      ]
    };
    //per captia
    this.chart8_options = {
      title: {
        text: 'Per Capita Emissions (tCO2e)',
        left: '30%',
      },
      xAxis: {
        data: ['2015/16', '2017/18', '2019/20',],
        type: 'category',
        name: 'Year',
        nameLocation: 'center',
        nameGap: 30,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'left',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
      },
      yAxis: {
        type: 'value',
        name: 'Emissions (tCO2e)',
        nameLocation: 'center',
        nameGap: 55,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'center',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
      },
      series: [{
        data: [820, 932, 901,],
        type: 'line'
      }]
    };
    //intensity
    this.chart9_options = {
      title: {
        text: 'Emission Intensity (tCO2e/ LKR)',
        left: '30%',
      },
      xAxis: {
        data: ['2015/16', '2017/18', '2019/20',],
        type: 'category',
        name: 'Year',
        nameLocation: 'center',
        nameGap: 30,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'left',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
      },
      yAxis: {
        type: 'value',
        name: 'Emissions (tCO2e/ LKR)',
        nameLocation: 'center',
        nameGap: 55,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'center',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
      },
      series: [{
        data: [820, 932, 901,],
        type: 'line'
      }]
    };
    //per prod
    this.chart10_options = {
      title: {
        text: 'Emissions per ton of production (tCO2e)',
        left: '30%',
      },
      xAxis: {
        data: ['2015/16', '2017/18', '2019/20',],
        type: 'category',
        name: 'Year',
        nameLocation: 'center',
        nameGap: 30,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'left',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
      },
      yAxis: {
        type: 'value',
        name: 'Emissions (tCO2e)',
        nameLocation: 'center',
        nameGap: 55,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'center',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
      },
      series: [{
        data: [820, 932, 901,],
        type: 'line'
      }]
    };

  }


  goForward() {

    if (this.selctedIndex == 0) {
        return this.initCharts_bo()
      // this.onSave(false);
      return;
    }


    this.selctedIndex++;
    this.myStepper.next();


    // console.log(this.reportZipReq)
  }

  getReport() {
    this.reportZipReq.projectId = this.metaData.projectId;
    //@ts-ignore
    if ( this.metaData.prod_ton  === undefined || this.metaData.prod_ton === '') {
      this.metaData.prod_ton = 0;
    }
    this.reportZipReq.metadata =  this.metaData;

    // console.log(this.reportZipReq)

    // this.jsonBodyInitCharts.num_emps = this.metaData.num_emps;
    // this.jsonBodyInitCharts.prod_ton = this.metaData.prod_ton;
    // this.jsonBodyInitCharts.revenue = this.metaData.revenue;
    //
    // for (let k of Object.keys(this.metaData.excludedSrcReasons)) {
    //   for (let j  of Object.keys(this.metaData.excludedSrcReasons[k])) {
    //     if (this.metaData.excludedSrcReasons[k][j] === true || this.metaData.excludedSrcReasons[k][j]  === false) {
    //       this.jsonBodyInitCharts[k] = this.metaData.excludedSrcReasons[k][j];
    //       break;
    //     }
    //   }
    // }

    let jsonBody = JSON.parse(JSON.stringify(this.reportZipReq));
    jsonBody['params'] = this.jsonBodyInitCharts;
    console.log(jsonBody)

    this.boService.sendRequestToBackend(RequestGroup.Report, 5, jsonBody)
      .then(data => {
        // console.log(data);
        if (data !== undefined && data.DAT !== undefined) {
          const url = data.DAT.s3url;
          // console.log(url);
          // window.open(url, "_blank");
          if (url !== undefined && url !== '') {
            // console.log('opening')
            this.dowloadFile(url)
            // window.open(url, "_blank");
            // this.boService.downloadS3Objects(url);
          }else {
            this.toastSerivce.show('', 'Error in  Generating Report files.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            });
            return;
          }
        }else {
          this.toastSerivce.show('', 'Error in  Generating Report files.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          });
          return;
        }
      });
  }

  dowloadFile(url: string) {
    var a = document.createElement('a');
    a.href = url;
    // a.download = "";
    a.click();
    a.remove()
  }

  public onStepChange($event) {
    // console.log($event)
    // this.selctedIndex = this.myStepper.selectedIndex;
    this.selctedIndex = $event.selectedIndex;
    // console.log('********************************************************************')
    // console.log(this.chart1)
    // console.log( this.chart1.nativeElement)
    // console.log( this.chart1.nativeElement.children[0].children)
    // console.log(this.chart1.nativeElement.children[0].children[0].toDataURL())
    // console.log(this.selctedIndex)
    // console.log('*********************************************************************')
    if (this.selctedIndex == 1) {
      // console.log('********************************************************************')
      // console.log(this.chart1)
      // console.log( this.chart1.nativeElement)
      // console.log( this.chart1.nativeElement.children[0].children)
      // console.log(this.chart1.nativeElement.children[0].children[0].toDataURL())
      // console.log('*********************************************************************')
      setTimeout(() =>   {
        this.reportZipReq.all_src = this.chart1.nativeElement.children[0].children[0].toDataURL();

      }, 2000)

    } else if (this.selctedIndex == 2) {
      setTimeout(() =>   {
        this.reportZipReq.indirect_v_direct = this.chart2.nativeElement.children[0].children[0].toDataURL();
      }, 2000)


    } else if (this.selctedIndex == 3) {
      setTimeout(() =>   {
        this.reportZipReq.pie_direct = this.chart3.nativeElement.children[0].children[0].toDataURL();
      }, 2000)


    } else if (this.selctedIndex == 4) {
      setTimeout(() =>   {
        this.reportZipReq.pie_indirect = this.chart4.nativeElement.children[0].children[0].toDataURL();
      }, 2000)


    } else if (this.selctedIndex == 5) {
      setTimeout(() =>   {
        this.reportZipReq.com_ghg_overyear = this.chart5.nativeElement.children[0].children[0].toDataURL();
      }, 2000)


    } else if (this.selctedIndex == 6) {
      setTimeout(() =>   {
        this.reportZipReq.direct_over_year = this.chart6.nativeElement.children[0].children[0].toDataURL();
      }, 2000)


    } else if (this.selctedIndex == 7) {
      setTimeout(() =>   {
        this.reportZipReq.indirect_over_year = this.chart7.nativeElement.children[0].children[0].toDataURL();
      }, 2000)


    } else if (this.selctedIndex == 8) {
      setTimeout(() =>   {
        this.reportZipReq.capita = this.chart8.nativeElement.children[0].children[0].toDataURL();
      }, 2000)

    } else if (this.selctedIndex == 9) {
      setTimeout(() =>   {
        this.reportZipReq.intensity = this.chart9.nativeElement.children[0].children[0].toDataURL();
      }, 2000)

    } else if (this.selctedIndex == 10) {
      setTimeout(() =>   {
        this.reportZipReq.per_prod = this.chart10.nativeElement.children[0].children[0].toDataURL();
      }, 2000)

    }
    // console.log(this.reportZipReq)
  }

  public loadChartData(): void {

  }

  public onFileChanged(pdf: any): void {
    const file: File = pdf.target.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      const pdfsrc = new ImageSnippet(event.target.result, file);
      if (pdfsrc !== undefined) {
        this.jsonbodyUploadReport.pdf = pdfsrc.src;
        // console.log(this.jsonbodyUploadReport.pdf)
        // if (this.comAdvInfo.logo !== '' && this.comAdvInfo.logo !== undefined) {
        //   this.logoImage = this.comAdvInfo.logo;
        //   this.logoAvailable = true;
        //   // this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.companyBasicInfo.logo)
        // }
      }
    });

    reader.readAsDataURL(file);
  }

  uploadReport() {
    if (this.jsonbodyUploadReport.projectId >0 && this.jsonbodyUploadReport.comId>0 && this.jsonbodyUploadReport.pdf !== undefined
      && this.jsonbodyUploadReport.pdf !== '' && this.jsonbodyUploadReport.reportType !== undefined && this.jsonbodyUploadReport.reportType !== -1) {

      this.boService.sendRequestToBackend(
        RequestGroup.Report,
        RequestType.ManageReport,
        {
          DATA: this.jsonbodyUploadReport
        }
      ).then(data => {
        // console.log(data)
        if (data !== undefined && data.DAT !== undefined && data.DAT.DATA !== undefined) {
          this.loadReport();
          this.boService.sendRequestToBackend(
          RequestGroup.GHGProject,
            RequestType.ManageProject,
            {DATA: {
                id:  this.jsonbodyUploadReport.projectId,
                status: (this.jsonbodyUploadReport.reportType === 1 ? 6 : (this.jsonbodyUploadReport.reportType === 2?  7: -1)),
                companyId: this.jsonbodyUploadReport.comId,
                statusStr: (this.jsonbodyUploadReport.reportType === 1 ? 'Draft Report' : (this.jsonbodyUploadReport.reportType === 2?  'Final Report': -1)),
              }}
        ).then(data => {
            // console.log(data)
            if (data.HED != undefined && data.HED.RES_STS == 1) {
              if (data.DAT != undefined && data.DAT.DTO != undefined) {
                // console.log(data.DAT.DTO)
                this.toastSerivce.show('', 'Report Uploaded Successfully', {
                  status: 'success',
                  destroyByClick: true,
                  duration: 2000,
                  hasIcon: false,
                  position: NbGlobalPhysicalPosition.TOP_RIGHT,
                  preventDuplicates: true,
                });
                return;

              }else {
                this.toastSerivce.show('', 'Error in  Uploading Report', {
                  status: 'danger',
                  destroyByClick: true,
                  duration: 2000,
                  hasIcon: false,
                  position: NbGlobalPhysicalPosition.TOP_RIGHT,
                  preventDuplicates: true,
                });
                return;
              }
            }else {
              this.toastSerivce.show('', 'Error in  Uploading Report', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              });
              return;
            }
          });
        }
      })


    } else {
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }
  }

  loadReport() {
   
    this.reports = []
    let reportFilter:any = {
      FILTER_MODEL: "",
      PAGE_NUMBER: -1,
    };
    if (this.jsonbodyUploadReport.comId === -1) {
      reportFilter = {
        FILTER_MODEL: "",
        PAGE_NUMBER: -1,
      };
    }else {
      reportFilter = {
        FILTER_MODEL: {
          comId: { value: this.jsonbodyUploadReport.comId , type: 1, col: 1}
        },
        PAGE_NUMBER: -1,
      }
    }
    console.log('reportFilter',reportFilter)

    this.boService.sendRequestToBackend(
      RequestGroup.Report,
      RequestType.ListReport,
      reportFilter
    ).then(data => {
      if (data !== undefined && data.DAT !== undefined && data.DAT.LIST !== undefined) {
        data.DAT.LIST.forEach(r => {
          let name;
          if (r.pdf !== undefined){
            let list = r.pdf.split('.com/');
            if (list.length >= 2) {
              name = list[1];
            }else {
              name = "Report";
            }
          }
          if (name !== undefined) {
             if(UserState.getInstance().adminId == 3 && reportFilter.FILTER_MODEL == ""){
            this.reports = []
            this.reports.push({
              "name": "PLC CFP 2021/22.pdf",
              "type": "Final",
              "url": "https://ghgreport.s3.ap-south-1.amazonaws.com/PLC CFP 2021/22.pdf"
          },
          {
            "name": "PLC CFP 2021/22.pdf",
            "type": "Final",
            "url": "https://ghgreport.s3.ap-south-1.amazonaws.com/PLC CFP 2021/22.pdf"
        })    
          }else{
            this.reports.push({
              name:name,
              type: r.reportType === 1? 'Draft': (r.reportType ===2 ? 'Final': ''),
              url: r.pdf,
            })
          }
         
          }


        })
      }
    })

  }


  onChangeCompany($event :MatSelectChange) {


    console.log('changed company----',$event)

    this.boService.sendRequestToBackend(
      RequestGroup.GHGProject,
      RequestType.ListProject,
      {
      FILTER_MODEL: {
        companyId: { value: this.jsonbodyUploadReport.comId , type: 1, col: 1},
        status: { value: 7, type: 2, col: 1}
      },
      PAGE_NUMBER: -1,
    }
      ).then(data => {
        if (data !== undefined && data.DAT !== undefined && data.DAT.LIST !== undefined) {
          data.DAT.LIST.forEach(p => {
            if (p.status >= 5) {
              this.projectsForReports.push({
                id: p.id,
                name: p.name,
                status: p.status,
              });
            }
          })
        }
    });
    this.loadReport();
  }

  initReports() {
    this.companies.push({id: -1, name: 'All', allowedEmissionSources: {}});
    this.masterData.getCompaninesFull().subscribe(d => {
      // console.log(d);
      this.companies.push(...d);
      if(UserState.getInstance().adminId == 3){
        this.companies = this.companies.slice(68,70);


      }
    })
    this.jsonbodyUploadReport.comId = -1;
    this.loadReport();
  }


  onClickDownloadReport(url: string) {
    window.open(url, "_blank");
  }

  public onAddPreviousYear() {
    //validate
    // console.log(this.isSelectedFy);
    // console.log(this.year);
    if (!validateYear(this.year, this.isSelectedFy)) {
      this.toastSerivce.show('', 'Enter Valid Previous Year', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }


    this.metaData.previousYearEmission[this.year] = {

      e_trans_hired_not_paid :0,
      e_elec: 0,
      e_emp_comm_not_paid: 0,
      e_t_d : 0,
      e_wasted_dis: 0,
      e_mw: 0,
      e_waste_trans: 0,
      e_trans_hired_paid: 0,
      e_trans_rented: 0,
      e_trans_loc_pur: 0,
      e_com_owned: 0,
      e_gen: 0,
      e_refri: 0,
      e_fire: 0,
      e_offroad: 0,
      e_emp_comm_paid: 0,
      e_air_travel: 0
    };
    this.metaData.previousYearEfs[this.year] = {
      grid: 0,
      mw: 0,
      td_loss: 0
    }

    this.metaData.previousYearStats[this.year] = {
      capita: 0,
      prod: 0,
      intensity: 0,
      t_direct: 0,
      t_indirect: 0,
    }

    this.previousYears = Object.keys(this.metaData.previousYearEmission).sort();
  }

  public onFileChangedFigure(image: any, type: number): void {
    const file: File = image.target.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      const image =new ImageSnippet(event.target.result, file);
      if (image) {
        if (type === 1) {
          this.metaData.figEnvMgmtFramework = image.src;
        }else if (type === 2) {
          this.metaData.figResUnit = image.src;
        }
        // console.log(image);
      }
    });

    reader.readAsDataURL(file);
  }


  //chart1
  public init_chart_by_all_src() {
    const data = [];
    const categories = [];
    // console.log(this.metaData.excludedSrcReasons)
    // console.log(this.fyCurrent)
    //todo: change value
    if (!this.metaData.excludedSrcReasons.wasteDis.wastDis){
        categories.push('Waste Disposal')
      data.push({
        value: this.emissionInfoCurrent.waste_disposal,
        itemStyle: {color: '#ffff00'},
      },)
    }
    if (!this.metaData.excludedSrcReasons.wasTrans.wasTrans) {
      categories.push('Waste Transportation')
      data.push({
        value: this.emissionInfoCurrent.waste_transport,
        itemStyle: {color: '#ffff00'},
      },)
    }


    if (!this.metaData.excludedSrcReasons.elec.elec) {
      categories.push('Grid Connected Electricity')
      data.push({
        value: this.emissionInfoCurrent.electricity,
        itemStyle: {color: '#ffff00'},
      },)
    }
    if (!this.metaData.excludedSrcReasons.air.air) {
      categories.push('Business Air Travels')
      data.push({
        value: this.emissionInfoCurrent.air_travel,
        itemStyle: {color: '#ffff00'},
      },)
    }
    if (!this.metaData.excludedSrcReasons.empComm_notpaid.empComm_notpaid) {
      categories.push('Employee Commuting,Not Paid by the Company')
      data.push({
        value: this.emissionInfoCurrent.emp_comm_not_paid,
        itemStyle: {color: '#ffff00'},
      },)
    }
    if (!this.metaData.excludedSrcReasons.mw.mw) {
      categories.push('Municipal Water')
      data.push({
        value: this.emissionInfoCurrent.mun_water,
        itemStyle: {color: '#ffff00'},
      },)
    }

    if (!this.metaData.excludedSrcReasons.hired_notpaid.hired_notpaid) {
      categories.push('Vehicle Hired, Not Paid')
      data.push({
        value: this.emissionInfoCurrent.transport_hired_not_paid,
        itemStyle: {color: '#ffff00'},
      },)
    }
    if (!this.metaData.excludedSrcReasons.td.td) {
      categories.push('T & D Loss')
      data.push({
        value: this.emissionInfoCurrent.t_d,
        itemStyle: {color: '#ffff00'},
      },)
    }




    if (!this.metaData.excludedSrcReasons.refri.refri){
      categories.push('Refirgerant Leakage')
      data.push({
        value: this.emissionInfoCurrent.refri_leakage,
        itemStyle: {color: '#001f60'},
      },)
    }
    if (!this.metaData.excludedSrcReasons.fire.fire) {
      categories.push( 'Fire Extinguishers')
      data.push({
        value: this.emissionInfoCurrent.fire_ext,
        itemStyle: {color: '#001f60'},
      },)
    }

    if (!this.metaData.excludedSrcReasons.rented.rented) {
      categories.push('Vehicle Rented')
      data.push({
        value: this.emissionInfoCurrent.transport_rented,
        itemStyle: {color: '#001f60'},
      },)
    }
    if (!this.metaData.excludedSrcReasons.hired_paid.hired_paid) {
      categories.push('Vehicle Hired, Paid')
      data.push({
        value: this.emissionInfoCurrent.transport_hired_paid,
        itemStyle: {color: '#001f60'},
      },)
    }

    if (!this.metaData.excludedSrcReasons.offRoad.offRoad) {
      categories.push('Off-Road Mobile Sources and Machinery-Forklifts')
      data.push({
        value: this.emissionInfoCurrent.offroad,
        itemStyle: {color: '#001f60'},
      },)
    }
    if (!this.metaData.excludedSrcReasons.comOwn.comOwn) {
      categories.push('Company Owned Vehicles')
      data.push({
        value: this.emissionInfoCurrent.company_owned,
        itemStyle: {color: '#001f60'},
      },)
    }
    if (!this.metaData.excludedSrcReasons.empComm_paid.empComm_paid) {
      categories.push('Employee Commuting, Paid by the Company')
      data.push({
        value: this.emissionInfoCurrent.emp_comm_paid,
        itemStyle: {color: '#001f60'},
      },)
    }

    if (!this.metaData.excludedSrcReasons.transLoc.transLoc) {
      categories.push('Transport Locally Purchased')
      data.push({
        value: this.emissionInfoCurrent.transport_loc_pur,
        itemStyle: {color: '#001f60'},
      },)
    }
    if (!this.metaData.excludedSrcReasons.gen.gen) {
      categories.push('Onsite Diesel Generators')
      data.push({
        value: this.emissionInfoCurrent.diesel_generators,
        itemStyle: {color: '#001f60'},
      },)
    }


    this.chart1_options =  {
      title: {
        text: 'GHG emissions of ' + (this.isSelectedFy ? 'Financial Year ' : 'Year ')  +  this.fyCurrent,
          left: '25%',
          right: 'auto'
      },
      yAxis: {
        type: 'category',
          name: 'Source of Emission',
          nameLocation: 'center',
          nameGap: 320,
          nameTextStyle: {
          fontWeight: 'bold',
            fontSize: 14,
            align: 'left',
            rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },

        data: categories,
        // data: [
        //   'Waste Transportation',
        //   'T & D Loss',
        //   'Waste Disposal',
        //   'Municipal Water',
        //   'Employee Commuting Not Paid by the Company',
        //   'Business Air Travels',
        //   'Grid Connected Electricity',
        //   'Transport Locally Purchased',
        //   'Vehicle Hired, Not Paid',
        //   'Employee Commuting, Paid by the Company',
        //   'Company Owned Vehicles',
        //   'Off-Road Mobile Sources and Machinery-Forklifts',
        //   'Fire Extinguishers',
        //   'Refirgerant Leakage',
        //   'Onsite Diesel Generators',
        //   'Vehicle Hired, Paid',
        //   'Vehicle Rented'
        // ]
      },
      xAxis: {
        type: 'value',
          name: 'GHG Emission (tCO2e)',
          nameGap: 30,
          nameTextStyle: {
          fontWeight: 'bold',
            fontSize: 14,
        },
        nameLocation: 'center',
          axisLine: {
          show: false
        },
        axisTick: {
          show: false
        },
        axisLabel: {
          textStyle: {
            color: '#999'
          }
        }
      },
      grid: {
        left: '40%',
      },
      dataZoom: [
        {
          type: 'inside'
        }
      ],
        series: [
      { // For shadow
        type: 'bar',
        itemStyle: {
          normal: {color: 'rgba(0,0,0,0.05)'}
        },
        barGap: '-100%',
        barCategoryGap: '40%',
        data: [],
        animation: false
      },
      {
        type: 'bar',

        data: data,
      }
    ]
    };

  //   itemStyle: {
    //           normal: {
    //             color: new echarts.graphic.LinearGradient(
    //               0, 0, 0, 1,
    //               [
    //                 {offset: 0, color: '#83bff6'},
    //                 {offset: 0.5, color: '#188df0'},
    //                 {offset: 1, color: '#188df0'}
    //               ]
    //             )
    //           },
    //           emphasis: {
    //             color: new echarts.graphic.LinearGradient(
    //               0, 0, 0, 1,
    //               [
    //                 {offset: 0, color: '#2378f7'},
    //                 {offset: 0.7, color: '#2378f7'},
    //                 {offset: 1, color: '#83bff6'}
    //               ]
    //             )
    //           }
    //         },

  }

  //chart2
  public init_direct_indirect() {
    this.chart2_options = {
      title: {
        text: 'Orgnaizational GHG emissions',
        left: '40%',
        right: '10%'
      },
      xAxis: {
        axisTick: {
          show: false
        },
        axisLine: {
          show: false
        },
        type: 'category',
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
        data: ['Direct', 'Indirect']
      },
      yAxis: {
        axisLine: {
          show: false
        },
        axisTick: {
          show: false
        },
        axisLabel: {
          textStyle: {
            color: '#999'
          }
        },
        name: 'GHG Emission(tCO2e)',
        nameGap: 80,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
        },
        nameLocation: 'center',
        type: 'value'
      },
      grid: {
        left: '10%',
        right: '4%',
        bottom: '13%',
        containLabel: true
      },
      dataZoom: [
        {
          type: 'inside'
        }
      ],
      series: [
        { // For shadow
          type: 'bar',
          itemStyle: {
            normal: {color: 'rgba(0,0,0,0.05)'}
          },
          barGap: '-100%',
          barCategoryGap: '40%',
          data: [],
          animation: false
        },
        {
          type: 'bar',

          data: [{
            value: this.emissionInfoCurrent.direct,
            itemStyle: {color: '#001f60'},
          },
            {
              value: this.emissionInfoCurrent.indirect,
              itemStyle: {color: '#ffff00'},
            },]
        }
      ]
    };

  //  itemStyle: {
    //             normal: {
    //               color: new echarts.graphic.LinearGradient(
    //                 0, 0, 0, 1,
    //                 [
    //                   {offset: 0, color: '#83bff6'},
    //                   {offset: 0.5, color: '#188df0'},
    //                   {offset: 1, color: '#188df0'}
    //                 ]
    //               )
    //             },
    //             emphasis: {
    //               color: new echarts.graphic.LinearGradient(
    //                 0, 0, 0, 1,
    //                 [
    //                   {offset: 0, color: '#2378f7'},
    //                   {offset: 0.7, color: '#2378f7'},
    //                   {offset: 1, color: '#83bff6'}
    //                 ]
    //               )
    //             }
    //           },
  }

  //chart3
  public init_pie_direct() {
    const categories = [];
    const data = [];


    if (!this.metaData.excludedSrcReasons.refri.refri){
      categories.push('Refirgerant Leakage')
      data.push({
        name: 'Refirgerant Leakage',
        value: this.emissionInfoCurrent.refri_leakage
      },)
    }
    if (!this.metaData.excludedSrcReasons.fire.fire) {
      categories.push( 'Fire Extinguishers')
      data.push({
        name: 'Fire Extinguishers',
        value: this.emissionInfoCurrent.fire_ext
      },)
    }

    if (!this.metaData.excludedSrcReasons.rented.rented) {
      categories.push('Vehicle Rented')
      data.push( {
        name: 'Vehicle Rented',
        value: this.emissionInfoCurrent.transport_rented,
      },)
    }
    if (!this.metaData.excludedSrcReasons.hired_paid.hired_paid) {
      categories.push('Vehicle Hired, Paid')
      data.push( {
        name: 'Vehicle Hired, Paid',
        value: this.emissionInfoCurrent.transport_hired_paid,
      },)
    }

    if (!this.metaData.excludedSrcReasons.offRoad.offRoad) {
      categories.push('Off-Road Mobile Sources and Machinery-Forklifts')
      data.push( {
        name: 'Off-Road Mobile Sources and Machinery-Forklifts',
        value: this.emissionInfoCurrent.offroad,
      },)
    }
    if (!this.metaData.excludedSrcReasons.comOwn.comOwn) {
      categories.push('Company Owned Vehicles')
      data.push( {
        name: 'Company Owned Vehicles',
        value: this.emissionInfoCurrent.company_owned
      },)
    }
    if (!this.metaData.excludedSrcReasons.empComm_paid.empComm_paid) {
      categories.push('Employee Commuting, Paid by the Company')
      data.push( {
        name: 'Employee Commuting, Paid by the Company',
        value: this.emissionInfoCurrent.emp_comm_paid
      },)
    }

    if (!this.metaData.excludedSrcReasons.transLoc.transLoc) {
      categories.push('Transport Locally Purchased')
      data.push( {
        name: 'Transport Locally Purchased',
        value: this.emissionInfoCurrent.transport_loc_pur
      },)
    }
    if (!this.metaData.excludedSrcReasons.gen.gen) {
      categories.push('Onsite Diesel Generators')
      data.push( {
        name: 'Onsite Diesel Generators',
        value: this.emissionInfoCurrent.diesel_generators
      },)
    }

    this.chart3_options = {
      title: {
        text: 'Direct GHG emissions over the ' + (this.isSelectedFy ? 'Financial Year' : 'Year ')  +  this.fyCurrent,

        x: 'center'
      },
      tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
      },
      legend: {
        // type: 'scroll',
        orient: 'vertical',
        right: 10,
        top: 40,
        bottom: 20,
        // data: ['Transport Hired (Paid)',
        //   'Transport Rented',
        //   'Transport Locally Purchased',
        //   'Transport Company Owned Vehicles',
        //   'Onsite Diesel Generator',
        //   'Refrigerant Leakage',
        //   'Fire Extinguishers',
        //   'Off-road Vehicles',
        //   'Employee Commuting (Paid)',
        //
        // ],
        data: categories,
      },
      series: [
        {
          name: '',
          type: 'pie',
          radius: '55%',
          center: ['40%', '50%'],
          label: {
            formatter: ' {d}%',
          },
          labelLine: {
            normal: {
              show: true
            }
          },
          // data: [
          //   {
          //     name: 'Transport Hired (Paid)',
          //     value: 33
          //   },
          //   {
          //     name: 'Transport Rented',
          //     value: 23
          //   },
          //   {
          //     name: 'Transport Locally Purchased',
          //     value: 22
          //   },
          //   {
          //     name: 'Transport Company Owned Vehicles',
          //     value: 25
          //   },
          //   {
          //     name: 'Onsite Diesel Generator',
          //     value: 22
          //   },
          //   {
          //     name: 'Refrigerant Leakage',
          //     value: 26
          //   },
          //   {
          //     name: 'Fire Extinguishers',
          //     value: 12
          //   },
          //   {
          //     name: 'Off-road Vehicles',
          //     value: 11
          //   },
          //   {
          //     name: 'Employee Commuting (Paid)',
          //     value: 63
          //   },
          //
          // ],
          data: data,
          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
          }
        }
      ]
    };
  }

  //chart4
  public init_pie_indirect() {
    const data = [];
    const categories = [];
    if (!this.metaData.excludedSrcReasons.wasteDis.wastDis){
      categories.push('Waste Disposal')
      data.push({
        name: 'Waste Disposal',
        value: this.emissionInfoCurrent.waste_disposal,
      },)
    }
    if (!this.metaData.excludedSrcReasons.wasTrans.wasTrans) {
      categories.push('Waste Transportation')
      data.push({
        name: 'Waste Transportation',
        value: this.emissionInfoCurrent.waste_transport
      },)
    }


    if (!this.metaData.excludedSrcReasons.elec.elec) {
      categories.push('Grid Connected Electricity')
      data.push({
        name: 'Grid Connected Electricity',
        value: this.emissionInfoCurrent.electricity
      },)
    }
    if (!this.metaData.excludedSrcReasons.air.air) {
      categories.push('Business Air Travels')
      data.push({
        name: 'Business Air Travels',
        value: this.emissionInfoCurrent.air_travel
      },)
    }
    if (!this.metaData.excludedSrcReasons.empComm_notpaid.empComm_notpaid) {
      categories.push('Employee Commuting, Not Paid by the Company')
      data.push({
        name: 'Employee Commuting, Not Paid by the Company',
        value: this.emissionInfoCurrent.emp_comm_not_paid
      },)
    }
    if (!this.metaData.excludedSrcReasons.mw.mw) {
      categories.push('Municipal Water')
      data.push({
        name: 'Municipal Water',
        value: this.emissionInfoCurrent.mun_water
      },)
    }

    if (!this.metaData.excludedSrcReasons.hired_notpaid.hired_notpaid) {
      categories.push('Vehicle Hired, Not Paid')
      data.push({
        name: 'Vehicle Hired, Not Paid',
        value: this.emissionInfoCurrent.transport_hired_not_paid
      },)
    }
    if (!this.metaData.excludedSrcReasons.td.td) {
      categories.push('T & D Loss')
      data.push({
        name: 'T & D Loss',
        value: this.emissionInfoCurrent.t_d
      },)
    }

    this.chart4_options = {
      title: {
        text: 'Indirect GHG emissions over the  '+ (this.isSelectedFy ? 'Financial Year' : 'Year ')  +  this.fyCurrent,
        x: 'center'
      },
      tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
      },
      legend: {
        // type: 'scroll',
        orient: 'vertical',
        right: 10,
        top: 40,
        bottom: 20,
        // data: ['Transport Hired (Not Paid)',
        //   'Grid Connected Electricity',
        //   'T and D Loss',
        //   'Business Air Travel',
        //   'Employee Commuting (Not Paid)',
        //   'Waste Disposal',
        //   'Waste Transport',
        //   'Municipal Water',
        //
        // ],
        data: categories,

      },
      series: [
        {
          name: '',
          type: 'pie',
          radius: '55%',
          center: ['40%', '50%'],
          label: {
            formatter: ' {d}%',
          },
          labelLine: {
            normal: {
              show: true
            }
          },
          data: data,
          // data: [
          //   {
          //     name: 'Transport Hired (Paid)',
          //     value: 33
          //   },
          //   {
          //     name: 'Grid Connected Electricity',
          //     value: 23
          //   },
          //   {
          //     name: 'T and D Loss',
          //     value: 22
          //   },
          //   {
          //     name: 'Business Air Travel',
          //     value: 25
          //   },
          //   {
          //     name: 'Employee Commuting (Not Paid)',
          //     value: 22
          //   },
          //   {
          //     name: 'Waste Disposal',
          //     value: 26
          //   },
          //   {
          //     name: 'Waste Transport',
          //     value: 12
          //   },
          //   {
          //     name: 'Municipal Water',
          //     value: 11
          //   },
          // ],
          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
          }
        }
      ]
    };

  }

// chart 5
  public init_com_ghg_years() {
    const direct = [];
    const indirect = [];
    if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
      const d = this.metaData.previousYearStats[this.previousYears[0]].t_direct;
      const ind = this.metaData.previousYearStats[this.previousYears[0]].t_indirect;
      direct.push(d)
      indirect.push(ind)

      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const d = this.metaData.previousYearStats[this.previousYears[1]].t_direct;
        const ind = this.metaData.previousYearStats[this.previousYears[1]].t_indirect;
        direct.push(d)
        indirect.push(ind)

      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const d = this.metaData.previousYearStats[this.previousYears[2]].t_direct;
        const ind = this.metaData.previousYearStats[this.previousYears[2]].t_indirect;
        direct.push( d)
        indirect.push( ind)

      }
      const years = [...this.previousYears, this.fyCurrent]

      //todo: push current
      direct.push(this.emissionInfoCurrent.direct)
      indirect.push(this.emissionInfoCurrent.indirect)

    this.chart5_options = {
      title: {
        text: 'Comparison of GHG emissions over the years',
        left: '20%'
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      legend: {
        data: ['Direct', 'Indirect'],
        right: '10%',
        top: '5%',
        bottom: '5%',
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      yAxis: {
        type: 'value',
        name: 'GHG Emission (tCO2e)',

        nameLocation: 'center',
        nameGap: 60,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'center',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
        boundaryGap: [0, 0.01]
      },
      xAxis: {
        nameGap: 20,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
        },
        nameLocation: 'center',
        name: 'Year',
        type: 'category',
        // data: ['2015/16', '2016/17', '2017/18', '2018/19',]
        data: years,
      },
      series: [
        {
          name: 'Direct',
          type: 'bar',
          data: direct,
          itemStyle: {
            color: '#001f60'
          }
        },
        {
          name: 'Indirect',
          type: 'bar',
          data: indirect,
          itemStyle: {
            color: '#ffff00'
          }
        }
      ]
    };
    }


  //chart6
  public init_com_direct_years() {
    const years = [...this.previousYears, this.fyCurrent]
    const categories = [];
    const data = [];
    const series = [
      {
        name: years[0],
        type: 'bar',
        data: [],
        itemStyle: {
          color: '#27ae60',
        }
      },
      {
        name: years[1],
        type: 'bar',
        data: [],
        itemStyle: {
          color: '#2980b9',
        }
      },
      {
        name: years[2],
        type: 'bar',
        data: [],
        itemStyle: { color: '#f1c40f' }
      },
      {
        name: this.fyCurrent,
        type: 'bar',
        data: [],
        itemStyle: { color: '#e67e22' }
      }
    ]
    //todo:

    //todo: add current to year to series
    if (!this.metaData.excludedSrcReasons.refri.refri){
      categories.push('Refirgerant Leakage')
      if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[0])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[0]].e_refri);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[1])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[1]].e_refri);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[2])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[2]].e_refri);
        }
      }

      const s = series.filter(s => s.name === this.fyCurrent)[0];
      if (s !== undefined) {
        s.data.push(this.emissionInfoCurrent.refri_leakage);
      }
    }
    if (!this.metaData.excludedSrcReasons.fire.fire) {
      categories.push( 'Fire Extinguishers')
      if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[0])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[0]].e_fire);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[1])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[1]].e_fire);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[2])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[2]].e_fire);
        }
      }
      const s = series.filter(s => s.name === this.fyCurrent)[0];
      if (s !== undefined) {
        s.data.push(this.emissionInfoCurrent.fire_ext);
      }

    }
    if (!this.metaData.excludedSrcReasons.rented.rented) {
      categories.push('Vehicle Rented')
      if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[0])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[0]].e_trans_rented);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[1])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[1]].e_trans_rented);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[2])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[2]].e_trans_rented);
        }
      }
      const s = series.filter(s => s.name === this.fyCurrent)[0];
      if (s !== undefined) {
        s.data.push(this.emissionInfoCurrent.transport_rented);
      }
    }
    if (!this.metaData.excludedSrcReasons.hired_paid.hired_paid) {
      categories.push('Vehicle Hired, Paid')
      if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[0])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[0]].e_trans_hired_paid);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[1])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[1]].e_trans_hired_paid);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[2])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[2]].e_trans_hired_paid);
        }
      }

      const s = series.filter(s => s.name === this.fyCurrent)[0];
      if (s !== undefined) {
        s.data.push(this.emissionInfoCurrent.transport_hired_paid);
      }
    }

    if (!this.metaData.excludedSrcReasons.offRoad.offRoad) {
      categories.push('Off-Road Mobile Sources and Machinery-Forklifts')
      if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[0])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[0]].e_offroad);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[1])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[1]].e_offroad);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[2])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[2]].e_offroad);
        }
      }
      const s = series.filter(s => s.name === this.fyCurrent)[0];
      if (s !== undefined) {
        s.data.push(this.emissionInfoCurrent.offroad);
      }
    }
    if (!this.metaData.excludedSrcReasons.comOwn.comOwn) {
      categories.push('Company Owned Vehicles')
      if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[0])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[0]].e_com_owned);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[1])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[1]].e_com_owned);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[2])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[2]].e_com_owned);
        }
      }
      const s = series.filter(s => s.name === this.fyCurrent)[0];
      if (s !== undefined) {
        s.data.push(this.emissionInfoCurrent.company_owned);
      }

    }
    if (!this.metaData.excludedSrcReasons.empComm_paid.empComm_paid) {
      categories.push('Employee Commuting, Paid by the Company')

      if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[0])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[0]].e_emp_comm_paid);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[1])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[1]].e_emp_comm_paid);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[2])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[2]].e_emp_comm_paid);
        }
      }
      const s = series.filter(s => s.name === this.fyCurrent)[0];
      if (s !== undefined) {
        s.data.push(this.emissionInfoCurrent.emp_comm_paid);
      }
    }

    if (!this.metaData.excludedSrcReasons.transLoc.transLoc) {
      categories.push('Transport Locally Purchased')
      if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[0])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[0]].e_trans_loc_pur);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[1])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[1]].e_trans_loc_pur);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[2])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[2]].e_trans_loc_pur);
        }
      }
      const s = series.filter(s => s.name === this.fyCurrent)[0];
      if (s !== undefined) {
        s.data.push(this.emissionInfoCurrent.transport_loc_pur);
      }
    }
    if (!this.metaData.excludedSrcReasons.gen.gen) {
      categories.push('Onsite Diesel Generators')
      if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[0])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[0]].e_gen);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[1])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[1]].e_gen);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[2])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[2]].e_gen);
        }
      }
      const s = series.filter(s => s.name === this.fyCurrent)[0];
      if (s !== undefined) {
        s.data.push(this.emissionInfoCurrent.diesel_generators);
      }
    }

    let s1 = [];
    //trim series
    for (let i =0; i < series.length; i++) {
      if (series[i].name !== undefined) {
        s1.push(series[i]);
      }
    }

    // console.log(series)

    this.chart6_options = {
      title: {
        text: 'Direct GHG emissions over the financial years',
        left: '20%',
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      legend: {
        data: years,
        // data: ['2015/16', '2017/18', '2019/20'],
        right: '10%',
        top: '5%'
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '20%',
        containLabel: true
      },
      yAxis: {
        name: 'GHG Emission (tCO2e)',

        nameLocation: 'center',
        nameGap: 40,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'center',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
      },
      xAxis: {
        name: 'Source of Emission',
        type: 'category',

        nameGap: 300,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
        },
        nameLocation: 'center',
        axisLabel: {
          rotate: -90,
        },
        data: categories,
        // data: ['Transport Hired (Paid)'
        //   , 'Transport Rented',
        //   'Transport Locally Purchased',
        //   'Transport Company Owned Vehicles',
        //   'Onsite Diesel Generato',
        //   'Refrigerant Leakage',
        //   'Fire Extinguishers',
        //   'Off-road Vehicles',
        //   'Employee Commuting (Paid)',
        // ]
      },
      // series: [
      //   {
      //     name: '2015/16',
      //     type: 'bar',
      //     data: [18, 23, 29, 10, 131, 63, 20, 20, 20]
      //   },
      //   {
      //     name: '2017/18',
      //     type: 'bar',
      //     data: [19, 238, 31, 12, 13, 68, 20, 15, 15]
      //   },
      //   {
      //     name: '2019/20',
      //     type: 'bar',
      //     data: [19, 23, 31, 12, 13, 68, 20, 17, 10]
      //   }
      // ]
      series: s1,
    };
  }

  //chart 7
  public init_com_indirect_years(){
    const years = [...this.previousYears, this.fyCurrent]
    const categories = [];
    const data = [];
    const series = [
      {
        name: years[0],
        type: 'bar',
        data: [],
        itemStyle: {
          color: '#27ae60',
        }
      },
      {
        name: years[1],
        type: 'bar',
        data: [],
        itemStyle: {
          color: '#2980b9',
        }
      },
      {
        name: years[2],
        type: 'bar',
        data: [],
        itemStyle: { color: '#f1c40f' }
      },
      {
        name: this.fyCurrent,
        type: 'bar',
        data: [],
        itemStyle: { color: '#e67e22' }
      }
    ]


    //todo: add this year emission

    if (!this.metaData.excludedSrcReasons.wasteDis.wastDis){
      categories.push('Waste Disposal')
      if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[0])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[0]].e_wasted_dis);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[1])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[1]].e_wasted_dis);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[2])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[2]].e_wasted_dis);
        }
      }
      const s = series.filter(s => s.name === this.fyCurrent)[0];
      if (s !== undefined) {
        s.data.push(this.emissionInfoCurrent.waste_disposal);
      }
    }
    if (!this.metaData.excludedSrcReasons.wasTrans.wasTrans) {
      categories.push('Waste Transportation')
      if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[0])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[0]].e_waste_trans);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[1])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[1]].e_waste_trans);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[2])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[2]].e_waste_trans);
        }
      }

      const s = series.filter(s => s.name === this.fyCurrent)[0];
      if (s !== undefined) {
        s.data.push(this.emissionInfoCurrent.waste_transport);
      }
    }


    if (!this.metaData.excludedSrcReasons.elec.elec) {
      categories.push('Grid Connected Electricity')
      if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[0])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[0]].e_elec);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[1])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[1]].e_elec);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[2])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[2]].e_elec);
        }
      }
      const s = series.filter(s => s.name === this.fyCurrent)[0];
      if (s !== undefined) {
        s.data.push(this.emissionInfoCurrent.electricity);
      }

    }
    if (!this.metaData.excludedSrcReasons.air.air) {
      categories.push('Business Air Travels')
      if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[0])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[0]].e_air_travel);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[1])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[1]].e_air_travel);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[2])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[2]].e_air_travel);
        }
      }
      const s = series.filter(s => s.name === this.fyCurrent)[0];
      if (s !== undefined) {
        s.data.push(this.emissionInfoCurrent.air_travel);
      }
    }
    if (!this.metaData.excludedSrcReasons.empComm_notpaid.empComm_notpaid) {
      categories.push('Employee Commuting, Not Paid by the Company')
      if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[0])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[0]].e_emp_comm_not_paid);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[1])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[1]].e_emp_comm_not_paid);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[2])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[2]].e_emp_comm_not_paid);
        }
      }
      const s = series.filter(s => s.name === this.fyCurrent)[0];
      if (s !== undefined) {
        s.data.push(this.emissionInfoCurrent.emp_comm_not_paid);
      }
    }
    if (!this.metaData.excludedSrcReasons.mw.mw) {
      categories.push('Municipal Water')
      if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[0])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[0]].e_mw);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[1])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[1]].e_mw);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[2])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[2]].e_mw);
        }
      }
      const s = series.filter(s => s.name === this.fyCurrent)[0];
      if (s !== undefined) {
        s.data.push(this.emissionInfoCurrent.mun_water);
      }

    }

    if (!this.metaData.excludedSrcReasons.hired_notpaid.hired_notpaid) {
      categories.push('Vehicle Hired, Not Paid')
      if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[0])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[0]].e_trans_hired_not_paid);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[1])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[1]].e_trans_hired_not_paid);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[2])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[2]].e_trans_hired_not_paid);
        }
      }

      const s = series.filter(s => s.name === this.fyCurrent)[0];
      if (s !== undefined) {
        s.data.push(this.emissionInfoCurrent.transport_hired_not_paid);
      }

    }
    if (!this.metaData.excludedSrcReasons.td.td) {
      categories.push('T & D Loss')
      if (this.metaData.previousYearStats[this.previousYears[0]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[0])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[0]].e_t_d);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[1]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[1])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[1]].e_t_d);
        }
      }
      if (this.metaData.previousYearStats[this.previousYears[2]] !== undefined) {
        const s = series.filter(s => s.name === this.previousYears[2])[0];
        if (s !== undefined) {
          s.data.push(this.metaData.previousYearEmission[this.previousYears[2]].e_t_d);
        }
      }
      const s = series.filter(s => s.name === this.fyCurrent)[0];
      if (s !== undefined) {
        s.data.push(this.emissionInfoCurrent.t_d);
      }
    }

    let s1 = [];
    //trim series
    for (let i =0; i < series.length; i++) {
      if (series[i].name !== undefined) {
        s1.push(series[i]);
      }
    }

    this.chart7_options = {
      title: {
        text: 'Indirect GHG emissions over the financial years',
        left: '20%',
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      legend: {
        data: years,
        right: '10%',
        top: '5%'
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '20%',
        containLabel: true
      },
      yAxis: {
        name: 'GHG Emission (tCO2e)',

        nameLocation: 'center',
        nameGap: 40,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'center',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
      },
      xAxis: {
        name: 'Source of Emission',
        type: 'category',

        nameGap: 300,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
        },
        nameLocation: 'center',
        axisLabel: {
          rotate: -90,
        },
        data: categories,
        // data: ['Transport Hired (Not Paid)'
        //   , 'Grid Connected Electricity',
        //   'T and D Loss}',
        //   'Business Air Travel',
        //   'Employee Commuting (Not Paid)',
        //   'Waste Disposal',
        //   'Waste Transport',
        //   'Municipal Water',
        // ]
      },
      series: s1,
      // series: [
      //   {
      //     name: '2015/16',
      //     type: 'bar',
      //     data: [18, 23, 29, 10, 131, 63, 20, 20]
      //   },
      //   {
      //     name: '2017/18',
      //     type: 'bar',
      //     data: [19, 238, 31, 12, 13, 68, 20, 15]
      //   },
      //   {
      //     name: '2019/20',
      //     type: 'bar',
      //     data: [19, 23, 31, 12, 13, 68, 20, 17]
      //   }
      // ]
    }

  }

  //chart 8
  public init_per_capita() {

    const years = [...this.previousYears, this.fyCurrent];
    let data = [];
    if ( this.metaData.previousYearStats[years[0]] !== undefined) {
      data.push(this.metaData.previousYearStats[years[0]].capita)
    }if ( this.metaData.previousYearStats[years[1]] !== undefined) {
      data.push(this.metaData.previousYearStats[years[1]].capita)
    }if ( this.metaData.previousYearStats[years[2]] !== undefined) {
      data.push(this.metaData.previousYearStats[years[2]].capita)
    }
    data.push(this.emissionInfoCurrent.per_capita)


    //trim series
    for (let i =0; i < data.length; i++) {
      if (data[i] === undefined) {
        data.splice(i, 1);
      }
    }
    this.chart8_options = {
      title: {
        text: 'Per Capita Emissions (tCO2e)',
        left: '30%',
      },
      xAxis: {
        data: years,
        // data: ['2015/16', '2017/18', '2019/20',],
        type: 'category',
        name: 'Year',
        nameLocation: 'center',
        nameGap: 30,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'left',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
      },
      yAxis: {
        type: 'value',
        name: 'Emissions (tCO2e)',
        nameLocation: 'center',
        nameGap: 55,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'center',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
      },
      series: [{
        data: data,
        // data: [820, 932, 901,],
        type: 'line'
      }]
    };
  }

  //chart9
  public init_intensity() {
    const years = [...this.previousYears, this.fyCurrent];
    let data = [];
    if ( this.metaData.previousYearStats[years[0]] !== undefined) {
      data.push(this.metaData.previousYearStats[years[0]].intensity)
    }if ( this.metaData.previousYearStats[years[1]] !== undefined) {
      data.push(this.metaData.previousYearStats[years[1]].intensity)
    }if ( this.metaData.previousYearStats[years[2]] !== undefined) {
      data.push(this.metaData.previousYearStats[years[2]].intensity)
    }
    data.push(this.emissionInfoCurrent.intensity)
    //trim series
    for (let i =0; i < data.length; i++) {
      if (data[i] === undefined) {
        data.splice(i, 1);
      }
    }
    this.chart9_options = {
      title: {
        text: 'Emission Intensity (tCO2e/ LKR)',
        left: '30%',
      },
      xAxis: {
        data: years,
        // data: ['2015/16', '2017/18', '2019/20',],
        type: 'category',
        name: 'Year',
        nameLocation: 'center',
        nameGap: 30,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'left',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
      },
      yAxis: {
        type: 'value',
        name: 'Emissions (tCO2e/ LKR)',
        nameLocation: 'center',
        nameGap: 55,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'center',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
      },
      series: [{
        // data: [820, 932, 901,],
        data: data,
        type: 'line'
      }]
    };

  }

  //chart10
  public init_prod() {
    const years = [...this.previousYears, this.fyCurrent];
    let data = [];
    if ( this.metaData.previousYearStats[years[0]] !== undefined) {
      data.push(this.metaData.previousYearStats[years[0]].prod)
    }if ( this.metaData.previousYearStats[years[1]] !== undefined) {
      data.push(this.metaData.previousYearStats[years[1]].prod)
    }if ( this.metaData.previousYearStats[years[2]] !== undefined) {
      data.push(this.metaData.previousYearStats[years[2]].prod)
    }
    data.push(this.emissionInfoCurrent.prod)
    //trim series
    for (let i =0; i < data.length; i++) {
      if (data[i] === undefined) {
        data.splice(i, 1);
      }
    }
    this.chart10_options = {
      title: {
        text: 'Emissions per ton of production (tCO2e)',
        left: '30%',
      },
      xAxis: {
        data: years,
        // data: ['2015/16', '2017/18', '2019/20',],
        type: 'category',
        name: 'Year',
        nameLocation: 'center',
        nameGap: 30,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'left',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
      },
      yAxis: {
        type: 'value',
        name: 'Emissions (tCO2e)',
        nameLocation: 'center',
        nameGap: 55,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'center',
          rich: {}
        },
        axisLabel: {
          padding: [0, 0, 0, 2],
        },
      },
      series: [{
        data: data,
        // data: [820, 932, 901,],
        type: 'line'
      }]
    };
  }

  public initCharts_bo() {
    if (!this.validate()) {
      if (this.toastMsg === undefined) {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
        return;
      } else {
        this.toastSerivce.show('', this.toastMsg, {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
        return;
      }
    }
    this.jsonBodyInitCharts.num_emps = this.metaData.num_emps;
    this.jsonBodyInitCharts.prod_ton = this.metaData.prod_ton;
    this.jsonBodyInitCharts.revenue = this.metaData.revenue;

    for (let k of Object.keys(this.metaData.excludedSrcReasons)) {
        for (let j  of Object.keys(this.metaData.excludedSrcReasons[k])) {
          if (this.metaData.excludedSrcReasons[k][j] === true || this.metaData.excludedSrcReasons[k][j]  === false) {
            this.jsonBodyInitCharts[k] = this.metaData.excludedSrcReasons[k][j];
            break;
          }
        }
    }

    this.boService.sendRequestToBackend(
      RequestGroup.Emission,
      RequestType.GHGEmissionSummaryExcluded,
      {
        params: this.jsonBodyInitCharts,
        companyId: this.comIdMetaData,
        branchId: -1,
      }).then(data => {
        console.log("--------------------------------------------------------------")
        console.log(data);
        console.log("--------------------------------------------------------------")


      if (data !== undefined && data.DAT !== undefined && data.DAT.list !== undefined) {
            const val = data.DAT.list[0];
            this.fyCurrent = val.emissionInfo.fy;
            this.emissionInfoCurrent.total = val.emissionInfo.total;
            this.emissionInfoCurrent.direct = val.emissionInfo.direct;
            this.emissionInfoCurrent.indirect = val.emissionInfo.indirect;
            this.emissionInfoCurrent.per_capita = val.emissionInfo.per_capita;
            this.emissionInfoCurrent.intensity = val.emissionInfo.intensity;
            this.emissionInfoCurrent.waste_transport = val.emissionInfo.waste_transport;
            this.emissionInfoCurrent.t_d = val.emissionInfo.t_d;
            this.emissionInfoCurrent.waste_disposal = val.emissionInfo.waste_disposal;
            this.emissionInfoCurrent.mun_water = val.emissionInfo.mun_water;
            this.emissionInfoCurrent.emp_comm_not_paid = val.emissionInfo.emp_comm_not_paid;
            this.emissionInfoCurrent.emp_comm_paid = val.emissionInfo.emp_comm_paid;
            this.emissionInfoCurrent.air_travel = val.emissionInfo.air_travel;
            this.emissionInfoCurrent.electricity = val.emissionInfo.electricity;
            this.emissionInfoCurrent.company_owned = val.emissionInfo.company_owned;
            this.emissionInfoCurrent.offroad = val.emissionInfo.offroad;
            this.emissionInfoCurrent.fire_ext = val.emissionInfo.fire_ext;
            this.emissionInfoCurrent.refri_leakage = val.emissionInfo.refri_leakage;
            this.emissionInfoCurrent.diesel_generators = val.emissionInfo.diesel_generators;
            this.emissionInfoCurrent.transport_loc_pur = val.emissionInfo.transport_loc_pur;
            this.emissionInfoCurrent.transport_hired_paid = val.emissionInfo.transport_hired_paid;
            this.emissionInfoCurrent.transport_hired_not_paid = val.emissionInfo.transport_hired_not_paid;
            this.emissionInfoCurrent.transport_rented = val.emissionInfo.transport_rented;
            this.emissionInfoCurrent.prod = val.emissionInfo.prod;
            // console.log(this.emissionInfoCurrent)

            if (data.DAT.list.length > 1) {
              for (let i = data.DAT.list.length -1 ; i > 0; i--) {
                const dto = data.DAT.list[i];
                if (dto !== undefined) {
                  this.previousYears.push(dto.fy);

                  this.metaData.previousYearEmission[dto.fy] = {

                    e_trans_hired_not_paid : dto.emissionInfo.transport_hired_not_paid,
                    e_elec: dto.emissionInfo.electricity,
                    e_emp_comm_not_paid: dto.emissionInfo.emp_comm_not_paid,
                    e_t_d : dto.emissionInfo.t_d,
                    e_wasted_dis: dto.emissionInfo.waste_disposal,
                    e_mw: dto.emissionInfo.mun_water,
                    e_waste_trans: dto.emissionInfo.waste_transport,
                    e_trans_hired_paid: dto.emissionInfo.transport_hired_paid,
                    e_trans_rented: dto.emissionInfo.transport_rented,
                    e_trans_loc_pur: dto.emissionInfo.transport_loc_pur,
                    e_com_owned: dto.emissionInfo.company_owned,
                    e_gen: dto.emissionInfo.diesel_generators,
                    e_refri: dto.emissionInfo.refri_leakage,
                    e_fire: dto.emissionInfo.fire_ext,
                    e_offroad: dto.emissionInfo.offroad,
                    e_emp_comm_paid: dto.emissionInfo.emp_comm_paid,
                    e_air_travel: dto.emissionInfo.air_travel
                  };
                  this.metaData.previousYearEfs[dto.fy] = {
                    grid: dto.emissionInfo.ef_grid_elec,
                    mw: dto.emissionInfo.ef_mw,
                    td_loss: dto.emissionInfo.ef_td
                  }

                  this.metaData.previousYearStats[dto.fy] = {
                    capita: dto.emissionInfo.per_capita,
                    prod: dto.emissionInfo.per_prod,
                    intensity: dto.emissionInfo.intensity,
                    t_direct: dto.emissionInfo.direct,
                    t_indirect: dto.emissionInfo.indirect,
                  }
                }
              }
            }

          this.init_chart_by_all_src();
          this.init_direct_indirect();
          this.init_pie_direct();
          this.init_pie_indirect();
          this.init_com_ghg_years();
          this.init_com_direct_years();
          this.init_com_indirect_years();
          this.init_per_capita();
          this.init_intensity();
          this.init_prod();
          this.selctedIndex++;
          this.myStepper.next();
          this.disableNext = false;
        }





    })
  }





}




