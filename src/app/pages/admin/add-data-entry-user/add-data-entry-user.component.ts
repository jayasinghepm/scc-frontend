import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from "@angular/material";
import {BackendService} from "../../../@core/rest/bo_service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {MassterDataService} from "../../../@core/service/masster-data.service";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";

@Component({
  selector: 'app-add-data-entry-user',
  templateUrl: './add-data-entry-user.component.html',
  styleUrls: ['./add-data-entry-user.component.scss']
})
export class AddDataEntryUserComponent implements OnInit {

  public header = ''
  public titles = []
  public companines = []

  public jsonbody_login = {
    userId: -1,
    userType: 4,
    loginStatus: 1,
    userName: '',
    password: '',
    isFirstTime: 2,
  }
  public jsonbody_dataentryusr = {
    id: -1,
    firstName:undefined,
    lastName:undefined,
    title:undefined,
    companyId:-1,
    company:undefined,
    userId: -1,
    position: ''
  }

  constructor(
    private boService:BackendService,
    public dialogRef: MatDialogRef<AddDataEntryUserComponent>,
    private  toastSerivce:  NbToastrService,
    private masterData: MassterDataService,
    @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any,

  ) {
    this.masterData.getCompaninesFull().subscribe( d => {
      this.companines = d;
    })
    this.masterData.getTitles().subscribe(d => {
      this.titles = d;
    })
    // console.log(this.popupData)

    if (popupData.isEdit) {
      this.header = "Edit Data Entry User"
      this.jsonbody_login.userId = this.popupData.data.userId;
      // console.log(this.popupData.data)
      this.jsonbody_dataentryusr.id = this.popupData.data.id;
    } else {
      this.header = "Create Data Entry User"
    }
  }

  ngOnInit() {
   if(this.popupData.isEdit) {
     this.jsonbody_dataentryusr.userId = this.popupData.data.userId
     this.jsonbody_dataentryusr.companyId = this.popupData.data.companyId
     this.jsonbody_dataentryusr.firstName = this.popupData.data.firstName;
     this.jsonbody_dataentryusr.lastName = this.popupData.data.lastName;
     this.jsonbody_dataentryusr.company = this.popupData.data.company;
     this.jsonbody_dataentryusr.title = this.popupData.data.title;
     this.jsonbody_dataentryusr.position = this.popupData.data.position;
   }
  }


  onClickSave() {
    if (!this.validate()){
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }
    this.boService.sendRequestToBackend(
      RequestGroup.LoginUser,
      RequestType.ManageUserLogin,
      {
        DATA: this.jsonbody_login,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.dto != undefined) {
          // this.toastSerivce.show('', 'Saved data successfully.', {
          //   status: 'success',
          //   destroyByClick: true,
          //   duration: 2000,
          //   hasIcon: false,
          //   position: NbGlobalPhysicalPosition.TOP_RIGHT,
          //   preventDuplicates: true,
          // });

          this.jsonbody_dataentryusr.userId = data.DAT.dto.userId;

          this.boService.sendRequestToBackend(
            RequestGroup.BusinessUsers,
            RequestType.ManageDataEntryUser,
            {DATA: this.jsonbody_dataentryusr}
          ).then(data => {
            if (data.HED != undefined && data.HED.RES_STS == 1) {
              if (data.DAT != undefined && data.DAT.dto != undefined) {
                this.toastSerivce.show('', 'Saved data successfully.', {
                  status: 'success',
                  destroyByClick: true,
                  duration: 2000,
                  hasIcon: false,
                  position: NbGlobalPhysicalPosition.TOP_RIGHT,
                  preventDuplicates: true,
                });

                // this.userId = data.DAT.dto.userId;
                this.dialogRef.close(true);
              } else {
                this.toastSerivce.show('', 'Error in saving data.', {
                  status: 'danger',
                  destroyByClick: true,
                  duration: 2000,
                  hasIcon: false,
                  position: NbGlobalPhysicalPosition.TOP_RIGHT,
                  preventDuplicates: true,
                })
              }
            }else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          })
        } else {
          this.toastSerivce.show('Try with different user name', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      }
      else {
        this.toastSerivce.show('Try with different user name', 'Error in saving data.', {
          status: 'danger',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    })

  }

  onChangeCompany($event: MatSelectChange) {
    this.jsonbody_dataentryusr.company = this.companines.filter(c => c.id === $event.value)[0].name;
  }

  closeDialog() {
    this.dialogRef.close();
  }

  validate() {
    if (!this.popupData.isEdit && (this.jsonbody_login.userName === undefined || this.jsonbody_login.userName === '')) {
      return false;
    }
    if (!this.popupData.isEdit && (this.jsonbody_login.password === undefined || this.jsonbody_login.password === '')) {
      return false;
    }
    if (this.popupData.isEdit && (this.jsonbody_login.userId === undefined || this.jsonbody_login.userId  <= 0)){
      return false;
    }
    if (this.popupData.isEdit && (this.jsonbody_dataentryusr.id === undefined || this.jsonbody_dataentryusr.id <=0 )) {
      return false;
    }
    if (this.jsonbody_dataentryusr.firstName === undefined || this.jsonbody_dataentryusr.firstName === '') {
      return false;
    }
    if (this.jsonbody_dataentryusr.lastName === undefined || this.jsonbody_dataentryusr.lastName === '') {
      return false;
    }
    if(this.jsonbody_dataentryusr.title === undefined || this.jsonbody_dataentryusr.title === '') {
      return false;
    }
    if(this.jsonbody_dataentryusr.companyId === undefined || this.jsonbody_dataentryusr.companyId <= 0) {
      return false;
    }
    if(this.jsonbody_dataentryusr.position === undefined || this.jsonbody_dataentryusr.position === '') {
      return false;
    }

    return true;
  }
}
