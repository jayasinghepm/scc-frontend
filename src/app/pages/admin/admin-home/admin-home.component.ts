import { Component, OnInit } from '@angular/core';
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {BackendService} from "../../../@core/rest/bo_service";
import * as moment from 'moment';
import { UserState } from 'src/app/@core/auth/UserState';
import { MassterDataService } from 'src/app/@core/service/masster-data.service';
import { callbackify } from 'util';
import { EmissionSourceEnum } from 'src/app/@core/enums/EmissionSourceEnum';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.scss']
})
export class AdminHomeComponent implements OnInit {

  sources = [
    'waste_transport', 
    't_d', 'waste_disposal',
    'mun_water', 'emp_comm_not_paid',
    'emp_comm_paid','air_travel','electricity', 
    'company_owned',
    'offroad',
    'fire_ext',
    'refri_leakage',
    'diesel_generators',
    'transport_loc_pur', 
    'transport_hired_paid',
    'transport_hired_not_paid',
    'transport_rented',
    'sea_freight',
    'air_freight',
    'lpg',
    'rented_not_paid',
    'rented_paid',
    'bio_mass',
    'raw_mat_trans',
    'forklifts_diesel',
    'forklifts_petrol',
    'welding',
    'waste_water',
    'freight'
  ];
  titleMap = {
    'waste_transport':"Waste Transport", 
    't_d':"t_d", 
    'waste_disposal':"Waste Disposal",
    'mun_water':"Municipal Water",
    'emp_comm_not_paid':"Employee Commuting, Not paid by the company",
    'emp_comm_paid':"Employee Commuting, Paid by the company",
    'air_travel':"Business Air Travels",
    'electricity':"Electricity", 
    'company_owned':"Company Owned Vehicles",
    'offroad':"Off-road Vehicles",
    'fire_ext':"Fire Extinguisher Refilled/ Replaced data",
    'refri_leakage':"Refirgerant Leakage",
    'diesel_generators':"Generators",
    'transport_loc_pur':"Transport Locally Purchased", 
    'transport_hired_paid':"Transport Hired, Paid",
    'transport_hired_not_paid':"Transport Hired, Not Paid",
    'transport_rented':"Transport Rented",
    'sea_freight':"Sea Freight Transport",
    'air_freight':"Air Freight Transport",
    'lpg':"LP Gas",
    'rented_not_paid':"Transport Rented, Not Paid",
    'rented_paid':"Transport Rented, Paid",
    'bio_mass':"Bio Mass",
    'raw_mat_trans':"Raw Material Transportation",
    'forklifts_diesel':"Fork-lifts, Diesel",
    'forklifts_petrol': "Fork-lifts, Petrol",
    'welding': 'Welding',
    'waste_water': 'Waste water',
    'freight': 'Freight transport'
  }

  public chart5_options = {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross',
        label: {
          backgroundColor: echarts.tooltipBackgroundColor,
        },
      },
    },
    backgroundColor: echarts.bg,
    title: {
      text: '',
      subtext: ''
    },
    legend: {
    },
    grid: {
      left: '12%',
      right: '4%',
      bottom: '10%',
      containLabel: true
    },
    yAxis: {
      name: 'GHG Emission tCO2e',
      nameGap: 50,
      nameTextStyle: {
        fontWeight: 'bold',
        fontSize: 14,
      },
      nameLocation: 'center',
      type: 'value'
    },
    xAxis: {
      type: 'category',
      axisLabel: {
        padding: [0,0,0,2],
      },
      data: ['Direct', 'Indirect']
    },
    series: {
      type: 'bar',
      stack: 'chart',
      barWidth: '70%',
      label: {
        normal: {
          position: 'right',
          show: true
        }
      },
      data: [],
    }
  };

  public chart7_options = {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross',
        label: {
          backgroundColor: echarts.tooltipBackgroundColor,
        },
      },
    },
    yAxis: {
      type: 'category',
      name: 'Emission source',
      nameLocation: 'center',
      nameGap: 320,
      nameTextStyle: {
        fontWeight: 'bold',
        fontSize: 14,
        align: 'left',
        rich: {

        }
      },
      axisLabel: {
        padding: [0,0,0,2],
      },
      splitArea: {
        interval: 0,
      },

      data: [],
    },
    xAxis: {
      type: 'value',
      name: 'GHG Emission (tCO2e)',
      nameGap: 30,
      nameTextStyle: {
        fontWeight: 'bold',
        fontSize: 14,
      },
      nameLocation: 'center',
    },
    grid: {
      left: '40%',
    },
    series: [{
      type: 'bar',
      stack: 'chart',
      label: {
        normal: {
          position: 'right',
          show: true
        }
      },
      data: [],
    }]
  }

  public chart6_options = {}

  public companies = []
  comId:any;
  comName:any;

  public adminId = UserState.getInstance().adminId

  public year : any;

  private colorPallete = ['#16a085', '#27ae60', '#2980b9', '#f1c40f', '#e67e22', '#e67e22', '#e74c3c',]


  public chartsData = {
    branch: {
      branches: [],
      scope_1: [],
      scope_2: [],
      scope_3: [],
    },
    categories: {
      waste_transport: 0,
      t_d: 0,
      waste_disposal: 0,
      mun_water: 0,
      emp_comm_not_paid: 0,
      emp_comm_paid: 0,
      air_travel: 0,
      electricity: 0,
      company_owned: 0,
      offroad: 0,
      fire_ext: 0,
      refri_leakage: 0,
      diesel_generators: 0,
      transport_loc_pur: 0,
      transport_hired_paid: 0,
      transport_hired_not_paid: 0,
      transport_rented: 0,
      sea_freight: 0,
      air_freight: 0,
      lpg: 0,
     // bioMass: 0,
      ash_trans: 0,
      forklifts_petrol: 0,
      forklifts_diesel: 0,
      furnace_oil: 0,
      lorry_trans_internal: 0,
      lorry_trans_external: 0,
      paper_waste: 0,
      paid_manager_vehicles: 0,
      oil_gas_trans: 0,
      raw_mat_trans: 0,
      saw_dust_trans: 0,
      sludge_trans: 0,
      vehicle_others: 0,
      staff_trans: 0,
      rented_paid: 0,
      rented_not_paid: 0,
      bio_mass:0,

    },
    indirect: 0,
    direct: 0,
    per_capita: [],
    intensity: [],
    emission: {
      tco2: [],
      co2: [],
      n20: [],
      ch4: []
    },
    fy: '',

  }


  public projectStatusOptions;
  public paymentStatusOptions;
  public projectStatusData = [
    { name: 'New', value: 0,},
    { name: 'Signed', value: 0,},
    {name: 'Approved', value: 0,},
    {name: 'Data Entry', value: 0},
    {name: 'Completeness Check', value: 0},
    {name: 'First Draft', value: 0 },
    {name: 'Final Submission', value: 0},
  ];
  public payemntStatusData = [
    { name: 'Not Paid', value: 0,},
    { name: 'Initial', value: 0,},
    {name: 'Second', value: 0,},
    {name: 'Final', value: 0},
  ]
  public expiredProjects :{name: string, comId: number, endDate: string}[] = [];
  public reports : {name: string, status: string} [] = []

  constructor(private boService: BackendService,
    private masterData: MassterDataService,
    ) 
  {
    this.projectStatusOptions = {
      title : {
        text: '',
        subtext: '',
        x:'center'
      },
      tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
      },
      legend: {
        type: 'scroll',
        orient: 'vertical',
        right: 10,
        top: 20,
        bottom: 20,
        data: ['New', 'Signed', 'Approved', 'Data Entry', 'Completeness Check', 'First Draft', 'Final Submission', 'Verification'],

        // selected: data.selected
      },
      series : [
        {
          name: 'Status',
          type: 'pie',
          radius : '55%',
          center: ['40%', '50%'],
          data: [
            { name: 'New', value: 4,},
            { name: 'Singed', value: 7,},
            {name: 'Approved', value: 8,},
            {name: 'Data Entry', value: 10},
            {name: 'Completeness Check', value: 13},
            {name: 'First Draft', value: 32 },
            {name: 'Final Submission', value: 2},
            {name: 'Verification', value: 3},
          ],
          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
          }
        }
      ]
    };
    this.paymentStatusOptions = {
      title : {
        text: '',
        subtext: '',
        x:'center'
      },
      tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
      },
      legend: {
        type: 'scroll',
        orient: 'vertical',
        right: 10,
        top: 20,
        bottom: 20,
        data: ['Not Paid', 'Initial', 'Second', 'Final'],

        // selected: data.selected
      },
      series : [
        {
          name: 'Status',
          type: 'pie',
          radius : '55%',
          center: ['40%', '50%'],
          data: [
            { name: 'Not Paid', value: 4,},
            { name: 'Initial', value: 7,},
            {name: 'Second', value: 8,},
            {name: 'Final', value: 10},
          ],
          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
          }
        }
      ]
    };

    this.expiredProjects.push(...[
      {name: 'ghg_com-11', comId: 3, endDate: '20-Nov-2019'},
      {name: 'ghg_com-12', comId: 3, endDate: '19-Oct-2019'},
    ])
    this.reports.push(...[
      {name: 'ghg_com-23', status: 'First Draft'},
      {name: 'ghg_com_25', status: 'Final Submission'}
    ])
    this.loadData();

    this.chart6_options = {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          label: {
            backgroundColor: echarts.tooltipBackgroundColor,
          },
        },
      },
      yAxis: {
        type: 'category',
        name: 'Source of Emission',
        nameLocation: 'center',
        nameGap: 320,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'left',
          rich: {

          }
        },
        axisLabel: {
          padding: [0,0,0,2],
        },
        splitArea: {
          interval: 0,
        },

        data: 11,
      },
      xAxis: {
        type: 'value',
        name: 'GHG Emission (tCO2e)',
        nameGap: 30,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
        },
        nameLocation: 'center',
      },
      grid: {
        left: '40%',
      },
      series: [{
        type: 'bar',
        stack: 'chart',
        label: {
          normal: {
            position: 'right',
            show: true
          }
        },
        data: 11,
      }]
    };

  }


  async loadAllComGrap(){
    console.log(UserState.getInstance())
    this.companies = await this.masterData.getCompaninesFull().toPromise();
    if(UserState.getInstance().adminId == 3){
      this.companies = this.companies.slice(67,69);

      // this.companies = this.companies.slice(67,68);

      let sumd:number=0
      let sumi:number=0
      
      await Promise.all(this.companies.map(async company => {
        const data = await this.boService.sendRequestToBackend(    
          RequestGroup.Emission,
          RequestType.GHGEmissionSummary,
          {
            companyId: company.id,
            branchId: -1,
            yearsBack: 2 // 
          }        
        );

        // console.log(data.DAT.list[0].emissionInfo);
        this.saveEmissionInfor(data.DAT.list[0].emissionInfo, true);
        sumd +=  +data.DAT.list[0].emissionInfo.direct;
        sumi += +data.DAT.list[0].emissionInfo.indirect;

      }));
      this.chartsData.indirect = sumi
      this.chartsData.direct = sumd

      this.chart5_options = {...this.chart5_options, series: {...this.chart5_options.series, data: [
        {
          value: this.chartsData.direct.toFixed(3),
          itemStyle: {color:this.colorPallete[2]},
        },
        {
          value: this.chartsData.indirect.toFixed(3),
          itemStyle: {color: this.colorPallete[3]},
        }
      ]}}   
    }
  }

   
  private ongraph(value:any){
    this.boService.sendRequestToBackend(
        
      RequestGroup.Emission,
      RequestType.GHGEmissionSummary,
      {
        companyId: value,
        branchId: -1,
        yearsBack: 2 // 
      }
    ).then(data => {
      if (data.DAT !== undefined && data.DAT.list !== undefined) {
        this.saveEmissionInfor(data.DAT.list[0].emissionInfo, false);
        this.year = data.DAT.list[0].fy
        this.chartsData.direct = data.DAT.list[0].emissionInfo.direct;
        this.chartsData.indirect =data.DAT.list[0].emissionInfo.indirect;
        this.chartsData.fy = data.DAT.list[0].fy;
      }

      this.chart5_options = {...this.chart5_options, series: {...this.chart5_options.series, data: [
        {
          value: this.chartsData.direct,
          itemStyle: {color:this.colorPallete[2]},
        },
        {
          value: this.chartsData.indirect,
          itemStyle: {color: this.colorPallete[3]},
        }
      ]}}      
    }); 
  }


  async ngOnInit() {
    await this.loadAllComGrap();
  }

  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.GHGProject,
      RequestType.ListProject,
      {
        PAGE_NUMBER: -1,
        FILTER_MODEL: {
          status: { value: 8, type: 2, col: 1}
        }
      }
    ).then(data => {
      // console.log(data)
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          let list = [];

          this.expiredProjects = [];
          this.reports = [];
          data.DAT.LIST.forEach(val => {
            if (val != undefined) {
              switch(val.status) {
                case 1: // new
                {
                  this.projectStatusData.filter(v => { return v.name === 'New'})[0].value++;
                  break;
                }
                case 2: // singend
                {
                  this.projectStatusData.filter(v => { return v.name === 'Signed'})[0].value++;
                  break;
                }
                case 3 : // approved
                {
                  this.projectStatusData.filter(v => { return v.name === 'Approved'})[0].value++;
                  break;
                }
                case 4:  // data entry
                {
                  this.projectStatusData.filter(v => { return v.name === 'Data Entry'})[0].value++;
                  break;
                }
                case 5: // completeness check
                {
                  this.projectStatusData.filter(v => { return v.name === 'Completeness Check'})[0].value++;
                  break;
                }
                case 6: // first draft submission
                {
                  this.projectStatusData.filter(v => { return v.name === 'First Draft'})[0].value++;
                  this.reports.push({name: val.name, status: 'First Draft'})
                  break;
                }
                case 7: // final submission
                {
                  this.projectStatusData.filter(v => { return v.name === 'Final Submission'})[0].value++;
                  this.reports.push({name: val.name, status: 'Final Submission'})
                  break;
                }
              }
              if (val.firstPayDate !== undefined) {
                this.payemntStatusData.filter(v=> { return v.name === 'Initial'})[0].value++;
              } else if (val.secondPayDate !== undefined) {
                this.payemntStatusData.filter(v=> {return v.name === 'Second'})[0].value++;
              } else if (val.finalPayDate) {
                this.payemntStatusData.filter(v=> { return v.name === 'Final'})[0].value++;
              } else {
                // not paid
                this.payemntStatusData.filter(v=> { return v.name === 'Not Paid'})[0].value++;
              }

              if (this.isExpired(val)) {
                this.expiredProjects.push({ name: val.name, endDate: val.endDate , comId: 1})
              }

            }
            this.projectStatusOptions = {
              title : {
                text: '',
                subtext: '',
                x:'center'
              },
              tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
              },
              legend: {
                type: 'scroll',
                orient: 'vertical',
                right: 10,
                top: 20,
                bottom: 20,
                data: ['New', 'Signed', 'Approved', 'Data Entry', 'Completeness Check', 'First Draft', 'Final Submission', 'Verification'],

                // selected: data.selected
              },
              series : [
                {
                  name: 'Status',
                  type: 'pie',
                  radius : '55%',
                  center: ['40%', '50%'],
                  data: this.projectStatusData,
                  itemStyle: {
                    emphasis: {
                      shadowBlur: 10,
                      shadowOffsetX: 0,
                      shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                  }
                }
              ]
            };
            this.paymentStatusOptions = {
              title : {
                text: '',
                subtext: '',
                x:'center'
              },
              tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
              },
              legend: {
                type: 'scroll',
                orient: 'vertical',
                right: 10,
                top: 20,
                bottom: 20,
                data: ['Not Paid', 'Initial', 'Second', 'Final'],

                // selected: data.selected
              },
              series : [
                {
                  name: 'Status',
                  type: 'pie',
                  radius : '55%',
                  center: ['40%', '50%'],
                  data: this.payemntStatusData,
                  itemStyle: {
                    emphasis: {
                      shadowBlur: 10,
                      shadowOffsetX: 0,
                      shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                  }
                }
              ]
            };
          });
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }
  
  
  private isExpired(project:any) {
    let remdays;
    if (project.extendedDate !== undefined && project.extendedDate  !== null) {
      // console.log('extended')
      const extend = moment(project.extendedDate, 'YYYY-MM-DD');
      const today = moment().startOf('day');
      remdays = extend.diff(today, 'days');
    } else {
      const today = moment().startOf('day');
      const end = moment(project.endDate , 'YYYY-MM-DD');
      // console.log(start);
      // console.log(end);
      const diff = end.diff(today, 'days');
      remdays = diff

    }
    return remdays < 0 ? true : false;
  }


  async onChangeCompany(value:any) {
    if(value === "all"){
      this.comId = -1;
      await this.loadAllComGrap();
    }else{
      this.comId = value;   
      this.ongraph(this.comId);  
      this.comName = this.companies.find(c => c.id == this.comId).name    
      return this.comId;
    }
  }

  private valueMapByKeys(keys: string[], isAllCom=false, emissionInfoCurrent){
    let data = {};
    keys.forEach(k => {
      if(k){
        if(isAllCom){
          if(this.chartsData.categories[k]){
            data[k] = emissionInfoCurrent[k] === undefined ? this.chartsData.categories[k] : this.chartsData.categories[k] + parseFloat(emissionInfoCurrent[k])
          }else{
            data[k] = emissionInfoCurrent[k] === undefined ? 0 : 0 + parseFloat(emissionInfoCurrent[k])
          }
        }else{
          data[k] = emissionInfoCurrent[k] === undefined ? 0 : parseFloat(emissionInfoCurrent[k])
        }
      }
    })

    return data;
  }

  private saveEmissionInfor(emissionInfoCurrent: any, isAll=false){

    const sources = this.sources;
    //@ts-ignore
    this.chartsData.categories = this.valueMapByKeys(sources, isAll, emissionInfoCurrent);
    // console.log(emissionInfoCurrent)
    // console.log(this.chartsData.categories);
    this.propulateChart7();   
  }
  

  private getEmissionSrcTitles(): string[] {
    let sources = this.sources;
    sources  = sources.map(s => {
      // let ss = s.split("_").map(s => {
      //   if(s){
      //     return s[0].toUpperCase()+s.substring(1)
      //   }else{
      //     return "";
      //   }               
      // }).join(" ");
      return this.titleMap[s]
    })
    return sources; 

  }


  private propulateChart7(){
    this.chart7_options = {
      ...this.chart7_options,
      yAxis: {
        ...this.chart7_options.yAxis,
        data: this.getEmissionSrcTitles()
      },
      series: [
        {
          ...this.chart7_options.series[0],
          data: this.getEmissionSeriesData()
        }
      ]
    }
  }

  private getEmissionSeriesData() : any[] {
    const getColor = (directCat) => {
      switch (directCat) {
        case 1 :
          return '#2980b9';//#faadaa
        case 2:
          return  '#1abc9c';
        default:
          return  '#faadaa'
      }
    }

    const seriesData = [];

    // console.log(this.chartsData.categories);
    const keys = Object.keys(this.chartsData.categories);
    keys.forEach((k, i) => {
      seriesData.push({
        value: this.chartsData.categories[k].toFixed(3),
        itemStyle: {color: getColor(1)},
      })
    })
    return seriesData;
  }

}
  

