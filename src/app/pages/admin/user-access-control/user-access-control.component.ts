import {Component, OnInit} from '@angular/core';
import {BackendService} from "../../../@core/rest/bo_service";
import {MassterDataService} from "../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {MatCheckboxChange, MatDialog, MatSelectChange} from "@angular/material";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {UserState} from "../../../@core/auth/UserState";
import {DeleteConfirmComponent} from "../../delete-confirm/delete-confirm.component";
import {ActivityLogsComponent} from "../activity-logs/activity-logs.component";
import { AddCadminComponent } from '../com-admins/add-cadmin/add-cadmin.component';
import { UpateCadminLorryTransComponent } from '../../cadmin/emission/cadmin-lorry-trans/upate-cadmin-lorry-trans/upate-cadmin-lorry-trans.component';

@Component({
  selector: 'app-user-access-control',
  templateUrl: './user-access-control.component.html',
  styleUrls: ['./user-access-control.component.scss']
})
export class UserAccessControlComponent implements OnInit {

  public companines = [];
  public branches = [];
  public users : {id: any, name: any, entitlements: any, userId: number, status: number, lastLoginDate: any, type: number} [] = [];
  public climatesi = false;
  private entilements = [
    "1-0-0-0",
    "2-0-0-0",
    "3-0-0-0",
    "4-0-0-0",
    "5-0-0-0",
    "6-0-0-0",
    "7-0-0-0",
    "8-0-0-0",
    "9-0-0-0",
    "10-0-0-0",
    "11-0-0-0",
    "12-0-0-0",
    "13-0-0-0",
    "14-0-0-0",
    "15-0-0-0",
    "16-0-0-0",
    "17-0-0-0",
    "18-0-0-0",
    "19-0-0-0",
    "20-0-0-0",
    "21-0-0-0",
    "22-0-0-0",
    "23-0-0-0",
    "24-0-0-0",
    "25-0-0-0",
    "26-0-0-0",
    "27-0-0-0",
    "28-0-0-0",
    "289-0-0-0"



  ];
  public userAccess = {
    1: {add: false, edit: false, delete: false},
    2: {add: false, edit: false, delete: false},
    3: {add: false, edit: false, delete: false},
    4: {add: false, edit: false, delete: false},
    5: {add: false, edit: false, delete: false},
    6: {add: false, edit: false, delete: false},
    7: {add: false, edit: false, delete: false},
    8: {add: false, edit: false, delete: false},
    9: {add: false, edit: false, delete: false},
    10: {add: false, edit: false, delete: false},
    11: {add: false, edit: false, delete: false},
    12: {add: false, edit: false, delete: false},
    13: {add: false, edit: false, delete: false},
    14: {add: false, edit: false, delete: false},
    15: {add: false, edit: false, delete: false},
    16: {add: false, edit: false, delete: false},
    17: {add: false, edit: false, delete: false},
    18: {add: false, edit: false, delete: false},
    19: {add: false, edit: false, delete: false},
    20: {add: false, edit: false, delete: false},
    21: {add: false, edit: false, delete: false},
    22: {add: false, edit: false, delete: false},
    23: {add: false, edit: false, delete: false},
    24: {add: false, edit: false, delete: false},
    25: {add: false, edit: false, delete: false},
    26: {add :false, edit: false, delete: false},
    27: {add :false, edit: false, delete: false},
    28: {add :false, edit: false, delete: false},
    29: {add :false, edit: false, delete: false},



  }
  public branchId = 0;
  public companyId  = 0;
  public userId = 0;
  public showControl = false;
  public resetpw = 'password';
  public loginProfile = {
    status: 1,
    lastLoginDate: '',
    loginName: '',
  }
  public statusList = [
    {value: 1, status: 'Active'},
    {value: 2, status: 'Lock'},
    {value: 3, status: 'Suspend'},
  ];

  public isMasterAdmin = false;
  public renderValue = '';

  constructor( private boService: BackendService,
               private masterData: MassterDataService,
               private toastSerivce: NbToastrService,
               private dialog: MatDialog) {
    this.companines.push({id: 0, name: 'Not Selected'})
    this.branches.push({id: 0, name: 'Not Selected'})
    this.users.push({id: 0, name: 'Not Selected', entitlements: undefined, userId: undefined, status: undefined, lastLoginDate: undefined, type: 0})
    this.masterData.loadCompaninesForDataEnry().subscribe(d => {
      this.companines = d;
      
      if(UserState.getInstance().adminId == 3){
        console.log("cccccccc====",this.companines.slice(0,5))
        this.companines = this.companines.slice(67,69);


      }

      this.companines.unshift({id: 0, name: 'Not Selected'})
    });

    this.isMasterAdmin = UserState.getInstance().ismasterAdmin;
    this.setStatusRenderValue();
  }

  loadData() {

  }

  ngOnInit() {
  }

  onClickSaveAll() {
    this.toEntitlements()

    if(this.climatesi) {
      this.boService.sendRequestToBackend(
        RequestGroup.BusinessUsers,
        RequestType.ManageAdmin,
        { DATA: {
            id: this.id_user,
            entitlements: this.entilements,
          }}
        ).then(data =>{
        console.log(data)
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.dto != undefined) {
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            });
            this.loadUsers(true, undefined, undefined)

            // this.userId = data.DAT.dto.userId;

          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        }else {
          this.toastSerivce.show('', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      });

    }else if (this.companyId != 0 && this.userId != 0 && this.branchId === 0) {
      if (this.usertype === 'Data Entry User') {
        this.boService.sendRequestToBackend(
          RequestGroup.BusinessUsers,
          RequestType.ManageDataEntryUser,
          {
            DATA: {
              id: this.id_user,
              entitlements: this.entilements,
            }
          }
        ).then( data => {
          console.log(data)
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.dto != undefined) {
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              });

            } else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          }else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      }else {
        this.boService.sendRequestToBackend(
          RequestGroup.BusinessUsers,
          RequestType.ManageComAdmin,
          {
            DATA: {
              id: this.id_user,
              entitlements: this.entilements,
            }
          }
        ).then( data => {
          console.log(data)
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.dto != undefined) {
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              });

            } else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          }else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      }


    }else if(this.companyId != 0 && this.branchId != 0 && this.userId != 0) {
      this.boService.sendRequestToBackend(
        RequestGroup.BusinessUsers,
        RequestType.ManageClient,
        {
          DATA: {
            id: this.id_user,
            entitlements: this.entilements,
          }
        }
      ).then( data => {
        console.log(data)
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.dto != undefined) {
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            });


            // this.userId = data.DAT.dto.userId;

          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        }else {
          this.toastSerivce.show('', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      });
    }

  }

  selectAll($event: MatCheckboxChange) {
    if (this.userAccess) {
      for (const key of Object.keys(this.userAccess)) {
        if ($event.checked) {
          this.userAccess[key] = {add : true, delete: true, edit: true};
        }else {
          this.userAccess[key] = {add : false, delete: false, edit: false};
        }
      }
    }
  }

  fromEntitlements() {
    this.entilements.forEach(v => {
      let values: string [] = v.split('-');
      let pageNum = +values[0];
      let add = values[1] === '0'? false: true;
      let edit = values[2] === '0'? false: true;
      let del = values[3] === '0'? false: true;
      this.userAccess[pageNum].add = add;
      this.userAccess[pageNum].edit = edit;
      this.userAccess[pageNum].delete = del;

    });
  }
  toEntitlements() {
    this.entilements = [];
    let keys = Object.keys(this.userAccess);
    keys.forEach(k => {
      let str = `${k}-${this.userAccess[k].add ? 1: 0}-${this.userAccess[k].edit ? 1: 0}-${this.userAccess[k].delete ? 1: 0}`;
      this.entilements.push(str);
    })
  }

  onChangeCompany(value: any) {
    this.branchId = 0;
    this.userId = 0;
    this.branches = [];
    if (value !== 0) {
      this.loadUsers(false, undefined, value);
    }

    if(this.branches.length == 0) {
      this.branches.push({id: 0, name: 'Not Selected'})
    }
    if (this.users.length == 0) {
      this.users.push({id: 0, name: 'Not Selected', entitlements: undefined, userId: 0, status: undefined, lastLoginDate: undefined, type: 0})
    }
     // this.loadBranches(value);
  }

  onChangeBranch(value: any) {
    this.userId = 0;
    if (this.branchId !== 0 && this.companyId != 0) {
      this.loadUsers(false, value, undefined);
    }
  }

  onChangeMatCheck($event: MatCheckboxChange) {
    this.userId = 0;
    this.companyId = 0;
    this.branchId = 0;
    if ($event.checked) {
      this.loadUsers(true, undefined, undefined)
    } else {

    }
  }

  public usertype = ''
  public id_user = -1;
  onChangeUsers($event: MatSelectChange) {
    if (this.userId === 0) return;
    const user = this.users.filter(v => {
      return v.userId === $event.value;
    })[0];
    this.id_user = user.id;
    console.log('-------------------------------------------')
    console.log(user);
    console.log('-------------------------------------------')

    switch(user.type) {
      case 1:
        this.usertype = 'Branch Account';
        break;
      case 2:
        this.usertype = 'Company Admin';
        break;
      case 3:
        this.usertype = 'Climate SI Admin';
        break;
      case 4:
        this.usertype = 'Data Entry User';
        break;
      default:
        this.usertype = ''
    }

    console.log(user)

    if (user !== undefined) {
      this.loadUserLoginProfile(user.userId)
      //load UserLoginProfiles
      this.entilements = user.entitlements;
      this.fromEntitlements();
      this.showControl = true;
    }
  }

  public resetPassword($event) {

    if (this.resetpw !== '') {
      const user = this.users.filter(u => u.userId === this.userId)[0];
      if (user === undefined) {
       
        return;
      }

      this.boService.sendRequestToBackend(
        RequestGroup.LoginUser,
        RequestType.ManageUserLogin,
        {
          DATA: {
            userId: user.userId,
            // userType: 3,
            // loginStatus: 1,
            // userName: this.settingModel.userName,
            password: this.resetpw,
            // isFirstTime: 1,
          }
        }
      ).then(data => {
        console.log(data)
        if (data.HED != undefined && data.HED.RES_STS == 1) {

          if (data.DAT != undefined && data.DAT.dto != undefined) {
            this.toastSerivce.show('', 'Password reset successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            });
          }else {
            this.toastSerivce.show('' ,'Error in resetting password.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        }else {
          this.toastSerivce.show('', 'Error in resetting password', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      })
    }else {

      this.toastSerivce.show('', 'Enter Password to Reset', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      })
    }
  }
  
  public viewLogs($event:any) {

    const user = this.users.filter(u => u.userId === this.userId)[0];
   if (user === undefined) {
   return;
    }
    const dialogRef = this.dialog.open(ActivityLogsComponent,//ActivityLogsComponent
      {
       data : { userId: user.userId},
        width: '400px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
        

    });
  }

  public setStatusRenderValue(){
    if(this.loginProfile.status ==1) {
      this.renderValue = 'Active'
      this.statusList = [
        {value: 2, status: 'Lock'},
        {value: 3, status: 'Suspend'},

      ]
    }else if (this.loginProfile.status == 2) {
      this.renderValue = 'Locked'
      this.statusList = [
        {value: 1, status: 'Active'},
        {value: 3, status: 'Suspend'},

      ]
    }else if (this.loginProfile.status == 3) {
      this.renderValue = 'Suspended'
      this.statusList = [
        {value: 1, status: 'Active'},
        {value: 2, status: 'Lock'},

      ]
    }

  }

  loadUserLoginProfile(userId: number) {
    this.boService.sendRequestToBackend(RequestGroup.LoginUser, RequestType.FindUserLogin, { userId: userId }).then(data=> {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        console.log(data)
        if (data.DAT != undefined ) {
          let user = this.users.filter(u => u.userId === userId)[0];
          user.lastLoginDate = data.DAT.lastLoginDate;
          user.status = data.DAT.status;
          this.loginProfile.status = user.status;
          this.loginProfile.lastLoginDate = user.lastLoginDate;
          this.loginProfile.loginName = data.DAT.loginName
          this.setStatusRenderValue();
        }else {
        }
      }else {

      }
    })
    console.log(this.users)
  }


  loadBranches(comId: number) {
    console.log(comId);
    this.masterData.getBranchesForCompany(comId).subscribe(d => this.branches = d)
  }
  loadUsers(climatesi:boolean, branchId: number, comId: number) {

    this.users = []

    if (climatesi) {
      //load admins
      this.boService.sendRequestToBackend(
        RequestGroup.BusinessUsers,
        RequestType.ListAdmin,
        {
          PAGE_NUMBER: -1,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];

            data.DAT.list.forEach(val => {
              if (val != undefined) {
                if (UserState.getInstance().userId !== val.userId) {
                  this.users.push({
                    id: val.id,
                    name: val.title + " " + val.firstName + " " + val.lastName,
                    entitlements: val.entitlements,
                    userId: val.userId,
                    status: undefined, lastLoginDate: undefined,
                    type: 3,
                  });
                }

                console.log(this.users);
              }
            });
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }else{
      if (comId !== undefined && comId !== 0 && (branchId ===0 || branchId === undefined)){
        this.boService.sendRequestToBackend(
          RequestGroup.BusinessUsers,
          RequestType.ListComAdmin,
          {
            FILTER_MODEL: {
              companyId: { value: comId, type: 1, col: 1}
            },
            PAGE_NUMBER: -1,
          }
        ).then(data => {
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.list != undefined) {
              let list = [];
              this.users = [];
              data.DAT.list.forEach(val => {
                if (val != undefined) {
                  console.log(val);
                  this.users.push(
                    {
                      id: val.id,
                      name: val.title + " " + val.firstName + " " + val.lastName,
                      entitlements: val.entitlements,
                      userId: val.userId,
                      status: undefined, lastLoginDate: undefined,
                      type: 2,
                    }
                  );
                  console.log(this.users);
                }
              });
              this.boService.sendRequestToBackend(
                RequestGroup.BusinessUsers,
                RequestType.ListDataEntryUser,
                {
                  FILTER_MODEL: {
                    companyId: { value: comId, type: 1, col: 1}
                  }
                }
              ).then(data => {
                if (data.HED != undefined && data.HED.RES_STS == 1) {
                  if (data.DAT != undefined && data.DAT.list != undefined) {
                    let list = [];
                    // this.users = [];
                    data.DAT.list.forEach(val => {
                      if (val != undefined) {
                        console.log(val);
                        this.users.push(
                          {
                            id: val.id,
                            name: val.title + " " + val.firstName + " " + val.lastName,
                            entitlements: val.entitlements,
                            userId: val.userId,
                            status: undefined, lastLoginDate: undefined,
                            type: 4
                          }
                        );
                        console.log(this.users);
                      }
                    });
                    this.boService.sendRequestToBackend(
                      RequestGroup.BusinessUsers,
                      RequestType.ListDataEntryUser,
                      {
                        FILTER_MODEL: {
                          companyId: { value: comId, type: 1, col: 1}
                        }
                      }
                    ).then(data => {
                      if (data.HED != undefined && data.HED.RES_STS == 1) {
                        if (data.DAT != undefined && data.DAT.list != undefined) {
                          let list = [];
                          data.DAT.list.forEach(val => {
                            if (val != undefined) {
                              if (this.users.filter(user => user.userId === val.id) === undefined ||
                                this.users.filter(user => user.userId === val.id) === []
                              ) {
                                this.users.push(
                                  {
                                    id: val.id,
                                    name: val.title + " " + val.firstName + " " + val.lastName,
                                    entitlements: val.entitlements,
                                    userId: val.userId,
                                    status: undefined, lastLoginDate: undefined,
                                    type: 4,
                                  }
                                );
                              }

                              console.log(this.users);
                            }
                          });
                        } else {
                          //  todo: show snack bar
                          console.log('error data 1')
                        }
                      } else {
                        //  todo: show snack bar
                        console.log('error data 2')
                      }
                    })
                  } else {
                    //  todo: show snack bar
                    console.log('error data 1')
                  }
                } else {
                  //  todo: show snack bar
                  console.log('error data 2')
                }
              })
            } else {
              //  todo: show snack bar
              console.log('error data 1')
            }
          } else {
            //  todo: show snack bar
            console.log('error data 2')
          }
        })

      }
      if ((comId === undefined || comId === 0 )&& branchId !== 0 && branchId !== undefined) {
        this.boService.sendRequestToBackend(
          RequestGroup.BusinessUsers,
          RequestType.ListClient,
          {
            FILTER_MODEL: {
              branchId: { value: branchId , type: 1, col: 1}
            },
            PAGE_NUMBER: -1,
          }
        ).then(data => {
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.list != undefined) {
              this.users = [];
              data.DAT.list.forEach(val => {
                if (val != undefined) {
                  this.users.push({
                    id: val.id,
                    name: val.title + " " + val.firstName + " " + val.lastName,
                    entitlements: val.entitlements,
                    userId: val.userId,
                    status: undefined, lastLoginDate: undefined,
                    type: 1,
                  });
                  console.log(this.users);
                }
              });
            } else {
              //  todo: show snack bar
              console.log('error data 1')
            }
          } else {
            //  todo: show snack bar
            console.log('error data 2')
          }
        })
      }

    }

  }


  onSearchBranch(value: any) {
    if (value === '') {
      return;
    }
    let queryReq = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        name: { value: value , type: 4, col: 3},

      },
    }
    if (this.companyId != 0) {
      queryReq.FILTER_MODEL[ 'companyId'] = { value: this.companyId, type: 1, col: 1}
    }
    this.branches = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      queryReq
    ).then(data => {
      console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.branches = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.branches.unshift({id: val.id, name: val.name});
            }
          });
          if (this.branches.length === 0){
            this.branches.push({id: 0, name: 'No Matches Found'})
          }
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  public disableUsers() :boolean {
    if (!this.climatesi && (this.companyId === 0)) return true
    return false;
  }
  public disableCompany(): boolean {
    if (this.climatesi) return true;
    return false;
  }

  public disableBranch(): boolean {
    if (this.climatesi) return true;
    if (this.companyId === 0) return true;
    return false;
  }

  onClickChangeStatus(value: any) {

    console.log(this.users)
    const user = this.users.filter(u => u.userId === this.userId)[0];
    if (user === undefined) {

      return;
    }
    this.boService.sendRequestToBackend(
      RequestGroup.LoginUser,
      RequestType.ManageUserLogin,
      {
        DATA: {
          userId: user.userId,
          // userType: 3,
          loginStatus: value,
          // userName: this.settingModel.userName,
          // password: this.resetpw,
          // isFirstTime: 1,
        }
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {

        if (data.DAT != undefined && data.DAT.dto != undefined) {
          this.loginProfile.status = value;
          this.setStatusRenderValue();
          this.toastSerivce.show('', 'Change status successfully.', {
            status: 'success',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          });
        }else {

          this.toastSerivce.show('' ,'Error in changing status.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      }else {

        this.toastSerivce.show('', 'Error in changing status.', {
          status: 'danger',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    })
  }
}
