import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminProjectSummaryComponent } from './admin-project-summary.component';

describe('AdminProjectSummaryComponent', () => {
  let component: AdminProjectSummaryComponent;
  let fixture: ComponentFixture<AdminProjectSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminProjectSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProjectSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
