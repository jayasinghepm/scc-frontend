import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCadminComponent } from './add-cadmin.component';

describe('AddCadminComponent', () => {
  let component: AddCadminComponent;
  let fixture: ComponentFixture<AddCadminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCadminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCadminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
