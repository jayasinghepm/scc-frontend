import {Component, Inject, OnInit, Optional} from '@angular/core';
import {BackendService} from "../../../../@core/rest/bo_service";
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from "@angular/material";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {MassterDataService} from "../../../../@core/service/masster-data.service";
import {RequestGroup} from "../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../@core/enums/request-type-enums";

@Component({
  selector: 'app-add-cadmin',
  templateUrl: './add-cadmin.component.html',
  styleUrls: ['./add-cadmin.component.scss']
})
export class AddCadminComponent implements OnInit {

  public endDate:any;
  public startDate: any;
  private userId: number;
  private clientId: number;

  public titlesOptions: string [];
  public companyOptions: any [];
  public branchOptions: string [];
  public emissionSrcOptions: string [];



  public isClientActive = false;
  // public btnText =

  public userName;
  public password;
  public header = "Create Company Admin"
  public btnText = ""
  public disableClose = false;
  public isDisableSave = false;



  public titleModel = ''
  public companyModel = ''
  public titles = [];
  public companines = [];
  public company;


  lastnameModel: any;
  firstNameModel: any;

  constructor(
    private boService:BackendService,
    public dialogRef: MatDialogRef<AddCadminComponent>,
    private  toastSerivce:  NbToastrService,
    private masterData: MassterDataService,
    @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any,

  ) {
    this.masterData.getCompaninesFull().subscribe( d => {
      this.companines = d;
    })
    this.masterData.getTitles().subscribe(d => {
      this.titles = d;
    })
    // console.log(this.popupData)

    if (popupData.isEdit) {
      this.header = "Edit Company Admin"
      this.isClientActive = true;
      this.btnText = "Save";
      this.userId = this.popupData.data.userId;
      // id,
      //   company: dto.company,
      //   firstName: dto.firstName,
      //   lastName: dto.lastName,
      //   email,
      //   mobileNo,
      //   telephoneNo,
      //   position,
      //   userId,
      //   companyId: dto.companyId,
      this.titleModel = this.popupData.data.title;
      this.firstNameModel = this.popupData.data.firstName;
      this.lastnameModel = this.popupData.data.lastName;
      this.companyModel = this.popupData.data.companyId;

      // console.log(this.popupData.data)
      this.clientId = this.popupData.data.id;
    } else {
      this.header = "Create Company Admin"
      this.isClientActive = false;
      this.btnText = "Save"
    }
  }

    ngOnInit() {
  }

  closeDialog() {
    if (!this.disableClose) {
      this.dialogRef.close();
    }
  }

  onClickSave() {
    if (this.popupData.isEdit) {

      let jsonBody = {
        DATA: {
          id: this.popupData.isEdit ? this.clientId : -1,
          title: this.titleModel,
          firstName: this.firstNameModel,
          lastName: this.lastnameModel,
          userId: this.userId,
          companyId: this.companyModel,
          company: this.company,
        }
      }

      this.boService.sendRequestToBackend(RequestGroup.BusinessUsers, RequestType.ManageComAdmin,jsonBody).then( data => {
        // console.log(data)
        this.isDisableSave= false;
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.dto != undefined) {
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            });

            this.disableClose = false;
            // this.userId = data.DAT.dto.userId;
            this.dialogRef.close(true);
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        }else {
          this.toastSerivce.show('', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      })
      return ;
    }
    if (this.validate()) {
      this.boService.sendRequestToBackend(
        RequestGroup.LoginUser,
        RequestType.ManageUserLogin,
        {
          DATA: {
            userId: -1,
            userType: 2,
            loginStatus: 1,
            userName: this.userName,
            password: this.password,
            isFirstTime: 2,
          }
        }
      ).then(data  => {
        this.isDisableSave= false;
        // console.log(data)
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.dto != undefined) {
            this.userId = data.DAT.dto.userId;

            let jsonBody = {
              DATA: {
                id: this.popupData.isEdit ? this.clientId : -1,
                title: this.titleModel,
                firstName: this.firstNameModel,
                lastName: this.lastnameModel,
                userId: this.userId,
                companyId: this.companyModel,
                company: this.company,
              }
            }

            this.boService.sendRequestToBackend(RequestGroup.BusinessUsers, RequestType.ManageComAdmin,jsonBody).then( data => {
              // console.log(data)
              this.isDisableSave= false;
              if (data.HED != undefined && data.HED.RES_STS == 1) {
                if (data.DAT != undefined && data.DAT.dto != undefined) {
                  this.toastSerivce.show('', 'Saved data successfully.', {
                    status: 'success',
                    destroyByClick: true,
                    duration: 2000,
                    hasIcon: false,
                    position: NbGlobalPhysicalPosition.TOP_RIGHT,
                    preventDuplicates: true,
                  });

                  this.disableClose = false;
                  // this.userId = data.DAT.dto.userId;
                  this.dialogRef.close(true);
                } else {
                  this.toastSerivce.show('', 'Error in saving data.', {
                    status: 'danger',
                    destroyByClick: true,
                    duration: 2000,
                    hasIcon: false,
                    position: NbGlobalPhysicalPosition.TOP_RIGHT,
                    preventDuplicates: true,
                  })
                }
              }else {
                this.toastSerivce.show('', 'Error in saving data.', {
                  status: 'danger',
                  destroyByClick: true,
                  duration: 2000,
                  hasIcon: false,
                  position: NbGlobalPhysicalPosition.TOP_RIGHT,
                  preventDuplicates: true,
                })
              }
            })
          } else {
            this.toastSerivce.show('Try with different user name', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        }
        else {
          this.toastSerivce.show('Try with different user name', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      })
    }else {
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }

    // ------------------------------------------------



  }






  private validate():boolean {
    if (this.password === undefined || this.password === '') {
      return false;
    }
    if (this.userName === undefined || this.userName === '') {
      return false;
    }
    if (this.titleModel === undefined || this.titleModel === '') {
      // console.log('title')
      return false;
    } if (this.firstNameModel === undefined || this.firstNameModel === '') {
      // console.log('firstname')
      return false;
    } if (this.lastnameModel === undefined || this.lastnameModel === '') {
      // console.log('lastname')
      return false;
    }
    // if (this.emissionSrcModel === undefined || typeof this.emissionSrcModel !== typeof [] ||  this.emissionSrcModel.length ===0 ) {
    //   return false;
    // }

    if (this.companyModel === undefined || this.companyModel === '') {
      // console.log('title')
      return false;
    }
    // if (this.branchModel === undefined || typeof this.branchModel !== typeof [] ||  this.branchModel.length ===0 ) {
    //   console.log(this.branchModel)
    //   return false;
    // } if (this.startDate === undefined || this.startDate === '') {
    //   return false;
    // } if (this.endDate === undefined || this.endDate === '') {
    //   return false;
    // }
    // if (this.userId === undefined ||  this.userId === 0) {
    //   // console.log('user id ljslfjsfjsf');
    //   return false;
    // }
    if (this.popupData.isEdit && (this.clientId === undefined || this.clientId === 0)) {
      // console.log('client id')
      return false;
    }

    return true;
  }

  onChangeCompany($event: MatSelectChange) {
    this.company = this.companines.filter(c => c.id === $event.value)[0].name;
  }
}
