import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewComadminComponent } from './view-comadmin.component';

describe('ViewComadminComponent', () => {
  let component: ViewComadminComponent;
  let fixture: ComponentFixture<ViewComadminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewComadminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewComadminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
