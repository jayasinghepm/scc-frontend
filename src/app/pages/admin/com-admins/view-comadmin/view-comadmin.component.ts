import { Component, OnInit } from '@angular/core';
import {CadminStatus} from "../../../../@core/enums/CadminStatus";
import {MassterDataService} from "../../../../@core/service/masster-data.service";
import {BackendService} from "../../../../@core/rest/bo_service";
import {DomSanitizer} from "@angular/platform-browser";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {RequestGroup} from "../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../@core/enums/request-type-enums";
import {UserState} from "../../../../@core/auth/UserState";
import {ActivatedRoute} from "@angular/router";


class ImageSnippet {
  constructor(public src: string, public file: File) {
  }
}


@Component({
  selector: 'app-view-comadmin',
  templateUrl: './view-comadmin.component.html',
  styleUrls: ['./view-comadmin.component.scss']
})
export class ViewComadminComponent implements OnInit {
  public selectedFile: ImageSnippet;
  public logoAvailable = false;

  public cadminStatus = CadminStatus.NotSubmitted;
  public cadminStatusMsg = '';

  public completed = {
    1: false,
    2: false,
    3: false,
    4: false,
  }

  public titlesOptions: string [];
  public titleConfig = {
    search: false,

  };
  public districtConfig = {
    search: false,
  }

  public districtsOptions: string[] =[]

  public projectId;

  public cadminInfo= {
    firstName: '',
    lastName: '',
    title: '',
    telephoneNo: '',
    mobileNo: '',
    position: '',
    email: ''
  }

  public companyBasicInfo = {
    name: '',
    sector: '',
    code: '',
    noOfEmployees: 0,
    noOfBranches: 0,
    addr1: '',
    addr2: '',
    district: '',

  }
  public comAdvInfo = {
    logo: '',
    baseYear: '',
    yearEmission: []
  }
  public comFinInfo = {
    fyMonthStart: '',
    fyMonthEnd: '',
    fyCurrentStart: 0,
    fyCurrentEnd: 0,
    annumRevFY: 0,
    targetGHG: 0,

  }

  public fyEmissionStartModel = '';
  public fyEmissionEndModel =''
  public emissionInputModel = '';


  confirmed = [

  ]

  public emission = {
    fyyearStart: '',
    fyyearEnd: '',
    emission: '',
  }

  emissionYears = [];
  logoImage = ''
  public monthOptions: string[] = [];

  private cadminId;
  private comId;


  constructor(private masterData : MassterDataService,
              private boService: BackendService,
              private _sanitizer: DomSanitizer,
              private  toastSerivce:  NbToastrService,
              private route: ActivatedRoute
  ) {
    this.masterData.getTitles().subscribe(d =>{ this.titlesOptions =d;});
    this.masterData.getDistricts().subscribe(d => { this.districtsOptions = d});
    this.masterData.getMonths().subscribe(d =>  this.monthOptions =d);
    // Todo: intiate data with existing
    // this.image = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + popupData.QuestionEntity.image);
    // todo:
    this.logoAvailable = true;


  }

  ngOnInit() {
    this.route.queryParams.subscribe(next => {
      // console.log(next)
      this.cadminId = next.cAdminId;
      this.comId = next.comId;
      this.loadCadminInfo();
      this.loadCompanyInfo();
      this.initProject();
      this.loadPreviousYearReports();
    });
  }

  public onClickAddEmission($event: MouseEvent) {
    // console.log('jlsfjsljfsjl')
    if (this.emission.fyyearEnd !== undefined && this.emission.fyyearEnd !== '' && this.emission.fyyearStart !== undefined && this.emission.fyyearStart !== '' && this.emission.emission !== undefined && this.emission.emission !== '') {
      // console.log("jlsjfsfs")
      this.emissionYears.push(JSON.parse(JSON.stringify(this.emission)));
      this.emission.emission = '';
      this.emission.fyyearStart = '';
      this.emission.fyyearEnd = ''
    }
  }
  public  onClickDeleteEmissionYears(item: any) {
    this.emissionYears = this.emissionYears.filter(v => { return !(v.fyyearStart === item.fyyearStart && v.fyyearEnd === item.fyyearEnd && v.emission === item.emission)})
  }

  public onFileChanged(image: any): void {
    const file: File = image.target.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.selectedFile =new ImageSnippet(event.target.result, file);
      if (this.selectedFile) {
        this.comAdvInfo.logo = this.selectedFile.src;
        // console.log(this.comAdvInfo.logo)
        if (this.comAdvInfo.logo !== '' && this.comAdvInfo.logo !== undefined) {
          this.logoImage = this.comAdvInfo.logo;
          this.logoAvailable = true;
          // this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + this.companyBasicInfo.logo)
        }
      }
    });

    reader.readAsDataURL(file);
  }


  public onMonthChange($event: any, num: number) {
    if ($event.value === '' || $event.value === undefined) return;
    if (num === 1) {
      this.comFinInfo.fyMonthStart = $event.value;
      // this.masterData.getMonthId($event.value).subscribe(d => this.comFinInfo.fyMonthStart =d);
    } else if(num === 2) {
      this.comFinInfo.fyMonthEnd = $event.value;
      // this.masterData.getMonthId($event.value).subscribe(d => this.comFinInfo.fyMonthEnd =d);
    }
  }


  private loadCadminInfo(): void {
    this.boService.sendRequestToBackend(
      RequestGroup.BusinessUsers,
      RequestType.ListComAdmin,
      {
        FILTER_MODEL: {
          id: { value: this.cadminId , type: 1, col: 1}
        },
        PAGE_NUM: 1,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {

          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.cadminInfo.firstName = val.firstName;
              this.cadminInfo.lastName = val.lastName;
              this.cadminInfo.telephoneNo = val.telephoneNo;
              this.cadminInfo.mobileNo = val.mobileNo;
              this.cadminInfo.title = val.title;
              this.cadminInfo.email = val.email;
              this.cadminInfo.position = val.position;
              this.cadminStatus = val.status;
              this.cadminStatusMsg = val.statusMessage ;
              if (val.firstName !== undefined && val.firstName !== ''
                && val.lastName !== undefined && val.lastName !== ''
                && val.telephoneNo !== undefined && val.telephoneNo !== ''
                && val.mobileNo !== undefined && val.mobileNo !== ''
                && val.title !== undefined && val.title !== ''
                && val.email !== undefined && val.email !== ''
                && val.position !== undefined && val.position !== ''
              ) {
                this.completed["1"] = true;
              }
            }
          });
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  private loadCompanyInfo(): void {
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListCompany,
      {
        isLogoRequired: 1,
        FILTER_MODEL: {
          id: { value: this.comId , type: 1, col: 1}
        },
        PAGE_NUM: 1,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {

          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.companyBasicInfo.name = val.name;
              this.companyBasicInfo.code = val.code ;
              this.companyBasicInfo.sector = val.sector ;
              this.companyBasicInfo.addr1 = val.addr1 ;
              this.companyBasicInfo.addr2 = val.addr2 ;
              this.companyBasicInfo.district = val.district;
              this.companyBasicInfo.noOfBranches = val.noOfBranches ;
              this.companyBasicInfo.noOfEmployees = val.noOfEmployees;

              if (val.name !== undefined && val.name !== ''
                && val.code !== undefined && val.code !== ''
                && val.sector !== undefined && val.sector !== ''
                && val.addr1 !== undefined && val.addr1 !== ''
                && val.addr2 !== undefined && val.addr2 !== ''
                && val.district !== undefined  && val.district !== ''
                && val.noOfBranches !== 0 && val.noOfBranches !== ''
                && val.noOfEmployees !== 0 &&  val.noOfEmployees !== ''
              ) {
                this.completed["2"] = true;
              }


              this.comAdvInfo.baseYear = val.baseYear;
              this.comAdvInfo.logo = val.logo;
              this.logoImage = val.logo;

              if (val.baseYear !== undefined && val.baseYear !== ''
                && val.logo !== undefined && val.logo !== ''
                && val.yearEmission.list !== undefined && val.yearEmission.list.length !== 0
              ) {
                this.completed["3"] = true;
              }

              if (this.logoImage !== undefined && this.logoImage !== '') {
                this.logoAvailable = true;
              }
              this.comAdvInfo.yearEmission = val.yearEmission.list;
              this.emissionYears = this.comAdvInfo.yearEmission;

              this.comFinInfo.annumRevFY = val.annumRevFY ;
              let monStart;
              let monEnd;
              this.masterData.getMonthName(val.fyMonthEnd ).subscribe(d => { monEnd  = d; console.log(d)} )
              this.comFinInfo.fyMonthEnd = monEnd ;
              this.comFinInfo.targetGHG = val.targetGHG;
              this.masterData.getMonthName(val.fyMonthStart).subscribe(d => { monStart = d; console.log(d)})
              this.comFinInfo.fyMonthStart = monStart ;
              this.comFinInfo.fyCurrentEnd = val.fyCurrentEnd ;
              this.comFinInfo.fyCurrentStart = val.fyCurrentStart ;
              if (val.annumRevFY !== undefined && val.annumRevFY !== 0
                && val.fyMonthEnd !== undefined && val.fyMonthEnd > 11
                && val.fyMonthStart !== undefined && val.fyMonthStart >11
                && val.fyCurrentStart !== undefined && val.fyCurrentStart <= 0
                && val.fyCurrentEnd !== undefined && val.fyCurrentEnd <=0

              ) {
                this.completed["4"] = true;
              }
              // console.log(this.comFinInfo)



            }
          });

        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  private updateCompanyBasicInfo() :void {
    if (!this.validateComBasicInfo()) {
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }
    let jsonData ={
      DATA: {
        id: UserState.getInstance().companyId,
        name: this.companyBasicInfo.name,
        sector: this.companyBasicInfo.sector,
        code: this.companyBasicInfo.code,
        noOfEmployees: this.companyBasicInfo.noOfEmployees,
        noOfBranches: this.companyBasicInfo.noOfBranches,
        addr1: this.companyBasicInfo.addr1,
        addr2: this.companyBasicInfo.addr2,
        district: this.companyBasicInfo.district,
      }
    }

    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ManageCompany,
      jsonData
    ).then( data => {
      if (data !== undefined && data.DAT !== undefined &&  data.DAT.dto !== undefined) {
        this.toastSerivce.show('', 'Saved data successfully.', {
          status: 'success',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        });
        this.completed["2"] = true;
        if (this.completed["1"] && this.completed["4"] && this.completed["3"]) {
          // todo:
        }
      } else {
        this.toastSerivce.show('', 'Error in saving data.', {
          status: 'danger',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    })

  }

  private updateCompanyAdvInfo():void {
    this.comAdvInfo.yearEmission = this.emissionYears;
    if (!this.validateComAdvInfo()) {
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
    }
    let jsonData ={
      DATA: {
        id: UserState.getInstance().companyId ,
        yearEmission: {list: this.comAdvInfo.yearEmission},
        baseYear: this.comAdvInfo.baseYear ,
        logo: this.comAdvInfo.logo
      }
    }
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ManageCompany,
      jsonData
    ).then( data => {
      if (data !== undefined && data.DAT !== undefined &&  data.DAT.dto !== undefined) {
        this.toastSerivce.show('', 'Saved data successfully.', {
          status: 'success',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
        this.completed["3"] = true;
        if (this.completed["1"] && this.completed["2"] && this.completed["4"]) {
          // todo:
        }
      } else {
        this.toastSerivce.show('', 'Error in saving data.', {
          status: 'danger',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    })
  }
  private updateCompanyFinInfo():void {
    if (!this.validateComFinaInfo()) {
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return
    }
    let monStart;
    let monEnd;
    this.masterData.getMonthId(this.comFinInfo.fyMonthStart).subscribe(d => { monStart =d;});
    this.masterData.getMonthId(this.comFinInfo.fyMonthEnd).subscribe(d => { monEnd = d;})
    let jsonData ={
      DATA: {
        id: UserState.getInstance().companyId,
        targetGHG: this.comFinInfo.targetGHG,
      }
    }
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ManageCompany,
      jsonData
    ).then( data => {
      if (data !== undefined && data.DAT !== undefined &&  data.DAT.dto !== undefined) {
        this.toastSerivce.show('', 'Saved data successfully.', {
          status: 'success',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
        this.completed["4"] = true;
        if (this.completed["1"] && this.completed["2"] && this.completed["3"]) {
          // todo:
        }
      } else {
        this.toastSerivce.show('', 'Error in saving data.', {
          status: 'danger',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    })
  }

  private updateCadminInfo(): void {
    if (!this.validateCadminInfo()) {
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }
    let jsonBody = {
      DATA: {
        id: UserState.getInstance().clientAdminId,
        title: this.cadminInfo.title,
        firstName: this.cadminInfo.firstName,
        lastName: this.cadminInfo.lastName,
        // userId: this.userId,
        email: this.cadminInfo.email,
        // companyId: companyId,
        telephoneNo: this.cadminInfo.telephoneNo,
        mobileNo: this.cadminInfo.mobileNo,
        position: this.cadminInfo.position,
        // status
        // entitlements: entitleMents ,
        // agreementStartDate: this.convert(this.startDate),
        // agreementEndDate: this.convert(this.endDate)
      }
    }

    this.boService.sendRequestToBackend(RequestGroup.BusinessUsers, RequestType.ManageComAdmin,jsonBody).then( data => {
      // console.log(data)
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.dto != undefined) {
          this.toastSerivce.show('', 'Saved data successfully.', {
            status: 'success',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          });
          this.completed["1"] = true;
          if (this.completed["2"] && this.completed["3"] && this.completed["4"]) {
            // todo status to pending
          }


        } else {
          this.toastSerivce.show('', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      }else {
        this.toastSerivce.show('', 'Error in saving data.', {
          status: 'danger',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    })
  }

  private validateComBasicInfo(): boolean {
    // console.log(this.companyBasicInfo)
    if (this.companyBasicInfo.name === undefined || this.companyBasicInfo.name === '') {
      return false;
    }
    if (this.companyBasicInfo.sector === undefined || this.companyBasicInfo.sector === '') {
      return false;
    }
    if (this.companyBasicInfo.code === undefined || this.companyBasicInfo.code === '') {
      return false;
    }
    if (this.companyBasicInfo.addr1 === undefined || this.companyBasicInfo.addr1 === '') {
      return false;
    }
    if (this.companyBasicInfo.addr2 === undefined || this.companyBasicInfo.addr2 === '') {
      return false;
    }
    if (this.companyBasicInfo.district === undefined || this.companyBasicInfo.district === '') {
      // console.log(this.companyBasicInfo.district)
      return false;
    }
    if (this.companyBasicInfo.noOfEmployees === undefined || this.companyBasicInfo.noOfEmployees <= 0) {
      return false;
    }
    if (this.companyBasicInfo.noOfBranches === undefined || this.companyBasicInfo.noOfBranches <= 0) {
      return false;
    }
    return true;
  }
  private validateComAdvInfo(): boolean  {
    if (this.comAdvInfo.logo === undefined || this.comAdvInfo.logo === '') {
      return false;
    }
    if (this.comAdvInfo.baseYear === undefined || this.comAdvInfo.baseYear === '') {
      return false;
    }
    if (this.comAdvInfo.yearEmission === undefined || this.comAdvInfo.yearEmission.length == 0) {
      return false;
    }
    return true;
  }
  private validateComFinaInfo(): boolean {
    if (this.comFinInfo.fyMonthStart === undefined || this.comFinInfo.fyMonthStart === '') {
      return false;
    }
    if (this.comFinInfo. fyMonthEnd === undefined || this.comFinInfo. fyMonthEnd  === '') {
      return false;
    }
    if (this.comFinInfo. fyCurrentStart === undefined || this.comFinInfo. fyCurrentStart <= 0 ) {
      return false;
    }
    if (this.comFinInfo.fyCurrentEnd === undefined || this.comFinInfo.fyCurrentEnd <= 0 ) {
      return false;
    }
    if (this.comFinInfo.annumRevFY === undefined || this.comFinInfo.annumRevFY <= 0 ) {
      return false;
    }
    return true;

  }
  private validateCadminInfo(): boolean {
    if (this.cadminInfo.firstName === '' || this.cadminInfo.firstName == undefined) {
      return false;
    }
    if (this.cadminInfo.lastName === '' || this.cadminInfo.lastName == undefined) {
      return false;
    }
    if (this.cadminInfo.title === '' || this.cadminInfo.title == undefined) {
      return false;
    }
    if (this.cadminInfo.telephoneNo === '' || this.cadminInfo.telephoneNo == undefined) {
      return false;
    }
    if (this.cadminInfo.mobileNo === '' || this.cadminInfo.mobileNo == undefined) {
      return false;
    }
    if (this.cadminInfo.position === '' || this.cadminInfo.position == undefined) {
      return false;
    }
    if (this.cadminInfo.email === '' || this.cadminInfo.email == undefined) {
      return false;
    }
    return true;
  }

  private changeToPending(): void {
    let jsonBody = {
      DATA: {
        id: UserState.getInstance().clientAdminId,
        // title: this.cadminInfo.title,
        // firstName: this.cadminInfo.firstName,
        // lastName: this.cadminInfo.lastName,
        // // userId: this.userId,
        // email: this.cadminInfo.email,
        // // companyId: companyId,
        // telephoneNo: this.cadminInfo.telephoneNo,
        // mobileNo: this.cadminInfo.mobileNo,
        // position: this.cadminInfo.position,
        status: CadminStatus.Pending,
        // entitlements: entitleMents ,
        // agreementStartDate: this.convert(this.startDate),
        // agreementEndDate: this.convert(this.endDate)
      }
    }

    this.boService.sendRequestToBackend(RequestGroup.BusinessUsers, RequestType.ManageComAdmin,jsonBody).then( data => {
      // console.log(data)
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.dto != undefined) {
          this.toastSerivce.show('', 'Information was sent to ClimateSI.', {
            status: 'success',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          });
          this.cadminStatus = CadminStatus.Pending;


        } else {
          this.toastSerivce.show('', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      }else {
        this.toastSerivce.show('', 'Error in saving data.', {
          status: 'danger',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    })
  }

  onApprove() {
    // if (this.comFinInfo.targetGHG === 0) {
    //   this.toastSerivce.show('', 'Enter Target GHG Emission', {
    //     status: 'warning',
    //     destroyByClick: true,
    //     duration: 2000,
    //     hasIcon: false,
    //     position: NbGlobalPhysicalPosition.TOP_RIGHT,
    //     preventDuplicates: true,
    //   });
    //   return;
    // }
    let jsonBody = {
      DATA: {
        id: this.cadminId,
        status: CadminStatus.Approved,
      }
    }

    this.boService.sendRequestToBackend(RequestGroup.BusinessUsers, RequestType.ManageComAdmin,jsonBody).then( data => {
      // console.log(data)
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.dto != undefined) {
          this.toastSerivce.show('', 'Approved Successfully', {
            status: 'success',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          });
          this.cadminStatus = CadminStatus.Approved;
          this.boService.sendRequestToBackend(
            RequestGroup.GHGProject,
            RequestType.ManageProject,
            {DATA: { id: this.projectId, status: 3 }}
          ).then(data => {
            // console.log(data)
            if (data.HED != undefined && data.HED.RES_STS == 1) {
              if (data.DAT != undefined && data.DAT.dto != undefined) {
                // console.log(data.DAT.dto)
                // this.toastSerivce.show('', 'Successfully Agreed.', {
                //   status: 'success',
                //   destroyByClick: true,
                //   duration: 2000,
                //   hasIcon: false,
                //   position: NbGlobalPhysicalPosition.TOP_RIGHT,
                //   preventDuplicates: true,
                // });
                let jsonData ={
                  DATA: {
                    id: UserState.getInstance().companyId,
                    targetGHG: this.comFinInfo.targetGHG,
                  }
                }
                this.boService.sendRequestToBackend(
                  RequestGroup.Institution,
                  RequestType.ManageCompany,
                  jsonData
                ).then( data => {
                  if (data !== undefined && data.DAT !== undefined &&  data.DAT.dto !== undefined) {
                    this.toastSerivce.show('', 'Saved data successfully.', {
                      status: 'success',
                      destroyByClick: true,
                      duration: 2000,
                      hasIcon: false,
                      position: NbGlobalPhysicalPosition.TOP_RIGHT,
                      preventDuplicates: true,
                    })

                  } else {
                    this.toastSerivce.show('', 'Error in saving data.', {
                      status: 'danger',
                      destroyByClick: true,
                      duration: 2000,
                      hasIcon: false,
                      position: NbGlobalPhysicalPosition.TOP_RIGHT,
                      preventDuplicates: true,
                    })
                  }
                })
              }
            }
          });

        } else {
          this.toastSerivce.show('', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      }else {
        this.toastSerivce.show('', 'Error in saving data.', {
          status: 'danger',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    })
  }

  onNeedInfo() {
    if (this.cadminStatusMsg === undefined || this.cadminStatusMsg === '') {
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      });
      return;
    }

    let jsonBody = {
      DATA: {
        id: this.cadminId,

        status: CadminStatus.NeedInfo,
        statusMessage: this.cadminStatusMsg
        // entitlements: entitleMents ,
        // agreementStartDate: this.convert(this.startDate),
        // agreementEndDate: this.convert(this.endDate)
      }
    }

    this.boService.sendRequestToBackend(RequestGroup.BusinessUsers, RequestType.ManageComAdmin,jsonBody).then( data => {
      // console.log(data)
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.dto != undefined) {
          this.toastSerivce.show('', 'Request was sent to Company Admin.', {
            status: 'success',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          });
          this.cadminStatus = CadminStatus.NeedInfo;


        } else {
          this.toastSerivce.show('', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      }else {
        this.toastSerivce.show('', 'Error in saving data.', {
          status: 'danger',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    })
  }

  public projectStatus = 0;
  initProject() {
    this.boService.sendRequestToBackend(
      RequestGroup.GHGProject,
      RequestType.ListProject,
      {
        PAGE_NUMBER: -1,
        FILTER_MODEL: {
          companyId: { value: this.comId , type: 1, col: 1},
          status: { value: 7, type: 2, col: 1}
        }
      }
    ).then(data => {
      // console.log(data)
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {

          if (data.DAT.LIST.length > 0) {
            let list = data.DAT.LIST.filter(v => { return v.status !== 7;})
            this.projectId = list[0].id;
            this.projectStatus = list[0].status;
          } else {
            // this.projectStatus = -1;
            this.projectId = -1;
          }
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  public reports = [];
  loadPreviousYearReports() {
    this.reports = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Report,
      RequestType.ListReport,
      {
        PAGE_NUMBER: -1,
        FILTER_MODEL: {
          comId: { value: this.comId , type: 1, col: 1},
        }
      }
    ).then(data => {
      console.log(data);
      if (data !== undefined && data.DAT !== undefined  && data.DAT.LIST !== undefined) {
        for (const dto of data.DAT.LIST) {
          console.log(dto)
          if (dto.projectId >0) {
            // this.climatesiReports.push(dto.year)
            continue;
          }
          this.reports.push({
            year: dto.year,
            projectId: dto.projectId,
            url: dto.pdf,
            comId: dto.comId,
          })
        }
      }
    })
  }
}
