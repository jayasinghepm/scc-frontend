import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AdminClientsComponent} from "./admin-clients/admin-clients.component";
import {AdminCompaninesComponent} from "./admin-companines/admin-companines.component";
import {AdminHomeComponent} from "./admin-home/admin-home.component";
import {AdminReportsComponent} from "./admin-reports/admin-reports.component";
import {ComAdminsComponent} from "./com-admins/com-admins.component";
import { AddClientComponent } from './admin-clients/add-client/add-client.component';
import { RemoveClientComponent } from './admin-clients/remove-client/remove-client.component';
import {
  NbCardModule,
  NbContextMenuModule,
  NbDatepickerModule, NbDialogModule,
  NbIconModule,
  NbInputModule, NbListModule, NbTabsetModule,
  NbWindowModule
} from "@nebular/theme";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AdminRoutingModule} from "./admin.routing.module";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { AddCompanyComponent } from './admin-companines/add-company/add-company.component';
import { RemoveCompanyComponent } from './admin-companines/remove-company/remove-company.component';
import {ClinetEmployeeComponent} from "./clinet-employee/clinet-employee.component";
import {ClientSummaryDataInputsComponent} from "./client-summary-data-inputs/client-summary-data-inputs.component";
import {ClientAirTravelComponent} from "./emission/client-air-travel/client-air-travel.component";
import {ClientElectricityComponent} from "./emission/client-electricity/client-electricity.component";
import {ClientMunicipalWaterComponent} from "./emission/client-municipal-water/client-municipal-water.component";
import {ClientFireExtComponent} from "./emission/client-fire-ext/client-fire-ext.component";
import {ClientEmpCommuComponent} from "./emission/client-emp-commu/client-emp-commu.component";
import {ClientGeneratorsComponent} from "./emission/client-generators/client-generators.component";
import {ClientRefriComponent} from "./emission/client-refri/client-refri.component";
import {ClientWasteTransportComponent} from "./emission/client-waste-transport/client-waste-transport.component";
import {ClientTransportComponent} from "./emission/client-transport/client-transport.component";
import {ClientTransLocPurComponent} from "./emission/client-trans-loc-pur/client-trans-loc-pur.component";
import {ClientWasteDisposalComponent} from "./emission/client-waste-disposal/client-waste-disposal.component";
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from "@angular/material";
import { AddCadminComponent } from './com-admins/add-cadmin/add-cadmin.component';
import { ViewClientComponent } from './admin-clients/view-client/view-client.component';
import { ViewComadminComponent } from './com-admins/view-comadmin/view-comadmin.component';
import {AddEmployeeComponent} from "./clinet-employee/add-employee/add-employee.component";
import {SelectDropDownModule} from "ngx-select-dropdown";
import { ElecFormulaComponent } from '../formulas/elec-formula/elec-formula.component';
import { EmissionFactorsComponent } from './emission-factors/emission-factors.component';
import { AirtravelFormulaComponent } from './emission/client-air-travel/airtravel-formula/airtravel-formula.component';
import { FireExtFormulaComponent } from '../formulas/fire-ext-formula/fire-ext-formula.component';
import { EmpCommFormulaComponent } from '../formulas/emp-comm-formula/emp-comm-formula.component';
import { GeneratorsFormulaComponent } from '../formulas/generators-formula/generators-formula.component';
import { MunWaterFormulaComponent } from '../formulas/mun-water-formula/mun-water-formula.component';
import { RefriFormulaComponent } from '../formulas/refri-formula/refri-formula.component';
import { TransLocPurFormulaComponent } from '../formulas/trans-loc-pur-formula/trans-loc-pur-formula.component';
import { TransportFormulaComponent } from '../formulas/transport-formula/transport-formula.component';
import { WastDisFormulaComponent } from '../formulas/wast-dis-formula/wast-dis-formula.component';
import { WastTransFormulaComponent } from '../formulas/wast-trans-formula/wast-trans-formula.component';
import {WasteDisFactorsComponent} from "./waste-dis-factors/waste-dis-factors.component";
import {PubTransFactorsComponent} from "./pub-trans-factors/pub-trans-factors.component";
import { UpdateAirtravelComponent } from './emission/client-air-travel/update-airtravel/update-airtravel.component';
import { UpdateElecComponent } from './emission/client-electricity/update-elec/update-elec.component';
import { UpdateEmpCommComponent } from './emission/client-emp-commu/update-emp-comm/update-emp-comm.component';
import { UpdateFireExtComponent } from './emission/client-fire-ext/update-fire-ext/update-fire-ext.component';
import { UpdateGeneratorsComponent } from './emission/client-generators/update-generators/update-generators.component';
import { UpdateMunWaterComponent } from './emission/client-municipal-water/update-mun-water/update-mun-water.component';
import { UpdateRefriComponent } from './emission/client-refri/update-refri/update-refri.component';
import { UpdateTransLocComponent } from './emission/client-trans-loc-pur/update-trans-loc/update-trans-loc.component';
import { UpdateTransportComponent } from './emission/client-transport/update-transport/update-transport.component';
import { UpdateWasteDisComponent } from './emission/client-waste-disposal/update-waste-dis/update-waste-dis.component';
import { UpdateWasteTransportComponent } from './emission/client-waste-transport/update-waste-transport/update-waste-transport.component';
import { GhgProjectComponent } from './ghg-project/ghg-project.component';
import { UpdateGhgProjectComponent } from './ghg-project/update-ghg-project/update-ghg-project.component';
import { ChangeStatusComponent } from './ghg-project/change-status/change-status.component';
import { UserAccessControlComponent } from './user-access-control/user-access-control.component';
import { UsersAdminsComponent } from './users-admins/users-admins.component';
import { CreateAdminComponent } from './users-admins/create-admin/create-admin.component';
import {NgxEchartsModule} from "ngx-echarts";
import { UpdateWasteDisposalComponent } from './update-waste-disposal/update-waste-disposal.component';
import {Ng2SmartTableModule} from "../../ng2-smart-table/src/lib/ng2-smart-table.module";
import {ActivityLogsComponent} from "./activity-logs/activity-logs.component";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {DataEntryUserComponent} from "./data-entry-user/data-entry-user.component";
import {AddDataEntryUserComponent} from "./add-data-entry-user/add-data-entry-user.component";
import { EmissionHistoryComponent } from './emission-history/emission-history.component';
import { SeaAirFreightComponent } from './emission/sea-air-freight/sea-air-freight.component';
import { AdminUpdateSeaAirFreightComponent } from './emission/sea-air-freight/admin-update-sea-air-freight/admin-update-sea-air-freight.component';
import { ManageMenuComponent } from './manage-menu/manage-menu.component';
import { BiogasComponent } from './emission/biogas/biogas.component';
import { LpgasComponent } from './emission/lpgas/lpgas.component';
import { UpdateLpgasComponent } from './emission/lpgas/update-lpgas/update-lpgas.component';
import { UpdateBgasComponent } from './emission/bgas/update-bgas/update-bgas.component';


import { UpdateBiogasComponent } from './emission/biogas/update-biogas/update-biogas.component';
import { AdminAshTransportationComponent } from './emission/admin-ash-transportation/admin-ash-transportation.component';
import { AdminForkliftsComponent } from './emission/admin-forklifts/admin-forklifts.component';
import { AdminFurnaceOilComponent } from './emission/admin-furnace-oil/admin-furnace-oil.component';
import { AdminLorryTransComponent } from './emission/admin-lorry-trans/admin-lorry-trans.component';
import { AdminOilgasTransComponent } from './emission/admin-oilgas-trans/admin-oilgas-trans.component';
import { AdminPaperWasteComponent } from './emission/admin-paper-waste/admin-paper-waste.component';
import { AdminPaidManagerVehicleComponent } from './emission/admin-paid-manager-vehicle/admin-paid-manager-vehicle.component';
import { AdminRawMaterialTransComponent } from './emission/admin-raw-material-trans/admin-raw-material-trans.component';
import { AdminSawDustTransComponent } from './emission/admin-saw-dust-trans/admin-saw-dust-trans.component';
import { AdminSludgeTransComponent } from './emission/admin-sludge-trans/admin-sludge-trans.component';
import { AdminStaffTransComponent } from './emission/admin-staff-trans/admin-staff-trans.component';
import { UpdateAdminAshTransComponent } from './emission/admin-ash-transportation/update-admin-ash-trans/update-admin-ash-trans.component';
import { UpdateAdminForkliftsComponent } from './emission/admin-forklifts/update-admin-forklifts/update-admin-forklifts.component';
import { UpdateAdminLorryTransComponent } from './emission/admin-lorry-trans/update-admin-lorry-trans/update-admin-lorry-trans.component';
import { UpdateAdminOilgasComponent } from './emission/admin-oilgas-trans/update-admin-oilgas/update-admin-oilgas.component';
import { UpdateAdminPaidManagerVehicleComponent } from './emission/admin-paid-manager-vehicle/update-admin-paid-manager-vehicle/update-admin-paid-manager-vehicle.component';
import { UpdateAdminPaperWasteComponent } from './emission/admin-paper-waste/update-admin-paper-waste/update-admin-paper-waste.component';
import { UpdateAdminRawMaterialTransComponent } from './emission/admin-raw-material-trans/update-admin-raw-material-trans/update-admin-raw-material-trans.component';
import { UpdateAdminSawDustTransComponent } from './emission/admin-saw-dust-trans/update-admin-saw-dust-trans/update-admin-saw-dust-trans.component';
import { UpdateAdminSludgeTransComponent } from './emission/admin-sludge-trans/update-admin-sludge-trans/update-admin-sludge-trans.component';
import { UpdateAdminStaffTransComponent } from './emission/admin-staff-trans/update-admin-staff-trans/update-admin-staff-trans.component';
import {UpdateAdminFurnaceComponent} from "./emission/admin-furnace-oil/update-admin-furnace/update-admin-furnace.component";
import { EmissionCategoriesComponent } from './emission-categories/emission-categories.component';
import { AdminVehicleOthersComponent } from './emission/admin-vehicle-others/admin-vehicle-others.component';
import { UpdateAdminVehicleOthersComponent } from './emission/admin-vehicle-others/update-admin-vehicle-others/update-admin-vehicle-others.component';
import { AdminProjectSummaryComponent } from './admin-project-summary/admin-project-summary.component';
import { BgasComponent } from './emission/bgas/bgas.component';
import { CadminGeneralInfoComponent } from '../cadmin/cadmin-general-information/cadmin-general-info-component';
import { ClientFreightWaterComponent } from './emission/client-freight-water/client-freight-water.component';
import { UpdateFreightWaterComponent } from './emission/client-freight-water/update-freightwater/update-freightwater.component';
import { ClientFreightAirComponent } from './emission/client-freight-air/client-freight-air.component';
import { UpdateFreightAirComponent } from './emission/client-freight-air/update-freightair/update-freightair.component';
import { ClientFreightRoadComponent } from './emission/client-freight-road/client-freight-road.component';
import { UpdateFreightRoadComponent } from './emission/client-freight-road/update-freightroad/update-freightroad.component';
import { ClientFreightRailComponent } from './emission/client-freight-rail/client-freight-rail.component';
import { UpdateFreightRailComponent } from './emission/client-freight-rail/update-freightrail/update-freightrail.component';
import { WasteWaterTreatmentComponent } from './emission/waste-water-treatment/waste-water-treatment.component';
import { UpdateWasteWaterTreatmentComponent } from './emission/waste-water-treatment/update-waste-water-treatment/update-waste-water-treatment.component';
import { WasteWaterTreatmentFormulaComponent } from '../formulas/waste-water-treatment-formula/waste-water-treatment-formula.component';
import { ClientWeldingComponent } from './emission/client-welding/client-welding.component';
import { UpdateWeldingComponent } from './emission/client-welding/update-welding/update-welding.component';


@NgModule({
  declarations: [
    AdminClientsComponent,
    AdminCompaninesComponent,
    AdminHomeComponent,
    AdminReportsComponent,
    ComAdminsComponent,
    ClinetEmployeeComponent,
    ClientSummaryDataInputsComponent,
    ClientAirTravelComponent,
    ClientElectricityComponent,
    ClientWeldingComponent,
    ClientEmpCommuComponent,
    ClientFireExtComponent,
    ClientGeneratorsComponent,
    ClientMunicipalWaterComponent,
    ClientRefriComponent,
    ClientTransLocPurComponent,
    ClientTransportComponent,
    ClientWasteTransportComponent,
    ClientWasteDisposalComponent,
    ClientFreightWaterComponent,
    ClientFreightRoadComponent,
    ClientFreightRailComponent,
    ViewClientComponent,
    ViewComadminComponent,
    AddCadminComponent,
    AddClientComponent,
    AddEmployeeComponent,
    RemoveClientComponent,
    EmissionFactorsComponent,
    AirtravelFormulaComponent,
    // EmpCommFormulaComponent,
    // FireExtFormulaComponent,
    // EmpCommFormulaComponent,
    // GeneratorsFormulaComponent,
    // MunWaterFormulaComponent,
    // RefriFormulaComponent,
    // TransLocPurFormulaComponent,
    // TransportFormulaComponent,
    // WastDisFormulaComponent,
    // WastTransFormulaComponent,
    // ElecFormulaComponent,
    WasteDisFactorsComponent,
    PubTransFactorsComponent,
    UpdateAirtravelComponent,
    UpdateElecComponent,
    UpdateWeldingComponent,
    UpdateEmpCommComponent,
    UpdateFireExtComponent,
    UpdateGeneratorsComponent,
    UpdateMunWaterComponent,
    UpdateRefriComponent,
    UpdateTransLocComponent,
    UpdateTransportComponent,
    UpdateWasteDisComponent,
    UpdateWasteTransportComponent,
    GhgProjectComponent,
    UpdateGhgProjectComponent,
    UserAccessControlComponent,
    UsersAdminsComponent,
    CreateAdminComponent,
    UpdateWasteDisposalComponent,
    ActivityLogsComponent,
    DataEntryUserComponent,
    AddDataEntryUserComponent,
    EmissionHistoryComponent,
    SeaAirFreightComponent,
    AdminUpdateSeaAirFreightComponent,
    ManageMenuComponent,
    BiogasComponent,
    LpgasComponent,
    BgasComponent,
    UpdateLpgasComponent,
    UpdateBgasComponent,
    UpdateBiogasComponent,
    AdminAshTransportationComponent,
    AdminForkliftsComponent,
    AdminFurnaceOilComponent,
    AdminLorryTransComponent,
    AdminOilgasTransComponent,
    AdminPaperWasteComponent,
    AdminPaidManagerVehicleComponent,
    AdminRawMaterialTransComponent,
    AdminSawDustTransComponent,
    AdminSludgeTransComponent,
    AdminStaffTransComponent,
    UpdateAdminAshTransComponent,
    UpdateAdminForkliftsComponent,
    UpdateAdminLorryTransComponent,
    UpdateAdminOilgasComponent,
    UpdateAdminPaidManagerVehicleComponent,
    UpdateAdminPaperWasteComponent,
    UpdateAdminRawMaterialTransComponent,
    UpdateAdminSawDustTransComponent,
    UpdateAdminSludgeTransComponent,
    UpdateAdminStaffTransComponent,
    UpdateAdminFurnaceComponent,
    EmissionCategoriesComponent,
    AdminVehicleOthersComponent,
    UpdateAdminVehicleOthersComponent,
    UpdateFreightWaterComponent,
    AdminProjectSummaryComponent,
    ClientFreightAirComponent,
    UpdateFreightAirComponent,
    UpdateFreightRoadComponent,
    UpdateFreightRailComponent,
    WasteWaterTreatmentComponent,
    UpdateWasteWaterTreatmentComponent,
    WasteWaterTreatmentFormulaComponent
    

  ],
  imports: [
    NbContextMenuModule,
    Ng2SmartTableModule,
    NbCardModule,
    ReactiveFormsModule,
    NbIconModule,
    FormsModule,
    CommonModule,
    NbWindowModule,
    NbDatepickerModule.forRoot(),
    AdminRoutingModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    SelectDropDownModule,
    NbDialogModule,
    NbListModule,
    NgxEchartsModule,
    NbTabsetModule,
    NgbModule
  ],
  providers:[
    CadminGeneralInfoComponent,
    ClientEmpCommuComponent
  ],
  entryComponents: [
    ViewClientComponent,
    AddCadminComponent,
    AddClientComponent,
    AddEmployeeComponent,
    RemoveClientComponent,
    AirtravelFormulaComponent,

    UpdateWeldingComponent,
    UpdateAirtravelComponent,
    UpdateElecComponent,
    UpdateEmpCommComponent,
    UpdateFireExtComponent,
    UpdateGeneratorsComponent,
    UpdateMunWaterComponent,
    UpdateRefriComponent,
    UpdateTransLocComponent,
    UpdateTransportComponent,
    UpdateWasteDisComponent,
    UpdateWasteTransportComponent,
    UpdateGhgProjectComponent,
    CreateAdminComponent,
    ActivityLogsComponent,
    AddDataEntryUserComponent,
    AdminUpdateSeaAirFreightComponent,
    UpdateBiogasComponent,
    UpdateLpgasComponent,
    UpdateBgasComponent,
    UpdateAdminAshTransComponent,
    UpdateAdminForkliftsComponent,
    UpdateAdminFurnaceComponent,
    UpdateAdminLorryTransComponent,
    UpdateAdminPaidManagerVehicleComponent,
    UpdateAdminPaperWasteComponent,
    UpdateAdminRawMaterialTransComponent,
    UpdateAdminSawDustTransComponent,
    UpdateAdminSludgeTransComponent,
    UpdateAdminStaffTransComponent,
    UpdateAdminOilgasComponent,
    UpdateAdminVehicleOthersComponent,
    UpdateFreightWaterComponent,
    UpdateFreightAirComponent,
    UpdateFreightRoadComponent,
    UpdateWasteWaterTreatmentComponent,
    WasteWaterTreatmentFormulaComponent
  ]
})
export class AdminModule { }
