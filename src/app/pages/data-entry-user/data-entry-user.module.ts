import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {FormsModule} from "@angular/forms";
import {
  NbAccordionModule,
  NbCardModule,
  NbIconModule,
  NbInputModule, NbListModule,
  NbRadioModule,
  NbSelectModule, NbTabsetModule,
  NbTreeGridModule, NbWindowModule
} from "@nebular/theme";
import {ThemeModule} from "../../@theme/theme.module";
import {Ng2SmartTableModule} from "../../ng2-smart-table/src/lib/ng2-smart-table.module";
import {CadminRoutingModule} from "../cadmin/cadmin.routing.module";
import {AngularDualListBoxModule} from "angular-dual-listbox";
import {OwlDateTimeModule, OwlNativeDateTimeModule} from "ng-pick-datetime";
import {SelectDropDownModule} from "ngx-select-dropdown";
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from "@angular/material";
import {NgxEchartsModule} from "ngx-echarts";
import {NgxChartsModule} from "@swimlane/ngx-charts";
import {CadminPageGuard} from "../cadmin/CadminPageGuard";
import {DataEntryUserSummaryDataInputsComponent} from "./client-summary-data-inputs/client-summary-data-inputs.component";
import {DataEntryUserFireExtComponent} from "./emission/client-fire-ext/client-fire-ext.component";
import {DataEntryUserEmpCommuComponent} from "./emission/client-emp-commu/client-emp-commu.component";
import {DataEntryUserMunicipalWaterComponent} from "./emission/client-municipal-water/client-municipal-water.component";
import {DataEntryUserWasteTransportComponent} from "./emission/client-waste-transport/client-waste-transport.component";
import {DataEntryUserAirTravelComponent} from "./emission/client-air-travel/client-air-travel.component";
import {DataEntryUserElectricityComponent} from "./emission/client-electricity/client-electricity.component";
import {DataEntryUserRefriComponent} from "./emission/client-refri/client-refri.component";
import {DataEntryUserTransportComponent} from "./emission/client-transport/client-transport.component";
import {DataEntryUserWasteDisposalComponent} from "./emission/client-waste-disposal/client-waste-disposal.component";
import {DataEntryUserTransLocPurComponent} from "./emission/client-trans-loc-pur/client-trans-loc-pur.component";
import {DataEntryUserGeneratorsComponent} from "./emission/client-generators/client-generators.component";
import {DataEntryUserUpdateAirtravelComponent} from "./emission/client-air-travel/update-airtravel/update-airtravel.component";
import {DataEntryUserUpdateFireExtComponent} from "./emission/client-fire-ext/update-fire-ext/update-fire-ext.component";
import {DataEntryUserUpdateRefriComponent} from "./emission/client-refri/update-refri/update-refri.component";
import {DataEntryUserUpdateElecComponent} from "./emission/client-electricity/update-elec/update-elec.component";
import {DataEntryUserUpdateGeneratorsComponent} from "./emission/client-generators/update-generators/update-generators.component";
import {DataEntryUserUpdateTransLocComponent} from "./emission/client-trans-loc-pur/update-trans-loc/update-trans-loc.component";
import {DataEntryUserUpdateTransportComponent} from "./emission/client-transport/update-transport/update-transport.component";
import {DataEntryUserUpdateWasteTransportComponent} from "./emission/client-waste-transport/update-waste-transport/update-waste-transport.component";
import {DataEntryUserUpdateMunWaterComponent} from "./emission/client-municipal-water/update-mun-water/update-mun-water.component";
import {UpdateWasteDisComponent} from "./emission/client-waste-disposal/update-waste-dis/update-waste-dis.component";
import {DataEntryUserHomeComponent} from "./cadmin-home/cadmin-home.component";
import {DataEntryUserBranchesComponent} from "./cadmin-branches/cadmin-branches.component";
import {DataEntryUserUpdateEmpCommComponent} from "./emission/client-emp-commu/update-emp-comm/update-emp-comm.component";
import {DataEntryUserRoutingModule} from "./data-entry-user-routing.module";
import {DataEntryUserReportsComponent} from "./cadmin-reports/cadmin-reports.component";
import { SeaAirFreightComponent } from './emission/sea-air-freight/sea-air-freight.component';
import { DeoUpdateSeaAirFreightComponent } from './emission/sea-air-freight/deo-update-sea-air-freight/deo-update-sea-air-freight.component';
import { LpgasComponent } from './emission/lpgas/lpgas.component';
import { BgasComponent } from './emission/bgas/bgas.component';
//UpdateBgasComponent
import { UpdateBgasComponent } from './emission/bgas/update-bgas/update-bgas.component';
import { BiomassComponent } from './emission/biomass/biomass.component';
import { UpdateBiomassComponent } from './emission/biomass/update-biomass/update-biomass.component';
import { UpdateLpgasComponent } from './emission/lpgas/update-lpgas/update-lpgas.component';
import { DeuAshTransComponent } from './emission/deu-ash-trans/deu-ash-trans.component';
import { DeuForkliftsComponent } from './emission/deu-forklifts/deu-forklifts.component';
import { DeuFurnaceComponent } from './emission/deu-furnace/deu-furnace.component';
import { DeuPaperWasteComponent } from './emission/deu-paper-waste/deu-paper-waste.component';
import { DeuPaidManagerVehicleComponent } from './emission/deu-paid-manager-vehicle/deu-paid-manager-vehicle.component';
import { UpdateDeuAshTransComponent } from './emission/deu-ash-trans/update-deu-ash-trans/update-deu-ash-trans.component';
import { UpdateDeuForkliftsComponent } from './emission/deu-forklifts/update-deu-forklifts/update-deu-forklifts.component';
import { UpdateDeuFurnaceOilComponent } from './emission/deu-furnace/update-deu-furnace-oil/update-deu-furnace-oil.component';
import { UpdateDeuPaperWasteComponent } from './emission/deu-paper-waste/update-deu-paper-waste/update-deu-paper-waste.component';
import { DeuOilGasTransComponent } from './emission/deu-oil-gas-trans/deu-oil-gas-trans.component';
import { DeuLorryTransComponent } from './emission/deu-lorry-trans/deu-lorry-trans.component';
import { DeuRawMaterialTransComponent } from './emission/deu-raw-material-trans/deu-raw-material-trans.component';
import { DeuSawDustTransComponent } from './emission/deu-saw-dust-trans/deu-saw-dust-trans.component';
import { DeuSludgeTransComponent } from './emission/deu-sludge-trans/deu-sludge-trans.component';
import { DueStaffTransComponent } from './emission/due-staff-trans/due-staff-trans.component';
import { UpdateDeuLorryTransComponent } from './emission/deu-lorry-trans/update-deu-lorry-trans/update-deu-lorry-trans.component';
import { UpdateOilgasTransComponent } from './emission/deu-oil-gas-trans/update-oilgas-trans/update-oilgas-trans.component';
import { UpdateRawMaterialTransComponent } from './emission/deu-raw-material-trans/update-raw-material-trans/update-raw-material-trans.component';
import { UpdateDeuSawDustComponent } from './emission/deu-saw-dust-trans/update-deu-saw-dust/update-deu-saw-dust.component';
import { UpdateSludgeTransComponent } from './emission/deu-sludge-trans/update-sludge-trans/update-sludge-trans.component';
import { UpdateDeuStaffTransComponent } from './emission/due-staff-trans/update-deu-staff-trans/update-deu-staff-trans.component';
import {UpdateClientAshTransComponent} from "../client/emission/client-ash-trans/update-client-ash-trans/update-client-ash-trans.component";
import {UpdateClientForkliftsComponent} from "../client/emission/client-forklifts/update-client-forklifts/update-client-forklifts.component";
import {UpdateDeuPaidManagerVehicleComponent} from "./emission/deu-paid-manager-vehicle/update-deu-paid-manager-vehicle/update-deu-paid-manager-vehicle.component";
import { DeuVehicleOthersComponent } from './emission/deu-vehicle-others/deu-vehicle-others.component';
import { UpdateVehicleOthersComponent } from './emission/deu-vehicle-others/update-vehicle-others/update-vehicle-others.component';
import { EmissionFactorsComponent } from './emission-factors/emission-factors.component';
import { PubTransFactorsComponent } from './pub-trans-factors/pub-trans-factors.component';
import { WasteDisComponent } from './waste-dis/waste-dis.component';
import { DeuProjectSummaryComponent } from './deu-project-summary/deu-project-summary.component';
import { DeoSoloarComponentComponent } from './deo-soloar-component/deo-soloar-component.component';
import { DeoDataEntrySolarComponentComponent } from './deo-soloar-component/components/deo-data-entry-solar-component/deo-data-entry-solar-component.component';
import { CadminGeneralInfoComponent } from '../cadmin/cadmin-general-information/cadmin-general-info-component';
import { ClientEmpCommuComponent } from '../client/emission/client-emp-commu/client-emp-commu.component';



@NgModule({
  declarations: [
    DataEntryUserHomeComponent,
    DataEntryUserBranchesComponent,
    DataEntryUserSummaryDataInputsComponent,
    DataEntryUserAirTravelComponent,
    DataEntryUserElectricityComponent,
    DataEntryUserEmpCommuComponent,
    DataEntryUserFireExtComponent,
    DataEntryUserGeneratorsComponent,
    DataEntryUserMunicipalWaterComponent,
    DataEntryUserRefriComponent,
    DataEntryUserTransLocPurComponent,
    DataEntryUserTransportComponent,
    DataEntryUserWasteTransportComponent,
    DataEntryUserWasteDisposalComponent,
    DataEntryUserUpdateAirtravelComponent,
    DataEntryUserUpdateElecComponent,
    DataEntryUserUpdateEmpCommComponent,
    DataEntryUserUpdateFireExtComponent,
    DataEntryUserUpdateGeneratorsComponent,
    DataEntryUserUpdateMunWaterComponent,
    DataEntryUserUpdateRefriComponent,
    DataEntryUserUpdateTransLocComponent,
    DataEntryUserUpdateTransportComponent,
    // UpdateWasteDisComponent,
    DataEntryUserUpdateWasteTransportComponent,
    UpdateWasteDisComponent,
    DataEntryUserReportsComponent,
    SeaAirFreightComponent,
    DeoUpdateSeaAirFreightComponent,
    LpgasComponent,
    BgasComponent,
    BiomassComponent,
    UpdateBiomassComponent,
    UpdateLpgasComponent,
    UpdateBgasComponent,
    DeuAshTransComponent,
    DeuForkliftsComponent,
    DeuFurnaceComponent,
    DeuPaperWasteComponent,
    DeuPaidManagerVehicleComponent,
    UpdateDeuAshTransComponent,
    UpdateDeuForkliftsComponent,
    UpdateDeuFurnaceOilComponent,

    UpdateDeuPaperWasteComponent,
    DeuOilGasTransComponent,
    DeuLorryTransComponent,
    DeuRawMaterialTransComponent,
    DeuSawDustTransComponent,
    DeuSludgeTransComponent,
    DueStaffTransComponent,
    UpdateDeuLorryTransComponent,
    UpdateOilgasTransComponent,
    UpdateRawMaterialTransComponent,
    UpdateDeuSawDustComponent,
    UpdateSludgeTransComponent,
    UpdateDeuStaffTransComponent,
    UpdateDeuPaidManagerVehicleComponent,
    DeuVehicleOthersComponent,
    UpdateVehicleOthersComponent,
   EmissionFactorsComponent,
    PubTransFactorsComponent,
    WasteDisComponent,
    DeuProjectSummaryComponent,
    DeoSoloarComponentComponent,
    DeoDataEntrySolarComponentComponent,

  ],
  imports: [
    FormsModule,
    CommonModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    Ng2SmartTableModule,
    NbSelectModule,
    NbRadioModule,
    NbWindowModule,
    NbListModule,
    AngularDualListBoxModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    SelectDropDownModule,
    NbListModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    NgxEchartsModule,
    NgxChartsModule,
    NbTabsetModule,
    DataEntryUserRoutingModule,
    NbAccordionModule,
  ],
  entryComponents: [
    UpdateVehicleOthersComponent,
    DataEntryUserUpdateAirtravelComponent,
    DataEntryUserUpdateElecComponent,
    DataEntryUserUpdateEmpCommComponent,
    DataEntryUserUpdateFireExtComponent,
    DataEntryUserUpdateGeneratorsComponent,
    DataEntryUserUpdateMunWaterComponent,
    DataEntryUserUpdateRefriComponent,
    DataEntryUserUpdateTransLocComponent,
    DataEntryUserUpdateTransportComponent,
    UpdateDeuSawDustComponent,
    // UpdateWasteDisComponent,
    DataEntryUserUpdateWasteTransportComponent,
    UpdateWasteDisComponent,
    DeoUpdateSeaAirFreightComponent,
    UpdateBiomassComponent,
    UpdateLpgasComponent,
    UpdateBgasComponent,
    UpdateDeuAshTransComponent,
    UpdateDeuForkliftsComponent,
    UpdateDeuFurnaceOilComponent,
  UpdateDeuPaidManagerVehicleComponent,
    UpdateDeuPaperWasteComponent,
    UpdateDeuLorryTransComponent,
    UpdateOilgasTransComponent,

    UpdateDeuPaperWasteComponent,
    UpdateRawMaterialTransComponent,
    UpdateSludgeTransComponent,
    UpdateDeuStaffTransComponent,

    DeoDataEntrySolarComponentComponent,

  ],
  providers: [CadminPageGuard,CadminGeneralInfoComponent,ClientEmpCommuComponent]
})
export class DataEntryUserModule { }
