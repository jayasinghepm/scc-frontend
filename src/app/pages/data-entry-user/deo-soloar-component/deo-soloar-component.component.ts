import {Component, OnInit, ViewChild} from '@angular/core';
import {BackendService} from "../../../@core/rest/bo_service";
import {NbDialogService, NbToastrService} from "@nebular/theme";
import {MatDialog} from "@angular/material/dialog";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {UserState} from "../../../@core/auth/UserState";
import {UpdateSolarProjectComponentComponent} from "../../cadmin/solar-component/components/update-solar-project-component/update-solar-project-component.component";
import {DeoDataEntrySolarComponentComponent} from "./components/deo-data-entry-solar-component/deo-data-entry-solar-component.component";
import {DeleteConfirmComponent} from "../../delete-confirm/delete-confirm.component";
import {MassterDataService} from "../../../@core/service/masster-data.service";

@Component({
  selector: 'app-deo-soloar-component',
  templateUrl: './deo-soloar-component.component.html',
  styleUrls: ['./deo-soloar-component.component.scss']
})
export class DeoSoloarComponentComponent implements OnInit {

  constructor(private boService:BackendService,
              private toastService: NbToastrService,
              private dialogService: NbDialogService,
              private masterData: MassterDataService,
              private dialog: MatDialog) {

    this.loadData();
  }



  public months = [];


  public projects:{id: number, name: string}[] = [];

  public data : any;

  ngOnInit() {
    this.months = [
      { id: 0, name: 'Jan'},
      { id: 1, name: 'Feb'},
      { id: 2, name: 'Mar'},
      { id: 3, name: 'Apr'},
      { id: 4, name: 'May'},
      { id: 5, name: 'Jun'},
      { id: 6, name: 'Jul'},
      { id: 7, name: 'Aug'},
      { id: 8, name: 'Sep'},
      { id: 9, name: 'Oct'},
      { id: 10, name: 'Nov'},
      { id: 11, name: 'Dec'},

    ]
  }

  getMonth(month) {
    return this.months.filter(m => m.id == month)[0].name;
  }

  loadData() {
    this.boService.sendRequestToBackend(RequestGroup.Mitigation, RequestType.GetMitigationProjectMonthlyActivity, {

        comId: UserState.getInstance().companyId,
        branchId: -1,

    }).then(data => {
      console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        //todo data and projects
        if (data.DAT && data.DAT.actData && data.DAT.actData && data.DAT.actData.projectsData) {
          this.data = data.DAT.actData.projectsData;
          this.projects = data.DAT.actData.projectsData ? data.DAT.actData.projectsData.map(project => {
            return {
              id : project.projectId,
              name: project.projectName,
            }
          }) : [];
        }

      }
    })
  }

  openEditData($event, data, projectName, projectId: number) {
    const dialogRef = this.dialog.open(DeoDataEntrySolarComponentComponent,
      {
        data : {
          isEdit : data,
          data ,
          projectName: projectName,
          projectId: projectId,
        },
        width: '500px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(canLoad => {
      if (canLoad) {
        this.loadData();
      }
    });
  }

  onDeleteProject($event, id: number) {

    if (id) {
      const dialogRef = this.dialog.open(DeleteConfirmComponent,
        {
          data : { },
          width: '500px',
          panelClass: 'no-border-page-wrapper',
          disableClose: true,
        }).afterClosed().subscribe(d => {
        if (d) {
          this.boService.sendRequestToBackend(RequestGroup.Mitigation, RequestType.ManageMitigationProjectMonthlyActivity, {
              data: {
                action: 3,
                id: id,
              }
          }).then(data=> {
            // console.log(data)
            if (data.HED != undefined && data.HED.RES_STS == 1) {
              this.toastService.success('','Deleted successfully!')
              this.loadData();
            }
          })


          // });
        }
      });

    }
  }

}
