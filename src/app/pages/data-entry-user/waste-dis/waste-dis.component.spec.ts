import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WasteDisComponent } from './waste-dis.component';

describe('WasteDisComponent', () => {
  let component: WasteDisComponent;
  let fixture: ComponentFixture<WasteDisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WasteDisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WasteDisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
