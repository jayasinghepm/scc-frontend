import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateDeuLorryTransComponent } from './update-deu-lorry-trans.component';

describe('UpdateDeuLorryTransComponent', () => {
  let component: UpdateDeuLorryTransComponent;
  let fixture: ComponentFixture<UpdateDeuLorryTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateDeuLorryTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateDeuLorryTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
