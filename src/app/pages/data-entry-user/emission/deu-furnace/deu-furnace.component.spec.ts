import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeuFurnaceComponent } from './deu-furnace.component';

describe('DeuFurnaceComponent', () => {
  let component: DeuFurnaceComponent;
  let fixture: ComponentFixture<DeuFurnaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeuFurnaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeuFurnaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
