import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateOilgasTransComponent } from './update-oilgas-trans.component';

describe('UpdateOilgasTransComponent', () => {
  let component: UpdateOilgasTransComponent;
  let fixture: ComponentFixture<UpdateOilgasTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateOilgasTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateOilgasTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
