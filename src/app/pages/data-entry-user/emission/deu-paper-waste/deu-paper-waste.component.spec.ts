import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeuPaperWasteComponent } from './deu-paper-waste.component';

describe('DeuPaperWasteComponent', () => {
  let component: DeuPaperWasteComponent;
  let fixture: ComponentFixture<DeuPaperWasteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeuPaperWasteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeuPaperWasteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
