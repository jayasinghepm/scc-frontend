import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeuRawMaterialTransComponent } from './deu-raw-material-trans.component';

describe('DeuRawMaterialTransComponent', () => {
  let component: DeuRawMaterialTransComponent;
  let fixture: ComponentFixture<DeuRawMaterialTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeuRawMaterialTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeuRawMaterialTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
