import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateRawMaterialTransComponent } from './update-raw-material-trans.component';

describe('UpdateRawMaterialTransComponent', () => {
  let component: UpdateRawMaterialTransComponent;
  let fixture: ComponentFixture<UpdateRawMaterialTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateRawMaterialTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateRawMaterialTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
