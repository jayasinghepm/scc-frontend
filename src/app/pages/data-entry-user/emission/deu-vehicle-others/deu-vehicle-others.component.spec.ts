import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeuVehicleOthersComponent } from './deu-vehicle-others.component';

describe('DeuVehicleOthersComponent', () => {
  let component: DeuVehicleOthersComponent;
  let fixture: ComponentFixture<DeuVehicleOthersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeuVehicleOthersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeuVehicleOthersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
