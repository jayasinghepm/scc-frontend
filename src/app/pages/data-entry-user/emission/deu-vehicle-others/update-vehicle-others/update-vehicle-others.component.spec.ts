import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateVehicleOthersComponent } from './update-vehicle-others.component';

describe('UpdateVehicleOthersComponent', () => {
  let component: UpdateVehicleOthersComponent;
  let fixture: ComponentFixture<UpdateVehicleOthersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateVehicleOthersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateVehicleOthersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
