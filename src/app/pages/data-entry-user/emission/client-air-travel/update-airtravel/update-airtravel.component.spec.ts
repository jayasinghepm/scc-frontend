import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAirtravelComponent } from './update-airtravel.component';

describe('UpdateAirtravelComponent', () => {
  let component: UpdateAirtravelComponent;
  let fixture: ComponentFixture<UpdateAirtravelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAirtravelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAirtravelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
