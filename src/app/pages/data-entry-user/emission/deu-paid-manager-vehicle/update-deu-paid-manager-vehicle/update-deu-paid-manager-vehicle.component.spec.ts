import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateDeuPaidManagerVehicleComponent } from './update-deu-paid-manager-vehicle.component';

describe('UpdateDeuPaidManagerVehicleComponent', () => {
  let component: UpdateDeuPaidManagerVehicleComponent;
  let fixture: ComponentFixture<UpdateDeuPaidManagerVehicleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateDeuPaidManagerVehicleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateDeuPaidManagerVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
