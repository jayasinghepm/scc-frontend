import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeuPaidManagerVehicleComponent } from './deu-paid-manager-vehicle.component';

describe('DeuPaidManagerVehicleComponent', () => {
  let component: DeuPaidManagerVehicleComponent;
  let fixture: ComponentFixture<DeuPaidManagerVehicleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeuPaidManagerVehicleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeuPaidManagerVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
