import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateDeuAshTransComponent } from './update-deu-ash-trans.component';

describe('UpdateDeuAshTransComponent', () => {
  let component: UpdateDeuAshTransComponent;
  let fixture: ComponentFixture<UpdateDeuAshTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateDeuAshTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateDeuAshTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
