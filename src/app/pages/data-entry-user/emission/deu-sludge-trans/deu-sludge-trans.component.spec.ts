import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeuSludgeTransComponent } from './deu-sludge-trans.component';

describe('DeuSludgeTransComponent', () => {
  let component: DeuSludgeTransComponent;
  let fixture: ComponentFixture<DeuSludgeTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeuSludgeTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeuSludgeTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
