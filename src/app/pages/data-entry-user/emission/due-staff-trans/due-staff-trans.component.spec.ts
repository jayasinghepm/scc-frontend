import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DueStaffTransComponent } from './due-staff-trans.component';

describe('DueStaffTransComponent', () => {
  let component: DueStaffTransComponent;
  let fixture: ComponentFixture<DueStaffTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DueStaffTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DueStaffTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
