import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeuSawDustTransComponent } from './deu-saw-dust-trans.component';

describe('DeuSawDustTransComponent', () => {
  let component: DeuSawDustTransComponent;
  let fixture: ComponentFixture<DeuSawDustTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeuSawDustTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeuSawDustTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
