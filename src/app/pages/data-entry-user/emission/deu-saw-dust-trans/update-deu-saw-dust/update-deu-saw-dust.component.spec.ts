import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateDeuSawDustComponent } from './update-deu-saw-dust.component';

describe('UpdateDeuSawDustComponent', () => {
  let component: UpdateDeuSawDustComponent;
  let fixture: ComponentFixture<UpdateDeuSawDustComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateDeuSawDustComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateDeuSawDustComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
