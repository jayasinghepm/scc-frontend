import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeuProjectSummaryComponent } from './deu-project-summary.component';

describe('DeuProjectSummaryComponent', () => {
  let component: DeuProjectSummaryComponent;
  let fixture: ComponentFixture<DeuProjectSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeuProjectSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeuProjectSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
