import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from '../pages.component';
// import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import {ClientAirTravelComponent} from "../client/emission/client-air-travel/client-air-travel.component";
import {ClientFireExtComponent} from "../client/emission/client-fire-ext/client-fire-ext.component";
import {ClientRefriComponent} from "../client/emission/client-refri/client-refri.component";
import {ClientGeneratorsComponent} from "../client/emission/client-generators/client-generators.component";
import {ClientElectricityComponent} from "../client/emission/client-electricity/client-electricity.component";
import {ClientWasteDisposalComponent} from "../client/emission/client-waste-disposal/client-waste-disposal.component";
import {ClientMunicipalWaterComponent} from "../client/emission/client-municipal-water/client-municipal-water.component";
import {ClientEmpCommuComponent} from "../client/emission/client-emp-commu/client-emp-commu.component";
import {ClientTransportComponent} from "../client/emission/client-transport/client-transport.component";
import {ClientWasteTransportComponent} from "../client/emission/client-waste-transport/client-waste-transport.component";
import {ClientTransLocPurComponent} from "../client/emission/client-trans-loc-pur/client-trans-loc-pur.component";
import {ClientHomeComponent} from "../client/client-home/client-home.component";
import {ClientReportsComponent} from "../client/client-reports/client-reports.component";
import {ClientSummaryDataInputsComponent} from "../client/client-summary-data-inputs/client-summary-data-inputs.component";
import {AdminHomeComponent} from "../admin/admin-home/admin-home.component";
import {AdminCompaninesComponent} from "../admin/admin-companines/admin-companines.component";
import {ComAdminsComponent} from "../admin/com-admins/com-admins.component";
import {AdminClientsComponent} from "../admin/admin-clients/admin-clients.component";
import {AdminReportsComponent} from "../admin/admin-reports/admin-reports.component";
import {CadminHomeComponent} from "../cadmin/cadmin-home/cadmin-home.component";
import {CadminBranchesComponent} from "../cadmin/cadmin-branches/cadmin-branches.component";
import {CadminGeneralInfoComponent} from "../cadmin/cadmin-general-information/cadmin-general-info-component";
import {CadminReportsComponent} from "../cadmin/cadmin-reports/cadmin-reports.component";
import {ClientRouteGuard} from "./guards/client-route.guard";
import {AdminRouteGuard} from "./guards/admin-route.guard";
import {CadminRouteGuard} from "./guards/cadmin-route.guard";

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'client',
      loadChildren: 'src/app/pages/client/client.module#ClientModule',
      canActivate: [ClientRouteGuard]
    },
    {
      path: 'admin',
      loadChildren: 'src/app/pages/admin/admin.module#AdminModule',
      canActivate: [AdminRouteGuard]
    },
    {
      path: 'com_admin',
      loadChildren: 'src/app/pages/cadmin/cadmin.module#CadminModule',
      canActivate: [CadminRouteGuard]
    },
    {
      path: 'data_entry_user',
      loadChildren: 'src/app/pages/data-entry-user/data-entry-user.module#DataEntryUserModule',
      canActivate: [CadminRouteGuard]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
