import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {UserState} from "../../../@core/auth/UserState";


@Injectable()
export class CadminRouteGuard implements CanActivate {
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    console.log("cadmin")
    console.log(route)
    console.log(state)
    if (UserState.getInstance().isAuthenticated) {

        return true;


    }
    console.log("cadmin")
    console.log(route)
    console.log(state)
    this.router.navigate(['home'], { queryParams: { returnUrl: state.url }}); // ##login
    return false;
    // return true;
  }
  constructor(private router: Router) {}
}
