import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {UserState} from "../../../@core/auth/UserState";


@Injectable()
export class AdminRouteGuard implements CanActivate {



  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (UserState.getInstance().isAuthenticated) {
      if (UserState.getInstance().isFisttime) {
        // todo :cant visit any pages
      }
      return true;
    }
    console.log("Admin guard")
    this.router.navigate(['home'], { queryParams: { returnUrl: state.url }}); // ##login
    return false;
    // return true;
  }
  constructor(private router: Router) {}

}
