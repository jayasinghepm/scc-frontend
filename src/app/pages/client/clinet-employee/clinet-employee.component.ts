import { Component, OnInit } from '@angular/core';
import {BackendService} from "../../../@core/rest/bo_service";
import {MassterDataService} from "../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {UserState} from "../../../@core/auth/UserState";
import {MatDialog} from "@angular/material";
import {LocalDataSource} from "../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";

@Component({
  selector: 'app-clinet-employee',
  templateUrl: './clinet-employee.component.html',
  styleUrls: ['./clinet-employee.component.scss']
})
export class ClinetEmployeeComponent implements OnInit {
  public pg_current = 0;
  public disableNext = false;
  public disablePrev = true;
  private fetchedCount = 0;
  settings = {
    mode: 'inline',
    pager: {
      display: false,
    },
    actions: {
      delete: false,
      edit: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
        filter: false,
        sort: false,
      },
      company: {
        title: 'Company',
        editable: false,
        addable: false,
        type: 'string',
        filter: false,
        sort: false,
      },
      //Todo: have to change
      branch: {
        title: 'Branch',
        type: 'string',
        filter: false,
        sort: false,
        editor: {
          type: 'completer',
          config: {
            completer: {
              data: [],
            }
          }
        }
      },
      employeeName: {
        title: 'Employee Name',
        type: 'string',
        filter: false,
        sort: false,
      },

    },
  };
  mySetting = JSON.parse(JSON.stringify(this.settings))


  source: LocalDataSource = new LocalDataSource();
  constructor(private boService: BackendService,
              private masterData: MassterDataService,
              private dialog: MatDialog,
              private  toastSerivce:  NbToastrService,) {

    // this.source.load(data)
    this.loadData();
  }

  ngOnInit() {
    this.masterData.getBranches().subscribe( data => {
      this.mySetting.columns.branch.editor.config.completer.data = data;
      this.settings = Object.assign({}, this.mySetting)
    });


  }



  onUpdate(event: any) {
    console.log(event);
    // update
    if (event.data !== undefined) {
      if (JSON.stringify(event.data) === JSON.stringify(event.newData)) {
        // ignore no change
        this.toastSerivce.show('', 'No change in data', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      } else {
        if (this.validateEntry(event.newData, true)) {
          this.boService.sendRequestToBackend(
            RequestGroup.BusinessUsers,
            RequestType.ManageEmployee,
            this.fromTable(event.newData, true)
          ).then( data => {
            if (data !== undefined && data.DAT !== undefined &&  data.DAT.dto!== undefined) {
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
              this.loadData();
              this.masterData.loadEmployees();
            } else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          })
        } else {
          this.toastSerivce.show('', 'Fill empty fields', {
            status: 'warning',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      }
    }
    // new
    else {
      if (this.validateEntry(event.newData, false)) {
        this.boService.sendRequestToBackend(
          RequestGroup.BusinessUsers,
           RequestType.ManageEmployee,
          this.fromTable(event.newData, false)
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.dto !== undefined) {
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
            this.loadData();
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }

  }
  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.BusinessUsers,
      RequestType.ListEmployee,
      {
        PAGE_NUMBER: this.pg_current,
        FILTER_MODEL: {
          branchId: { value: UserState.getInstance().branchId , type: 1, col: 1}
        }
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              list.push(this.fromListRequest(val));
            }
          });
          this.source.load(list);
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  private validateEntry(data: any, onEdit: boolean): boolean {
    if (data.branch === undefined || data.branch === '' ){
      return false;
    }

    if (data.employeeName === undefined || data.employeeName === '' ){
      return false;
    }
    if (onEdit && (data.id === undefined || data.id == "")) {
      //show snack bar
      return false;
    }
    if (onEdit && (data.company === undefined || data.company === '') ){
      return false;
    }
    return true;
  }

  private fromListRequest(dto: any): any {
    let company;
    let branch;
    let employeeName;
    let id;
    this.masterData.getBranchName(dto.branchId).subscribe(data => { branch = data; }).unsubscribe();

    this.masterData.getCompanyName(dto.comId).subscribe(data => { company = data; }).unsubscribe();


    id = dto.id;
    employeeName = dto.name;




    return {
      id,
      company,
      branch,
      employeeName,
    };



  }

  private fromTable(data: any, onEdit: boolean): any {
    let company;
    let branch;
    let employeeName;
    let id;



    // todo : company
    this.masterData.getBranchId(data.branch).subscribe(data => { branch = data;});
    return {
      DATA: {
        id: onEdit ? data.id : -1,
        empNo: employeeName,
        comId : UserState.getInstance().companyId, // todo:
        branchId: branch,
        name: data.employeeName,
      }
    }

  }

  onClickNextPage() {
    this.pg_current++;
    this.loadData();
  }

  onClickPrevPage() {
    if (this.pg_current >0) {
      this.pg_current--;
      this.disablePrev = this.pg_current === 0 ? true : false;

      this.loadData();
    }
  }

}
