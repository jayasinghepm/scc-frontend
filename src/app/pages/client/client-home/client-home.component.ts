import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {NbThemeService} from "@nebular/theme";
import {BackendService} from "../../../@core/rest/bo_service";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {UserState} from "../../../@core/auth/UserState";
import {Router} from "@angular/router";

@Component({
  selector: 'app-client-home',
  templateUrl: './client-home.component.html',
  styleUrls: ['./client-home.component.scss']
})
export class ClientHomeComponent implements AfterViewInit, OnDestroy{
  public chart5_options = {}
  public chart6_options = {}
  public chart1_options = {}
  public chart2_options = {}
  public chart3_options = {};
  public chart4_options = {};
  public chart7_options = {};

  public isFy = false;

  private targetGHG  = 0;
  private actual = 0;

  private colorPallete = ['#16a085', '#27ae60', '#2980b9', '#f1c40f', '#e67e22', '#e67e22', '#e74c3c',]

  public chartsData = {
    branch: {
      branches: [],
      scope_1: [],
      scope_2: [],
      scope_3: [],
    },
    categories: {
      waste_transport: 0,
      t_d: 0,
      waste_disposal: 0,
      mun_water: 0,
      emp_comm_not_paid: 0,
      emp_comm_paid: 0,
      air_travel: 0,
      electricity: 0,
      company_owned: 0,
      offroad: 0,
      fire_ext: 0,
      refri_leakage: 0,
      diesel_generators: 0,
      transport_loc_pur: 0,
      transport_hired_paid: 0,
      transport_hired_not_paid: 0,
      transport_rented: 0,
      sea_freight: 0,
      air_freight: 0,
    },
    indirect: 0,
    direct: 0,
    per_capita: [],
    intensity: [],
    emission: {
      tco2: [],
      co2: [],
      n20: [],
      ch4: []
    },
    fy: '',

  }


  constructor(
    private theme: NbThemeService,
    private boService:BackendService,
    private router:Router
  ) {
    let inensityYear = [];
    let capitaYear = [];
    let tCo2year = [];
    if ( !UserState.getInstance().existActiveProject) {
      this.router.navigate(['/pages/client/reports']);
    }else {
      this.boService.sendRequestToBackend(
        RequestGroup.Institution,
        RequestType.ListCompany,
        {
          FILTER_MODEL: {
            id: { value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            if (data.DAT.list.length !== 0) {
              const company = data.DAT.list[0];
              this.targetGHG = company.targetGHG;
              this.isFy = company.fyCurrentStart !== company.fyCurrentEnd;
              this.chart1_options =  {

                tooltip: {
                  trigger: 'axis',
                  axisPointer: {
                    type: 'cross',
                    label: {
                      backgroundColor: echarts.tooltipBackgroundColor,
                    },
                  },
                },
                backgroundColor: echarts.bg,
                title: {
                  text: '',
                  subtext: ''
                },
                legend: {
                  //   data: ['Estimated', 'Real'],
                  //   textStyle: {
                  //      color: echarts.textColor,
                  //   },
                },
                grid: {
                  left: '14%',
                  right: '4%',
                  bottom: '10%',
                  containLabel: true
                },
                yAxis: {
                  name: 'GHG Emission tCO2e',
                  nameGap: 83,
                  nameTextStyle: {
                    fontWeight: 'bold',
                    fontSize: 14,
                  },
                  nameLocation: 'center',
                  type: 'value'
                },
                xAxis: {
                  type: 'category',
                  //   name: 'Source of Emission',
                  //   nameLocation: 'center',
                  //   nameGap: 320,
                  //   nameTextStyle: {
                  //     fontWeight: 'bold',
                  //     fontSize: 14,
                  //     align: 'left',
                  //     rich: {

                  //     }
                  //   },
                  axisLabel: {
                    padding: [0,0,0,2],
                  },
                  data: ['Target', 'Real']
                },
                series: {
                  type: 'bar',
                  stack: 'chart',
                  barWidth: '70%',
                  label: {
                    normal: {
                      position: 'right',
                      show: true
                    }
                  },
                  data:
                    [
                      {
                        value: this.targetGHG,
                        itemStyle: {
                          color: '#27ae60',
                        }
                      },
                      {
                        value: this.actual,
                        itemStyle: {
                          color: '#2980b9',
                        }
                      }
                    ]
                }
              };
            }
          }}

      })


      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.GHGEmissionSummary,
        {
          companyId: UserState.getInstance().companyId,
          branchId: -1,
          yearsBack: 2
        }
      ).then(data => {
        console.log(data);
        if (data.DAT !== undefined && data.DAT.list !== undefined) {
          //current year
          const emissionInfoCurrent = data.DAT.list[0].emissionInfo;
          if (emissionInfoCurrent !== undefined) {
            this.actual = emissionInfoCurrent.total === undefined ? 0 : emissionInfoCurrent.total;
            this.chartsData.direct = emissionInfoCurrent.direct === undefined ? 0 : emissionInfoCurrent.direct;
            this.chartsData.indirect = emissionInfoCurrent.indirect === undefined ? 0 : emissionInfoCurrent.indirect;
            this.chartsData.categories.waste_transport = emissionInfoCurrent.waste_transport  === undefined ? 0 : emissionInfoCurrent.waste_transport;
            this.chartsData.categories.t_d = emissionInfoCurrent.t_d  === undefined ? 0 : emissionInfoCurrent.t_d;
            this.chartsData.categories.waste_disposal = emissionInfoCurrent.waste_disposal  === undefined ? 0 : emissionInfoCurrent.waste_disposal;
            this.chartsData.categories.mun_water = emissionInfoCurrent.mun_water  === undefined ? 0 : emissionInfoCurrent.mun_water;
            this.chartsData.categories.emp_comm_not_paid = emissionInfoCurrent.emp_comm_not_paid === undefined ? 0 : emissionInfoCurrent.emp_comm_not_paid;
            this.chartsData.categories.emp_comm_paid = emissionInfoCurrent.emp_comm_paid === undefined ? 0 : emissionInfoCurrent.emp_comm_paid;
            this.chartsData.categories.air_travel = emissionInfoCurrent.air_travel  === undefined ? 0 : emissionInfoCurrent.air_travel;
            this.chartsData.categories.electricity = emissionInfoCurrent.electricity  === undefined ? 0 : emissionInfoCurrent.electricity;
            this.chartsData.categories.company_owned = emissionInfoCurrent.company_owned  === undefined ? 0 : emissionInfoCurrent.company_owned;
            this.chartsData.categories.offroad = emissionInfoCurrent.offroad  === undefined ? 0 : emissionInfoCurrent.offroad;
            this.chartsData.categories.fire_ext = emissionInfoCurrent.fire_ext  === undefined ? 0 : emissionInfoCurrent.fire_ext;
            this.chartsData.categories.refri_leakage = emissionInfoCurrent.refri_leakage  === undefined ? 0 : emissionInfoCurrent.refri_leakage;
            this.chartsData.categories.diesel_generators = emissionInfoCurrent.diesel_generators  === undefined ? 0 : emissionInfoCurrent.diesel_generators;
            this.chartsData.categories.transport_loc_pur = emissionInfoCurrent.transport_loc_pur  === undefined ? 0 : emissionInfoCurrent.transport_loc_pur;
            this.chartsData.categories.transport_hired_paid = emissionInfoCurrent.transport_hired_paid  === undefined ? 0 : emissionInfoCurrent.transport_hired_paid;
            this.chartsData.categories.transport_hired_not_paid = emissionInfoCurrent.transport_hired_not_paid  === undefined ? 0 : emissionInfoCurrent.transport_hired_not_paid;
            this.chartsData.categories.transport_rented = emissionInfoCurrent.transport_rented  === undefined ? 0 : emissionInfoCurrent.transport_rented;
            this.chartsData.categories.sea_freight = emissionInfoCurrent.sea_freight  === undefined ? 0 : emissionInfoCurrent.sea_freight;
            this.chartsData.categories.air_freight = emissionInfoCurrent.air_freight  === undefined ? 0 : emissionInfoCurrent.air_freight;
            this.chartsData.fy = data.DAT.list[0].fy;



          }
          for(let i = 0; i < data.DAT.list.length; i++) {
            const info = data.DAT.list[i];
            this.chartsData.emission.tco2.unshift({
              year: info.emissionInfo.fy,
              em: info.emissionInfo.total === undefined? 0: info.emissionInfo.total,
            })
            this.chartsData.per_capita.unshift({
              year: info.emissionInfo.fy,
              em:info.emissionInfo.per_capita === undefined ? 0: info.emissionInfo.per_capita,
            })
            this.chartsData.intensity.unshift({
              year: info.emissionInfo.fy,
              em: info.emissionInfo.intensity === undefined ? 0 : info.emissionInfo.intensity,
            })
          }


          for (let i = 0; i < data.DAT.list.length; i++) {
            if(this.chartsData.per_capita[i].year !== undefined) {
              capitaYear.push(this.chartsData.per_capita[i])
            }
            if(this.chartsData.intensity[i].year !== undefined) {
              inensityYear.push(this.chartsData.intensity[i])
            }
            if(this.chartsData.emission.tco2[i].year !== undefined) {
              tCo2year.push(this.chartsData.emission.tco2[i])
            }
          }

        }

        this.chart6_options = {
          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'cross',
              label: {
                backgroundColor: echarts.tooltipBackgroundColor,
              },
            },
          },
          yAxis: {
            type: 'category',
            name: 'Source of Emission',
            nameLocation: 'center',
            nameGap: 320,
            nameTextStyle: {
              fontWeight: 'bold',
              fontSize: 14,
              align: 'left',
              rich: {

              }
            },
            axisLabel: {
              padding: [0,0,0,2],
            },

            data: [
              'Waste Transportation',
              'T & D Loss',
              'Waste Disposal',
              'Municipal Water',
              'Employee Commuting Not Paid by the Company',
              'Business Air Travels',
              'Grid Connected Electricity',
              'Vehicle Hired, Not Paid',

              'Employee Commuting, Paid by the Company',
              'Company Owned Vehicles',
              'Off-Road Mobile Sources and Machinery-Forklifts',
              'Fire Extinguishers',
              'Refirgerant Leakage',
              'Onsite Diesel Generators',
              'Vehicle Hired, Paid',
              'Vehicle Rented',
              'Transport Locally Purchased',
            ]
          },
          xAxis: {
            type: 'value',
            name: 'GHG Emission (tCO2e)',
            nameGap: 30,
            nameTextStyle: {
              fontWeight: 'bold',
              fontSize: 14,
            },
            nameLocation: 'center',
          },
          grid: {
            left: '40%',
          },
          series: [{
            type: 'bar',
            stack: 'chart',
            label: {
              normal: {
                position: 'right',
                show: true
              }
            },
            data: [
              {
                value: this.chartsData.categories.waste_transport,
                itemStyle: {color: '#1abc9c'},
              },
              {
                value: this.chartsData.categories.t_d ,
                itemStyle: {color: '#1abc9c'},
              },
              {
                value: this.chartsData.categories.waste_disposal,
                itemStyle: {color: '#1abc9c'},
              },
              {
                value: this.chartsData.categories.mun_water,
                itemStyle: {color: '#1abc9c'},
              },
              {
                value: this.chartsData.categories.emp_comm_not_paid,
                itemStyle: {color: '#1abc9c'},
              },
              {
                value: this.chartsData.categories.air_travel ,
                itemStyle: {color: '#1abc9c'},
              },
              {
                value:this.chartsData.categories.electricity,
                itemStyle: {color: '#1abc9c'},
              },
              {
                value: this.chartsData.categories.transport_loc_pur,
                itemStyle: {color: '#1abc9c'},
              },
              {
                value: this.chartsData.categories.emp_comm_paid,
                itemStyle: {color: '#2980b9'},
              },
              {
                value: this.chartsData.categories.company_owned,
                itemStyle: {color: '#2980b9'},
              },
              {
                value: this.chartsData.categories.offroad,
                itemStyle: {color: '#2980b9'},
              },
              {
                value: this.chartsData.categories.fire_ext,
                itemStyle: {color: '#2980b9'},
              },
              {
                value: this.chartsData.categories.refri_leakage,
                itemStyle: {color: '#2980b9'},
              },
              {
                value: this.chartsData.categories.diesel_generators,
                itemStyle: {color: '#2980b9'},
              },
              {
                value: this.chartsData.categories.transport_hired_paid,
                itemStyle: {color: '#2980b9'},
              },
              {
                value: this.chartsData.categories.transport_rented,
                itemStyle: {color: '#2980b9'},
              },
              {
                value: this.chartsData.categories.transport_hired_not_paid,
                itemStyle: {color: '#2980b9'},
              },
            ]
          }]
        };
        this.chart5_options =  {

          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'cross',
              label: {
                backgroundColor: echarts.tooltipBackgroundColor,
              },
            },
          },
          backgroundColor: echarts.bg,
          title: {
            text: '',
            subtext: ''
          },
          legend: {
            //   data: ['Estimated', 'Real'],
            //   textStyle: {
            //      color: echarts.textColor,
            //   },
          },
          grid: {
            left: '12%',
            right: '4%',
            bottom: '10%',
            containLabel: true
          },
          yAxis: {
            name: 'GHG Emission tCO2e',
            nameGap: 50,
            nameTextStyle: {
              fontWeight: 'bold',
              fontSize: 14,
            },
            nameLocation: 'center',
            type: 'value'
          },
          xAxis: {
            type: 'category',
            //   name: 'Source of Emission',
            //   nameLocation: 'center',
            //   nameGap: 320,
            //   nameTextStyle: {
            //     fontWeight: 'bold',
            //     fontSize: 14,
            //     align: 'left',
            //     rich: {

            //     }
            //   },
            axisLabel: {
              padding: [0,0,0,2],
            },
            data: ['Direct', 'Indirect']
          },
          series: {
            type: 'bar',
            stack: 'chart',
            barWidth: '70%',
            label: {
              normal: {
                position: 'right',
                show: true
              }
            },
            data:
              [
                {
                  value: this.chartsData.direct,
                  itemStyle: {color:this.colorPallete[2]},
                },
                {
                  value: this.chartsData.indirect,
                  itemStyle: {color: this.colorPallete[3]},
                },
              ],
          }
        };
        this.chart2_options =  {

          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'cross',
              label: {
                backgroundColor: echarts.tooltipBackgroundColor,
              },
            },
          },
          backgroundColor: echarts.bg,
          title: {
            text: '',
            subtext: ''
          },
          legend: {
            //   data: ['Estimated', 'Real'],
            //   textStyle: {
            //      color: echarts.textColor,
            //   },
          },
          grid: {
            left: '11%',
            right: '4%',
            bottom: '12%',
            containLabel: true
          },
          yAxis: {
            name: 'GHG Emission (tCO2e) per Captia',

            nameGap: 40,
            nameTextStyle: {
              fontWeight: 'bold',
              fontSize: 14,
            },
            nameLocation: 'center',
            type: 'value'
          },
          xAxis: {
            type: 'category',
            nameGap: 30,
            //   name: 'Source of Emission',
            //   nameLocation: 'center',
            //   nameGap: 320,
            //   nameTextStyle: {
            //     fontWeight: 'bold',
            //     fontSize: 14,
            //     align: 'left',
            //     rich: {

            //     }
            //   },
            axisLabel: {
              padding: [0,0,0,2],
            },
            data : capitaYear.map(v => v.year),
          },
          series: {
            // barWidth: '20%',
            // name:'Emission',
            // type:'bar',
            data: capitaYear.map((v,i) => {
              return { value: v.em,
                itemStyle: { color: this.colorPallete[i] }}
            }),

            type: 'bar',
            stack: 'chart',
            barWidth: '20%',
            label: {
              normal: {
                position: 'right',
                show: true
              }
            },

          }
        };
        this.chart3_options =  {
          title : {
            text: '',
            subtext: ''
          },
          backgroundColor: echarts.bg,
          // color: [colors.infoLight, colors.dangerLight, colors.successLight, colors.primaryLight],
          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'cross',
              label: {
                backgroundColor: echarts.tooltipBackgroundColor,
              },
            },
          },
          grid: {
            left: '10%',
            right: '4%',
            bottom: '12%',
            containLabel: true
          },
          xAxis : [
            {
              type : 'category',
              data : tCo2year.map(v => v.year),
              name: 'Years',
              nameGap: 50,
              nameTextStyle: {
                fontWeight: 'bold',
                fontSize: 14,
              },
              nameLocation: 'center',
            }
          ],
          yAxis : [
            {
              axisLabel: {
                padding: [0,0,0,2],
              },
              name: 'GHG Emission tCO2e',
              nameGap: 40,
              nameTextStyle: {
                fontWeight: 'bold',
                fontSize: 14,
              },
              nameLocation: 'center',
              type : 'value'
            }
          ],
          series : [
            {
              barWidth: '20%',
              name:'Emission',
              type:'bar',
              data: tCo2year.map((v, i) => {return {
                value: v.em,
                itemStyle: { color: this.colorPallete[i] },
              } }),
            },
          ]
        };
        this.chart1_options =  {

          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'cross',
              label: {
                backgroundColor: echarts.tooltipBackgroundColor,
              },
            },
          },
          backgroundColor: echarts.bg,
          title: {
            text: '',
            subtext: ''
          },
          legend: {
            //   data: ['Estimated', 'Real'],
            //   textStyle: {
            //      color: echarts.textColor,
            //   },
          },
          grid: {
            left: '14%',
            right: '4%',
            bottom: '10%',
            containLabel: true
          },
          yAxis: {
            name: 'GHG Emission tCO2e',
            nameGap: 83,
            nameTextStyle: {
              fontWeight: 'bold',
              fontSize: 14,
            },
            nameLocation: 'center',
            type: 'value'
          },
          xAxis: {
            type: 'category',
            nameGap: 30,
            //   name: 'Source of Emission',
            //   nameLocation: 'center',
            //   nameGap: 320,
            //   nameTextStyle: {
            //     fontWeight: 'bold',
            //     fontSize: 14,
            //     align: 'left',
            //     rich: {

            //     }
            //   },
            axisLabel: {
              padding: [0,0,0,2],
            },
            data: ['Estimated', 'Real']
          },
          series: {
            type: 'bar',
            stack: 'chart',
            barWidth: '70%',
            label: {
              normal: {
                position: 'right',
                show: true
              }
            },
            data:
              [
                {
                  value: this.targetGHG,
                  itemStyle: {
                    color: '#27ae60',
                  }
                },
                {
                  value: this.actual,
                  itemStyle: {
                    color: '#2980b9',
                  }
                }
              ]
          }
        };
        this.chart4_options = {
          title: {
            text: '',

          },
          grid: {
            left: '6%',
            right: '4%',
            bottom: '12%',
            containLabel: true
          },
          tooltip: {
            trigger: 'axis'
          },
          // legend: {
          //   data:['Co2', 'N2o', 'Ch4',]
          // },
          xAxis:  {
            type: 'category',
            boundaryGap: false,
            data: inensityYear.map(v => v.year),
            name: 'Years',
            nameGap: 30,
            nameTextStyle: {
              fontWeight: 'bold',
              fontSize: 14,
            },
            nameLocation: 'center',
          },
          yAxis: {
            type: 'value',
            axisLabel: {
              formatter: '{value}'
            },
            name: 'GHG Emission (tCO2e/LKR)',
            nameGap: 50,
            nameTextStyle: {
              fontWeight: 'bold',
              fontSize: 14,
            },
            nameLocation: 'center',
          },
          series: [
            {
              name:'Co2',
              type:'line',
              data: inensityYear.map(v => v.em),

            },

          ]
        };

        console.log(this.chartsData)
      })

      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.GHGEmissionSummaryBranchwise,
        {
          companyId: UserState.getInstance().companyId,
          branchId: -1,
          yearsBack: 2
        }
      ).then(data => {
        console.log(data);
        if (data.DAT !== undefined && data.DAT.list !== undefined) {
          //current year
          const emissionInfoCurrent = data.DAT.list[0].emissionInfo;
          if (emissionInfoCurrent !== undefined) {
            this.chartsData.branch.branches = Object.keys(emissionInfoCurrent);
            this.chartsData.branch.branches.forEach(b => {
              let scopes = emissionInfoCurrent[b].split('-');
              this.chartsData.branch.scope_1.push(scopes[0]);
              this.chartsData.branch.scope_2.push(scopes[1]);
              this.chartsData.branch.scope_3.push(scopes[2]);

              this.chart7_options = {
                tooltip : {
                  trigger: 'axis',
                  axisPointer: {
                    type: 'cross',
                    label: {
                      backgroundColor: echarts.tooltipBackgroundColor,
                    },
                  },
                },
                legend: {
                  data: ['Scope 1', 'Scope 2', 'Scope 3']
                },
                grid: {
                  left: '8%',
                  right: '4%',
                  bottom: '3%',
                  containLabel: true
                },
                yAxis:  {
                  name: 'GHG Emission(tCO2e)',
                  nameGap: 70,
                  nameTextStyle: {
                    fontWeight: 'bold',
                    fontSize: 14,
                  },
                  nameLocation: 'center',
                  type: 'value'
                },
                xAxis: {
                  type: 'category',
                  data: this.chartsData.branch.branches,
                  axisLabel: {
                    rotate: -90,
                  }
                },
                series: [
                  {
                    name: 'Scope 1',
                    type: 'bar',
                    barWidth: '40',
                    stack: '总量',
                    label: {
                      normal: {
                        show: true,
                        position: 'insideRight'
                      }
                    },
                    data: this.chartsData.branch.scope_1.map(v => { return {value: v}}),
                    // data: [
                    //   {
                    //     value: 300,
                    //     itemStyle: {
                    //       // color: '#2980b9'
                    //     }
                    //   },
                    //   {
                    //     value: 300,
                    //     itemStyle: {
                    //       // color: '#2980b9'
                    //     }
                    //   }
                    // ]
                  },
                  {
                    name: 'Scope 2',
                    type: 'bar',
                    stack: '总量',
                    label: {
                      normal: {
                        show: true,
                        position: 'insideRight'
                      }
                    },
                    data: this.chartsData.branch.scope_2.map(v => { return {value: v}}),
                    // data: [
                    //   {
                    //     value: 300,
                    //     itemStyle: {
                    //       // color: '#27ae60'
                    //     }
                    //   },
                    //   {
                    //     value: 300,
                    //     itemStyle: {
                    //       // color: '#27ae60'
                    //     }
                    //   }
                    // ]
                  },
                  {
                    name: 'Scope 3',
                    type: 'bar',
                    stack: '总量',
                    label: {
                      normal: {
                        show: true,
                        position: 'insideRight'
                      }
                    },
                    data: this.chartsData.branch.scope_3.map(v => { return {value: v}}),
                    // data: [
                    //   {
                    //     value: 300,
                    //     itemStyle: {
                    //       // color: '#16a085'
                    //     }
                    //   },
                    //   {
                    //     value: 300,
                    //     itemStyle: {
                    //       // color: '#16a085'
                    //     }
                    //   }
                    // ]
                  },

                ]
              };
            });

            console.log(this.chartsData)
          }

        }
      });

    }

    // todo: new request

  }

  ngAfterViewInit() {




    this.chart1_options =  {

      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          label: {
            backgroundColor: echarts.tooltipBackgroundColor,
          },
        },
      },
      backgroundColor: echarts.bg,
      title: {
        text: '',
        subtext: ''
      },
      legend: {
        //   data: ['Estimated', 'Real'],
        //   textStyle: {
        //      color: echarts.textColor,
        //   },
      },
      grid: {
        left: '14%',
        right: '4%',
        bottom: '10%',
        containLabel: true
      },
      yAxis: {
        name: 'GHG Emission tCO2e',
        nameGap: 83,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
        },
        nameLocation: 'center',
        type: 'value'
      },
      xAxis: {
        type: 'category',
        //   name: 'Source of Emission',
        //   nameLocation: 'center',
        //   nameGap: 320,
        //   nameTextStyle: {
        //     fontWeight: 'bold',
        //     fontSize: 14,
        //     align: 'left',
        //     rich: {

        //     }
        //   },
        axisLabel: {
          padding: [0,0,0,2],
        },
        data: ['Estimated', 'Real']
      },
      series: {
        type: 'bar',
        stack: 'chart',
        barWidth: '70%',
        label: {
          normal: {
            position: 'right',
            show: true
          }
        },
        data:
          [
            {
              value: 222,
              itemStyle: {
                color: '#27ae60',
              }
            },
            {
              value: 299,
              itemStyle: {
                color: '#2980b9',
              }
            }
          ]
      }
    };

    this.chart2_options =  {
      title : {
        text: '',
        subtext: ''
      },
      backgroundColor: echarts.bg,
      // color: [colors.infoLight, colors.dangerLight, colors.successLight, colors.primaryLight],
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          label: {
            backgroundColor: echarts.tooltipBackgroundColor,
          },
        },
      },
      xAxis : [
        {
          type : 'category',
          data : ['2017', '2018', '2019', '2020', '2021'],
          name: 'Years',
          nameGap: 30,
          nameTextStyle: {
            fontWeight: 'bold',
            fontSize: 14,
          },
          nameLocation: 'center',
        }
      ],
      yAxis : [
        {
          axisLabel: {
            padding: [0,0,0,2],
          },
          name: 'GHG Emission (tCO2e) per Captia',
          nameGap: 30,
          nameTextStyle: {
            fontWeight: 'bold',
            fontSize: 14,
          },
          nameLocation: 'center',
          type : 'value'
        }
      ],
      series : [
        {
          name:'Emission',
          type:'bar',
          data:[
            {
              value: 2,
              itemStyle: { color: '#16a085' }
            },
            {
              value: 2.6,
              itemStyle: { color: '#27ae60' }
            },
            {
              value: 7,
              itemStyle: { color: '#2980b9' }
            },
            {
              value: 5.6,
              itemStyle: { color: '#f1c40f' }
            },
            {
              value: 7.8,
              itemStyle: { color: '#e67e22' }
            }
          ],

        },

      ]
    };

    this.chart3_options = {
      title : {
        text: '',
        subtext: ''
      },
      backgroundColor: echarts.bg,
      // color: [colors.infoLight, colors.dangerLight, colors.successLight, colors.primaryLight],
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          label: {
            backgroundColor: echarts.tooltipBackgroundColor,
          },
        },
      },
      // legend: {
      //   data: ['2017', '2018', '2019', '2020', '2021'],
      //   textStyle: {
      //     color: echarts.textColor,
      //   },
      // },
      // toolbox: {
      //   show : true,
      //   feature : {
      //     dataView : {show: true, readOnly: false},
      //     magicType : {show: true, type: ['line', 'bar']},
      //     restore : {show: true},
      //     saveAsImage : {show: true}
      //   }
      // },
      // calculable : true,
      xAxis : [
        {
          type : 'category',
          data : ['2017', '2018', '2019', '2020', '2021'],
        }
      ],
      yAxis : [
        {
          type : 'value'
        }
      ],
      series : [
        {
          name:'Emission',
          type:'bar',
          data:[
            {
              value: 2,
              itemStyle: { color: this.colorPallete[0] }
            },
            {
              value: 2.6,
              itemStyle: { color: this.colorPallete[1]}
            },
            {
              value: 7,
              itemStyle: { color: this.colorPallete[2] }
            },
            {
              value: 5.6,
              itemStyle: { color: this.colorPallete[3] }
            },
            {
              value: 7.8,
              itemStyle: { color: this.colorPallete[4] }
            }
          ],

        },

      ]
    };

    this.chart5_options = {
      xAxis: {
        type: 'category',
        data: ['Direct', 'Indirect']
      },
      yAxis: {
        type: 'value'
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          label: {
            backgroundColor: echarts.tooltipBackgroundColor,
          },
        },
      },
      series: [{
        data: [
          {
            value: this.chartsData.direct,
            itemStyle: {color: '#1abc9c'},
          },
          {
            value: this.chartsData.indirect,
            itemStyle: {color: 'red'},
          },
        ],
        type: 'bar'
      }],
      graph: {
        // color: colorPalette
      }
    };
    this.chart6_options = {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          label: {
            backgroundColor: echarts.tooltipBackgroundColor,
          },
        },
      },
      yAxis: {
        type: 'category',
        name: 'Source of Emission',
        nameLocation: 'center',
        nameGap: 320,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
          align: 'left',
          rich: {

          }
        },
        axisLabel: {
          padding: [0,0,0,2],
        },

        data: [
          'Waste Transportation',
          'T & D Loss',
          'Waste Disposal',
          'Municipal Water',
          'Employee Commuting Not Paid by the Company',
          'Business Air Travels',
          'Grid Connected Electricity',
          'Transport Locally Purchased',
          'Vehicle Hired, Not Paid',
          'Employee Commuting, Paid by the Company',
          'Company Owned Vehicles',
          'Off-Road Mobile Sources and Machinery-Forklifts',
          'Fire Extinguishers',
          'Refirgerant Leakage',
          'Onsite Diesel Generators',
          'Vehicle Hired, Paid',
          'Vehicle Rented'
        ]
      },
      xAxis: {
        type: 'value',
        name: 'GHG Emission tCO2e',
        nameGap: 30,
        nameTextStyle: {
          fontWeight: 'bold',
          fontSize: 14,
        },
        nameLocation: 'center',
      },
      grid: {
        left: '40%',
      },
      series: [{
        type: 'bar',
        stack: 'chart',
        label: {
          normal: {
            position: 'right',
            show: true
          }
        },
        data: [
          {
            value: 1,
            itemStyle: {color: '#1abc9c'},
          },
          {
            value: 4,
            itemStyle: {color: '#1abc9c'},
          },
          {
            value: 8,
            itemStyle: {color: '#1abc9c'},
          },
          {
            value: 3,
            itemStyle: {color: '#1abc9c'},
          },
          {
            value: 6,
            itemStyle: {color: '#1abc9c'},
          },
          {
            value: 0 ,
            itemStyle: {color: '#1abc9c'},
          },
          {
            value: 11,
            itemStyle: {color: '#1abc9c'},
          },
          {
            value: 14,
            itemStyle: {color: '#1abc9c'},
          },
          {
            value: 16,
            itemStyle: {color: '#1abc9c'},
          },
          {
            value: 17,
            itemStyle: {color: '#2980b9'},
          },
          {
            value: 10,
            itemStyle: {color: '#2980b9'},
          },
          {
            value: 15,
            itemStyle: {color: '#2980b9'},
          },
          {
            value: 10,
            itemStyle: {color: '#2980b9'},
          },
          {
            value: 14,
            itemStyle: {color: '#2980b9'},
          },
          {
            value: 17,
            itemStyle: {color: '#2980b9'},
          },
          {
            value: 15,
            itemStyle: {color: '#2980b9'},
          },
          {
            value: 11,
            itemStyle: {color: '#2980b9'},
          },
        ]
      }]
    }



    this.chart4_options = {
      title: {
        text: '',

      },
      tooltip: {
        trigger: 'axis'
      },
      legend: {
        // data:['Co2', 'N2o', 'Ch4',]
      },
      xAxis:  {
        type: 'category',
        boundaryGap: false,
        data: ['2019','2020','2021','2022']
      },
      yAxis: {
        type: 'value',
        axisLabel: {
          formatter: '{value}'
        }
      },
      series: [
        {
          name:'Co2',
          type:'line',
          data:[11, 11, 15, 13, 12, 13, -20],

        },
      ]
    };

  }

  ngOnDestroy(): void {
  }

  public showBranchWiseChart():Boolean {
    if (this.chartsData.branch.branches === undefined) {
      return false;
    }
    if (this.chartsData.branch.branches.length === 0) {
      return false;
    }
    return true;
  }
}
