import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {ClientHomeComponent} from "./client-home/client-home.component";
import {ClientReportsComponent} from "./client-reports/client-reports.component";
import {ClientSummaryDataInputsComponent} from "./client-summary-data-inputs/client-summary-data-inputs.component";
import {ClientAirTravelComponent} from "./emission/client-air-travel/client-air-travel.component";
import {ClientFireExtComponent} from "./emission/client-fire-ext/client-fire-ext.component";
import {ClientRefriComponent} from "./emission/client-refri/client-refri.component";
import {ClientGeneratorsComponent} from "./emission/client-generators/client-generators.component";
import {ClientElectricityComponent} from "./emission/client-electricity/client-electricity.component";
import {ClientWasteDisposalComponent} from "./emission/client-waste-disposal/client-waste-disposal.component";
import {ClientMunicipalWaterComponent} from "./emission/client-municipal-water/client-municipal-water.component";
import {ClientEmpCommuComponent} from "./emission/client-emp-commu/client-emp-commu.component";
import {ClientTransportComponent} from "./emission/client-transport/client-transport.component";
import {ClientWasteTransportComponent} from "./emission/client-waste-transport/client-waste-transport.component";
import {ClientTransLocPurComponent} from "./emission/client-trans-loc-pur/client-trans-loc-pur.component";
import {ClinetEmployeeComponent} from "./clinet-employee/clinet-employee.component";
import {SeaAirFreightComponent} from "./emission/sea-air-freight/sea-air-freight.component";
import {BiomassComponent} from "./emission/biomass/biomass.component";
import {LpgasComponent} from "./emission/lpgas/lpgas.component";
import {ClientAshTransComponent} from "./emission/client-ash-trans/client-ash-trans.component";
import {ClientForkliftsComponent} from "./emission/client-forklifts/client-forklifts.component";
import {ClientFurnaceOilComponent} from "./emission/client-furnace-oil/client-furnace-oil.component";
import {ClientLorryTransComponent} from "./emission/client-lorry-trans/client-lorry-trans.component";
import {ClientOilgasTransComponent} from "./emission/client-oilgas-trans/client-oilgas-trans.component";
import {ClientRawMaterialTransComponent} from "./emission/client-raw-material-trans/client-raw-material-trans.component";
import {ClientSawDustTransComponent} from "./emission/client-saw-dust-trans/client-saw-dust-trans.component";
import {ClientSludgeTransComponent} from "./emission/client-sludge-trans/client-sludge-trans.component";
import {ClientStaffTransComponent} from "./emission/client-staff-trans/client-staff-trans.component";
import {ClientVehicleOthersComponent} from "./emission/client-vehicle-others/client-vehicle-others.component";
import {ClientPaidManagerVehicleComponent} from "./emission/client-paid-manager-vehicle/client-paid-manager-vehicle.component";
import {ClientPaperwasteComponent} from "./emission/client-paperwaste/client-paperwaste.component";
import { ClientWeldingComponent } from "../admin/emission/client-welding/client-welding.component";



const routes: Routes = [
  {
    path: 'home',
    component: ClientHomeComponent,
  },
  {
    path: 'reports',
    component: ClientReportsComponent
  },
  {
    path: 'summary_data_inputs',
    component: ClientSummaryDataInputsComponent,
  },
  {
    path: 'air_travel',
    component: ClientAirTravelComponent,
  },
  {
    path: 'fire_ext',
    component: ClientFireExtComponent,
  },
  {
    path: 'refri',
    component: ClientRefriComponent
  },
  {
    path: 'generators',
    component: ClientGeneratorsComponent,
  },
  {
    path: 'electricity',
    component: ClientElectricityComponent
  },
  {
    path: 'weldinf',
    component: ClientWeldingComponent
  },
  {
    path: 'waste_disposal',
    component: ClientWasteDisposalComponent,
  },
  {
    path: 'municipal_water',
    component: ClientMunicipalWaterComponent,
  },
  {
    path: 'emp_comm',
    component: ClientEmpCommuComponent,
  },
  {
    path: 'transport',
    component: ClientTransportComponent,
  },
  {
    path: 'waste_transport',
    component: ClientWasteTransportComponent,
  },
  {
    path: 'trans_loc_purch',
    component: ClientTransLocPurComponent,
  },
  {
    path: 'freight_transport',
    component: SeaAirFreightComponent,
  },
  {
    path: 'employees',
    component: ClinetEmployeeComponent,
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'biomass',
    component: BiomassComponent,

  },
  {
    path: 'lpgas',
    component: LpgasComponent,

  },
  {
    path: 'ash_transport',
    component: ClientAshTransComponent,

  },
  {
    path: 'forklifts',
    component: ClientForkliftsComponent,

  }
  ,
  {
    path: 'furnace-oil',
    component: ClientFurnaceOilComponent,

  }
  ,{
    path: 'lorry-transport',
    component: ClientLorryTransComponent,

  },
  {
    path: 'paid-manager-vehicle',
    component: ClientPaidManagerVehicleComponent,

  },
  {
    path: 'paper-waste',
    component: ClientPaperwasteComponent,

  }
  ,{
    path: 'oilgas-transport',
    component: ClientOilgasTransComponent,

  }
  ,{
    path: 'raw-material-transport',
    component: ClientRawMaterialTransComponent,

  },
  {
    path: 'saw-dust-transport',
    component: ClientSawDustTransComponent,

  },
  {
    path: 'sludge-transport',
    component: ClientSludgeTransComponent,

  },
  {
    path: 'staff-transport',
    component: ClientStaffTransComponent,

  },
  {
    path: 'vehicle_others',
    component: ClientVehicleOthersComponent,
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class ClientRoutingModule {

}

