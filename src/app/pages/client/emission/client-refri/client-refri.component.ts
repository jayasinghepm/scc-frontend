import { Component, OnInit } from '@angular/core';
import {SmartTableService} from "../../../../@core/service/smart-table.service";
import {BackendService} from "../../../../@core/rest/bo_service";
import {MassterDataService} from "../../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {RequestGroup} from "../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../@core/enums/request-type-enums";
import {UserState} from "../../../../@core/auth/UserState";
import {MatDialog} from "@angular/material";
import {DeleteConfirmComponent} from "../../../delete-confirm/delete-confirm.component";
import {ClientUpdateRefriComponent} from "./update-refri/update-refri.component";
import {LocalDataSource} from "../../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";
import {Router} from "@angular/router";

@Component({
  selector: 'app-client-refri',
  templateUrl: './client-refri.component.html',
  styleUrls: ['./client-refri.component.scss']
})
export class ClientRefriComponent implements OnInit {
  public pg_current = 0;
  public fetchedCount =0;
  public disableNext = false;
  public disablePrev = true;
  public totalCount = 0;
  public pages = 0;
  settings = {
    mode: 'external',
    pager: {
      display: false,
    },
    actions: {
      // delete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // id: {
      //   title: 'ID',
      //   type: 'number',
      //   editable: false,
      //   addable: false,
      //   filter: true,
      //   sort: true,
      //
      // },
      company: {
        title: 'Company',
        editable: false,
        addable: false,
        type: 'string',
        filter: false,
        sort: false,
      },
      //Todo: have to change
      branch: {
        title: 'Branch',
        type: 'string',
        filter: false,
        sort: false,
        editable: false,
        addable: false,

      },
      refriType: {
        title: 'Refrigerant Type',
        type: 'string',
        filter: true,
        sort: true,
      },
      amountRefilled: {
        title: 'Amount Refilled(kg)',
        type: 'number',
        filter: true,
        sort: true,
      },
      month: {
        title: 'Month',
        type: 'string',
        filter: true,
        sort: true,
      },
      year: {
        title: 'Year',
        type: 'string',
        filter: true,
        sort: true,
      },
      emission: {
        title: 'Emission (tCO2e)',
        type: 'number',
        addable: false,
        editable: false,
        filter: false,
        sort: false,
      },
    },
  };

  mySetting = JSON.parse(JSON.stringify(this.settings))
  private filterModel = {
    company: { value: '' , type: 4, col: 3},
    branchId: { value: UserState.getInstance().branchId , type: 1, col: 1},
    mon: { value: '' , type: 4, col: 3},
    amountRefilled: { value: 0 , type: 1, col:5},
    refrigerent: { value: '' , type: 4, col: 3},
    refrigerantsEntryId: { value: 0 , type: 1, col: 1},
    year: { value: '' , type: 4, col: 3},
  }


  private sortModel = {
    company: { dir: '' },
    branch: { dir: '' },
    mon: { dir: '' },
    amountRefilled: { dir: '' },
    refrigerent: { dir: '' },
    refrigerantsEntryId: { dir: '' },
    year: { dir: '' },
  }


  source: LocalDataSource = new LocalDataSource();
  constructor(private boService: BackendService,
              private masterData: MassterDataService,
              private dialog: MatDialog,
              private  toastSerivce:  NbToastrService,
              private router:Router
  ) {
    if ( !UserState.getInstance().existActiveProject) {
      // this.router.navigate(['/pages/client/reports']);
    }else {
      this.loadFiltered(this.pg_current);
    }
    this.loadFiltered(this.pg_current);
  }

  ngOnInit() {

    this.mySetting.actions = this.initActions()
    this.settings = Object.assign({}, this.mySetting);
  }

  initActions():any {

    if (UserState.getInstance().projectStatus == 4) { //data entry
      const entitle = UserState.getInstance().pageRoutes.filter(v => {
        let page = v.split('-')[0];
        if (+page === 3) {
          return v;
        }
      })[0];
      if (entitle !== undefined) {
        const add = entitle.split('-')[1] === '1'? true: false;
        const edit = entitle.split('-')[2] === '1'? true: false;
        const del = entitle.split('-')[3] === '1'? true: false;
        return {
          add: add,
          edit: edit,
          delete: del,
        }

      }
    }else {
      return {
        add: false,
        edit: false,
        delete: false,
      }
    }



  }

  private loadData() {
    let jsonBody   = {
      PAGE_NUMBER: this.pg_current,
      FILTER_MODEL: {
        branchId: { value: UserState.getInstance().branchId , type: 1, col: 1}
      }
    }
    this.boService.sendRequestToBackend(
      RequestGroup.Emission,
      RequestType.ListRefriEntry,
      jsonBody
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          let list = [];
          this.totalCount = data.DAT.TOTAL_COUNT;
          this.pages = Math.ceil(this.totalCount/20);
          this.fetchedCount = data.DAT.LIST.length;
          if (this.fetchedCount < 20) {
            this.disableNext = true;
          }
          data.DAT.LIST.forEach(val => {
            if (val != undefined) {
              list.push(this.fromListRequest(val));
            }
          });
          this.source.load(list);
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  private validateEntry(data: any, onEdit: boolean): boolean {
    // if (data.branch === undefined || data.branch === '' ){
    //   return false;
    // }
    if (onEdit && (data.company === undefined || data.company === '') ){
      return false;
    }
    if (data.refriType === undefined || data.refriType === '' ){
      return false;
    }
    if (data.amountRefilled === undefined || data.amountRefilled === '' ){
      return false;
    }

    if (data.year === undefined || data.year == "") {
      //show snack bar
      return false;
    }
    if (data.month === undefined || data.month == "") {
      //show snack bar
      return false;
    }
    if (onEdit && (data.id === undefined || data.id == "")) {
      //show snack bar
      return false;
    }
    return true;
  }

  private fromListRequest(dto: any): any {
    let company;
    let branch;
    let refriType;
    let amountRefilled;
    let year;
    let id;
    let month;

    // this.masterData.getCompanyName(dto.COMPANY_ID).subscribe(d => company =d).unsubscribe();
    // this.masterData.getRefriTrypName(dto.REFRIGERENT_TYPE).subscribe(d => refriType = d).unsubscribe();
    // this.masterData.getBranchName(dto.BRANCH_ID).subscribe(data => { branch = data; }).unsubscribe();
    this.masterData.getMonthName(dto.MONTH).subscribe(data => { month = data; }).unsubscribe();
    year = dto.YEAR;
    id = dto.REFRI_ENTRY_ID;
    amountRefilled = dto.AMOUNT_REFILLED;


    return {
      id,
      idCom: dto.COMPANY_ID,
      idBran: dto.BRANCH_ID,
      idRefriType: dto.REFRIGERENT_TYPE,
      idMon: dto.MONTH,
      company: dto.company,
      branch:dto.branch,
      amountRefilled,
      refriType: dto.refrigerent,
      refriTypeId: dto.REFRIGERENT_TYPE,
      year,
      month,
      emission:isNaN(+ dto.EMISSION_DETAILS.tco2)? 0.0 : (+ dto.EMISSION_DETAILS.tco2).toFixed(5),
      emission_info: dto.EMISSION_DETAILS,
      quantity: dto.EMISSION_DETAILS.quantity,
      gwp_410a: dto.EMISSION_DETAILS.gwp_410a,
      gwp_r407c: dto.EMISSION_DETAILS.gwp_r407c,
      gwp_r22: dto.EMISSION_DETAILS.gwp_r22,
    };



  }

  onUpdate(event: any, isNew:boolean) {
    console.log(event);
    const dialogRef = this.dialog.open(ClientUpdateRefriComponent,
      {
        data : {data: event.data , isNew: isNew, header: isNew ? 'New Entry': 'Edit Entry' },
        width: '400px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.loadFiltered(this.pg_current);
      }
    });


  }


  onClickNextPage() {
    this.pg_current++;
    this.disablePrev = false;
    if (this.isFilterEnabled()) {
      return this.loadFiltered(this.pg_current)
    }
    if (this.isSorterEnabled()) {
      return this.loadSorted(this.pg_current)
    }
    this.loadFiltered(this.pg_current);
  }

  onClickPrevPage() {
    if (this.pg_current >0) {
      this.pg_current--;
      this.disablePrev = this.pg_current === 0 ? true : false;
      if (this.isFilterEnabled()) {
        return this.loadFiltered(this.pg_current)
      }
      if (this.isSorterEnabled()) {
        return this.loadSorted(this.pg_current)
      }
      this.loadFiltered(this.pg_current);
    }
  }

  private isFilterEnabled():boolean {
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (this.filterModel[k].value !== '' || this.filterModel[k].value !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  private isSorterEnabled():boolean {
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (this.sortModel[k].dir !== '' || this.sortModel[k].dir !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  onFilter($event) {
    console.log($event)
    switch($event.query.column.id) {
      case 'id':  {
        this.filterModel.refrigerantsEntryId.value = +$event.query.query;
        break;
      } case 'amountRefilled':  {
        this.filterModel.amountRefilled.value = $event.query.query;
        break;
      }  case 'company':  {
        this.filterModel.company.value = $event.query.query;
        break;
      } case 'refriType':  {
        this.filterModel.refrigerent.value = $event.query.query;
        break;
      } case 'month':  {
        this.filterModel.mon.value = $event.query.query;
        break;
      }  case 'year':  {
        this.filterModel.year.value = $event.query.query;
        break;
      }
    }
    this.pg_current = 0;
    this.loadFiltered(0);
  }

  onSort($event) {
    this.sortModel  =  {
      company: { dir: '' },
      branch: { dir: '' },
      mon: { dir: '' },
      amountRefilled: { dir: '' },
      refrigerent: { dir: '' },
      refrigerantsEntryId: { dir: '' },
      year: { dir: '' },
    };
    console.log($event)
    switch($event.id) {
      case 'id':  {
        this.sortModel.refrigerantsEntryId.dir = $event.direction;
        break;
      } case 'amountRefilled':  {
        this.sortModel.amountRefilled.dir = $event.direction;
        break;
      }  case 'company':  {
        this.sortModel.company.dir = $event.direction;
        break;
      }  case 'branch':  {
        this.sortModel.branch.dir = $event.direction;
        break;
      }  case 'refriType':  {
        this.sortModel.refrigerent.dir = $event.direction;
        break;
      } case 'month':  {
        this.sortModel.mon.dir = $event.direction;
        break;
      }  case 'year':  {
        this.sortModel.year.dir = $event.direction;
        break;
      }
    }
    this.loadSorted(0)
  }

  loadFiltered(pageNum: number) {
    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {

        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          console.log(k)
          delete effectiveFilter[k];
        }
      }
    }
    console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }

    if (this.isSorterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListRefriEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });

            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListRefriEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }


  }

  loadSorted(pageNum: number) {

    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }
    if(this.isFilterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListRefriEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }  else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListRefriEntry,
        {
          PAGE_NUMBER: pageNum,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }
  }

  public onDelete($event: any) {
    const dialogRef = this.dialog.open(DeleteConfirmComponent,
      {
        data : { },
        width: '300px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageRefriEntry,
          {
            DATA: {
              REFRI_ENTRY_ID: $event.data.id,
              isDeleted: 1,
            }
          }).then(data => {
          console.log(data)
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.DTO != undefined) {
              this.loadFiltered(this.pg_current);
            }
          }
        });
      }
    });
  }
}
