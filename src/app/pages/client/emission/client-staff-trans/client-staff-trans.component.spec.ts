import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientStaffTransComponent } from './client-staff-trans.component';

describe('ClientStaffTransComponent', () => {
  let component: ClientStaffTransComponent;
  let fixture: ComponentFixture<ClientStaffTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientStaffTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientStaffTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
