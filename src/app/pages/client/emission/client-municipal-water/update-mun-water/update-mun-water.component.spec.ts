import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateMunWaterComponent } from './update-mun-water.component';

describe('UpdateMunWaterComponent', () => {
  let component: UpdateMunWaterComponent;
  let fixture: ComponentFixture<UpdateMunWaterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateMunWaterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateMunWaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
