import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateGeneratorsComponent } from './update-generators.component';

describe('UpdateGeneratorsComponent', () => {
  let component: UpdateGeneratorsComponent;
  let fixture: ComponentFixture<UpdateGeneratorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateGeneratorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateGeneratorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
