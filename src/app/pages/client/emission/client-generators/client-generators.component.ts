import { Component, OnInit } from '@angular/core';
import {SmartTableService} from "../../../../@core/service/smart-table.service";
import {BackendService} from "../../../../@core/rest/bo_service";
import {MassterDataService} from "../../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {RequestGroup} from "../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../@core/enums/request-type-enums";
import {UserState} from "../../../../@core/auth/UserState";
import {MatDialog} from "@angular/material";
import {DeleteConfirmComponent} from "../../../delete-confirm/delete-confirm.component";
import {ClientUpdateGeneratorsComponent} from "./update-generators/update-generators.component";
import {LocalDataSource} from "../../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";
import {Router} from "@angular/router";

@Component({
  selector: 'app-client-generators',
  templateUrl: './client-generators.component.html',
  styleUrls: ['./client-generators.component.scss']
})
export class ClientGeneratorsComponent implements OnInit {
  public pg_current = 0;
  public fetchedCount =0;
  public disableNext = false;
  public disablePrev = true;
  public totalCount = 0;
  public pages = 0;
  settings = {
    mode: 'external',
    pager: {
      display: false,
    },
    actions: {
      delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // id: {
      //   title: 'ID',
      //   type: 'number',
      //   editable: false,
      //   addable: false,
      //   filter: true,
      //   sort: true,
      //
      // },
      company: {
        title: 'Company',
        editable: false,
        addable: false,
        type: 'string',
        filter: false,
        sort: false,
      },
      //Todo: have to change
      branch: {
        title: 'Branch',
        type: 'string',
        filter: false,
        sort: false,
        editable: false,
        addable: false,

      },
      fuelType: {
        title: 'Fuel Type',
        type: 'string',
        filter: true,
        sort: true,
      },
      units: {
        title: 'Unit',
        type: 'string',
        filter: false,
        sort: false,
      },
      amountOfFuel: {
        title: 'Amount of Fuel',
        type: 'number',
        filter: true,
        sort: true,
      },
      generatorNumber: {
        title: 'Generator Number',
        type: 'number',
        filter: true,
        sort: true,
      },
      month: {
        title: 'Month',
        type: 'string',
        filter: true,
        sort: true,
      },
      year: {
        title: 'Year',
        type: 'string',
        filter: true,
        sort: true,
      },
      emission: {
        title: 'Emission (tCO2e)',
        type: 'number',
        addable: false,
        editable: false,
        filter: false,
        sort: false,
      },
    },
  };
  mySetting = JSON.parse(JSON.stringify(this.settings))

  source: LocalDataSource = new LocalDataSource();
  private filterModel = {
    company: { value: '' , type: 4, col: 3},
    branchId: { value: UserState.getInstance().branchId , type: 1, col: 1},
    mon: { value: '' , type: 4, col: 3},
    consumption: { value: 0 , type: 1, col:5},
    generatorNumber: { value: '' , type: 4, col: 3},
    entryId: { value: 0 , type: 1, col: 1},

    year: { value: '' , type: 4, col: 3},
  }


  private sortModel = {
    company: { dir: '' },
    branch: { dir: '' },
    mon: { dir: '' },
    consumption: { dir: '' },
    generatorNumber: { dir: '' },
    entryId: { dir: '' },
    year: { dir: '' },
  }

  constructor(private boService: BackendService,
              private masterData: MassterDataService,
              private dialog: MatDialog,
              private  toastSerivce:  NbToastrService,
              private router:Router
  ) {
    if ( !UserState.getInstance().existActiveProject) {
      // this.router.navigate(['/pages/client/reports']);
    }else {
      this.loadFiltered(this.pg_current);
    }
    this.loadFiltered(this.pg_current);
  }

  ngOnInit() {

    this.mySetting.actions = this.initActions()
    this.settings = Object.assign({}, this.mySetting);
  }

  initActions():any {

    if (UserState.getInstance().projectStatus == 4) { //data entry
      const entitle = UserState.getInstance().pageRoutes.filter(v => {
        let page = v.split('-')[0];
        if (+page === 11) {
          return v;
        }
      })[0];
      if (entitle !== undefined) {
        const add = entitle.split('-')[1] === '1'? true: false;
        const edit = entitle.split('-')[2] === '1'? true: false;
        const del = entitle.split('-')[3] === '1'? true: false;
        return {
          add: add,
          edit: edit,
          delete: del,
        }

      }
    }else {
      return {
        add: false,
        edit: false,
        delete: false,
      }
    }



  }

  private loadData() {
    let jsonBody   = {
      PAGE_NUMBER: this.pg_current,
      FILTER_MODEL: {
        branchId: { value: UserState.getInstance().branchId , type: 1, col: 1}
      }
    }
    this.boService.sendRequestToBackend(
      RequestGroup.Emission,
      RequestType.ListGeneratorsEntry,
      jsonBody
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          let list = [];
          this.totalCount = data.DAT.TOTAL_COUNT;
          this.pages = Math.ceil(this.totalCount/20);
          this.fetchedCount = data.DAT.LIST.length;
          if (this.fetchedCount < 20) {
            this.disableNext = true;
          }
          data.DAT.LIST.forEach(val => {
            if (val != undefined) {
              list.push(this.fromListRequest(val));
            }
          });
          this.source.load(list);
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  onUpdate(event: any, isNew:boolean) {
    console.log(event);
    const dialogRef = this.dialog.open(ClientUpdateGeneratorsComponent,
      {
        data : {data: event.data , isNew: isNew, header: isNew ? 'New Entry': 'Edit Entry' },
        width: '400px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.loadFiltered(this.pg_current);
      }
    });


  }


  private validateEntry(data: any, onEdit: boolean): boolean {
    // if (data.branch === undefined || data.branch === '' ){
    //   return false;
    // }

    // if (data.numOfGenerators === undefined || data.numOfGenerators === '' ){
    //   return false;
    // }
    if (data.generatorNumber === undefined || data.generatorNumber === '' ){
      return false;
    }
    if (data.fuelType === undefined || data.fuelType === '' ){
      return false;
    }
    if (data.amountOfFuel === undefined || data.amountOfFuel === '' ){
      return false;
    }
    if (data.units === undefined || data.units === '' ){
      return false;
    }


    if (data.year === undefined || data.year == "") {
      //show snack bar
      return false;
    }
    if (data.month === undefined || data.month == "") {
      //show snack bar
      return false;
    }
    if (onEdit && (data.id === undefined || data.id == "")) {
      //show snack bar
      return false;
    }
    if (onEdit && (data.company === undefined || data.company === '') ){
      return false;
    }
    return true;
  }

  private fromListRequest(dto: any): any {
    let company;
    let branch;
    let numOfGenerators;
    let fuelType;
    let units
    let amountOfFuel;
    let month;
    let year;
    let id;

    this.masterData.getFuelTypeName(dto.FUEL_TYPE).subscribe(d => fuelType =d).unsubscribe();
    // this.masterData.getBranchName(dto.BRANCH_ID).subscribe(data => { branch = data; }).unsubscribe();
    this.masterData.getUnitsName(dto.UNITS).subscribe(d => units =d).unsubscribe();
    // this.masterData.getCompanyName(dto.COMPANY_ID).subscribe(data => { company = data; }).unsubscribe();
    this.masterData.getMonthName(dto.MONTH).subscribe(data => { month = data; }).unsubscribe();
    year = dto.YEAR;
    id = dto.GENERATORS_ENTRY_ID;
    numOfGenerators = dto.NUM_GENERATORS;
    amountOfFuel = dto.FUEL_CONSUMPTION;
    let generatorNumber = dto.GENERATOR_NUMBER;

    return {
      id,
      idCom: dto.COMPANY_ID,
      idBran: dto.BRANCH_ID,
      idFuel: dto.FUEL_TYPE,
      idUnits: dto.UNITS,
      idMon: dto.MONTH,
      company: dto.company,
      branch: dto.branch,
      numOfGenerators,
      amountOfFuel,
      units,
      fuelType,
      year,
      month,
      generatorNumber,
      emission: isNaN(+dto.EMISSION_INFO.tco2) ? 0.0 : (+dto.EMISSION_INFO.tco2).toFixed(5),
      emission_info: dto.EMISSION_INFO,
      tco2: dto.EMISSION_INFO.tco2,
      co2: dto.EMISSION_INFO.co2,
      n20: dto.EMISSION_INFO.n20,
      ch4: dto.EMISSION_INFO.ch4,
      quantity: dto.EMISSION_INFO.quantity,
      direct: dto.EMISSION_INFO.direct,
      density_diesel: dto.EMISSION_INFO.density_diesel,
      net_caloric: dto.EMISSION_INFO.net_caloric,
      ef_co2: dto.EMISSION_INFO.ef_co2,
      ef_ch4: dto.EMISSION_INFO.ef_ch4,
      ef_n20: dto.EMISSION_INFO.ef_n20,
      // ef_n20: dto.EMISSION_INFO.ef_n20,
      gwp_co2: dto.EMISSION_INFO.gwp_co2,
      gwp_ch4: dto.EMISSION_INFO.gwp_ch4,
      gwp_n20: dto.EMISSION_INFO.gwp_n20,
      te_dg : dto.EMISSION_INFO.te_dg,
    };



  }

  public onDelete($event: any) {
    const dialogRef = this.dialog.open(DeleteConfirmComponent,
      {
        data : { },
        width: '300px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageGeneratorsEntry,
          {
            DATA: {
              GENERATORS_ENTRY_ID: $event.data.id,
              isDeleted: 1,
            }
          }).then(data => {
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.DTO != undefined) {
              this.loadFiltered(this.pg_current);
            }
          }
        });
      }
    });
  }

  onClickNextPage() {
    this.pg_current++;
    this.disablePrev = false;
    if (this.isFilterEnabled()) {
      return this.loadFiltered(this.pg_current)
    }
    if (this.isSorterEnabled()) {
      return this.loadSorted(this.pg_current)
    }
    this.loadFiltered(this.pg_current);
  }

  onClickPrevPage() {
    if (this.pg_current >0) {
      this.pg_current--;
      this.disablePrev = this.pg_current === 0 ? true : false;
      if (this.isFilterEnabled()) {
        return this.loadFiltered(this.pg_current)
      }
      if (this.isSorterEnabled()) {
        return this.loadSorted(this.pg_current)
      }
      this.loadFiltered(this.pg_current);
    }
  }

  private isFilterEnabled():boolean {
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (this.filterModel[k].value !== '' || this.filterModel[k].value !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  private isSorterEnabled():boolean {
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (this.sortModel[k].dir !== '' || this.sortModel[k].dir !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  onFilter($event) {
    console.log($event)
    switch($event.query.column.id) {
      case 'id':  {
        this.filterModel.entryId.value = +$event.query.query;
        break;
      } case 'amountOfFuel':  {
        this.filterModel.consumption.value = $event.query.query;
        break;
      }  case 'company':  {
        this.filterModel.company.value = $event.query.query;
        break;
      }case 'generatorNumber':  {
        this.filterModel.generatorNumber.value = $event.query.query;
        break;
      } case 'month':  {
        this.filterModel.mon.value = $event.query.query;
        break;
      }  case 'year':  {
        this.filterModel.year.value = $event.query.query;
        break;
      }
    }
    this.pg_current = 0;
    this.loadFiltered(0);
  }

  onSort($event) {
    this.sortModel  =  {
      company: { dir: '' },
      branch: { dir: '' },
      mon: { dir: '' },
      consumption: { dir: '' },
      generatorNumber: { dir: '' },
      entryId: { dir: '' },
      year: { dir: '' },


    };
    console.log($event)
    switch($event.id) {
      case 'id':  {
        this.sortModel.entryId.dir = $event.direction;
        break;
      } case 'amountOfFuel':  {
        this.sortModel.consumption.dir = $event.direction;
        break;
      }  case 'company':  {
        this.sortModel.company.dir = $event.direction;
        break;
      }  case 'branch':  {
        this.sortModel.branch.dir = $event.direction;
        break;
      }  case 'generatorNumber':  {
        this.sortModel.generatorNumber.dir = $event.direction;
        break;
      } case 'month':  {
        this.sortModel.mon.dir = $event.direction;
        break;
      }  case 'year':  {
        this.sortModel.year.dir = $event.direction;
        break;
      }
    }
    this.loadSorted(0)
  }

  loadFiltered(pageNum: number) {
    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }

    if (this.isSorterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListGeneratorsEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListGeneratorsEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }


  }

  loadSorted(pageNum: number) {

    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }
    if(this.isFilterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListGeneratorsEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }  else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListGeneratorsEntry,
        {
          PAGE_NUMBER: pageNum,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }
  }
}
