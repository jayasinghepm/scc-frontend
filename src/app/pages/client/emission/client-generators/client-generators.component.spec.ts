import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientGeneratorsComponent } from './client-generators.component';

describe('ClientGeneratorsComponent', () => {
  let component: ClientGeneratorsComponent;
  let fixture: ComponentFixture<ClientGeneratorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientGeneratorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientGeneratorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
