import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientOilgasTransComponent } from './client-oilgas-trans.component';

describe('ClientOilgasTransComponent', () => {
  let component: ClientOilgasTransComponent;
  let fixture: ComponentFixture<ClientOilgasTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientOilgasTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientOilgasTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
