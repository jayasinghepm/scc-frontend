import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateClientOilgasComponent } from './update-client-oilgas.component';

describe('UpdateClientOilgasComponent', () => {
  let component: UpdateClientOilgasComponent;
  let fixture: ComponentFixture<UpdateClientOilgasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateClientOilgasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClientOilgasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
