import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateClientLorryTransComponent } from './update-client-lorry-trans.component';

describe('UpdateClientLorryTransComponent', () => {
  let component: UpdateClientLorryTransComponent;
  let fixture: ComponentFixture<UpdateClientLorryTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateClientLorryTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClientLorryTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
