import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientLorryTransComponent } from './client-lorry-trans.component';

describe('ClientLorryTransComponent', () => {
  let component: ClientLorryTransComponent;
  let fixture: ComponentFixture<ClientLorryTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientLorryTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientLorryTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
