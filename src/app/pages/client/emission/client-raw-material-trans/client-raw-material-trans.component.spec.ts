import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientRawMaterialTransComponent } from './client-raw-material-trans.component';

describe('ClientRawMaterialTransComponent', () => {
  let component: ClientRawMaterialTransComponent;
  let fixture: ComponentFixture<ClientRawMaterialTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientRawMaterialTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientRawMaterialTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
