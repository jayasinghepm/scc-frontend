import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateClientRawMaterialTransComponent } from './update-client-raw-material-trans.component';

describe('UpdateClientRawMaterialTransComponent', () => {
  let component: UpdateClientRawMaterialTransComponent;
  let fixture: ComponentFixture<UpdateClientRawMaterialTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateClientRawMaterialTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClientRawMaterialTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
