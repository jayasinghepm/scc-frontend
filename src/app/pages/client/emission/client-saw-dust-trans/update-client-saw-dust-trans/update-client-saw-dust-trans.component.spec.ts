import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateClientSawDustTransComponent } from './update-client-saw-dust-trans.component';

describe('UpdateClientSawDustTransComponent', () => {
  let component: UpdateClientSawDustTransComponent;
  let fixture: ComponentFixture<UpdateClientSawDustTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateClientSawDustTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClientSawDustTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
