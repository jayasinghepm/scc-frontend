import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientFireExtComponent } from './client-fire-ext.component';

describe('ClientFireExtComponent', () => {
  let component: ClientFireExtComponent;
  let fixture: ComponentFixture<ClientFireExtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientFireExtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientFireExtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
