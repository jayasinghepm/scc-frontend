import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateClientForkliftsComponent } from './update-client-forklifts.component';

describe('UpdateClientForkliftsComponent', () => {
  let component: UpdateClientForkliftsComponent;
  let fixture: ComponentFixture<UpdateClientForkliftsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateClientForkliftsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClientForkliftsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
