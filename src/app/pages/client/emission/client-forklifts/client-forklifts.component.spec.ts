import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientForkliftsComponent } from './client-forklifts.component';

describe('ClientForkliftsComponent', () => {
  let component: ClientForkliftsComponent;
  let fixture: ComponentFixture<ClientForkliftsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientForkliftsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientForkliftsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
