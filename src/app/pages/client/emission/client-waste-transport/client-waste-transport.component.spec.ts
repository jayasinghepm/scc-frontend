import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientWasteTransportComponent } from './client-waste-transport.component';

describe('ClientWasteTransportComponent', () => {
  let component: ClientWasteTransportComponent;
  let fixture: ComponentFixture<ClientWasteTransportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientWasteTransportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientWasteTransportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
