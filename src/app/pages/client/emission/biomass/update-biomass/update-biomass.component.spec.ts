import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateBiomassComponent } from './update-biomass.component';

describe('UpdateBiomassComponent', () => {
  let component: UpdateBiomassComponent;
  let fixture: ComponentFixture<UpdateBiomassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateBiomassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBiomassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
