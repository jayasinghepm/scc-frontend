import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateClientPaidManagerVehicleComponent } from './update-client-paid-manager-vehicle.component';

describe('UpdateClientPaidManagerVehicleComponent', () => {
  let component: UpdateClientPaidManagerVehicleComponent;
  let fixture: ComponentFixture<UpdateClientPaidManagerVehicleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateClientPaidManagerVehicleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClientPaidManagerVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
