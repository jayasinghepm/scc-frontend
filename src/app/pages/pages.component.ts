import {ChangeDetectorRef, Component, OnInit} from '@angular/core';

import {
  CLIENT_MENU_ITEMS,
  ADMIN_MENU_ITEMS,
  COM_ADMIN_MENU_ITEMS,
  MASTER_ADMIN_ITEMS,
  DATA_ENTRY_USER_MENU_ITEMS, getClientMenus, getComAdminMenus, getDataEntryUserMenus, ADMIN_SPECIAL_MENU_ITEMS
} from './pages-menu';
import {UserState} from "../@core/auth/UserState";

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
     <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent implements OnInit {

  // menu = CLIENT_MENU_ITEMS;
  // menu = COM_ADMIN_MENU_ITEMS;
  menu :any;

  constructor(private ref: ChangeDetectorRef) {

    switch (UserState.getInstance().userType) {
      case 1:
      {
        this.menu = getClientMenus();
        break;
      }
      case 2: {
        this.menu = getComAdminMenus();
        break;
      }

      case 3: {
        if (UserState.getInstance().ismasterAdmin) {
          this.menu = MASTER_ADMIN_ITEMS;
        }else {
                    this.menu = ADMIN_MENU_ITEMS; 

          if(UserState.getInstance().adminId == 3){
            console.log("mmmmmmmmmmmmmmmmm---yyy",UserState.getInstance().adminId)

            this.menu = ADMIN_SPECIAL_MENU_ITEMS;
          }

        }

        break;
      }
      case 4: {
        this.menu = getDataEntryUserMenus();
        break;
      }
    }

  }

  ngOnInit(): void {
    //
    // switch (UserState.getInstance().userType) {
    //   case 1:
    //   {
    //     this.menu = CLIENT_MENU_ITEMS;
    //     break;
    //   }
    //   case 2: {
    //     this.menu = COM_ADMIN_MENU_ITEMS;
    //     break;
    //   }
    //
    //   case 3: {
    //     if (UserState.getInstance().ismasterAdmin) {
    //       this.menu = MASTER_ADMIN_ITEMS;
    //     }else {
    //       this.menu = ADMIN_MENU_ITEMS;
    //     }
    //
    //     break;
    //   }
    //   case 4: {
    //     this.menu = DATA_ENTRY_USER_MENU_ITEMS;
    //     break;
    //   }
    // }
    // this.ref.detectChanges()
  }



}
