import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCadminAshTransComponent } from './update-cadmin-ash-trans.component';

describe('UpdateCadminAshTransComponent', () => {
  let component: UpdateCadminAshTransComponent;
  let fixture: ComponentFixture<UpdateCadminAshTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCadminAshTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCadminAshTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
