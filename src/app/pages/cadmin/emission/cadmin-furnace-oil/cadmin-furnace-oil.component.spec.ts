import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadminFurnaceOilComponent } from './cadmin-furnace-oil.component';

describe('CadminFurnaceOilComponent', () => {
  let component: CadminFurnaceOilComponent;
  let fixture: ComponentFixture<CadminFurnaceOilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadminFurnaceOilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadminFurnaceOilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
