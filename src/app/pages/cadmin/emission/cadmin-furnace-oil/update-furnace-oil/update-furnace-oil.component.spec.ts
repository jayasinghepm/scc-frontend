import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFurnaceOilComponent } from './update-furnace-oil.component';

describe('UpdateFurnaceOilComponent', () => {
  let component: UpdateFurnaceOilComponent;
  let fixture: ComponentFixture<UpdateFurnaceOilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFurnaceOilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFurnaceOilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
