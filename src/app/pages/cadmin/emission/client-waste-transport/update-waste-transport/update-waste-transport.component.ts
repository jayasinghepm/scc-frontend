import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from "@angular/material";
import {MassterDataService} from "../../../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import {BackendService} from "../../../../../@core/rest/bo_service";
import {UserState} from "../../../../../@core/auth/UserState";

@Component({
  selector: 'app-update-waste-transport',
  templateUrl: './update-waste-transport.component.html',
  styleUrls: ['./update-waste-transport.component.scss']
})
export class CadminUpdateWasteTransportComponent implements OnInit {
  private isSaved = false;

  public companies = [];
  public branches = [];
  public months = [];
  public years = [];
  public wastes = [];
  public fuels = [];
  public vehicles = []


  public jsonBody = {
    WASTE_TRANSPORT_ENTRY_ID:  -1,
    BRANCH_ID: undefined,
    COMPANY_ID: undefined, // todo:
    YEAR: undefined,
    MONTH: undefined,
    WASTE_TYPE: undefined,
    SUB_CONTRACTOR_NAME: undefined,
    FUEL_TYPE: undefined,
    VEHICLE_TYPE: undefined,
    FUEL_ECONOMY: undefined,
    LOADING_CAPACITY: undefined,
    DISTANCE_TRAVELLED: undefined ,
    company: undefined,
    branch: undefined,
    mon: undefined,
    waste: undefined,
    fuel: undefined,
    vehicle: undefined,
    WASTE_TONS: 0,
    noOfTurns: 1,
  }

  constructor( public dialogRef: MatDialogRef<CadminUpdateWasteTransportComponent>,
               private masterData: MassterDataService,
               private toastSerivce: NbToastrService,
               private boService: BackendService,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    this.init(this.popupData.data, this.popupData.isNew)
    console.log(this.popupData)
  }

  private init(data: any, isNew: boolean) {
    this.jsonBody.COMPANY_ID = UserState.getInstance().companyId;
    this.masterData.getWasteTransVehicleTypesFull().subscribe(d => {
      this.vehicles = d;
    })
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companies = d;
      this.jsonBody.company = this.companies.filter(c => c.id === this.jsonBody.COMPANY_ID)[0].name;
    })
    this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
      console.log(d);
      this.years = d;
    })
    // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
    //   this.branches = d;
    //   console.log(d);
    // })
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });
    this.masterData.getFuelTypesVehicleFull().subscribe(d => {
      this.fuels = d;
    })
    this.masterData.getWasteTypesFull().subscribe(d => {
      this.wastes = d;
    })

    this.branches.push({
      id: 0, name: 'Search Branch'
    })

    if (!isNew) {
      this.jsonBody.WASTE_TRANSPORT_ENTRY_ID = data.id;
      this.jsonBody.COMPANY_ID = data.idCom ;
      this.jsonBody.BRANCH_ID = data.idBran ;
      this.jsonBody.WASTE_TYPE = data.idWaste ;
      this.jsonBody.VEHICLE_TYPE = data.idVehicle ;
      this.jsonBody.FUEL_TYPE = data.idFuel ;
      this.jsonBody.MONTH = data.idMon ;
      this.jsonBody.LOADING_CAPACITY = data.loadingCapacity ;
      this.jsonBody.FUEL_ECONOMY = data.fuelEconomy ;
      this.jsonBody.DISTANCE_TRAVELLED = data.distance ;
      this.jsonBody.SUB_CONTRACTOR_NAME = data.subContractor ;
      this.jsonBody.YEAR = data.year;
      this.jsonBody.WASTE_TONS = data.wasteTons;
      this.jsonBody.noOfTurns = data.noOfTurns
      // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => this.branches = d)
      this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
        console.log(d);
        this.years = d;
      })

      this.branches.push({
        id: data.idBran, name: data.branch
      })
    }
  }

  onSearchBranch(value: any) {
    if (value === '') {
      return;
    }
    let queryReq = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        name: { value: value , type: 4, col: 3},
        companyId: { value: this.jsonBody.COMPANY_ID, type: 1, col: 1},
      },
    }
    // if (this.selectedComId != 0) {
    //   queryReq.FILTER_MODEL[ 'companyId'] = { value: this.selectedComId, type: 1, col: 1}
    // }
    this.branches = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      queryReq
    ).then(data => {
      console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.branches = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.branches.unshift({id: val.id, name: val.name});
            }
          });
          if (this.branches.length === 0){
            this.branches.push({id: 0, name: 'No Matches Found'})
          }
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  private validateEntry(onEdit: boolean): boolean {
    console.log(this.jsonBody)
    // if (this.jsonBody.BRANCH_ID === undefined || this.jsonBody.BRANCH_ID === ''  || this.jsonBody.BRANCH_ID === 0){
    //   console.log(1)
    //   return false;
    // }
    if (onEdit && (this.jsonBody.COMPANY_ID === undefined || this.jsonBody.COMPANY_ID === '') ){
      console.log(2)
      return false;
    }
    if (this.jsonBody.WASTE_TYPE === undefined || this.jsonBody.WASTE_TYPE === '' ){
      console.log(3)
      return false;
    }
    if (this.jsonBody.SUB_CONTRACTOR_NAME === undefined || this.jsonBody.SUB_CONTRACTOR_NAME === '' ){
      console.log(4)
      return false;
    }
    if (this.jsonBody.VEHICLE_TYPE === undefined || this.jsonBody.VEHICLE_TYPE === '' ){
      console.log(5)
      return false;
    }
    if (this.jsonBody.FUEL_TYPE === undefined || this.jsonBody.FUEL_TYPE === '' ){
      console.log(6)
      return false;
    }
    if (this.jsonBody.DISTANCE_TRAVELLED === undefined || this.jsonBody.DISTANCE_TRAVELLED === '' ){
      console.log(7)
      return false;
    }
    if (this.jsonBody.FUEL_ECONOMY === undefined || this.jsonBody.FUEL_ECONOMY === '' ){
      console.log(8)
      return false;
    }
    // if (this.jsonBody.year === undefined || this.jsonBody.year == "") {
    //   //show snack bar
    //   return false;
    // }
    if (this.jsonBody.YEAR === undefined || this.jsonBody.YEAR == "") {
      //show snack bar
      console.log(9)
      return false;
    }
    if (this.jsonBody.MONTH === undefined || this.jsonBody.MONTH < 0) {
      //show snack bar
      console.log(10)
      return false;
    }
    if (this.jsonBody.LOADING_CAPACITY === undefined || this.jsonBody.LOADING_CAPACITY == "") {
      console.log(11)
      //show snack bar
      return false;
    }
    if (onEdit && (this.jsonBody.WASTE_TRANSPORT_ENTRY_ID === undefined || this.jsonBody.WASTE_TRANSPORT_ENTRY_ID <= 0)) {
      console.log(12)
      //show snack bar
      return false;
    }

    return true;
  }


  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close(this.isSaved);

  }

  onChangeCompany(value: any) {
    this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)
    this.masterData.getYearsForCompany(value).subscribe(d => {
      console.log(d);
      this.years = d;
    })
  }

  onClickSave() {
    if (!this.popupData.isNew) {
      // update
      if (this.validateEntry(!this.popupData.isNew)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageWasteTransportEntry,
          {DATA: this.jsonBody}
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
            // event.confirm.resolve(event.newData);
            // this.loadData();
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }
    else {
      if (this.validateEntry( false)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageWasteTransportEntry,
          {DATA: this.jsonBody}
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
            // event.confirm.resolve(event.newData);
            // this.loadData();
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }

  }


  onChangeBranch($event: MatSelectChange) {
    this.jsonBody.branch = this.branches.filter(b => b.id === $event.value)[0].name;

  }

  onChangeMonth($event: MatSelectChange) {
    this.jsonBody.mon = this.months.filter(m => m.id === $event.value)[0].name;
  }

  onChangeFuel($event: MatSelectChange) {
    this.jsonBody.fuel = this.fuels.filter(f => f.id === $event.value)[0].name;
  }

  onChangeVehicle($event: MatSelectChange) {
    this.jsonBody.vehicle = this.vehicles.filter(v => v.id === $event.value)[0].name;
  }

  onChangeWaste($event: MatSelectChange) {
    this.jsonBody.waste = this.wastes.filter(w => w.id === $event.value)[0].name;
  }
}
