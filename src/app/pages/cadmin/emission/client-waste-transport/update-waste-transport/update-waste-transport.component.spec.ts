import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateWasteTransportComponent } from './update-waste-transport.component';

describe('UpdateWasteTransportComponent', () => {
  let component: UpdateWasteTransportComponent;
  let fixture: ComponentFixture<UpdateWasteTransportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateWasteTransportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateWasteTransportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
