import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCadminPaperwasteComponent } from './update-cadmin-paperwaste.component';

describe('UpdateCadminPaperwasteComponent', () => {
  let component: UpdateCadminPaperwasteComponent;
  let fixture: ComponentFixture<UpdateCadminPaperwasteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCadminPaperwasteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCadminPaperwasteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
