import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadminRawMaterialTransComponent } from './cadmin-raw-material-trans.component';

describe('CadminRawMaterialTransComponent', () => {
  let component: CadminRawMaterialTransComponent;
  let fixture: ComponentFixture<CadminRawMaterialTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadminRawMaterialTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadminRawMaterialTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
