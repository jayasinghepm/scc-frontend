import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadminSludgeTransComponent } from './cadmin-sludge-trans.component';

describe('CadminSludgeTransComponent', () => {
  let component: CadminSludgeTransComponent;
  let fixture: ComponentFixture<CadminSludgeTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadminSludgeTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadminSludgeTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
