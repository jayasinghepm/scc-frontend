import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCadminSludgeTransComponent } from './update-cadmin-sludge-trans.component';

describe('UpdateCadminSludgeTransComponent', () => {
  let component: UpdateCadminSludgeTransComponent;
  let fixture: ComponentFixture<UpdateCadminSludgeTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCadminSludgeTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCadminSludgeTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
