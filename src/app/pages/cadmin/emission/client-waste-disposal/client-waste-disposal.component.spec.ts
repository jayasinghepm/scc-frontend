import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientWasteDisposalComponent } from './client-waste-disposal.component';

describe('ClientWasteDisposalComponent', () => {
  let component: ClientWasteDisposalComponent;
  let fixture: ComponentFixture<ClientWasteDisposalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientWasteDisposalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientWasteDisposalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
