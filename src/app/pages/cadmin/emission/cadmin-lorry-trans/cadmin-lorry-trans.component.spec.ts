import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadminLorryTransComponent } from './cadmin-lorry-trans.component';

describe('CadminLorryTransComponent', () => {
  let component: CadminLorryTransComponent;
  let fixture: ComponentFixture<CadminLorryTransComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadminLorryTransComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadminLorryTransComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
