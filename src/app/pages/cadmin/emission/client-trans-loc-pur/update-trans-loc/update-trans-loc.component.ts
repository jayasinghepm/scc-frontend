import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from "@angular/material";
import {MassterDataService} from "../../../../../@core/service/masster-data.service";
import {BackendService} from "../../../../../@core/rest/bo_service";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {UserState} from "../../../../../@core/auth/UserState";

@Component({
  selector: 'app-update-trans-loc',
  templateUrl: './update-trans-loc.component.html',
  styleUrls: ['./update-trans-loc.component.scss']
})
export class CadminUpdateTransLocComponent implements OnInit {
  private isSaved = false;

  public jsonBody = {
    TRANSPORT_LOCAL_ENTRY_ID:  -1,
    BRANCH_ID: undefined,
    COMPANY_ID:undefined, // todo:
    MATERIAL_TYPE: undefined, // todo
    YEAR: undefined,
    MONTH: undefined,
    FUEL_TYPE: undefined,
    VEHICLE_TYPE: undefined,
    FUEL_ECONOMY:undefined,
    DISTANCE_TRAVELLED: undefined,
    company: undefined,
    branch: undefined,
    mon: undefined,
    fuel: undefined,
    vehicle: undefined,
    noOfTrips : 1,
    fuelConsumption:undefined,
    weight: undefined,
    LOADING_CAPACITY: undefined,
  }

  public companies = [];
  public branches = [];
  public months = [];
  public years = [];
  public fuels = [];
  public vehicleModels = [];
  public matTypes = []


  constructor( public dialogRef: MatDialogRef<CadminUpdateTransLocComponent>,
               private masterData: MassterDataService,
               private boService: BackendService,
               private toastSerivce: NbToastrService,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    this.init(this.popupData.data, this.popupData.isNew);
    console.log(this.popupData)
  }

  private init(data:any, isNew:boolean){
    this.jsonBody.COMPANY_ID = UserState.getInstance().companyId;
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companies = d;
      this.jsonBody.company = this.companies.filter(c => c.id === this.jsonBody.COMPANY_ID)[0].name;
    });
    this.masterData.getMatrialtypesFull().subscribe(d => {
      this.matTypes = d;
    })
    this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
      console.log(d);
      this.years = d;
    })
    // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
    //   this.branches = d;
    //   console.log(d);
    // })
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });
    this.masterData.getFuelTypesVehicleFull().subscribe(d => {
      this.fuels = d;
    })
    this.masterData.getVehicleModelsFull().subscribe(d => {
      this.vehicleModels = d;
    });

    this.branches.push({
      id: 0, name: 'Search Branch'
    })

    if (!isNew) {
      this.jsonBody.TRANSPORT_LOCAL_ENTRY_ID = data.id ;
      this.jsonBody.BRANCH_ID = data.idBran ;
      this.jsonBody.COMPANY_ID= data.idCom ;
      this.jsonBody.MATERIAL_TYPE = data.idMat ;
      this.jsonBody.YEAR = data.year ;
      this.jsonBody.MONTH = data.idMon ;
      this.jsonBody.FUEL_ECONOMY = data.fuelEconomy;
      this.jsonBody.VEHICLE_TYPE = data.idVeh ;
      this.jsonBody.FUEL_TYPE = data.idFuel ;
      this.jsonBody.DISTANCE_TRAVELLED = data.distance ;
      this.jsonBody.weight = data.weight;
      this.jsonBody.LOADING_CAPACITY = data.loadingCapacity;
      // this.masterData.getBranchesForCompany(this.jsonBody.COMPANY_ID).subscribe(d => this.branches = d)
      this.masterData.getYearsForCompany(this.jsonBody.COMPANY_ID).subscribe(d => {
        console.log(d);
        this.years = d;
      })
      this.jsonBody.fuelConsumption = data.fuelConsumption ;
      this.jsonBody.noOfTrips = data.noOfTrips;
      this.branches.push({
        id: data.idBran, name: data.branch
      })
    }

  }

  onSearchBranch(value: any) {
    if (value === '') {
      return;
    }
    let queryReq = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        name: { value: value , type: 4, col: 3},
        companyId: { value: this.jsonBody.COMPANY_ID, type: 1, col: 1},
      },
    }
    // if (this.selectedComId != 0) {
    //   queryReq.FILTER_MODEL[ 'companyId'] = { value: this.selectedComId, type: 1, col: 1}
    // }
    this.branches = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      queryReq
    ).then(data => {
      console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.branches = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.branches.unshift({id: val.id, name: val.name});
            }
          });
          if (this.branches.length === 0){
            this.branches.push({id: 0, name: 'No Matches Found'})
          }
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  private validateEntry( onEdit: boolean): boolean {
    // if (this.jsonBody.BRANCH_ID === undefined || this.jsonBody.BRANCH_ID === '' || this.jsonBody.BRANCH_ID === 0){
    //   return false;
    // }
    if (onEdit && (this.jsonBody.COMPANY_ID === undefined || this.jsonBody.COMPANY_ID === '') ){

      return false;
    }
    if (this.jsonBody.MATERIAL_TYPE === undefined || this.jsonBody.MATERIAL_TYPE === '' ){

      return false;
    }
    if (this.jsonBody.VEHICLE_TYPE === undefined || this.jsonBody.VEHICLE_TYPE === '' ){

      return false;
    }
    // if (this.jsonBody.DISTANCE_TRAVELLED === undefined || this.jsonBody.DISTANCE_TRAVELLED=== '' ){

    //   return false;
    // }
    if (this.jsonBody.FUEL_TYPE === undefined || this.jsonBody.FUEL_TYPE == "") {
      //show snack bar

      return false;
    }
    // if (this.jsonBody.FUEL_ECONOMY === undefined || this.jsonBody.FUEL_ECONOMY == "") {
    //   //show snack bar

    //   return false;
    // }
    if (this.jsonBody.YEAR === undefined || this.jsonBody.YEAR == "") {
      //show snack bar

      return false;
    }
    if (this.jsonBody.MONTH === undefined || this.jsonBody.MONTH < 0) {
      //show snack bar

      return false;
    }
    // if (!(
    //   (this.jsonBody.fuelConsumption > 0) || (this.jsonBody.DISTANCE_TRAVELLED > 0 &&
    //     this.jsonBody.FUEL_ECONOMY  > 0 && this.jsonBody.noOfTrips > 0))) {

    //   return false;
    // }
    if (onEdit && (this.jsonBody.TRANSPORT_LOCAL_ENTRY_ID === undefined || this.jsonBody.TRANSPORT_LOCAL_ENTRY_ID <= 0)) {
      //show snack bar
      return false;
    }
    return true;
  }

  changeFuelConsumption ($event) {
    if (this.jsonBody.FUEL_ECONOMY == 0) {
      this.jsonBody.fuelConsumption = 0;
      return;
    }
    this.jsonBody.fuelConsumption = (!this.jsonBody.FUEL_ECONOMY || !this.jsonBody.LOADING_CAPACITY) ? 0 : this.jsonBody.noOfTrips * (this.jsonBody.weight/this.jsonBody.LOADING_CAPACITY) *
      (this.jsonBody.DISTANCE_TRAVELLED/this.jsonBody.FUEL_ECONOMY);  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close(this.isSaved);

  }

  onChangeCompany(value: any) {
    this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)
    this.masterData.getYearsForCompany(value).subscribe(d => {
      console.log(d);
      this.years = d;
    })
  }

  public onClickSave() {
    // update
    if (!this.popupData.isNew) {
        if (this.validateEntry( true)) {
          this.boService.sendRequestToBackend(
            RequestGroup.Emission,
            RequestType.ManageTransLocEntry,
            { DATA: this.jsonBody}
          ).then( data => {
            if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
              this.isSaved = true;
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            } else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          })
        } else {
          this.toastSerivce.show('', 'Fill empty fields', {
            status: 'warning',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }

    }
    // new
    else {
      if (this.validateEntry( false)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageTransLocEntry,
          { DATA: this.jsonBody}
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }
  }

  onChangeBranch($event: MatSelectChange) {
    this.jsonBody.branch = this.branches.filter(b => b.id === $event)[0].name;

  }

  onChangeMonth($event: MatSelectChange) {
    this.jsonBody.mon = this.months.filter(m => m.id === $event.value)[0].name;
  }

  onChangeFuel($event: MatSelectChange) {
    this.jsonBody.fuel = this.fuels.filter(f  => f.id === $event.value)[0].name;
  }

  onChangeVehicle($event: MatSelectChange) {
    this.jsonBody.vehicle = this.vehicleModels.filter(v => v.id === $event.value)[0].name;
  }
}
