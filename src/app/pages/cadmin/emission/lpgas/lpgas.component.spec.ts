import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LpgasComponent } from './lpgas.component';

describe('LpgasComponent', () => {
  let component: LpgasComponent;
  let fixture: ComponentFixture<LpgasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LpgasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LpgasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
