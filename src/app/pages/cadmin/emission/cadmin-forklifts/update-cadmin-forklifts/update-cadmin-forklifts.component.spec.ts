import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCadminForkliftsComponent } from './update-cadmin-forklifts.component';

describe('UpdateCadminForkliftsComponent', () => {
  let component: UpdateCadminForkliftsComponent;
  let fixture: ComponentFixture<UpdateCadminForkliftsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCadminForkliftsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCadminForkliftsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
