import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatCheckboxChange, MatDialogRef, MatSelectChange} from "@angular/material";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import {MassterDataService} from "../../../../../@core/service/masster-data.service";
import {BackendService} from "../../../../../@core/rest/bo_service";
import {UserState} from "../../../../../@core/auth/UserState";

@Component({
  selector: 'app-update-emp-comm',
  templateUrl: './update-emp-comm.component.html',
  styleUrls: ['./update-emp-comm.component.scss']
})
export class CadminUpdateEmpCommComponent implements OnInit {

  public isOneWay = false;
  public showPublicUp = true;
  public showNoEmUp = true;
  public showOwnUp = true;
  public showPublicDown = true;
  public showNoEmDown = true;
  public showOwn = true;

  public  tabNum = 1;

  public showDirectMode = false;

  public jsonBody = {
    id:  -1,
    companyId: undefined ,
    branchId: undefined,
    empId: undefined,
    noEmissionMode_UP: 0,
    noEmissionMode_DOWN: 0,
    noEmission_distance_UP:0 ,
    noEmission_distance_DOWN: 0,
    publicTransMode_UP: 0,
    publicTransMode_DOWN:0,
    publicTrans_distance_UP: 0,
    publicTrans_distance_DOWN: 0,
    ownTransMode_UP: 0,
    ownTransMode_DOWN: 0,
    ownTrans_distance_UP: 0,
    ownTrans_distance_DOWN: 0,
    ownTrans_fuelEconomy_DOWN: 0,
    ownTrans_fuelType_UP: 0,
    ownTrans_fuelType_DOWN: 0,
    paidByCom: false,
    noOfWorkingDays: 0,
    isTwoWay: undefined,
    ownTrans_fuelEconomy_UP: 0,
    month: undefined,
    year: undefined,
    company: undefined,
    branch: undefined,
    mon: undefined,
    paid: undefined,
    twoWay: undefined,

    companyPetrolLiters: 0,
    companyDieselLiters: 0,
    ownPetrolLiters: 0,
    ownDieselLiters: 0,

    petrolLiters: 0,
    dieselLiters: 0,
  }

  public months = [];
  public years = [];
  public branches = [];
  public companies = [];
  public fuels = [];
  public noEmissionOptions = [];
  public pubTransOptions = [];
  public ownTransOptions = [];

  constructor( public dialogRef: MatDialogRef<CadminUpdateEmpCommComponent>,
               private masterData: MassterDataService,
               private boService: BackendService,
               private toastSerivce: NbToastrService,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any) {
    this.init(this.popupData.data, this.popupData.isNew);
    console.log(this.popupData)
  }

  private init(data: any, isNew:boolean) {
    this.jsonBody.companyId = UserState.getInstance().companyId;
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companies = d;
      this.jsonBody.company = this.companies.filter(c => c.id === this.jsonBody.companyId)[0].name;
    })
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });
    this.masterData.getFuelTypesVehicleFull().subscribe(d => {
      this.fuels = d;
    })
    this.masterData.getNoEmissionOptionsFull().subscribe(d => {
      this.noEmissionOptions = d;
    });
    this.masterData.getPublicVehicleTypesFull().subscribe(d => {
      this.pubTransOptions = d;
    });
    this.masterData.getVehiclesOwnFull().subscribe(d => {
      this.ownTransOptions = d;
    })
    this.masterData.getYearsForCompany(this.jsonBody.companyId).subscribe(d => {
      console.log(d);
      this.years = d;
    })
    // this.masterData.getBranchesForCompany(this.jsonBody.companyId).subscribe(d => {
    //   this.branches = d;
    //   console.log(d);
    // })

    this.branches.push({
      id: 0, name: 'Search Branch'
    })

    if (!isNew) {
      this.jsonBody.id = data.id;
      this.jsonBody.year = data.year;
      this.jsonBody.month = data.idMon;
      this.jsonBody.companyId = data.idCom ;
      this.jsonBody.branchId = data.idBran ;
      this.jsonBody.empId = data.empId ;
      this.jsonBody.noEmissionMode_UP = data.idNoEmUp ;
      this.jsonBody.noEmissionMode_DOWN = data.idNoEmDown ;
      this.jsonBody.noEmission_distance_DOWN = data.noEmission_distance_DOWN ;
      this.jsonBody.noEmission_distance_UP = data.noEmission_distance_UP ;
      this.jsonBody.publicTransMode_DOWN = data.idPubTransDown ;
      this.jsonBody.publicTransMode_UP = data.idPubTransUp ;
      this.jsonBody.publicTrans_distance_UP = data.publicTrans_distance_UP ;
      this.jsonBody.publicTrans_distance_DOWN = data.publicTrans_distance_DOWN ;
      this.jsonBody.ownTransMode_UP = data.idOwnTranUp ;
      this.jsonBody.ownTransMode_DOWN = data.idOwnTransDown ;
      this.jsonBody.ownTrans_distance_UP = data.ownTrans_distance_UP ;
      this.jsonBody.ownTrans_distance_DOWN = data.ownTrans_distance_DOWN ;
      this.jsonBody.ownTrans_fuelEconomy_DOWN = data.ownTrans_fuelEconomy_DOWN ;
      this.jsonBody.ownTrans_fuelEconomy_UP = data.ownTrans_fuelEconomy_UP ;
      this.jsonBody.ownTrans_fuelType_UP = data.idFuelTypeUp ;
      this.jsonBody.isTwoWay = data.isTwoWay ;
      this.jsonBody.paidByCom = data.paidByCom;;
      this.jsonBody.ownTrans_fuelType_DOWN = data.idFuelTypeDown ;
      this.jsonBody.noOfWorkingDays = data.noOfWorkingDays;
      this.jsonBody.ownPetrolLiters = data.ownPetrolLiters;
      this.jsonBody.ownDieselLiters = data.ownDieselLiters;
      this.jsonBody.companyDieselLiters = data.companyDieselLiters;
      this.jsonBody.companyPetrolLiters = data.companyPetrolLiters;

      if (this.jsonBody.companyPetrolLiters || this.jsonBody.companyDieselLiters || this.jsonBody.ownDieselLiters ||
      this.jsonBody.ownPetrolLiters) {
        this.showDirectMode = true;
      }else {
        this.showDirectMode = false;
      }


      // this.masterData.getBranchesForCompany(this.jsonBody.companyId).subscribe(d => this.branches = d)
      this.masterData.getYearsForCompany(this.jsonBody.companyId).subscribe(d => {
        this.years = d;
      })

      this.branches.push({
        id: data.idBran, name: data.branch
      })
    }
  }

  onChangeCompany(value: any) {
    this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)
    this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)
    this.masterData.getYearsForCompany(value).subscribe(d => {
      console.log(d);
      this.years = d;
    })
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();

  }

  onSearchBranch(value: any) {
    if (value === '') {
      return;
    }
    let queryReq = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        name: { value: value , type: 4, col: 3},
        companyId: { value: this.jsonBody.companyId, type: 1, col: 1},
      },
    }
    // if (this.selectedComId != 0) {
    //   queryReq.FILTER_MODEL[ 'companyId'] = { value: this.selectedComId, type: 1, col: 1}
    // }
    this.branches = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      queryReq
    ).then(data => {
      console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.branches = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.branches.unshift({id: val.id, name: val.name});
            }
          });
          if (this.branches.length === 0){
            this.branches.push({id: 0, name: 'No Matches Found'})
          }
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  private validateEntry( onEdit: boolean): boolean {
    console.log(this.jsonBody)
    if (onEdit && (this.jsonBody.id === undefined || this.jsonBody.id <= 0)) {
      console.log(1)
      return false;
    }
    if (this.jsonBody.companyId === undefined || this.jsonBody.companyId <= 0) {
      console.log(2)
      return false;
    }
    // if (this.jsonBody.branchId === undefined || this.jsonBody.branchId <= 0) {
    //   console.log(3)
    //   return false;
    // }
    if (this.jsonBody.empId === undefined || this.jsonBody.empId === '' ) {
      console.log(4)
      return false;
    }
    if (this.jsonBody.month === undefined || this.jsonBody.month < 0) {
      console.log(21)
      return false;
    }
    if (this.jsonBody.year === undefined || this.jsonBody.year === '') {
      console.log(22)
      return false;
    }
    if (this.showDirectMode) {
      return true;
    }

    if (this.jsonBody.noOfWorkingDays === null || this.jsonBody.noOfWorkingDays === undefined || this.jsonBody.noOfWorkingDays === 0) {
      return false;
    }
    if (this.jsonBody.noEmissionMode_UP === undefined || this.jsonBody.noEmissionMode_UP <= 0) {
      console.log(5)
      return false;
    }
    if (this.jsonBody.isTwoWay && (this.jsonBody.noEmissionMode_DOWN === undefined || this.jsonBody.noEmissionMode_DOWN <= 0)) {
      console.log(6)
      return false;
    }
    if (this.jsonBody.publicTransMode_UP === undefined || this.jsonBody.publicTransMode_UP <= 0) {
      console.log(7)
      return true;
    }
    if (this.jsonBody.isTwoWay && (this.jsonBody.publicTransMode_DOWN === undefined || this.jsonBody.publicTransMode_DOWN <= 0)) {
      console.log(8)
      return false;
    }
    if (this.jsonBody.ownTransMode_UP === undefined || this.jsonBody.ownTransMode_UP <= 0) {
      console.log(9)
      return false;
    }
    if (this.jsonBody.isTwoWay && (this.jsonBody.ownTransMode_DOWN === undefined || this.jsonBody.ownTransMode_DOWN <= 0)) {
      console.log(10)
      return false;
    }
    if (this.jsonBody.ownTransMode_UP !== 20 && (this.jsonBody.ownTrans_fuelType_UP === undefined || this.jsonBody.ownTrans_fuelType_UP <= 0)) {
      console.log(11)
      return false;
    }
    if (this.jsonBody.isTwoWay && (this.jsonBody.ownTransMode_DOWN !== 20 && (this.jsonBody.ownTrans_fuelType_DOWN === undefined || this.jsonBody.ownTrans_fuelType_DOWN <= 0))) {
      console.log(12)
      return false;
    }if (this.jsonBody.noEmissionMode_UP !== 20 && (this.jsonBody.noEmission_distance_UP === undefined || this.jsonBody.noEmission_distance_UP < 0)) {
      console.log(13)
      return false;
    }if (this.jsonBody.isTwoWay && (this.jsonBody.noEmissionMode_DOWN !== 20 && (this.jsonBody.noEmission_distance_DOWN === undefined || this.jsonBody.noEmission_distance_DOWN < 0))) {
      console.log(14)
      return false;
    }if (this.jsonBody.publicTransMode_UP !== 20 && (this.jsonBody.publicTrans_distance_UP === undefined || this.jsonBody.publicTrans_distance_UP < 0)) {
      console.log(15)
      return false;
    }if (this.jsonBody.isTwoWay && (this.jsonBody.publicTransMode_DOWN !== 20 && (this.jsonBody.publicTrans_distance_DOWN === undefined || this.jsonBody.publicTrans_distance_DOWN < 0))) {
      console.log(16)
      return false;
    }if (this.jsonBody.isTwoWay && (this.jsonBody.ownTransMode_DOWN !==20 && (this.jsonBody.ownTrans_distance_DOWN === undefined || this.jsonBody.ownTrans_distance_DOWN <0))) {
      console.log(17)
      return false;
    }if (this.jsonBody.ownTransMode_UP !== 20 && (this.jsonBody.ownTrans_distance_UP === undefined || this.jsonBody.ownTrans_distance_UP < 0)) {
      console.log(18)
      return false;
    }if (this.jsonBody.isTwoWay && (this.jsonBody.ownTransMode_DOWN !== 20 && (this.jsonBody.ownTrans_fuelEconomy_DOWN === undefined || this.jsonBody.ownTrans_fuelEconomy_DOWN <= 0))) {
      console.log(19)
      return false;
    }
    if (this.jsonBody.ownTransMode_UP !== 20 && (this.jsonBody.ownTrans_fuelEconomy_UP === undefined || this.jsonBody.ownTrans_fuelEconomy_UP <= 0)) {
      console.log(20)
      return false;
    }

    return true;

  }


  onClickSave() {
    console.log("updateeeeee=======",this.jsonBody)
    this.jsonBody.companyPetrolLiters = this.jsonBody.petrolLiters;
    this.jsonBody.companyDieselLiters = this.jsonBody.dieselLiters;
    this.jsonBody.ownPetrolLiters = this.jsonBody.petrolLiters;
    this.jsonBody.ownDieselLiters = this.jsonBody.dieselLiters;
    
    if (!this.popupData.isNew) {
        if (this.validateEntry(true)) {
          this.boService.sendRequestToBackend(
            RequestGroup.Emission,
            RequestType.ManageEmpCommuting,
            { DATA: this.jsonBody}
          ).then( data => {
            if (data !== undefined && data.DAT !== undefined &&  data.DAT.dto !== undefined) {
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            } else {
              this.toastSerivce.show('', 'Error in saving data1.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
          })
        } else {
          this.toastSerivce.show('', 'Fill empty fields', {
            status: 'warning',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }

      }
    // new
    else {
      if (this.jsonBody.twoWay == undefined || this.jsonBody.twoWay == '') {
        this.jsonBody.twoWay = "No";
      }
      if (this.jsonBody.paid == undefined || this.jsonBody.paid == '') {
        this.jsonBody.paid ="No";
      }
      if (this.validateEntry(false)) {
        console.log("okkkkkkk====")
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageEmpCommuting,
          { DATA: this.jsonBody}
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.dto !== undefined) {
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })

          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields-xx', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }
  }

  public onClickNext() {
    if (this.tabNum === 3) {
      this.tabNum = 1;
      return;
    }
    this.tabNum++;

  }
  public onClickPrev() {
    if (this.tabNum === 1) {
      this.tabNum = 3;
      return;
    }
    this.tabNum--;

  }

  onChangeBranch($event: MatSelectChange) {




    this.jsonBody.branch = this.branches.filter(b => b.id === $event.value)[0].name;

  }

  onChangeMonth($event: MatSelectChange) {
    this.jsonBody.mon = this.months.filter(m => m.id === $event.value)[0].name;
  }

  onChangePaid($event: MatCheckboxChange) {
    this.jsonBody.paid = $event.checked ? "Yes": "No";
  }

  onChangeEntryMode($event : MatCheckboxChange) {
    this.showDirectMode = $event.checked;
  }

  onChangeTwoway($event: MatCheckboxChange) {
    this.jsonBody.twoWay = $event.checked ? "Yes": "No";

  }
}
