import { Component, OnInit } from '@angular/core';
import {BackendService} from "../../../../../@core/rest/bo_service";
import {MassterDataService} from "../../../../../@core/service/masster-data.service";
import {NbToastrService} from "@nebular/theme";
import {Router} from "@angular/router";
import {UserState} from "../../../../../@core/auth/UserState";
import {MatDialog} from "@angular/material/dialog";
import {LocalDataSource} from "../../../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import {CadminUpdateEmpCommComponent} from "../../client-emp-commu/update-emp-comm/update-emp-comm.component";
import {DeleteConfirmComponent} from "../../../../delete-confirm/delete-confirm.component";
import {UpdateSeaAirFreightComponent} from "../update-sea-air-freight/update-sea-air-freight.component";
import {AshTransFormulaComponent} from "../../../../formulas/ash-trans-formula/ash-trans-formula.component";
import {FreightFormulaComponent} from "../../../../formulas/freight-formula/freight-formula.component";

@Component({
  selector: 'app-sea-air-freight',
  templateUrl: './sea-air-freight.component.html',
  styleUrls: ['./sea-air-freight.component.scss']
})
export class SeaAirFreightComponent implements OnInit {

  public pg_current = 0;
  public fetchedCount =0;
  public disableNext = false;
  public disablePrev = true;
  public totalCount = 0;
  public pages = 0;
  settings = {
    mode: 'external',
    pager: {
      display: false,
    },
    actions: {
      // delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    editor: {
      type: 'completer',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {

      company: {
        title: 'Company',
        type: 'string',
        filter: false,
        sort: false,

      },
      //Todo: have to change
      branch: {
        title: 'Branch',
        type: 'string',
        filter: true,
        sort: true,

      },
      portFromText : {
        title: 'Departure',
        type: 'string',
        filter: true,
        sort: true,
      },
      airFreight : {
        title: 'Freight Type',
        type: 'string',
        filter: false,
        sort: false,

      },
      freightKg : {
        title: 'Weight(tons)',
        type: 'string',
        filter: true,
        sort: true,
      },




      month: {
        title: 'Month',
        type: 'string',
        filter: true,
        sort: true,
      },
      year: {
        title: 'Year',
        type: 'number',
        filter: true,
        sort: true,
      },
      emission: {
        title: 'Emission (tCO2e)',
        type: 'number',
        addable: false,
        editable: false,
        filter: false,
        sort: false,
      },

    },
  };

  mySetting = JSON.parse(JSON.stringify(this.settings))

  source: LocalDataSource = new LocalDataSource();

  private filterModel = {
    companyId: { value: UserState.getInstance().companyId , type: 1, col: 1},
    branch: { value: '' , type: 4, col: 3},
    mon: { value: '' , type: 4, col: 3},
    portFromText: { value: '' , type: 4, col: 3},
    id: { value: 0 , type: 1, col: 1},
    freightKg: { value: 0 , type: 1, col: 1},
    year: { value: '' , type: 4, col: 3},
    airFreight: { value: '' , type: 4, col: 3},

  }


  private sortModel = {
    company: { dir: '' },
    branch: { dir: '' },
    mon: { dir: '' },
    portFromText: { dir: '' },
    id: { dir: '' },
    freightKg: { dir: '' },
    year: { dir: '' },
    airFreight: { dir: '' },

  }

  constructor(
    private boService: BackendService,
    private masterData: MassterDataService,
    private dialog: MatDialog,
    private  toastSerivce:  NbToastrService ,
    private router :Router
  ) {

    if (!UserState.getInstance().existActiveProject) {
      // this.router.navigate(['/pages/com_admin/reports']);
    }else {


    }
    this.loadData()

  }

  ngOnInit() {
    this.mySetting.actions = this.initActions()
    this.settings = Object.assign({}, this.mySetting);
  }

  initActions():any {

    if (UserState.getInstance().projectStatus == 4) { //data entry
      const entitle = UserState.getInstance().pageRoutes.filter(v => {
        let page = v.split('-')[0];
        if (+page === 14) {
          return v;
        }
      })[0];
      if (entitle !== undefined) {
        const add = entitle.split('-')[1] === '1'? true: false;
        const edit = entitle.split('-')[2] === '1'? true: false;
        const del = entitle.split('-')[3] === '1'? true: false;
        return {
          add: add,
          edit: edit,
          delete: del,
        }

      }
    }else {
      return {
        add: false,
        edit: false,
        delete: false,
      }
    }



  }
  private validateEntry(data: any, onEdit: boolean): boolean {
    if (data.branch === undefined || data.branch == "") {
      //   //show snack bar
      console.log('branch')
      return false;
    }

    if (data.company_distance === undefined || data.company_distance == "") {
      //show snack bar
      console.log('compan distance')

      return false;
    }
    if (data.companyFuelMode === undefined || data.companyFuelMode == "") {
      //show snack bar
      console.log('compan fuel mode')

      return false;
    }
    if (data.month === undefined || data.month == "") {
      //show snack bar
      console.log('month')

      return false;
    } if (data.ownTrans_noOfTrips === undefined || data.ownTrans_noOfTrips == "") {
      //show snack bar
      console.log('own trans no of trips')

      return false;
    } if (data.ownTrans_fuelEconomy === undefined || data.ownTrans_fuelEconomy == "") {
      //show snack bar
      console.log('own trans fuel econom')

      return false;
    } if (data.ownTrans_noOfTrips === undefined || data.ownTrans_noOfTrips == "") {
      //show snack bar
      console.log('ow n no of trips')
      return false;
    }
    if (data.own_distance === undefined || data.own_distance == "") {
      //show snack bar
      console.log('own distance')
      return false;
    }
    if (data.ownTransMode === undefined || data.ownTransMode == "") {
      //show snack bar
      console.log('own trans mode')
      return false;
    }
    if (data.public_distance === undefined || data.public_distance == "") {
      //show snack bar
      console.log('public distance')
      return false;
    }
    if (data.empIDorName === undefined || data.empIDorName == "") {
      //show snack bar
      console.log('emp id or name')
      return false;
    }

    if (data.noEmissionMode === undefined || data.noEmissionMode == "") {
      //show snack bar
      console.log('no emission mode')
      return false;
    }
    if (data.noEmission_distance === undefined || data.noEmission_distance == "") {
      //show snack bar
      console.log('no emisssion distance')
      return false;
    }
    if (data.publicTranMode === undefined || data.publicTranMode == "") {
      //show snack bar
      console.log('public tans mdode')
      return false;
    }
    if (data.year === undefined || data.year == "") {
      //show snack bar
      console.log('year')
      return false;
    }
    // if (data.month === undefined || data.month == "") {
    //   //show snack bar
    //   return false;
    // }
    // if (data.transit1 === undefined || data.transit1 == "") {
    //   //show snack bar
    //   return false;
    // }
    if (onEdit && (data.id === undefined || data.id == "")) {
      //show snack bar
      console.log('id')
      return false;
    }
    if (onEdit && (data.company === undefined || data.company === '') ){
      console.log('company')
      return false;
    }
    return true;

  }





  private loadData() {
    let jsonBody   = {
      PAGE_NUMBER: this.pg_current,
      FILTER_MODEL: {
        companyId: { value: UserState.getInstance().companyId , type: 1, col: 1}
      }
    }
    this.boService.sendRequestToBackend(
      RequestGroup.Emission,
      RequestType.ListSeaAirFreightEntry,
      jsonBody
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          let list = [];
          this.totalCount = data.DAT.TOTAL_COUNT;
          this.pages = Math.ceil(this.totalCount/20);
          this.fetchedCount = data.DAT.LIST.length;
          if (this.fetchedCount < 20) {
            this.disableNext = true;
          } else {
            this.disableNext = false;
          }
          data.DAT.LIST.forEach(val => {
            if (val != undefined) {
              list.push(this.fromListRequest(val));
            }
          });
          this.source.load(list);
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  private fromListRequest(dto: any): any {

    let month;
    let year;
    let id;



    // this.masterData.getBranchName(dto.branchId).subscribe(data => { branch = data; }).unsubscribe();
    // // this.masterData.getA
    // this.masterData.getCompanyName(dto.companyId).subscribe(data => { company = data; }).unsubscribe();
    this.masterData.getMonthName(dto.month).subscribe(data => { month = data; }).unsubscribe();
    year = dto.year;
    id = dto.id;

    return {
      id,
      company: dto.company,
      idCom: dto.companyId,
      idBran: dto.branchId,
      branch: dto.branch,

      airFreight: dto.isAirFreight == 1 ? "Sea" : "Air",
      isAirFreight: dto.isAirFreight,
      freightKg: dto.freightKg,
      portFromText: dto.portFromText,
      portFrom: dto.portFrom,

      emissionInfo: dto.emissionDetails,
      emission: isNaN(+dto.emissionDetails.tco2) ? 0.0 : (+dto.emissionDetails.tco2).toFixed(5),
      year,
      month,
      idMon: dto.month,
    };



  }


  onClickNextPage() {
    this.pg_current++;
    this.disablePrev = false;
    if (this.isFilterEnabled()) {
      return this.loadFiltered(this.pg_current)
    }
    if (this.isSorterEnabled()) {
      return this.loadSorted(this.pg_current)
    }
    this.loadFiltered(this.pg_current);
  }

  onClickPrevPage() {
    if (this.pg_current >0) {
      this.pg_current--;
      this.disablePrev = this.pg_current === 0 ? true : false;
      if (this.isFilterEnabled()) {
        return this.loadFiltered(this.pg_current)
      }
      if (this.isSorterEnabled()) {
        return this.loadSorted(this.pg_current)
      }
      this.loadFiltered(this.pg_current);
    }
  }

  private isFilterEnabled():boolean {
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (this.filterModel[k].value !== '' || this.filterModel[k].value !== 0) {
          return true;
        }
      }
    }
    return false;
  }

  private isSorterEnabled():boolean {
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (this.sortModel[k].dir !== '' || this.sortModel[k].dir !== 0) {
          return true;
        }
      }
    }
    return false;
  }




  onFilter($event) {
    switch($event.query.column.id) {
      case 'id':  {
        this.filterModel.id.value = $event.query.query;
        break;
      } case 'branch':  {
        this.filterModel.branch.value = $event.query.query;
        break;
      }  case 'portFromText':  {
        this.filterModel.portFromText.value = $event.query.query;
        break;
      }  case 'freightKg':  {
        this.filterModel.freightKg.value = $event.query.query;
        break;
      } case 'month':  {
        this.filterModel.mon.value = $event.query.query;
        break;
      }  case 'year':  {
        this.filterModel.year.value = $event.query.query;
        break;
      }
    }
    this.pg_current = 0;
    this.loadFiltered(0);
  }

  onSort($event) {
    this.sortModel  =  {
      company: { dir: '' },
      branch: { dir: '' },
      mon: { dir: '' },
      portFromText: { dir: '' },
      id: { dir: '' },
      freightKg: { dir: '' },
      year: { dir: '' },
      airFreight: { dir: '' },


    };
    console.log($event)
    switch($event.id) {
      case 'id':  {
        this.sortModel.id.dir = $event.direction;
        break;
      }  case 'company':  {
        this.sortModel.company.dir = $event.direction;
        break;
      }  case 'branch':  {
        this.sortModel.branch.dir = $event.direction;
        break;
      }  case 'portFromText':  {
        this.sortModel.portFromText.dir = $event.direction;
        break;
      }  case 'freightKg':  {
        this.sortModel.freightKg.dir = $event.direction;
        break;
      } case 'month':  {
        this.sortModel.mon.dir = $event.direction;
        break;
      }  case 'year':  {
        this.sortModel.year.dir = $event.direction;
        break;
      }
    }
    this.loadSorted(0)
  }

  loadFiltered(pageNum: number) {
    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }

    if (this.isSorterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListSeaAirFreightEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        console.log(data);
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });

            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListSeaAirFreightEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }

    // this.boService.sendRequestToBackend(
    //   RequestGroup.Emission,
    //   RequestType.ListElectricityEntry,
    //   {
    //     PAGE_NUMBER: pageNum,
    //     FILTER_MODEL: effectiveFilter,
    //   }
    // ).then(data => {
    //   if (data.HED != undefined && data.HED.RES_STS == 1) {
    //     if (data.DAT != undefined && data.DAT.LIST != undefined) {
    //       let list = [];
    //       this.totalCount = data.DAT.TOTAL_COUNT;
    //       this.pages = Math.ceil(this.totalCount/20);
    //       this.fetchedCount = data.DAT.LIST.length;
    //       if (this.fetchedCount < 20) {
    //         this.disableNext = true;
    //       } else {
    //         this.disableNext = false;
    //       }
    //       data.DAT.LIST.forEach(val => {
    //         if (val != undefined) {
    //           list.push(this.fromListRequest(val));
    //         }
    //       });
    //       this.source.load(list);
    //     } else {
    //       //  todo: show snack bar
    //       console.log('error data 1')
    //     }
    //   } else {
    //     //  todo: show snack bar
    //     console.log('error data 2')
    //   }
    // })
  }

  loadSorted(pageNum: number) {

    let effectiveFilter = JSON.parse(JSON.stringify(this.filterModel));
    for (let k of Object.keys(this.filterModel)) {
      if (k !== undefined) {
        if (effectiveFilter[k].value === '' || effectiveFilter[k].value === 0) {
          delete effectiveFilter[k];
        }
      }
    }
    console.log(effectiveFilter)
    if (Object.keys(effectiveFilter).length === 0) {
      effectiveFilter = '';
    }

    let effectiveSorter = JSON.parse(JSON.stringify(this.sortModel));
    for (let k of Object.keys(this.sortModel)) {
      if (k !== undefined) {
        if (effectiveSorter[k].dir === '' || effectiveSorter[k].dir === 0) {
          delete effectiveSorter[k];
        }
      }
    }
    console.log(effectiveSorter)
    if (Object.keys(effectiveSorter).length === 0) {
      effectiveSorter = '';
    }
    if(this.isFilterEnabled()) {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListSeaAirFreightEntry,
        {
          PAGE_NUMBER: pageNum,
          FILTER_MODEL: effectiveFilter,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }  else {
      this.boService.sendRequestToBackend(
        RequestGroup.Emission,
        RequestType.ListSeaAirFreightEntry,
        {
          PAGE_NUMBER: pageNum,
          SORT_MODEL: effectiveSorter,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.LIST != undefined) {
            let list = [];
            this.totalCount = data.DAT.TOTAL_COUNT;
            this.pages = Math.ceil(this.totalCount/20);
            this.fetchedCount = data.DAT.LIST.length;
            if (this.fetchedCount < 20) {
              this.disableNext = true;
            } else {
              this.disableNext = false;
            }
            data.DAT.LIST.forEach(val => {
              if (val != undefined) {
                list.push(this.fromListRequest(val));
              }
            });
            this.source.load(list);
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }
  }

  onUpdate(event: any, isNew:boolean) {
    console.log(event);
    const dialogRef = this.dialog.open(UpdateSeaAirFreightComponent,
      {
        data : {data: event.data , isNew: isNew, header: isNew ? 'New Entry': 'Edit Entry' },
        width: '820px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      this.loadFiltered(this.pg_current);
      if (d) {
        this.loadFiltered(this.pg_current);
      }
    });

  }

  public onDelete($event: any) {
    const dialogRef = this.dialog.open(DeleteConfirmComponent,
      {
        data : { },
        width: '300px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      }).afterClosed().subscribe(d => {
      if (d) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageSeaAirFreightEntry,
          {
            DATA: {
              id: $event.data.id,
              isDeleted: 1,
            }
          }).then(data => {
            console.log(data)
          if (data.HED != undefined && data.HED.RES_STS == 1) {
            if (data.DAT != undefined && data.DAT.DTO != undefined) {
              this.loadFiltered(this.pg_current);
            }
          }
        });
      }
    });
  }

  onShowFormula($event: any) {
    if (UserState.getInstance().disableFormulas) {
      return;
    }
    const dialogRef = this.dialog.open(FreightFormulaComponent,
      {
        data : {data: $event.data},
        width: '500px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      })
  }
}
