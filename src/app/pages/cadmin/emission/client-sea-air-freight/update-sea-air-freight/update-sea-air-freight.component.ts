import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MassterDataService} from "../../../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {BackendService} from "../../../../../@core/rest/bo_service";
import {UserState} from "../../../../../@core/auth/UserState";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import {MatSelectChange} from "@angular/material/select";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

interface Aport {
  value: number;
  name: string;
}


@Component({
  selector: 'app-update-sea-air-freight',
  templateUrl: './update-sea-air-freight.component.html',
  styleUrls: ['./update-sea-air-freight.component.scss']
})
export class UpdateSeaAirFreightComponent implements OnInit {

  public isSaved = false;



  public  jsonBody = {
     id: -1,
    portFrom: undefined,
   portFromText: undefined,
    isAirFreight: undefined,
    freightKg: undefined,
    containerType: undefined, // 1-40ft 2-20ft 3-14.5
   branchId: undefined,
    companyId: undefined,
    month: undefined,
    year: undefined,
    company: undefined,
     branch: undefined,
     mon: undefined,

  };



  public months = [];
  public companies = []
  public years = [];
  public branches = [];
  public freightTypes = [];
  public ports = [];



  constructor(
    public dialogRef: MatDialogRef<UpdateSeaAirFreightComponent>,
    private masterData: MassterDataService,
    private toastSerivce: NbToastrService,
    private boService: BackendService,
    @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any,

  ) {
    this.init(this.popupData.data, this.popupData.isNew);

  }

  private init(data: any, isNew:boolean) {
    console.log(data)
    this.jsonBody.companyId = UserState.getInstance().companyId;
    this.masterData.getCompaninesFull().subscribe(d => {
      this.companies = d;
      this.jsonBody.company = this.companies.filter(c => c.id === this.jsonBody.companyId)[0].name;
    })
    this.masterData.getYearsForCompany(this.jsonBody.companyId).subscribe(d => {
      console.log(d);
      this.years = d;
    })
    // this.masterData.getBranchesForCompany(this.jsonBody.companyId).subscribe(d => {
    //   this.branches = d;
    //   console.log(d);
    // })
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });
    this.masterData.getPortsFull().subscribe(data => {
      this.ports = data;
    })
    this.masterData.getFreightTypesFull().subscribe(data => {
      this.freightTypes = data;
    })


    this.branches.push({
      id: 0, name: 'Search Branch'
    })

    if (!isNew) {
      this.jsonBody.id = data.id ;
      this.jsonBody.branchId = data.idBran ;
      this.jsonBody.freightKg = data.freightKg ;
      this.jsonBody.isAirFreight = data.isAirFreight;
      this.jsonBody.portFrom = data.portFrom;
      this.jsonBody.portFromText = data.portFromText;


      this.jsonBody.year = data.year ;
      this.jsonBody.month = data.idMon ;
      // this.masterData.getBranchesForCompany(this.jsonBody.companyId).subscribe(d => this.branches = d)
      this.masterData.getYearsForCompany(this.jsonBody.companyId).subscribe(d => {
        console.log(d);
        this.years = d;
      })

      this.branches.push({
        id: data.idBran, name: data.branch
      })
      this.companies.push({
        id: data.idCom, name: data.company
      })

      console.log(this.jsonBody)

    }

  }

  onSearchBranch(value: any) {
    if (value === '') {
      return;
    }
    let queryReq = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        name: { value: value , type: 4, col: 3},
        companyId: { value: this.jsonBody.companyId, type: 1, col: 1},
      },
    }
    // if (this.selectedComId != 0) {
    //   queryReq.FILTER_MODEL[ 'companyId'] = { value: this.selectedComId, type: 1, col: 1}
    // }
    this.branches = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      queryReq
    ).then(data => {
      console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.branches = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.branches.unshift({id: val.id, name: val.name});
            }
          });
          if (this.branches.length === 0){
            this.branches.push({id: 0, name: 'No Matches Found'})
          }
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  private validateEntry(onEdit: boolean): boolean {
    console.log(this.jsonBody)
    // if (this.jsonBody.BRANCH_ID === undefined || this.jsonBody.BRANCH_ID === '' || this.jsonBody.BRANCH_ID === 0 ){
    //   return false;
    // }
    if (onEdit && (this.jsonBody.companyId === undefined || this.jsonBody.companyId === '') ){
      return false;
    }

    if (this.jsonBody.freightKg === undefined || this.jsonBody.freightKg === '' ){
      return false;
    }
    // if (this.jsonBody.amountRefilled === undefined || this.jsonBody.amountRefilled === '' ){
    //   return false;
    // }
    if (this.jsonBody.portFrom === undefined || this.jsonBody.portFrom === '' ){
      return false;
    }
    // if (this.jsonBody.units === undefined || this.jsonBody.units === '' ){
    //   return false;
    // }


    if (this.jsonBody.year === undefined || this.jsonBody.year == "") {
      //show snack bar
      return false;
    }
    if (this.jsonBody.month=== undefined|| this.jsonBody.month < 0) {
      console.log(this.jsonBody.month)
      //show snack bar
      return false;
    }
    if (this.jsonBody.isAirFreight === undefined || this.jsonBody.isAirFreight === '') {
      return false;
    }
    if (onEdit && (this.jsonBody.id === undefined || this.jsonBody.id <=0)) {
      //show snack bar
      return false;
    }
    return true;
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close(this.isSaved);

  }

  onChangeCompany(value: any) {
    this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)
    this.masterData.getYearsForCompany(value).subscribe(d => {
      console.log(d);
      this.years = d;
    })
  }

  public onClickSave() {
    // // update
    if (!this.popupData.isNew) {
      if (this.validateEntry( true)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageSeaAirFreightEntry,
          { DATA: this.jsonBody }
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })

          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }
    // new
    else {
      if (this.validateEntry( false)) {
        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
          RequestType.ManageSeaAirFreightEntry,
          { DATA: this.jsonBody}
        ).then( data => {
          if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
            this.isSaved = true;
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }

  }

  onChangeBranch($event: MatSelectChange) {
    this.jsonBody.branch = this.branches.filter(b => b.id === $event)[0].name;
  }

  onChangeMonth($event: MatSelectChange) {
    this.jsonBody.mon = this.months.filter(m => m.id === $event.value)[0].name;
  }

  onChangePort($event: MatSelectChange) {
    this.jsonBody.portFromText = this.ports.filter(m => m.id === $event.value)[0].name;
  }
  aports: Aport[] = [
    {value: 1, name: 'SHANGHAI (CHINA)'},
    {value: 2, name: 'JAKARTA (INDONESIA)'},
    {value: 3, name: 'HOCHIMINH (VIETNAM)'},
    {value: 4, name: 'SINGAPORE'},
    {value: 5, name: 'MUNDRA (INDIA)'},
    {value: 6, name: 'TUTICORIN (INDIA)'},
    {value: 7, name: 'KAOHSIUNG (TAIWAN)'},
    {value: 8, name: 'QINGDAO (CHINA)'},
    {value: 9, name: 'KARACHCHI (PAKISTHAN)'}
  ];



}
