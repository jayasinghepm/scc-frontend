import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientFreightWaterComponent } from './client-freight-water.component';

describe('ClientAirTravelComponent', () => {
  let component: ClientFreightWaterComponent;
  let fixture: ComponentFixture<ClientFreightWaterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientFreightWaterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientFreightWaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
