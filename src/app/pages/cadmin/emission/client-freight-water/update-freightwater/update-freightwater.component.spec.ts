import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFreightWaterComponent } from './update-freightwater.component';

describe('UpdateAirtravelComponent', () => {
  let component: UpdateFreightWaterComponent;
  let fixture: ComponentFixture<UpdateFreightWaterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFreightWaterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFreightWaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
