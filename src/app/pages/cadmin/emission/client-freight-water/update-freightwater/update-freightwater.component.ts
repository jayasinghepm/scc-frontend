import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSelectChange} from "@angular/material";
import {MassterDataService} from "../../../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {BackendService} from "../../../../../@core/rest/bo_service";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import {MatCheckboxChange} from "@angular/material/typings/esm5/checkbox";
import { UserState } from 'src/app/@core/auth/UserState';

@Component({
  selector: 'app-update-freightwater',
  templateUrl: './update-freightwater.component.html',
  styleUrls: ['./update-freightwater.component.scss']
})
export class UpdateFreightWaterComponent implements OnInit {

  private isSaved = false;
  public twoWay = false;

  public body = {
    entryId: undefined, 
    VEHICLE_NUMBER: '',
    branchId: 0,
    companyId: 0,
    month: '',
    year: '',
    ownerShip: '',

    method: '',
    freightMode: '',
    cargoType: '',

    activity: '',
    type: '',
    size: '',

    deptCountry: '-',
    destCountry: '-',
    deptPort: '-',
    destPort: '-',
    t1Port: '-',
    t2Port: '-',
    t3Port: '-',
    
    isTwoWay: false,
    totalDistance: "",
    distanceUnit: 'km',
    weight: 0,

    vessel: '',
    fuelType: '',
    stroke: '-',
    fuelConsumption: '',
    fuelDensity: '21',
    nvc: '21',
    isNotAvailable: false,
    fuelEconomy: 0,
    isFuelPaidByCompany: false,
    fuelUnit: '',
    vehicleModel:''

  }


  public jsonBody = {
    entryId: -1,
    branchId: undefined,
    companyId: undefined, // todo:
    EMP_ID: undefined, // todo
    year: undefined,
    month: undefined,

    DEP_AIRPORT: undefined,
    DEP_COUNTRY: undefined,
    TRANSIST_1: undefined,
    TRANSIST_2: undefined,
    TRANSIST_3: undefined,
    AIR_TRAVEL_WAY: undefined,
    DEST_AIRPORT: undefined,
    DEST_COUNTRY: undefined,
    ACTIVITY:undefined,
    TYPE:undefined,
    SIZE:undefined,

    company: "-",
    branch: "-",
    mon: "-",
    deptCountry:  "-",
    destCountry:  "-",
    destPort:  "-",
    deptPort:  "-",
    t1Port:  "-",
    t2Port:  "-",
    t3Port:  "-",


   

    ownerShip: "-",
    weight: "-",
    totalDistance: "-",
    distanceUnit: "-",
    vehicleNumber: "-",
    method: "-",
    freightMode: undefined,   
    activity:  "-",
    type: "-",
    size: "-",
    isTwoWay: false,

    fuelConsumption: "-",
    fuelType: "-",
    vessel: "-",
    fuelUnit:  "-"


  
  }

  public countries_dp = [];
  public countries_ds = [];

  public airports = [];
  public airports_dp = [];
  public airports_ds = [];
  public airportst1 = [];
  public airportst2 = [];
  public airportst3 = [];
  
  public freightAirports = [];
  public stations = [];
  public backupstations = [];
  public seeports = [];
  public seeports_dp = [];
  public seeports_ds = [];
  public seeports1 = [];
  public seeports2 = [];
  public seeports3 = [];


  public backupseaports = [];


  public months = [];
  public years = [];
  public branches = [];
  public companies = [];
  public seatClasses = [];
  public freightTypes = [];
  public owenership = [];
  public method = [];
  public cargoTypes = [];
  public activities = [];
  public types = [];
  public sizes = [];
  public fuels = [];
  public fuelsNotWater = [];
  public units = [];
  public vehicleModels = [];
  public strokes = []



  public countriesBackup = [];
  private portsAdded = [];
  private countyAdded =  [];

  public isFuelPaidByCompany;
  public isNotAvaliable;

  constructor(
                public dialogRef: MatDialogRef<UpdateFreightWaterComponent>,
                private masterData: MassterDataService,
                private toastSerivce: NbToastrService,
                private boService: BackendService,
               @Optional() @Inject(MAT_DIALOG_DATA) public popupData: any,
                ) {
    //  //console.log(this.popupData)
    this.init(this.popupData.data, this.popupData.isNew);
  }

  private init(data:any, isNew: boolean) {
    this.masterData.loadCompaninesForDataEnry().subscribe(d => {
      this.companies = d;
      if(UserState.getInstance().adminId == 3){
        this.companies = this.companies.slice(1,5);
      }
    })
    this.masterData.getYearsForCompany(this.jsonBody.companyId).subscribe(d => {
      //  //console.log(d);
      this.years = d;
    })
    this.cargoTypes = this.masterData.getCargoTypes();

    this.masterData.getFreightAirportsFull().subscribe(data => {
      // @ts-ignore
      this.airports.push(...data);
      this.airports_dp.push(...data);;
      this.airports_ds.push(...data);;
      this.airportst1.push(...data);
      this.airportst2.push(...data);
      this.airportst3.push(...data);
    });

    this.masterData.getStatinsFull().subscribe(data => {
      // @ts-ignore
      this.stations.push(...data);
      this.backupstations.push(...data);
    });

    this.masterData.getSeeportsFull().subscribe(data => {
      // @ts-ignore
      this.seeports1.push(...data);
      this.seeports2.push(...data);
      this.seeports3.push(...data);
      this.seeports_ds.push(...data);
      this.seeports_dp.push(...data);
      this.seeports.push(...data);

      this.backupseaports.push(...data);
    });

    this.loadCountries();

    this.masterData.getSeatClassesFull().subscribe(data => {
     this.seatClasses = data;
    });
    this.masterData.getMonthsFull().subscribe(data => {
      this.months = data;
    });

    this.masterData.getFreightTypesFull().subscribe(data => {
      this.freightTypes = data;
    });

    this.masterData.getVehicleCategoriesFull().subscribe(d => {
      this.owenership = d;
    })



 

    this.masterData.getMethod().subscribe(d => {
      this.method= d;
    });
    
    this.masterData.getStrokeFull().subscribe(d => {
      this.strokes= d;
    })
    this.loadActivities();
    this.masterData.getFuelTypesVehicleFullFreight().subscribe(d => {
      this.fuels = d;
    })
    this.masterData.getFuelTypesVehicleFull().subscribe(d => {
      this.fuelsNotWater = d;
    })

    this.masterData.getUnitDistance().subscribe(d => {
       //console.log("air----",this.body.freightMode)
      if(this.body.freightMode == "air"){
        this.units = [{ id: 1, name: 'km'}]
    
      }
      else {
        this.units = d;
      }
    })

   
    //----------------------------branch search ---------------------
    this.branches.push({
      id: 0, name: 'Search Branch'
    })
    //---------------------------------------------------------------
    if (!isNew) {
       //console.log(data)

      this.loadCountries();
      


      this.body.entryId = data.FREIGHT_TRANSPORT_ENTRY_ID;
      this.body.VEHICLE_NUMBER = data.VEHICLE_NUMBER;
      this.body.companyId=data.companyId;
      this.selectedComId = this.body.companyId;
      // this.onSearchBranch(" ");

      this.body.branchId=data.branchId;
      
      this.body.month = data.month;
      this.masterData.getYearsForCompany(this.body.companyId).subscribe(d => {
        this.years = d;
        this.body.year = data.year;
      })
      
      this.body.ownerShip = data.ownerShip;

      this.body.method = data.method;
      this.body.freightMode = data.freightMode;
      this.loadvehicleModels();

      this.body.cargoType = data.cargoType;

      this.loadActivities();      
      if(data.activity){
        const activity = this.activities.find(a =>a.name===data.activity);
        this.body.activity = activity.id;

        this.masterData.getType(activity.id).subscribe(d => {
          this.types = d;
        })

        const type = this.types.find(a =>a.name===data.type);
        this.body.type = type.id;
  
        this.masterData.getSize(type.id).subscribe(d => {
          this.sizes = d;
        })
        const size = this.sizes.find(a =>a.name===data.size);
        this.body.size = size.id;
      }
      

      this.body.deptCountry = data.deptCountry;
      this.body.destCountry = data.destCountry;

      this.onChangeMode(this.body.freightMode);
      this.onChangeMethod(this.body.method);
      this.onChangeDesCountry(this.body.destCountry);
      this.onChangeDepCountry(this.body.deptCountry);

      this.body.deptPort = data.deptPort;
      this.body.destPort = data.destPort;
      this.body.t1Port = data.t1Port;
      this.body.t2Port = data.t2Port;
      this.body.t3Port = data.t3Port;


      this.body.isTwoWay = false;
      this.body.totalDistance = data.totalDistance;
      this.body.distanceUnit = data.distanceUnit;
      this.body.weight = data.weight;


      this.body.vessel = data.vessel;
      this.body.fuelType = data.fuelType;
      this.body.stroke = data.stroke;
      this.body.fuelConsumption = data.fuelConsumption;
      this.body.fuelDensity =  '21';
      this.body.nvc = '21';
      this.body.isNotAvailable = data.isNotAvailable;
      this.body.fuelEconomy = data.fuelEconomy;
      this.body.isFuelPaidByCompany = data.isFuelPaidByCompany;
      this.body.fuelUnit = data.fuelUnit;
      this.body.vehicleModel = data.vehicleModel;


      this.jsonBody.DEP_AIRPORT = data.idDepPort ;
      this.jsonBody.DEP_COUNTRY = data.idDepCou ;
      this.jsonBody.DEST_AIRPORT = data.idDesPort ;
      this.jsonBody.DEST_COUNTRY = data.idDesCou ;
      this.jsonBody.TRANSIST_1 = data.idTran1 ;
      this.jsonBody.TRANSIST_2 = data.idTrans2 ;
      this.jsonBody.TRANSIST_3 = data.idTrans3 ;
      this.jsonBody.AIR_TRAVEL_WAY = data.isTwoWay === 'No'? 1: 2 ;

      
      // this.masterData.getYearsForCompany(this.body.companyId).subscribe(d => {
      //   //  //console.log(d);
      //   this.years = d;
      // })
      // //----------------branch search-------------------------
      // this.branches.push({
      //   id: data.idBran, name: data.branch
      // })
      // this.selectedComId =  this.body.companyId;

      // ----------------------------------------------------



      // this.portsAdded.push(...[
      //   { id: this.jsonBody.DEP_AIRPORT, name: this.jsonBody.deptPort},
      //   { id: this.jsonBody.DEST_AIRPORT, name: this.jsonBody.destPort},
      // ])


      // if (this.jsonBody.t1Port !== undefined) {
      //   this.portsAdded.push({ id: this.jsonBody.TRANSIST_1 , name: this.jsonBody.t1Port})
      // }
      // if (this.jsonBody.t2Port !== undefined) {
      //   this.portsAdded.push({ id: this.jsonBody.TRANSIST_2, name: this.jsonBody.t2Port})
      // }
      // if (this.jsonBody.t3Port !== undefined) {
      //   this.portsAdded.push({ id: this.jsonBody.TRANSIST_3, name: this.jsonBody.t3Port})
      // }
      // this.airports.push(...this.portsAdded);

      //  //console.log(this.jsonBody);
      //  //console.log(this.countries);
      //  //console.log(this.airports);
    }

  }

  private loadCountries(){
    this.masterData.getCountriesFull().subscribe(data => {
      // @ts-ignore
      this.countries_dp  = data;
      this.countries_ds  = data;

     this.countriesBackup = data;
    });
  }

  private loadActivities(){
    this.masterData.getActivity().subscribe(d => {
      this.activities= d;
    })
  }

  private validateEntry(onEdit: boolean): boolean {
     //console.log("---------------------------ddhnjhcbndjhcbndhfjbhj")
    // if(this.twoWay) { this.body.AIR_TRAVEL_WAY = 2} else { this.jsonBody.AIR_TRAVEL_WAY = 1;}

    if (this.body.freightMode === undefined) {
      //show snack bar
      return false;
    }

    if (this.body.method === undefined) {
      //show snack bar
      return false;
    }


    
    if(this.body.freightMode == 'water' && this.body.method != 'fuel_base' && this.body.totalDistance == undefined  ){
       //console.log('iiinside')

      if(this.body.deptPort == undefined || this.body.deptPort === '-'){
        return false;        
      }
      if(this.body.destPort == undefined || this.body.destPort === '-'){
        return false;        
      }
      if(this.body.totalDistance == undefined || this.body.totalDistance  === '-'){
        return false;        
      }
      if(this.body.distanceUnit == undefined || this.body.distanceUnit  === '-'){
        return false;        
      }
      if(this.body.activity == undefined || this.body.activity  === '-'){
        return false;        
      }
      if(this.body.type == undefined || this.body.type  === '-'){
        return false;        
      }
      if(this.body.size == undefined || this.body.size  === '-'){
        return false;        
      }
    }

    if(this.body.freightMode == 'water' && this.body.method != 'fuel_base'){
       //console.log('jjjjj-----',this.body.activity)


      if(this.body.totalDistance == undefined || this.body.totalDistance  === '-'){
        return false;        
      }
      if(this.body.distanceUnit == undefined || this.body.distanceUnit  === '-'){
        return false;        
      }
      if(this.body.activity == undefined || this.body.activity  === ""){
         //console.log('jjjjjwww')

        
        return false;        
      }
      if(this.body.type == undefined || this.body.type  === ''){
         //console.log('weee')

        return false;        
      }
      if(this.body.size == undefined || this.body.size  === ''){
         //console.log('weees4')

        return false;        
      }

    }
  

    
    if (this.body.method === 'fuel_base') {
      if(this.body.fuelConsumption == '' && this.body.fuelEconomy == 0){
        return false;        
      }
      if(this.body.isNotAvailable && this.body.totalDistance == ""){
        return false;
      }
      if(this.body.isNotAvailable && this.body.fuelEconomy == 0){
        return false;
      }
      if(!this.body.isNotAvailable && this.body.fuelUnit == ''){
        return false;        
      }
      if(this.body.fuelType == ''){
        return false;        
      }
      if(this.body.fuelType == 'Petrol' && this.body.stroke === '-'){
        return false;        
      }
    }

    if (this.body.year === undefined || this.jsonBody.year == "") {
      //show snack bar
      return false;
    }
    if (this.body.month === undefined || this.jsonBody.month < 0 ){
      //show snack bar
      return false;
    }
    if (onEdit && (this.body.companyId === undefined || this.body.companyId <= 0) ){
      return false;
    }
    return true;

  }


  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close(this.isSaved);

  }

  public selectedComId = 0;
  onChangeCompany(value: any) {
    this.selectedComId = value;
    this.branches = [];
    this.branches.push({
      id: 0, name: 'Search Branch'
    })
    this.body.companyId = this.companies.filter(d => {return d.id === value})[0].id;
    // this.masterData.getBranchesForCompany(value).subscribe(d => this.branches = d)

    this.masterData.getYearsForCompany(value).subscribe(d => {
      this.years = d;
    })
  }

  onChangeActivity(value: any) {
      //console.log("val---",value);
    // this.body.activity = this.activities.filter(d => d.id === value)[0].name;
    this.masterData.getType(value).subscribe(d => {
      this.types = d;

    })
  }

  

 onChangeMethod(value: any){
  this.body.isNotAvailable =false;
  //  //console.log(value, this.body.freightMode)
  if(value === 'fuel_base' && this.body.freightMode == 'rail'){
    this.fuelsNotWater = [
      {id: 3, name: 'Coal', code: 'Coal'},
      {id: 2, name: 'Diesel', code: 'Diesel' },
    ]
  }
 }


 loadvehicleModels(){
  if(this.body.freightMode == 'off-road'){

    this.masterData.getOffRoadVehicleFull().subscribe(d => {
      this.vehicleModels = d;
    })

  }else if(this.body.freightMode == 'road'){

    this.masterData.getRoadVehicleFull().subscribe(d => {
      this.vehicleModels = d;
    })

  }
 }

 onChangeMode(value: any) {
   //console.log("selectmode---",value);
  this.body.isNotAvailable =false;


  if(value === 'air'){
    this.units =  this.units = [{ id: 1, name: 'km'}]
    this.method = [ {id: 2, name: 'distance_base'}]    
  }else if(value === 'off-road'){
    this.units =  this.units = [{ id: 1, name: 'km'},{ id: 2, name: 'Nm'}]
    this.method = [ {id: 1, name: 'fuel_base'}] 
  }else if(value === 'rail'){
    this.units =  this.units = [{ id: 1, name: 'km'},{ id: 2, name: 'Nm'}]

    this.method = [ {id: 1, name: 'fuel_base'}] 
    this.fuels = [
      {id: 3, name: 'Coal', code: 'Coal'},
      {id: 2, name: 'Diesel', code: 'Diesel' },
    ]
  }
  else{
    this.units =  this.units = [{ id: 1, name: 'km'},{ id: 2, name: 'Nm'}]


    this.masterData.getMethod().subscribe(d => {
      this.method= d;
     
    });

  }

  this.loadvehicleModels();



}





  onChangeType(value: any) {
     //console.log("val---",value);
  //  this.body.type = this.types.filter(d => d.id === value)[0].name;
   this.masterData.getSize(value).subscribe(d => {
     this.sizes = d;

   })
 }

  public onClickSave(){

   
     //console.log("sendingBody--",this.body)

    if(this.body.activity !== ''){
      this.body.activity = this.activities.find(a =>a.id===this.body.activity).name
    }
    if(this.body.type !== ''){
      this.body.type = this.types.find(a =>a.id===this.body.type).name
    }
    if(this.body.size !== ''){
      this.body.size = this.sizes.find(a =>a.id===this.body.size).name
    }

    if(this.body.totalDistance === ""){
      this.body.totalDistance = "-1";
    }

     //console.log("--",this.popupData.isNew)

    // update
    if (!this.popupData.isNew) {

        if (this.validateEntry( true)) {
          this.body.totalDistance === undefined? this.body.totalDistance =" -1": "";

            this.boService.sendRequestToBackend(
              RequestGroup.Emission,
               RequestType.ManageFrieghtTransport,
              { DATA: {...this.body, FREIGHT_TRANSPORT_ENTRY_ID: this.body.entryId}}
               ).then( data => {
                if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
                  this.isSaved = true;
                  this.toastSerivce.show('', 'Saved data successfully.', {
                    status: 'success',
                    destroyByClick: true,
                    duration: 2000,
                    hasIcon: false,
                    position: NbGlobalPhysicalPosition.TOP_RIGHT,
                    preventDuplicates: true,
                  })
                } else {
                  this.toastSerivce.show('', 'Error in saving data.', {
                    status: 'danger',
                    destroyByClick: true,
                    duration: 2000,
                    hasIcon: false,
                    position: NbGlobalPhysicalPosition.TOP_RIGHT,
                    preventDuplicates: true,
                  })
                }
               })
        } else {
          this.toastSerivce.show('', 'Fill empty fields', {
            status: 'warning',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
    }
    // new
    else {
       //console.log("add neww")
      if (this.validateEntry( false)) {
         //console.log("sending Jsn after validate--",this.jsonBody)
        delete this.body.entryId;
        this.body.totalDistance === undefined? this.body.totalDistance = '-1': "";

        this.boService.sendRequestToBackend(
          RequestGroup.Emission,
           RequestType.ManageFrieghtTransport,
          { DATA: this.body}
           ).then( data => {
            if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
              this.isSaved = true;
              this.toastSerivce.show('', 'Saved data successfully.', {
                status: 'success',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            } else {
              this.toastSerivce.show('', 'Error in saving data.', {
                status: 'danger',
                destroyByClick: true,
                duration: 2000,
                hasIcon: false,
                position: NbGlobalPhysicalPosition.TOP_RIGHT,
                preventDuplicates: true,
              })
            }
           })
      } else {
        this.toastSerivce.show('', 'Fill empty fields', {
          status: 'warning',
          destroyByClick: true,
          duration: 2000,
          hasIcon: false,
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
          preventDuplicates: true,
        })
      }
    }
  }

  onChangeDepCountry(value: any) {
    this.countries_dp = this.countriesBackup;
  
    this.airports_dp = this.airports.filter(a => a.country.toLowerCase() === value.toLowerCase())
    this.seeports_dp = this.seeports.filter(a => a.country.toLowerCase() === value.toLowerCase())
  }
  
  onChangeDesCountry(value: any) {  
    this.countries_ds = this.countriesBackup;

    this.airports_ds = this.airports.filter(a => a.country.toLowerCase() === value.toLowerCase())
    this.seeports_ds = this.seeports.filter(a => a.country.toLowerCase() === value.toLowerCase())
  }



  onSearchSeaport_dp(value:any){
    this.seeports_dp = this.seeports.filter(a => {
      const cond1 = a.name.toLowerCase().startsWith(value.toLowerCase());
      const cond2 = this.body.deptCountry ? this.body.deptCountry.toLowerCase() === a.country.toLowerCase() : true;
      return cond1 && cond2;
    })
  }

  onSearchSeaport_ds(value:any){
    this.seeports_ds = this.seeports.filter(a => {
      const cond1 = a.name.toLowerCase().startsWith(value.toLowerCase());
      const cond2 = this.body.destCountry ? this.body.destCountry.toLowerCase() === a.country.toLowerCase() : true;
      return cond1 && cond2;
    })
  }

  onSearchSeaport1(value:any){
    this.seeports1 = this.seeports.filter(a => a.name.toLowerCase().startsWith(value.toLowerCase()))
  }

  onSearchSeaport2(value:any){
    this.seeports2 = this.seeports.filter(a => a.name.toLowerCase().startsWith(value.toLowerCase()))
  }

  onSearchSeaport3(value:any){
    this.seeports3 = this.seeports.filter(a => a.name.toLowerCase().startsWith(value.toLowerCase()))
  }



  onSearchAirport_ds(value: any) {
    this.airports_ds = this.airports.filter(a => {
      const cond1 = a.name.toLowerCase().startsWith(value.toLowerCase());
      const cond2 = this.body.destCountry ? this.body.destCountry.toLowerCase() === a.country.toLowerCase() : true;
      return cond1 && cond2;
    })
  }

  onSearchAirport_dp(value: any) {
    this.airports_dp = this.airports.filter(a => {
      const cond1 = a.name.toLowerCase().startsWith(value.toLowerCase());
      const cond2 = this.body.deptCountry ? this.body.deptCountry.toLowerCase() === a.country.toLowerCase() : true;
      return cond1 && cond2;
    })
  }

  onSearchAirport_t1(value: any) {
    this.airportst1 = this.airports.filter(a => a.name.toLowerCase().startsWith(value.toLowerCase()))
  }

  onSearchAirport_t2(value: any) {
    this.airportst2 = this.airports.filter(a => a.name.toLowerCase().startsWith(value.toLowerCase()))
  }

  onSearchAirport_t3(value: any) {
    this.airportst3 = this.airports.filter(a => a.name.toLowerCase().startsWith(value.toLowerCase()))
  }
  


  onSearchBranch(value: any) {
    if (value === '') {
      return;
    }
    let queryReq = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        name: { value: value , type: 4, col: 3},

      },
    }
    if (this.selectedComId != 0) {
      queryReq.FILTER_MODEL[ 'companyId'] = { value: this.selectedComId, type: 1, col: 1}
    }
    this.branches = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      queryReq
    ).then(data => {
      //  //console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.branches = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.branches.unshift({id: val.id, name: val.name});
            }
          });
          if (this.branches.length === 0){
            this.branches.push({id: 0, name: 'No Matches Found'})
          }
        } else {
          //  todo: show snack bar
          //  //console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        //  //console.log('error data 2')
      }
    })
  }

  onChangeBranch($event: MatSelectChange) {
    this.jsonBody.branch = this.branches.filter(b => b.id === $event)[0].name;

  }

  onChangeMonth($event: MatSelectChange) {
    this.jsonBody.mon = this.months.filter(m => m.id === $event.value)[0].name;
  }
  
  onChangePort($event: MatSelectChange, field: number) {}
  
  onSearchCountry_dp(value:any){
    this.countries_dp = this.countries_dp.filter(name => {
      if(value){      
        return name.name.toLowerCase().startsWith(value.toLowerCase())    
      }else{    
        return true;      
      }      
    })
  }
  
  onSearchCountry_ds(value:any){
    this.countries_ds = this.countries_ds.filter(name => {
      if(value){      
        return name.name.toLowerCase().startsWith(value.toLowerCase())    
      }else{    
        return true;      
      }      
    })
  }

  onChangeWay($event: MatCheckboxChange) {
    this.body.isTwoWay = $event.checked ? true:false;
  }

  onChangeFuelPaidByCompany($event: MatCheckboxChange) {
    this.body.isFuelPaidByCompany = $event.checked ? true:false;
  }
  onChangeNotAvaliable($event: MatCheckboxChange) {
    this.body.isNotAvailable = $event.checked ? true:false;
  }

  onCloseSearch($event: KeyboardEvent) {
   // this.countries = JSON.parse(JSON.stringify(this.countriesBackup));
  }

  onSearchPortClose($event: KeyboardEvent) {
  }

  onSearcCountryClose1($event: KeyboardEvent) {
   this.countries_dp = this.countriesBackup;
  }

  onSearcCountryClose($event: KeyboardEvent) {
    this.countries_ds = this.countriesBackup;
  }


  onChangeSize($event: MatSelectChange) {
    // this.body.size = this.sizes.filter(w => w.id === $event.value)[0].name;
  }
}