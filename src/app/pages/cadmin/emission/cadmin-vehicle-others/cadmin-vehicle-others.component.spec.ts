import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadminVehicleOthersComponent } from './cadmin-vehicle-others.component';

describe('CadminVehicleOthersComponent', () => {
  let component: CadminVehicleOthersComponent;
  let fixture: ComponentFixture<CadminVehicleOthersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadminVehicleOthersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadminVehicleOthersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
