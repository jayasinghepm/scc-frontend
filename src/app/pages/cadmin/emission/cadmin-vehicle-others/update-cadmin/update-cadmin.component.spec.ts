import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCadminComponent } from './update-cadmin.component';

describe('UpdateCadminComponent', () => {
  let component: UpdateCadminComponent;
  let fixture: ComponentFixture<UpdateCadminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCadminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCadminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
