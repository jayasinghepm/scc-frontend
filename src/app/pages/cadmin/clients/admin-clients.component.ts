import { Component, OnInit } from '@angular/core';
import {BackendService} from "../../../@core/rest/bo_service";
import {MassterDataService} from "../../../@core/service/masster-data.service";
import {NbToastrService, NbWindowService} from "@nebular/theme";
import {AddClientComponent} from "./add-client/add-client.component";
import {RemoveClientComponent} from "./remove-client/remove-client.component";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {MatDialog} from "@angular/material";
import {LocalDataSource} from "../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";

@Component({
  selector: 'app-admin-clients',
  templateUrl: './admin-clients.component.html',
  styleUrls: ['./admin-clients.component.scss']
})
export class AdminClientsComponent implements OnInit {
  settings = {
    mode: 'external',
    pager: {
      display: false,
    },
    actions: {
      delete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    editor: {
      type: 'completer',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
        filter: false,
        sort: false,

      },
      company: {
        title: 'Company',
        editable: false,
        addable: false,
        type: 'string',
        filter: false,
        sort: false,
      },
      //Todo: have to change
      branch: {
        title: 'Branch',
        type: 'string',
        filter: false,
        sort: false,
        editor: {
          type: 'completer',
          config: {
            completer: {

              data: [],
            }
          }
        }

      },
      name: {
        title: 'Name',
        type: 'string',
        filter: false,
        sort: false,
        // editor: {
        //   type: 'completer',
        //   config: {
        //     completer: {
        //       data: [],
        //     }
        //   }
        // }
      },
      email: {
        title: 'Email',
        type: 'string',
        filter: false,
        sort: false,
      },
      mobileNo: {
        title: 'Mobile No',
        type: 'string',
        filter: false,
        sort: false,

      },
      telephoneNo: {
        title: 'Telephone NO',
        type: 'string',
        filter: false,
        sort: false,

      },
      agreementStartDate: {
        title: 'Agreement Start',
        type: 'string',
        filter: false,
        sort: false,
      },
      agreementEndDate: {
        title: 'Agreement End',
        type: 'string',
        filter: false,
        sort: false,
      },
      sector: {
        title: 'Sector',
        type: 'string',
        filter: false,
        sort: false,
      },
      clientCode: {
        title: 'Code ',
        type: 'string',
        filter: false,
        sort: false,

      },

    }
  };
  mySetting = JSON.parse(JSON.stringify(this.settings))

  source: LocalDataSource = new LocalDataSource();


  constructor(
    private boService: BackendService,
    private masterData: MassterDataService,
    private  toastSerivce:  NbToastrService,
    private dialog: MatDialog,
    private windowService: NbWindowService,
  ) {
    this.loadData();
  }

  ngOnInit() {
  }

  public onUpdate($event: any, isEdit: boolean) {
    if (isEdit) {
      this.windowService.open(AddClientComponent, {title: 'Edit Client',  closeOnBackdropClick: true,})
    } else {
      this.windowService.open(AddClientComponent, {title: 'Add Client',  closeOnBackdropClick: true,})

    }
  }

  public onDelete($event: any) {
    this.windowService.open(RemoveClientComponent, {title: 'Remove Client',  closeOnBackdropClick: true,})
  }

  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.BusinessUsers,
      RequestType.ListClient,
      {
        PAGE_NUM: 1,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              list.push(this.fromListRequest(val));
            }
          });
          this.source.load(list);
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

   // todo : validation

  private fromListRequest(dto: any): any {
    let company;
    let branch;
    let id;
    let name;
    let email
    let mobileNo;
    let telephoneNo;
    let agreementStartDate;
    let agreementEndDate;
    let sector;
    let clientCode;


    this.masterData.getBranchName(dto.branchId).subscribe(data => { branch = data; }).unsubscribe();
    // this.masterData.getA
    this.masterData.getCompanyName(dto.companyId).subscribe(data => { company = data; }).unsubscribe();

    id = dto.id;
    name = dto.title + " " + dto.firstName + " " + dto.lastName;
    email = dto.email;
    mobileNo = dto.mobileNo;
    telephoneNo = dto.telephoneNo;
    sector = dto.sector;
    clientCode = dto.clientCode;
    agreementStartDate = dto.agreementStartDate;
    agreementEndDate = dto.agreementEndDate




    return {
      id,
      company,
      branch,
      name,
      email,
      mobileNo,
      telephoneNo,
      sector,
      clientCode,
      agreementStartDate,
      agreementEndDate,
    };



  }

  private fromTable(data: any, onEdit: boolean): any {
    let company;
    let branch;
    let id;
    let name;
    let email
    let mobileNo;
    let telephoneNo;
    let agreementStartDate;
    let agreementEndDate;
    let sector;
    let clientCode;




    // Todo: branch
    this.masterData.getBranchId(data.branch).subscribe(data => { branch = data;});
    return {
      // todo:
      // DATA: {
      //   id: onEdit ? data.id : -1,
      //   branchId: branch,
      //   companyId: 1, // todo:
      //   firstName: 1, // todo
      //   lastName: data.year,
      //   title: month,
      //   email: depPort,
      //   telephoneNo: depCountry,
      //   mobileNo: transit1,
      //   sector: transit2,
      //   agreementStartDate: transit3,
      //   agreementEndDate: travelWay,
      //   clientCode: seatClass ,
      //   userId: destPort,
      //   entitlements:
      // }
    }

  }
}
