import { Component, OnInit } from '@angular/core';
import {LocalDataSource} from "../../../ng2-smart-table/src/lib/lib/data-source/local/local.data-source";
import {BackendService} from "../../../@core/rest/bo_service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";

@Component({
  selector: 'app-waste-dis-factors',
  templateUrl: './waste-dis-factors.component.html',
  styleUrls: ['./waste-dis-factors.component.scss']
})
export class WasteDisFactorsComponent implements OnInit {

  settings = {
    mode: 'inline',
    pager: {
      display: false,
    },
    actions: {
      delete: false,
      add: false,
      edit: false,
    },
    // add: {
    //   // addButtonContent: '<i class="nb-plus"></i>',
    //   createButtonContent: '<i class="nb-checkmark"></i>',
    //   cancelButtonContent: '<i class="nb-close"></i>',
    //   confirmCreate: true,
    // },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      wasteName: {
        title: 'Waste Name',
        type: 'string',
        editable: false,
        addable: false,
        filter: false,
        sort: false,
      },
      reuse: {
        title: 'Re-use',
        type: 'number',
        filter: false,
        sort: false,
      },
      //Todo: have to change
      open_loop: {
        title: 'Open Loop',
        type: 'number',
        filter: false,
        sort: false,

      },
      closed_loop: {
        title: 'Closed Loop',
        type: 'number',
        filter: false,
        sort: false,
      },
      combustion: {
        title: 'Combustion',
        type: 'number',
        filter: false,
        sort: false,
      },
      landfill: {
        title: 'Landfill',
        type: 'number',
        filter: false,
        sort: false,
      },
      anaerobic_digestion: {
        title: 'Anaerobic Digestion',
        type: 'number',
        filter: false,
        sort: false,
      },
    }
  };

  public source: LocalDataSource = new LocalDataSource();

  constructor(private boService: BackendService, private toastSerivce: NbToastrService) {
    this.loadData()
  }

  ngOnInit() {
  }

  onUpdate($event: any) {
    console.log($event)
    let jsonBody = {
      wasteType: $event.newData.wasteType,
      wasteName: $event.newData.wasteName,
      reuse: $event.newData.reuse,
      open_loop: $event.newData.open_loop,
      combustion: $event.newData.combustion,
      composting: $event.newData.composting,
      landfill: $event.newData.landfill,
      anaerobic_digestion: $event.newData.anaerobic_digestion,
    }


    if ((jsonBody.wasteType === undefined || jsonBody.wasteType === '' || jsonBody.wasteType < 1) ||
      (jsonBody.wasteName === undefined || jsonBody.wasteName === '')
      || (jsonBody.reuse === undefined || jsonBody.reuse=== '' || jsonBody.reuse < 0)
      || (jsonBody.open_loop === undefined || jsonBody.open_loop === '' || jsonBody.open_loop < 0)
      || (jsonBody.combustion === undefined || jsonBody.combustion === '' || jsonBody.combustion < 0)
      || (jsonBody.composting === undefined || jsonBody.composting === '' || jsonBody.composting < 0)
      || (jsonBody.landfill === undefined || jsonBody.landfill === '' || jsonBody.landfill < 0)
      || (jsonBody.anaerobic_digestion === undefined || jsonBody.anaerobic_digestion === '' || jsonBody.anaerobic_digestion < 0)

    ) {
      this.toastSerivce.show('', 'Fill empty fields', {
        status: 'warning',
        destroyByClick: true,
        duration: 2000,
        hasIcon: false,
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
        preventDuplicates: true,
      })
      return;
    }
    this.boService.sendRequestToBackend(
      RequestGroup.EmissionFactors,
      RequestType.ManageWasteDisFactors,
      { DATA: jsonBody}
    ).then(data => {

        if (data !== undefined && data.DAT !== undefined &&  data.DAT.DTO !== undefined) {
          this.toastSerivce.show('', 'Saved data successfully.', {
            status: 'success',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
          $event.confirm.resolve($event.newData);
          this.loadData();
        } else {
          this.toastSerivce.show('', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      }

    )
  }

  private loadData() {
    this.boService.sendRequestToBackend(
      RequestGroup.EmissionFactors,
      RequestType.ListWastDisFactors,
      {
        PAGE_NUMBER: -1,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          let list = [];
          console.log(data.DAT.LIST)
          data.DAT.LIST.forEach(dto => {
            list.push(this.fromListRequest(dto));
          });
          this.source.load(list);
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  private fromListRequest(dto: any) :any {
    return {
      wasteType: dto.wasteType,
      wasteName: dto.wasteName,
      reuse: dto.reuse,
      open_loop: dto.open_loop,
      closed_loop: dto.closed_loop,
      combustion: dto.combustion,
      composting: dto.composting,
      landfill: dto.landfill,
      anaerobic_digestion: dto.anaerobic_digestion,
    }
  }

}
