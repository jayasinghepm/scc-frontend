import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadminHomeComponent } from './cadmin-home.component';

describe('CadminHomeComponent', () => {
  let component: CadminHomeComponent;
  let fixture: ComponentFixture<CadminHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadminHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadminHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
