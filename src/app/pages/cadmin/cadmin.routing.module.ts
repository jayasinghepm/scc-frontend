import {Router, RouterModule, Routes} from "@angular/router";
import {ModuleWithProviders, NgModule} from "@angular/core";
import {CadminHomeComponent} from "./cadmin-home/cadmin-home.component";
import {CadminBranchesComponent} from "./cadmin-branches/cadmin-branches.component";
import {CadminGeneralInfoComponent} from "./cadmin-general-information/cadmin-general-info-component";
import {CadminReportsComponent} from "./cadmin-reports/cadmin-reports.component";
import {ClientAirTravelComponent} from "./emission/client-air-travel/client-air-travel.component";
import {ClientFireExtComponent} from "./emission/client-fire-ext/client-fire-ext.component";
import {ClientRefriComponent} from "./emission/client-refri/client-refri.component";
import {ClientGeneratorsComponent} from "./emission/client-generators/client-generators.component";
import {ClientElectricityComponent} from "./emission/client-electricity/client-electricity.component";
import {ClientWasteDisposalComponent} from "./emission/client-waste-disposal/client-waste-disposal.component";
import {ClientMunicipalWaterComponent} from "./emission/client-municipal-water/client-municipal-water.component";
import {ClientEmpCommuComponent} from "./emission/client-emp-commu/client-emp-commu.component";
import {ClientTransportComponent} from "./emission/client-transport/client-transport.component";
import {ClientWasteTransportComponent} from "./emission/client-waste-transport/client-waste-transport.component";
import {ClientTransLocPurComponent} from "./emission/client-trans-loc-pur/client-trans-loc-pur.component";
import {ClinetEmployeeComponent} from "./employees/clinet-employee.component";
import {ClientSummaryDataInputsComponent} from "./client-summary-data-inputs/client-summary-data-inputs.component";
import {CadminPageGuard} from "./CadminPageGuard";
import {AdminRouteGuard} from "../routing/guards/admin-route.guard";
import {CadminRouteGuard} from "../routing/guards/cadmin-route.guard";
import {ClientRouteGuard} from "../routing/guards/client-route.guard";
import {DataEntryUserComponent} from "./data-entry-user/data-entry-user.component";
import {CadminUserAccessControlComponent} from "./user-access-control/user-access-control.component";
import {BiogasComponent} from "../admin/emission/biogas/biogas.component";
import {LpgasComponent} from "./emission/lpgas/lpgas.component";
import {BgasComponent} from "./emission/bgas/bgas.component";


import {BiomassComponent} from "./emission/biomass/biomass.component";
import {AdminAshTransportationComponent} from "../admin/emission/admin-ash-transportation/admin-ash-transportation.component";
import {AdminForkliftsComponent} from "../admin/emission/admin-forklifts/admin-forklifts.component";
import {AdminFurnaceOilComponent} from "../admin/emission/admin-furnace-oil/admin-furnace-oil.component";
import {CadminAshTransComponent} from "./emission/cadmin-ash-trans/cadmin-ash-trans.component";
import {CadminForkliftsComponent} from "./emission/cadmin-forklifts/cadmin-forklifts.component";
import {CadminFurnaceOilComponent} from "./emission/cadmin-furnace-oil/cadmin-furnace-oil.component";
import {CadminOilgasTransComponent} from "./emission/cadmin-oilgas-trans/cadmin-oilgas-trans.component";
import {CadminRawMaterialTransComponent} from "./emission/cadmin-raw-material-trans/cadmin-raw-material-trans.component";
import {CadminSawDustTransComponent} from "./emission/cadmin-saw-dust-trans/cadmin-saw-dust-trans.component";
import {CadminSludgeTransComponent} from "./emission/cadmin-sludge-trans/cadmin-sludge-trans.component";
import {CadminStaffTransComponent} from "./emission/cadmin-staff-trans/cadmin-staff-trans.component";
import {CadminLorryTransComponent} from "./emission/cadmin-lorry-trans/cadmin-lorry-trans.component";
import {CadminVehicleOthersComponent} from "./emission/cadmin-vehicle-others/cadmin-vehicle-others.component";
import {CadminPaperwasteComponent} from "./emission/cadmin-paperwaste/cadmin-paperwaste.component";
import {CadminPainManageVehicleComponent} from "./emission/cadmin-pain-manage-vehicle/cadmin-pain-manage-vehicle.component";
import {EmissionFactorsComponent} from "./emission-factors/emission-factors.component";
import {PubTransFactorsComponent} from "./pub-trans-factors/pub-trans-factors.component";
import {WasteDisFactorsComponent} from "./waste-dis-factors/waste-dis-factors.component";
import {CadminProjectSummaryComponent} from "./cadmin-project-summary/cadmin-project-summary.component";
import {SolarComponentComponent} from "./solar-component/solar-component.component";
import { ClientWeldingComponent } from "../admin/emission/client-welding/client-welding.component";
import { ClientFreightWaterComponent } from "./emission/client-freight-water/client-freight-water.component";
import { WasteWaterTreatmentComponent } from "../admin/emission/waste-water-treatment/waste-water-treatment.component";


const routes: Routes = [
  {
    path: 'home',
    component: CadminHomeComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'branches',
    component: CadminBranchesComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'info',
    component: CadminGeneralInfoComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'reports',
    component: CadminReportsComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'summary_data_inputs',
    component: ClientSummaryDataInputsComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'air_travel',
    component: ClientAirTravelComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'fire_ext',
    component: ClientFireExtComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'refri',
    component: ClientRefriComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'generators',
    component: ClientGeneratorsComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'electricity',
    component: ClientElectricityComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'welding',
    component: ClientWeldingComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'waste_water_treatment',
    component: WasteWaterTreatmentComponent,
  },
  {
    path: 'waste_disposal',
    component: ClientWasteDisposalComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'municipal_water',
    component: ClientMunicipalWaterComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'emp_comm',
    component: ClientEmpCommuComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'transport',
    component: ClientTransportComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'waste_transport',
    component: ClientWasteTransportComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'trans_loc_purch',
    component: ClientTransLocPurComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'frieght_water',
    component:ClientFreightWaterComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'employees',
    component: ClinetEmployeeComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'data_entry_users',
    component: DataEntryUserComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'access',
    component: CadminUserAccessControlComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'biomass',
    component: BiomassComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'lpgas',
    component: LpgasComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'bgas',
    component: BgasComponent,
    canActivate: [CadminPageGuard]
  },
  {
    path: 'ash_transport',
    component: CadminAshTransComponent,

  },
  {
    path: 'forklifts',
    component: CadminForkliftsComponent,

  }
  ,
  {
    path: 'furnace-oil',
    component: CadminFurnaceOilComponent,

  }
  ,{
    path: 'lorry-transport',
    component: CadminLorryTransComponent,

  }
  ,{
    path: 'oilgas-transport',
    component: CadminOilgasTransComponent,

  },
  {
    path: 'paper-waste',
    component: CadminPaperwasteComponent,

  },
  {
    path: 'paid-manager-vehicle',
    component: CadminPainManageVehicleComponent,

  }
  ,{
    path: 'raw-material-transport',
    component: CadminRawMaterialTransComponent,

  },
  {
    path: 'saw-dust-transport',
    component: CadminSawDustTransComponent,

  },
  {
    path: 'sludge-transport',
    component: CadminSludgeTransComponent,

  },
  {
    path: 'staff-transport',
    component: CadminStaffTransComponent,

  },
  {
    path: 'vehicle_others',
    component: CadminVehicleOthersComponent,
  },
  {
    path: 'factors',
    component: EmissionFactorsComponent,

  },
  {
    path: 'pub_factors',
    component: PubTransFactorsComponent,

  },
  {
    path: 'waste_factors',
    component: WasteDisFactorsComponent,
  },
  {
    path: 'project_summary',
    component: CadminProjectSummaryComponent,
  },
  {
    path: 'solar',
    component: SolarComponentComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadminRoutingModule{
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CadminRoutingModule,
      providers: [
       CadminPageGuard
      ]
    };
  }
}
