import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {UserState} from "../../@core/auth/UserState";
import {CadminStatus} from "../../@core/enums/CadminStatus";

@Injectable()
export class CadminPageGuard implements CanActivate {
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (UserState.getInstance().isAuthenticated) {

      if (UserState.getInstance().existActiveProject) {
        if (UserState.getInstance().cadminStatus !== CadminStatus.Approved && state.url !== '/pages/com_admin/info') {
          this.router.navigate(['/pages/com_admin/info'], { queryParams: { returnUrl: state.url }});
          return false;
        }
      }else {

      }

      return true;
    }

    this.router.navigate(['home'], { queryParams: { returnUrl: state.url }}); // ##login
    return false;
    // return true;
  }
  constructor(private router: Router) {}
}
