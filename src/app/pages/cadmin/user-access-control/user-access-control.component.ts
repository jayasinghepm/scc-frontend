import {Component, OnInit} from '@angular/core';
import {BackendService} from "../../../@core/rest/bo_service";
import {MassterDataService} from "../../../@core/service/masster-data.service";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";
import {MatCheckboxChange, MatDialog, MatSelectChange} from "@angular/material";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {UserState} from "../../../@core/auth/UserState";
import {DeleteConfirmComponent} from "../../delete-confirm/delete-confirm.component";

@Component({
  selector: 'app-user-access-control',
  templateUrl: './user-access-control.component.html',
  styleUrls: ['./user-access-control.component.scss']
})
export class CadminUserAccessControlComponent implements OnInit {


  public isDEO = true;

  public users : {id: any, name: any, entitlements: any, userId: number, status: number, lastLoginDate: any, type: any} [] = [];
  private entilements = [
    "1-0-0-0",
    "2-0-0-0",
    "3-0-0-0",
    "4-0-0-0",
    "5-0-0-0",
    "6-0-0-0",
    "7-0-0-0",
    "8-0-0-0",
    "9-0-0-0",
    "10-0-0-0",
    "11-0-0-0",
    "12-0-0-0",
    "13-0-0-0",
    "14-0-0-0",
    "15-0-0-0",
    "16-0-0-0",
    "17-0-0-0",
    "18-0-0-0",
    "19-0-0-0",
    "20-0-0-0",
    "21-0-0-0",
    "22-0-0-0",
    "23-0-0-0",
    "24-0-0-0",
    "25-0-0-0",
    "26-0-0-0",
    "27-0-0-0",

  ];
  public userAccess = {
    1: {add: false, edit: false, delete: false},
    2: {add: false, edit: false, delete: false},
    3: {add: false, edit: false, delete: false},
    4: {add: false, edit: false, delete: false},
    5: {add: false, edit: false, delete: false},
    6: {add: false, edit: false, delete: false},
    7: {add: false, edit: false, delete: false},
    8: {add: false, edit: false, delete: false},
    9: {add: false, edit: false, delete: false},
    10: {add: false, edit: false, delete: false},
    11: {add: false, edit: false, delete: false},
    12: {add: false, edit: false, delete: false},
    13: {add: false, edit: false, delete: false},
    14: {add: false, edit: false, delete: false},
    15: {add: false, edit: false, delete: false},
    16: {add: false, edit: false, delete: false},
    17: {add: false, edit: false, delete: false},
    18: {add: false, edit: false, delete: false},
    19: {add: false, edit: false, delete: false},
    20: {add: false, edit: false, delete: false},
    21: {add: false, edit: false, delete: false},
    22: {add: false, edit: false, delete: false},
    23: {add: false, edit: false, delete: false},
    24: {add: false, edit: false, delete: false},
    25: {add: false, edit: false, delete: false},
    26: {add: false, edit: false, delete: false},
    27: {add: false, edit: false, delete: false},
  }

  public userId = 0;
  public showControl = false;

  public pages = UserState.getInstance().pages;
  private branches: any[] =[];
  private branchId: number;



  constructor( private boService: BackendService,
               private masterData: MassterDataService,
               private toastSerivce: NbToastrService,
               private dialog: MatDialog) {
    this.branches.push({id: 0, name: 'Not Selected'})
    this.users.push({id: 0, name: 'Not Selected', entitlements: undefined, userId: undefined, status: undefined, lastLoginDate: undefined, type: 0})
    this.loadUsers(true)

  }

  selectAll($event: MatCheckboxChange) {
    if (this.userAccess) {
      for (const key of Object.keys(this.userAccess)) {
        if ($event.checked) {
          this.userAccess[key] = {add : true, delete: true, edit: true};
        }else {
          this.userAccess[key] = {add : false, delete: false, edit: false};
        }
      }
    }
  }


  onClickSaveAll() {
    this.toEntitlements()

    if (this.isDEO) {
      this.boService.sendRequestToBackend(
        RequestGroup.BusinessUsers,
        RequestType.ManageDataEntryUser,
        { DATA: {
            id: this.userId,
            entitlements: this.entilements,
          }}
      ).then(data =>{
        console.log(data)
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          this.loadUsers(true)
          if (data.DAT != undefined && data.DAT.dto != undefined) {
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            });


            // this.userId = data.DAT.dto.userId;

          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        }else {
          this.toastSerivce.show('', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      });
    }else {
      this.boService.sendRequestToBackend(
        RequestGroup.BusinessUsers,
        RequestType.ManageClient,
        {
          DATA: {
            id: this.userId,
            entitlements: this.entilements,
          }
        }
      ).then( data => {
        console.log(data)
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.dto != undefined) {
            this.toastSerivce.show('', 'Saved data successfully.', {
              status: 'success',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            });


            // this.userId = data.DAT.dto.userId;

          } else {
            this.toastSerivce.show('', 'Error in saving data.', {
              status: 'danger',
              destroyByClick: true,
              duration: 2000,
              hasIcon: false,
              position: NbGlobalPhysicalPosition.TOP_RIGHT,
              preventDuplicates: true,
            })
          }
        }else {
          this.toastSerivce.show('', 'Error in saving data.', {
            status: 'danger',
            destroyByClick: true,
            duration: 2000,
            hasIcon: false,
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
            preventDuplicates: true,
          })
        }
      });
    }


  }

  fromEntitlements() {
    this.entilements.forEach(v => {
      let values: string [] = v.split('-');
      let pageNum = +values[0];
      let add = values[1] === '0'? false: true;
      let edit = values[2] === '0'? false: true;
      let del = values[3] === '0'? false: true;
      this.userAccess[pageNum].add = add;
      this.userAccess[pageNum].edit = edit;
      this.userAccess[pageNum].delete = del;

    });
  }
  toEntitlements() {
    this.entilements = [];
    let keys = Object.keys(this.userAccess);
    keys.forEach(k => {
      let str = `${k}-${this.userAccess[k].add ? 1: 0}-${this.userAccess[k].edit ? 1: 0}-${this.userAccess[k].delete ? 1: 0}`;
      this.entilements.push(str);
    })
  }


  onChangeBranch(value: any) {
    this.userId = 0;
    if (this.branchId !== 0 ) {
      this.loadUsers();
    }
  }

  onChangeMatCheck($event: MatCheckboxChange) {
    this.userId = 0;
    this.branchId = 0;
    if ($event.checked) {
      this.loadUsers(true)
    } else {
      this.loadUsers()
    }
  }

  loadBranches(comId: number) {
    console.log(comId);
    this.masterData.getBranchesForCompany(comId).subscribe(d => this.branches = d)
  }

  onSearchBranch(value: any) {
    if (value === '') {
      return;
    }
    let queryReq = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        name: { value: value , type: 4, col: 3},
        companyId: { value: UserState.getInstance().companyId, type: 1, col: 1}
      },
    }

    this.branches = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      queryReq
    ).then(data => {
      console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.branches = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.branches.unshift({id: val.id, name: val.name});
            }
          });
          if (this.branches.length === 0){
            this.branches.push({id: 0, name: 'No Matches Found'})
          }
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }





  onChangeUsers($event: MatSelectChange) {
    if (this.userId === 0) return;
    const user = this.users.filter(v => {
      return v.id === $event.value;
    })[0];

    console.log(user)

    if (user !== undefined) {
      //load UserLoginProfiles
      this.entilements = user.entitlements;
      this.fromEntitlements();
      this.showControl = true;
    }
  }









  loadUsers(isDEO? : true) {

    if (isDEO) {
      this.boService.sendRequestToBackend(
        RequestGroup.BusinessUsers,
        RequestType.ListDataEntryUser,
        {
          FILTER_MODEL: {
            companyId: {value: UserState.getInstance().companyId, type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            let list = [];
            this.users = [];
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                console.log(val);
                this.users.push(
                  {
                    id: val.id,
                    name: val.title + " " + val.firstName + " " + val.lastName,
                    entitlements: val.entitlements,
                    userId: val.userId,
                    status: undefined, lastLoginDate: undefined,
                    type: 0,
                  }
                );
                console.log(this.users);
              }
            });
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })

    } else {
      this.boService.sendRequestToBackend(
        RequestGroup.BusinessUsers,
        RequestType.ListClient,
        {
          FILTER_MODEL: {
            branchId: {value: this.branchId, type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
      ).then(data => {
        if (data.HED != undefined && data.HED.RES_STS == 1) {
          if (data.DAT != undefined && data.DAT.list != undefined) {
            this.users = [];
            data.DAT.list.forEach(val => {
              if (val != undefined) {
                this.users.push({
                  id: val.id,
                  name: val.title + " " + val.firstName + " " + val.lastName,
                  entitlements: val.entitlements,
                  userId: val.userId,
                  status: undefined, lastLoginDate: undefined,
                  type: 1,
                });
                console.log(this.users);
              }
            });
          } else {
            //  todo: show snack bar
            console.log('error data 1')
          }
        } else {
          //  todo: show snack bar
          console.log('error data 2')
        }
      })
    }

  }



  ngOnInit(): void {
  }




}
