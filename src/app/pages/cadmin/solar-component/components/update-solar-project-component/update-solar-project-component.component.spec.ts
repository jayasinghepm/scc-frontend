import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateSolarProjectComponentComponent } from './update-solar-project-component.component';

describe('UpdateSolarProjectComponentComponent', () => {
  let component: UpdateSolarProjectComponentComponent;
  let fixture: ComponentFixture<UpdateSolarProjectComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateSolarProjectComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateSolarProjectComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
