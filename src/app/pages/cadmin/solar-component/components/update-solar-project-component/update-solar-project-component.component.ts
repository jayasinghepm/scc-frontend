import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {NbToastrService} from "@nebular/theme";
import {BackendService} from "../../../../../@core/rest/bo_service";
import {RequestGroup} from "../../../../../@core/enums/request-group-enum";
import {RequestType} from "../../../../../@core/enums/request-type-enums";
import {UserState} from "../../../../../@core/auth/UserState";
import { MassterDataService } from 'src/app/@core/service/masster-data.service';

@Component({
  selector: 'app-update-solar-project-component',
  templateUrl: './update-solar-project-component.component.html',
  styleUrls: ['./update-solar-project-component.component.scss']
})
export class UpdateSolarProjectComponentComponent implements OnInit {

  public projectDto = {
   action: 1,
   projectId: -1,
   name: '',
   description:'' ,
   type : 1,
   comId: UserState.getInstance().companyId,
   branchId :-1
  }

  public branches = [];
  public mitigationActionTypes = [];
  mitigationnActionType: any;
  projectType: string;

  constructor(
    public dialogRef: MatDialogRef<UpdateSolarProjectComponentComponent>,
    private masterData: MassterDataService,
    private toastService: NbToastrService,
    private boService: BackendService,
    @Optional() @Inject(MAT_DIALOG_DATA) public popupData: {isEdit: boolean, data: any}
    ) {
    if (popupData.isEdit) {
      this.projectDto = {
        ...popupData.data,
        action: 2,

      }
    }

  }

  ngOnInit() {
    this.projectDto.comId = UserState.getInstance().companyId;

    this.masterData.getMitigationActionFull().subscribe(data => {
      console.log('data',data)
      this.mitigationActionTypes = data;
    
    });

    
  }

  save() {
    console.log('hhhhhhhh',this.mitigationnActionType)
   
    this.projectDto.type=this.mitigationnActionType;
    if (!this.isValid()) {
      this.toastService.warning('', 'Enter valid inputs!')
      return;
    }
    this.boService.sendRequestToBackend(RequestGroup.Mitigation, RequestType.ManageMitigationProject, {
      dto: {
        ...this.projectDto,
        type: this.mitigationnActionType,
        action: this.popupData.isEdit ? 2 : 1,
      }
    }).then(data=> {
      // console.log(data)
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        this.toastService.success('','Saved successfully!')
        this.dialogRef.close(true);
      }
    })


  }

  onSearchBranch(value: any) {
    if (value === '') {
      return;
    }
    let queryReq = {
      PAGE_NUMBER: -1,
      FILTER_MODEL: {
        name: { value: value , type: 4, col: 3},
        companyId: { value: UserState.getInstance().companyId, type: 1, col: 1},
      },
    }
    // if (this.selectedComId != 0) {
    //   queryReq.FILTER_MODEL[ 'companyId'] = { value: this.selectedComId, type: 1, col: 1}
    // }
    this.branches = [];
    this.boService.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      queryReq
    ).then(data => {
      console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          this.branches = [];
          // const set = new Set(this.airports);
          // this.airports.unshift(...data.DAT.list);
          // this.airports.push(...data.DAT.list)
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.branches.unshift({id: val.id, name: val.name});
            }
          });
          if (this.branches.length === 0){
            this.branches.push({id: 0, name: 'No Matches Found'})
          }
        } else {
          //  todo: show snack bar
          console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        console.log('error data 2')
      }
    })
  }

  isValid(): boolean {

    if (this.popupData.isEdit && this.projectDto.projectId <= 0) {

      return false;
    }
    if (!this.projectDto.name) {

      return false;
    }
    if (!this.projectDto.description) {

      return false;
    }
    if (!this.projectDto.comId) {

      return false;
    }
    if (!this.projectDto.type) {

    }
    return true;
  }

  closeDialog() {
    this.dialogRef.close(false);
  }

}
