import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDataEntryUserComponent } from './add-data-entry-user.component';

describe('AddDataEntryUserComponent', () => {
  let component: AddDataEntryUserComponent;
  let fixture: ComponentFixture<AddDataEntryUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDataEntryUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDataEntryUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
