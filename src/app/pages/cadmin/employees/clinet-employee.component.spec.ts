import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinetEmployeeComponent } from './clinet-employee.component';

describe('ClinetEmployeeComponent', () => {
  let component: ClinetEmployeeComponent;
  let fixture: ComponentFixture<ClinetEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinetEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinetEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
