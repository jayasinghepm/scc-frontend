import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CadminGeneralInfoComponent} from "./cadmin-general-information/cadmin-general-info-component";
import {CadminReportsComponent} from "./cadmin-reports/cadmin-reports.component";
import {CadminHomeComponent} from "./cadmin-home/cadmin-home.component";
import {CadminBranchesComponent} from "./cadmin-branches/cadmin-branches.component";
import {
  NbCardModule,
  NbTreeGridModule,
  NbIconModule,
  NbInputModule,
  NbSelectModule,
  NbRadioModule,
  NbWindowModule, NbListModule, NbTabsetModule
} from '@nebular/theme';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from "@angular/material";
import { ThemeModule } from 'src/app/@theme/theme.module';
import { CadminRoutingModule } from './cadmin.routing.module';
import { UpdateBranchComponent } from './cadmin-branches/update-branch/update-branch.component';
import { RemoveBranchComponent } from './cadmin-branches/remove-branch/remove-branch.component';
import {AngularDualListBoxModule} from "angular-dual-listbox";
import {FormsModule} from "@angular/forms";
import {AdminClientsComponent} from "./clients/admin-clients.component";
import {ClinetEmployeeComponent} from "./employees/clinet-employee.component";
import {ClientAirTravelComponent} from "./emission/client-air-travel/client-air-travel.component";
import {ClientElectricityComponent} from "./emission/client-electricity/client-electricity.component";
import {ClientEmpCommuComponent} from "./emission/client-emp-commu/client-emp-commu.component";
import {ClientFireExtComponent} from "./emission/client-fire-ext/client-fire-ext.component";
import {ClientGeneratorsComponent} from "./emission/client-generators/client-generators.component";
import {ClientMunicipalWaterComponent} from "./emission/client-municipal-water/client-municipal-water.component";
import {ClientRefriComponent} from "./emission/client-refri/client-refri.component";
import {ClientTransLocPurComponent} from "./emission/client-trans-loc-pur/client-trans-loc-pur.component";
import {ClientTransportComponent} from "./emission/client-transport/client-transport.component";
import {ClientWasteTransportComponent} from "./emission/client-waste-transport/client-waste-transport.component";
import {ClientSummaryDataInputsComponent} from "./client-summary-data-inputs/client-summary-data-inputs.component";
import {ClientWasteDisposalComponent} from "./emission/client-waste-disposal/client-waste-disposal.component";
import {RemoveClientComponent} from "./clients/remove-client/remove-client.component";
import {AddClientComponent} from "./clients/add-client/add-client.component";
import {AddEmployeeComponent} from "./employees/add-employee/add-employee.component";
import {OwlDateTimeModule, OwlNativeDateTimeModule} from "ng-pick-datetime";
import {SelectDropDownModule} from "ngx-select-dropdown";
import {CadminPageGuard} from "./CadminPageGuard";
import {NgxEchartsModule} from "ngx-echarts";
import {NgxChartsModule} from "@swimlane/ngx-charts";

import {CadminUpdateElecComponent} from "./emission/client-electricity/update-elec/update-elec.component";
import {CadminUpdateEmpCommComponent} from "./emission/client-emp-commu/update-emp-comm/update-emp-comm.component";
import {CadminUpdateFireExtComponent} from "./emission/client-fire-ext/update-fire-ext/update-fire-ext.component";
import {CadminUpdateGeneratorsComponent} from "./emission/client-generators/update-generators/update-generators.component";
import {CadminUpdateMunWaterComponent} from "./emission/client-municipal-water/update-mun-water/update-mun-water.component";
import {CadminUpdateRefriComponent} from "./emission/client-refri/update-refri/update-refri.component";
import {CadminUpdateTransLocComponent} from "./emission/client-trans-loc-pur/update-trans-loc/update-trans-loc.component";
import {CadminUpdateTransportComponent} from "./emission/client-transport/update-transport/update-transport.component";
// import {UpdateWasteDisComponent} from "./emission/client-waste-disposal/update-waste-dis/update-waste-dis.component";
import {CadminUpdateWasteTransportComponent} from "./emission/client-waste-transport/update-waste-transport/update-waste-transport.component";
import {CadminUpdateAirtravelComponent} from './emission/client-air-travel/update-airtravel/update-airtravel.component';
import {Ng2SmartTableModule} from "../../ng2-smart-table/src/lib/ng2-smart-table.module";
import {UpdateWasteDisComponent} from "./emission/client-waste-disposal/update-waste-dis/update-waste-dis.component";
import {DataEntryUserComponent} from "./data-entry-user/data-entry-user.component";
import {CadminUserAccessControlComponent} from "./user-access-control/user-access-control.component";
import {AddDataEntryUserComponent} from "./add-data-entry-user/add-data-entry-user.component";
import { SeaAirFreightComponent } from './emission/client-sea-air-freight/sea-air-freight/sea-air-freight.component';
import { UpdateSeaAirFreightComponent } from './emission/client-sea-air-freight/update-sea-air-freight/update-sea-air-freight.component';
import { LpgasComponent } from './emission/lpgas/lpgas.component';
import { BgasComponent } from './emission/bgas/bgas.component';
import { UpdateBgasComponent } from './emission/bgas/update-bgas/update-bgas.component';

import { BiomassComponent } from './emission/biomass/biomass.component';
import { UpdateBiomassComponent } from './emission/biomass/update-biomass/update-biomass.component';
import { UpdateLpgasComponent } from './emission/lpgas/update-lpgas/update-lpgas.component';
import { CadminAshTransComponent } from './emission/cadmin-ash-trans/cadmin-ash-trans.component';
import { CadminForkliftsComponent } from './emission/cadmin-forklifts/cadmin-forklifts.component';
import { CadminFurnaceOilComponent } from './emission/cadmin-furnace-oil/cadmin-furnace-oil.component';
import { CadminPaperwasteComponent } from './emission/cadmin-paperwaste/cadmin-paperwaste.component';
import { CadminPainManageVehicleComponent } from './emission/cadmin-pain-manage-vehicle/cadmin-pain-manage-vehicle.component';
import { CadminOilgasTransComponent } from './emission/cadmin-oilgas-trans/cadmin-oilgas-trans.component';
import { CadminRawMaterialTransComponent } from './emission/cadmin-raw-material-trans/cadmin-raw-material-trans.component';
import { CadminSawDustTransComponent } from './emission/cadmin-saw-dust-trans/cadmin-saw-dust-trans.component';
import { CadminSludgeTransComponent } from './emission/cadmin-sludge-trans/cadmin-sludge-trans.component';
import { CadminStaffTransComponent } from './emission/cadmin-staff-trans/cadmin-staff-trans.component';
import { UpdateCadminAshTransComponent } from './emission/cadmin-ash-trans/update-cadmin-ash-trans/update-cadmin-ash-trans.component';
import { UpdateCadminForkliftsComponent } from './emission/cadmin-forklifts/update-cadmin-forklifts/update-cadmin-forklifts.component';
import { UpdateFurnaceOilComponent } from './emission/cadmin-furnace-oil/update-furnace-oil/update-furnace-oil.component';
import { UpdateCadminOilgasComponent } from './emission/cadmin-oilgas-trans/update-cadmin-oilgas/update-cadmin-oilgas.component';
import { UpdateCadminPaidManagerVehicleComponent } from './emission/cadmin-pain-manage-vehicle/update-cadmin-paid-manager-vehicle/update-cadmin-paid-manager-vehicle.component';
import { UpdateCadminPaperwasteComponent } from './emission/cadmin-paperwaste/update-cadmin-paperwaste/update-cadmin-paperwaste.component';
import { UpdateRawMaterialTransComponent } from './emission/cadmin-raw-material-trans/update-raw-material-trans/update-raw-material-trans.component';
import { UpdateCadminSawDustTransComponent } from './emission/cadmin-saw-dust-trans/update-cadmin-saw-dust-trans/update-cadmin-saw-dust-trans.component';
import { UpdateCadminSludgeTransComponent } from './emission/cadmin-sludge-trans/update-cadmin-sludge-trans/update-cadmin-sludge-trans.component';
import { UpdateCadminStaffTransComponent } from './emission/cadmin-staff-trans/update-cadmin-staff-trans/update-cadmin-staff-trans.component';
import { CadminLorryTransComponent } from './emission/cadmin-lorry-trans/cadmin-lorry-trans.component';
import { UpateCadminLorryTransComponent } from './emission/cadmin-lorry-trans/upate-cadmin-lorry-trans/upate-cadmin-lorry-trans.component';
import { CadminVehicleOthersComponent } from './emission/cadmin-vehicle-others/cadmin-vehicle-others.component';
import { UpdateCadminComponent } from './emission/cadmin-vehicle-others/update-cadmin/update-cadmin.component';
import {PubTransFactorsComponent} from "./pub-trans-factors/pub-trans-factors.component";
import {EmissionFactorsComponent} from "./emission-factors/emission-factors.component";
import {WasteDisFactorsComponent} from "./waste-dis-factors/waste-dis-factors.component";
import { CadminProjectSummaryComponent } from './cadmin-project-summary/cadmin-project-summary.component';
import { SolarComponentComponent } from './solar-component/solar-component.component';
import { UpdateSolarProjectComponentComponent } from './solar-component/components/update-solar-project-component/update-solar-project-component.component';
import { MitigationViewComponent } from './cadmin-home/mitigation-view/mitigation-view.component';
import { ClientWeldingComponent } from '../admin/emission/client-welding/client-welding.component';
import { UpdateWeldingComponent } from '../admin/emission/client-welding/update-welding/update-welding.component';
import { ClientFreightWaterComponent } from './emission/client-freight-water/client-freight-water.component';
import { UpdateFreightWaterComponent } from './emission/client-freight-water/update-freightwater/update-freightwater.component';
import { WasteWaterTreatmentComponent } from '../admin/emission/waste-water-treatment/waste-water-treatment.component';
import { UpdateWasteWaterTreatmentComponent } from '../admin/emission/waste-water-treatment/update-waste-water-treatment/update-waste-water-treatment.component';



@NgModule(
  {
  declarations: [
    CadminGeneralInfoComponent,
    CadminReportsComponent,
    CadminHomeComponent,
    CadminBranchesComponent,
   AdminClientsComponent,
    ClinetEmployeeComponent,
    ClientSummaryDataInputsComponent,
    ClientAirTravelComponent,
    ClientElectricityComponent,
    ClientWeldingComponent,
    ClientEmpCommuComponent,
    ClientFireExtComponent,
    ClientGeneratorsComponent,
    ClientMunicipalWaterComponent,
    ClientRefriComponent,
    ClientTransLocPurComponent,
    ClientTransportComponent,
    ClientWasteTransportComponent,
    ClientWasteDisposalComponent,
    AddClientComponent,
    RemoveClientComponent,
    AddEmployeeComponent,
    CadminUpdateAirtravelComponent,
    CadminUpdateElecComponent,
    CadminUpdateEmpCommComponent,
    CadminUpdateFireExtComponent,
    CadminUpdateGeneratorsComponent,
    CadminUpdateMunWaterComponent,
    CadminUpdateRefriComponent,
    CadminUpdateTransLocComponent,
    CadminUpdateTransportComponent,
    // UpdateWasteDisComponent,
    CadminUpdateWasteTransportComponent,
    UpdateWasteDisComponent,
    DataEntryUserComponent,
    CadminUserAccessControlComponent,
    AddDataEntryUserComponent,
    SeaAirFreightComponent,
    UpdateSeaAirFreightComponent,
    LpgasComponent,
    BgasComponent,
    UpdateBgasComponent,
    BiomassComponent,
    UpdateBiomassComponent,
    UpdateLpgasComponent,
    CadminAshTransComponent,
    CadminForkliftsComponent,
    CadminFurnaceOilComponent,
    CadminPaperwasteComponent,
    CadminPainManageVehicleComponent,
    CadminOilgasTransComponent,
    CadminRawMaterialTransComponent,
    CadminSawDustTransComponent,
    CadminSludgeTransComponent,
    CadminStaffTransComponent,
    UpdateCadminAshTransComponent,
    UpdateCadminForkliftsComponent,
    UpdateFurnaceOilComponent,
    UpdateCadminOilgasComponent,
    UpdateCadminPaidManagerVehicleComponent,
    UpdateCadminPaperwasteComponent,
    UpdateRawMaterialTransComponent,
    UpdateCadminSawDustTransComponent,
    UpdateCadminSludgeTransComponent,
    UpdateCadminStaffTransComponent,
    CadminLorryTransComponent,
    UpateCadminLorryTransComponent,
    CadminVehicleOthersComponent,
    UpdateCadminComponent,
    PubTransFactorsComponent,
    EmissionFactorsComponent,
    WasteDisFactorsComponent,
    CadminProjectSummaryComponent,
    SolarComponentComponent,
    UpdateSolarProjectComponentComponent,
    MitigationViewComponent,
    UpdateWeldingComponent,
    ClientFreightWaterComponent,
    UpdateFreightWaterComponent,
    WasteWaterTreatmentComponent,
    UpdateWasteWaterTreatmentComponent

  ],
  imports: [
    FormsModule,
    CommonModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    Ng2SmartTableModule,
    CadminRoutingModule,
    NbSelectModule,
    NbRadioModule,
    NbWindowModule,
    NbListModule,
    AngularDualListBoxModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    SelectDropDownModule,
    NbListModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    NgxEchartsModule,
    NgxChartsModule,
    NbTabsetModule,
  ],
  entryComponents: [
    UpdateCadminComponent,
    UpdateSeaAirFreightComponent,
    AddClientComponent,
    RemoveClientComponent,
    AddEmployeeComponent,
    CadminUpdateAirtravelComponent,
    CadminUpdateElecComponent,
    UpdateWeldingComponent,
    CadminUpdateEmpCommComponent,
    CadminUpdateFireExtComponent,
    CadminUpdateGeneratorsComponent,
    CadminUpdateMunWaterComponent,
    CadminUpdateRefriComponent,
    CadminUpdateTransLocComponent,
    CadminUpdateTransportComponent,
    UpdateWasteDisComponent,
    CadminUpdateWasteTransportComponent,
    AddDataEntryUserComponent,
    UpdateBiomassComponent,
    UpdateLpgasComponent,
    UpdateBgasComponent,
    UpdateCadminAshTransComponent,
    UpdateCadminForkliftsComponent,
    UpdateFurnaceOilComponent,
    UpateCadminLorryTransComponent,
    UpdateCadminOilgasComponent,
    UpdateCadminPaidManagerVehicleComponent,
    UpdateCadminPaperwasteComponent,
    UpdateRawMaterialTransComponent,
    UpdateCadminSawDustTransComponent,
    UpdateCadminSludgeTransComponent,
    UpdateCadminStaffTransComponent,
    UpdateSolarProjectComponentComponent,
    UpdateFreightWaterComponent,
    UpdateWasteWaterTreatmentComponent
  

  ],
  providers: [CadminPageGuard,CadminGeneralInfoComponent,ClientEmpCommuComponent]
  
})
export class CadminModule { }
