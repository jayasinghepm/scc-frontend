import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataEntryUserComponent } from './data-entry-user.component';

describe('DataEntryUserComponent', () => {
  let component: DataEntryUserComponent;
  let fixture: ComponentFixture<DataEntryUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataEntryUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataEntryUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
