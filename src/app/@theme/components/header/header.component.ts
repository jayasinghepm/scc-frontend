import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService} from '@nebular/theme';

import {LayoutService} from '../../../@core/utils';
import {map, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {Router} from "@angular/router";
import {UserState} from "../../../@core/auth/UserState";
import {MassterDataService} from "../../../@core/service/masster-data.service";
import {BackendService} from "../../../@core/rest/bo_service";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
import {AddClientComponent} from "../../../pages/admin/admin-clients/add-client/add-client.component";
import {SettingComponent} from "../../../pages/setting/setting.component";
import {MatDialog} from "@angular/material";
import * as eva from 'eva-icons';
import {NotificationComponent} from "../notification/notification.component";
import {config} from "../../../@core/config/app-config";
import * as moment from 'moment';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  @ViewChild('notification',{static: false}) public trigger : ElementRef;
  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  public userType;
  user: any;

  themes = [
    {
      value: 'default',
      name: 'Light',
    },
    {
      value: 'dark',
      name: 'Dark',
    },
    {
      value: 'cosmic',
      name: 'Cosmic',
    },
    {
      value: 'corporate',
      name: 'Corporate',
    },
  ];

  public userName = "";
  public title = ""

  currentTheme = 'default';

  userMenu = [ { title: 'Profile' }, { title: 'Log out' } ];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private themeService: NbThemeService,
              private masterData: MassterDataService,
              private boService: BackendService,
              private dialog: MatDialog,
              private layoutService: LayoutService,
              private breakpointService: NbMediaBreakpointsService,
              private router: Router,
              private element: ElementRef
  ) {
      if (!UserState.getInstance().isAuthenticated) {
        this.router.navigate(['home']);
      }else{
        this.userType = UserState.getInstance().userType;
        this.userName = UserState.getInstance().firstName + " " + UserState.getInstance().lastName;
        if( UserState.getInstance().userType === 1) {
          this.title = "Branch : " + UserState.getInstance().branchName
          // this.masterData.getCompanyName(UserState.getInstance().companyId).subscribe(d =>  this.title = d);
        }else if( UserState.getInstance().userType === 2) {
          this.userName = UserState.getInstance().firstName + " " + UserState.getInstance().lastName + " (Admin)";
           // this.masterData.getCompanyName(UserState.getInstance().companyId).subscribe(d => this.title = d);
          this.title = UserState.getInstance().companyName;
        } else if( UserState.getInstance().userType === 3) {
            this.title = 'Climate SI'
        }
      }
      
  }

  ngOnInit() {
    this.currentTheme = this.themeService.currentTheme;

    this.initNotification();

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }

  public onclickLogout($event: MouseEvent) {
    console.log('log out works')
    this.boService.sendRequestToBackend(RequestGroup.Auth, RequestType.LogoutNormal,{
      USER_NAME: UserState.getInstance().username,
      PASSWORD: UserState.getInstance().password,
    } ).then(data => {
      // consider other cases todo
        if (data !== undefined && data.HED !== undefined && data.HED.RES_STS == 1) {
          this.router.navigateByUrl('home'); // ##login
        }
    })
  }

  public onclickSetting($event: MouseEvent) {
    console.log($event)
    const dialogRef = this.dialog.open(SettingComponent,
      {
        data : { disableClose: false,  isFirstTime: false},
        width: '390px',
        panelClass: 'no-border-page-wrapper',
        disableClose: false,
      })
    dialogRef.afterClosed().subscribe(d => {
      console.log(d);
      if (d) {
        // this.loadData();
      }
    })
  }

  public readNotfs = [];
  onShowNotification($event: MouseEvent) {
    // this.readNotfs = [];
    // console.log($event)
    // // this.notifications = '';
    // const dialogRef = this.dialog.open(NotificationComponent, {
    //   width: '250px', data: { notfs: this.listNotifs, trigger: this.element, x : $event.x, y: $event.y, readNotf: this.readNotfs},
    //     hasBackdrop: true,
    //     maxHeight: '300px',
    //     panelClass: 'no-border-page-wrapper',
    //     backdropClass: 'cdk-overlay-transparent-backdrop',
    //     // disableClose: false,
    // },

    //   ).afterClosed().subscribe(data => {
    //     if (data !== undefined && data.length !== 0) {
    //       // this.listNotifs.length - this.data.length
    //       console.log(this.listNotifs.length - data.length)

    //       data.forEach(not=> {
    //         if (not !== undefined) {}
    //         not.addedTime = undefined;
    //         not.read = 2;
    //         this.wsMarkAsRead(not);
    //       })
    //     }
    //     console.log(data);
    //     console.log(this.readNotfs)
    // })
  }

  private markedNots = []
  private wsMarkAsRead(not:any) {
    if(this.markedNots.filter(n => n === not.id)[0] === undefined  || this.markedNots.filter(n => n === not.id)[0].length === 0) {
      this.wsNotification.send(JSON.stringify(not));
      this.markedNots.push(not.id);
      this.notifCount--;
      const read = this.listNotifs.filter(n => n.id === not.id)[0];
      if (read !== undefined){
        read.read = 2;
      }
      let unRead = 0;
      for (let n of  this.listNotifs) {
        if (n.read === 1 && !this.markedNots.includes(n.id)) {
          unRead++;

        }
      }
      if (unRead === 0) {
        this.notifications = '';

      }else{
        this.notifications = "" + unRead;
      }

    }



  }

  public wsNotification:WebSocket;
  public notifications = "";
  public notifCount = 0;
  public listNotifs = [];

  public tryCount = 0;
  public initNotification() {
    this.wsNotification =  new WebSocket(config.bo.ws + UserState.getInstance().userId);
    this.wsNotification.onopen = (msg)=> {
      console.log(msg);
    }
    this.wsNotification.onclose = (msg) => {
      console.log(msg)
      if (this.tryCount <5 ) {
        this.initNotification();
        this.tryCount++;
      }
      // if (this.tryCount === 5) {
      //   this.
      // }

      // alert("socket closed")
    }
    this.wsNotification.onerror = (msg) => {

      console.log(msg);
      if (this.tryCount <5 ) {
        this.initNotification();
        this.tryCount++;
      }
    }

    this.wsNotification.onmessage = (msg) => {
      console.log(msg);
      if (msg !== undefined && msg.data !== undefined) {
        // console.log(JSON.parse(msg.data))
        if (msg.data !== 'Empty Notifications') {
          this.listNotifs.push(JSON.parse(msg.data))
          console.log('---------------------------')
          console.log(JSON.parse(msg.data));
          console.log('***************************')
           this.listNotifs.sort((o1,o2) =>{
            const d1 =  moment(o1.addedTime, 'DD-MM-YY HH:mm:SSS')
            const d2 =  moment(o2.addedTime, 'DD-MM-YY HH:mm:SSS')
             if (d1.isBefore(d2)) {
               return 1;
             }else if (d1.isAfter(d2)) {
               return 1;
             }else {
               return -1;
             }

              // if (o1.)    return -1;
              // else if(sort_o1_after_o2) return  1;
              // else                      return  0;
            });
          console.log('---------------------------')
          console.log('after sort: ' + this.listNotifs)
          this.notifCount++;

          let unRead = 0;
          for (let n of  this.listNotifs) {
            if (n.read === 1 && !this.markedNots.includes(n.id)) {
              unRead++;

            }
          }
          // if (unRead === 0) {
          //   this.notifications = '';

          // }else{
          //   this.notifications = "" + unRead;
          // }

          // this.notifications  = "43"
        }

      }

    }

  }


}
