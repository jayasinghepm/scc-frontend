import {Component, ElementRef, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogConfig, MatDialogRef} from "@angular/material";
import * as moment from 'moment';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  private readonly _matDialogRef: MatDialogRef<NotificationComponent>;
  private readonly triggerElementRef: ElementRef;
  private x:number;
  private y:number
  public notfs = [];

  public readNotfs = [];

  constructor(
     _matDialogRef : MatDialogRef<NotificationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {notfs: any [], trigger: ElementRef, x: number, y:number,  readNotf: any[] }
              ) {
    this._matDialogRef = _matDialogRef;
    this.triggerElementRef = data.trigger;
    this.x = data.x;
    this.y = data.y;
    this.notfs = JSON.parse(JSON.stringify(data.notfs));
    this.toFromNow();
  }

  private toFromNow() {
    if (this.notfs === undefined) return ;
    this.notfs.sort(function(a, b) {
      const keyA = moment(a.addedTime, 'DD-MM-YY HH:mm:SSS');
        const keyB =moment(b.addedTime, 'DD-MM-YY HH:mm:SSS');
      // Compare the 2 dates
      if (keyA > keyB) return -1;
      if (keyA < keyB) return 1;
      return 0;
    });
    for(let i =0 ;i < this.notfs.length; i++) {
      // Jan 23, 2020, 6:05:41 AM
      // this.notfs[i].addedTime = moment(this.notfs[i].addedTime).format('YYYY-MM-DDTHH:mm:ss:SSZ');
      // this.notfs[i].addedTime = moment(this.notfs[i].addedTime, 'MMM DD, YYYY, hh:mm:ss A').fromNow();
      // 25-01-20 11:06:523 IST
      if (this.notfs[i].addedTime !== undefined && this.notfs[i].addedTime !== '') {
        this.notfs[i].addedTime = this.notfs[i].addedTime.split('IST')[0];
        this.notfs[i].addedTime = moment(this.notfs[i].addedTime, 'DD-MM-YY HH:mm:SSS').fromNow();
      }

      // console.log(moment(this.notfs[i].addedTime, 'MMM DD, YYYY, hh:mm:ss A'))
    }
  }

  ngOnInit() {
    const matDialogConfig: MatDialogConfig = new MatDialogConfig();
    const rect = this.triggerElementRef.nativeElement.getBoundingClientRect();
    console.log(rect)
    matDialogConfig.position = { left: `${this.x - 300}px`, top: `${rect.bottom+5}px` };
    matDialogConfig.width = '300px';
    matDialogConfig.height = '400px';
    this._matDialogRef.updateSize(matDialogConfig.width, matDialogConfig.height);
    this._matDialogRef.updatePosition(matDialogConfig.position);
    this._matDialogRef.beforeClosed().subscribe(d => {
      this._matDialogRef.close(JSON.parse(JSON.stringify(this.readNotfs)))
    })
  }

  onRead(id: number) {
    console.log("on read " + id);
    console.log(this.readNotfs)
    for (let not of  this.readNotfs) {
      if (id === not.id ) {
        return;
      }
    }
    const not = this.notfs.filter(n => n.id === id)[0];
    if (not.read === 2) {
      return;
    }
    not.read = 2;
    this.readNotfs.push(JSON.parse(JSON.stringify(not)));
    this.data.readNotf.push(JSON.parse(JSON.stringify(not)));

  }


}
