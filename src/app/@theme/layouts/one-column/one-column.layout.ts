import { Component } from '@angular/core';
import {UserState} from "../../../@core/auth/UserState";
import {RequestGroup} from "../../../@core/enums/request-group-enum";
import {RequestType} from "../../../@core/enums/request-type-enums";
// import * as moment from "../../../@core/components/auth/login/login.component";
import {BackendService} from "../../../@core/rest/bo_service";
import * as moment from 'moment';


@Component({
  selector: 'ngx-one-column-layout',
  styleUrls: ['./one-column.layout.scss'],
  templateUrl: './one-column.layout.html',
})
export class OneColumnLayoutComponent {

  public userType;
  public projectStauts = -1;
  public remDays
  constructor(private boService :BackendService) {
    this.userType = UserState.getInstance().userType;
    this.projectStauts = 4;
    this.remDays = 0;

    if (UserState.getInstance().userType === 1 || UserState.getInstance().userType === 2 ) {
      this.initProjectDetails();
    }
  }


  public initProjectDetails() {
    this.boService.sendRequestToBackend(
      RequestGroup.GHGProject,
      RequestType.ListProject,
      {
        PAGE_NUMBER: -1,
        FILTER_MODEL: {
          companyId: { value: UserState.getInstance().companyId , type: 1, col: 1}
        }
      }
    ).then(data => {
      console.log(data)
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {
          if (data.DAT.LIST.length !== 0) {

          } else {
            data.DAT.LIST.forEach(value => {
              if (value.status !== 4) {
                UserState.getInstance().projectStatus = -1;
              } else {
                UserState.getInstance().projectStatus = value.status;
                let remdays;
                if (value.isExtended) {
                  const extend = moment(value.extendedDate, 'YYYY-MM-DD');
                  const start = moment(value.intiatedDate , 'YYYY-MM-DD');
                  remdays = extend.diff(start, 'days');
                  UserState.getInstance().remainingDaysProject = remdays;
                } else {
                  const start = moment(value.intiatedDate , 'YYYY-MM-DD');
                  const end = moment(value.endDate , 'YYYY-MM-DD');
                  console.log(start);
                  console.log(end);
                  const diff = end.diff(start, 'days');
                  remdays = diff
                  console.log(remdays);
                  UserState.getInstance().remainingDaysProject = remdays;


                }
                this.projectStauts = UserState.getInstance().projectStatus;
                this.remDays = UserState.getInstance().remainingDaysProject;
              }
            })
          }
        }
      }
    });
  }
}
