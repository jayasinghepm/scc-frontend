import { trigger, state, style, transition, animate } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';
import { RequestGroup } from '../../enums/request-group-enum';
import { BackendService } from '../../rest/bo_service';






@Component({
  selector: 'app-landing-component',
  templateUrl: './landing-component.component.html',
  styleUrls: ['./css-bootstrap.css','css-style.css'],
  animations: [
    trigger('flyInOut', [
      state('in', style({ transform: 'translateX(0)' })),
      transition('void => *', [
        style({ transform: 'translateX(-100%)' }),
        animate(200)
      ]),
      transition('* => void', [
        animate(200, style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]
})
export class LandingComponentComponent implements OnInit {


  public isSent: boolean = false;
  public isError: boolean = false;
  public name:string = "";
  public email:string = "";
  public phone:string = "";
  public message:string = "";
  public coppyToMe:boolean = false;

  public contacted: boolean = false;
  public tryingContact: boolean= false;

  index: number = 0;
  numImages: number = 4;
  imagesLoaded: number = 0;
  loading: boolean = true;
  imagesUrl = [
    "../../assets/img/slider1.png",
    "../../assets/img/slider2.png",
    "../../assets/img/slider3.png",
    "../../assets/img/slider4.png",
  ]

  constructor(private boService:BackendService) { }

  ngOnInit() {
    
    this.imagesUrl.forEach((x, index) => {
      const image = new Image();
      image.onload = (() => {
        this.imagesLoaded++;
        this.loading = (this.imagesLoaded != this.numImages)
      })
      image.src = x
    })
    interval(5000).subscribe(() => {
      if (!this.loading)
        this.index = (this.index + 1) % this.numImages
    })
  }

  scroll(id) {
    let el = document.getElementById(id);
    el.scrollIntoView();
  }

  submit(){
    if (this.name === "" || this.email == "" || this.phone == "" || this.message == ""){
      this.isError = true;
    }else{
      this.isError = false;
      const data = {
        name: this.name,
        email: this.email,
        phone: this.phone,
        message: this.message
      }
      if(this.coppyToMe){
        data["cc"] = this.email
      }

      try{
        this.boService.sendRequestToBackend(
          RequestGroup.ContactUs,
          1,
          {
            DATA: data,
          }
        ).then(res => {
          this.isSent = true;
          setTimeout(()=> {
            this.isSent = false;
          }, 5000)
        }).catch(err => {
          console.log(err);
          this.isSent = true;
          setTimeout(()=> {
            this.isSent = false;
          }, 5000)
        })
      }catch(err){
        this.isSent = true;
        setTimeout(()=> {
          this.isSent = false;
        }, 5000)
      }


    }
  }


}
