/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import {AuthHandlerService} from "../../../auth/AuthHandlerService";
import {UserState} from "../../../auth/UserState";
import {MassterDataService} from "../../../service/masster-data.service";
import {SettingComponent} from "../../../../pages/setting/setting.component";
import {MatDialog} from "@angular/material";
import {CadminStatus} from "../../../enums/CadminStatus";
import {RequestGroup} from "../../../enums/request-group-enum";
import {RequestType} from "../../../enums/request-type-enums";
import {BackendService} from "../../../rest/bo_service";
import {WebSocketService} from "../../../service/web-socket-service";

// import * as moment from "../../../../pages/admin/ghg-project/ghg-project.component";
// import { NB_AUTH_OPTIONS, NbAuthSocialLink } from '../../auth.options';
// import { getDeepFromObject } from '../../helpers';

// import { NbAuthService } from '../../services/auth.service';
// import { NbAuthResult } from '../../services/auth-result';


@Component({
  selector: 'nb-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NbLoginComponent implements OnInit,AfterViewInit{

  public username = "";
  public password = "";
  public rememberMe = false;
  public error = false;
  public errorMsg = '';
  public submitted: boolean;






  constructor(
    protected cd: ChangeDetectorRef,
    protected router: Router,
    private authHandler: AuthHandlerService,
    private materData: MassterDataService,
    private boService: BackendService,
    private dialog: MatDialog,
    private webSocket: WebSocketService,
  ) {


  }

  login(): void {
    this.error = false;
    this.errorMsg = "";
    this.submitted = true;
    this.cd.detectChanges();

    // console.log(this.password);
    // console.log(this.username);
    // console.log(this.rememberMe);
    UserState.getInstance().username = this.username;
    UserState.getInstance().password = this.password;
    this.authHandler.authenticateConnection().subscribe(data => {

      if (data === 1) {

          localStorage.removeItem("user");
          localStorage.removeItem("rememberMe");
          if(this.rememberMe) {
            localStorage.setItem("user", JSON.stringify(UserState.getInstance()));
            localStorage.setItem("rememberMe", JSON.stringify(this.rememberMe) );
          }

        this.materData.init();
        this.webSocket.init();

        if (UserState.getInstance().isFisttime) {
         // if(UserState.getInstance().companyId == 2){this.HNB = true}
          console.log(UserState.getInstance())
          return this.showSettingWindow();
        }

        switch(UserState.getInstance().userType) {
          case 1:{
            // console.log('working')

            this.router.navigateByUrl('/pages/client/home');
            break;
          }
          case 2: {
            // this.initProjectDetails();
            // console.log('working')
            if (UserState.getInstance().cadminStatus === CadminStatus.Approved) {
              this.router.navigateByUrl('/pages/com_admin/home');
            } else {
              this.router.navigateByUrl('/pages/com_admin/info');
            }

            break;
          }
          case 3: {
            // console.log('working')
            this.router.navigateByUrl('/pages/admin/home');

            break;
          }
          case 4: {
            this.router.navigateByUrl('/pages/data_entry_user/home');
            break;
          }
        }
      } else {
        this.submitted = false;
        this.error = true;
        this.errorMsg = data
        this.cd.detectChanges();
        // console.log(this.errorMsg);

      }
    });
   
  }



  public showSettingWindow() {

    const dialogRef = this.dialog.open(SettingComponent,
      {
        data : { disableClose: true, isFirstTime: true},
        width: '390px',
        panelClass: 'no-border-page-wrapper',
        disableClose: true,
      })
    dialogRef.afterClosed().subscribe(d => {
      // console.log(d);
      if (d) {
        switch(UserState.getInstance().userType) {
          case 1:{
            // console.log('working')
            this.router.navigateByUrl('/pages/client/home');
            break;
          }
          case 2: {
            // console.log('working')
            if (UserState.getInstance().cadminStatus === CadminStatus.Approved) {
              this.router.navigateByUrl('/pages/com_admin/home');
            } else {
              this.router.navigateByUrl('/pages/com_admin/info');
            }

            break;
          }
          case 3: {
            // console.log('working')
            this.router.navigateByUrl('/pages/admin/home');

            break;
          }
          case 4: {
            console.log(UserState.getInstance().dataEntryUserId)
            this.router.navigateByUrl('/pages/data_entry_user/home');
            break;
          }
        }
      }
    })
  }

  ngOnInit(): void {
    // if (localStorage.getItem("rememberMe") !== null  && localStorage.getItem("rememberMe") !== undefined)
    // {
    //   const rem = JSON.parse(localStorage.getItem("rememberMe"));
    //   console.log(JSON.parse(localStorage.getItem("rememberMe")))
    //   if (rem) {
    //     if (localStorage.getItem("user") !== null && localStorage.getItem("user") !== undefined) {
    //       UserState.setInstace(JSON.parse(localStorage.getItem("user")));
    //       switch(UserState.getInstance().userType) {
    //         case 1:{
    //           console.log('working')
    //
    //           this.router.navigateByUrl('/pages/client/home');
    //           break;
    //         }
    //         case 2: {
    //           // this.initProjectDetails();
    //           console.log('working')
    //           if (UserState.getInstance().cadminStatus === CadminStatus.Approved) {
    //             this.router.navigateByUrl('/pages/com_admin/home');
    //           } else {
    //             this.router.navigateByUrl('/pages/com_admin/info');
    //           }
    //
    //           break;
    //         }
    //         case 3: {
    //           console.log('working')
    //           this.router.navigateByUrl('/pages/admin/home');
    //
    //           break;
    //         }
    //       }
    //     }
    //     console.log(JSON.parse(localStorage.getItem("user")))
    //   }
    // }
  }

  ngAfterViewInit(): void {
    if (UserState.getInstance() !== undefined  && UserState.getInstance().isAuthenticated) {
      return;
    }
    if (localStorage.getItem("rememberMe") !== null  && localStorage.getItem("rememberMe") !== undefined)
    {
      const rem = JSON.parse(localStorage.getItem("rememberMe"));
      // console.log(JSON.parse(localStorage.getItem("rememberMe")))
      if (rem) {
        if (localStorage.getItem("user") !== null && localStorage.getItem("user") !== undefined) {
          // UserState.setInstace();
          this.password = JSON.parse(localStorage.getItem("user"))._password;
          this.username = JSON.parse(localStorage.getItem("user"))._username;
          this.login()
          // switch(UserState.getInstance().userType) {
          //   case 1:{
          //     console.log('working')
          //
          //     this.router.navigateByUrl('/pages/client/home');
          //     break;
          //   }
          //   case 2: {
          //     // this.initProjectDetails();
          //     console.log('working')
          //     if (UserState.getInstance().cadminStatus === CadminStatus.Approved) {
          //       this.router.navigateByUrl('/pages/com_admin/home');
          //     } else {
          //       this.router.navigateByUrl('/pages/com_admin/info');
          //     }
          //
          //     break;
          //   }
          //   case 3: {
          //     console.log('working')
          //     this.router.navigateByUrl('/pages/admin/home');
          //
          //     break;
          //   }
          // }
        }
        // console.log(JSON.parse(localStorage.getItem("user")))
      }
    }
  }


}
