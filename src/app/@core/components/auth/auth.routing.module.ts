import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { NbAuthComponent } from './auth.component';
import { NbLoginComponent } from './login/login.component';
import { LandingComponentComponent } from '../landing-component/landing-component.component';


const routes : Routes = [
    {
        path: '',
        component: NbAuthComponent,
        children: [
           {
               path: '',             
               redirectTo: 'home',
               pathMatch: 'full',
           },
           {
            path: 'home',
            component: LandingComponentComponent,
        },
           {
               path: 'login',
               component: NbLoginComponent,
           },           
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports:[RouterModule]
})
export class AuthRoutingModule {

}
