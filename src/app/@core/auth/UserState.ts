
export class UserState {
  get pageRoutes(): string [] {
    return this._pageRoutes;
  }

  set pageRoutes(value: string []) {
    this._pageRoutes = value;
  }
  get userType(): number {
    return this._userType;
  }

  set userType(value: number) {
    this._userType = value;
  }


  private static _instance: UserState;

  private _dataEntryExpired: boolean;
  private _username: string;
  private _password: string;
  private _isAuthenticated = false;
  private _isEmailVerified = false;
  private _isMobileVerfied = false;
  private _sessionId = "";
  private _userType = 1;
  private _adminId: number;
  private _clientId: number;
  private _clientAdminId: number;
  private _dataEntryUserId: number;
  private _branchId: number;
  private _companyId: number;
  private _pageRoutes: string [];

  private _cadminStatus: number;
  private _cadminStatusMsg: string;
  private _projectStatus: number;
  private _remainingDaysProject: number;
  private _existActiveProject:boolean;

  private _companyName: string;
  private _branchName: string;
  private _ismasterAdmin: boolean;

  private _pages : number[];
  private _emSources: number[];
  private _allowedEmissionSources: any;

  private _disableFormulas: number;


  get disableFormulas(): number {
    return this._disableFormulas;
  }

  set disableFormulas(value: number) {
    this._disableFormulas = value;
  }

  get allowedEmissionSources(): any {
    return this._allowedEmissionSources;
  }

  set allowedEmissionSources(value: any) {
    this._allowedEmissionSources = value;
  }

  get pages(): number[] {
    return this._pages;
  }

  set pages(value: number[]) {
    this._pages = value;
  }

  get emSources(): number[] {
    return this._emSources;
  }

  set emSources(value: number[]) {
    this._emSources = value;
  }

  get dataEntryUserId(): number {
    return this._dataEntryUserId;
  }

  set dataEntryUserId(value: number) {
    this._dataEntryUserId = value;
  }

  get companyName(): string {
    return this._companyName;
  }

  set companyName(value: string) {
    this._companyName = value;
  }

  get branchName(): string {
    return this._branchName;
  }

  set branchName(value: string) {
    this._branchName = value;
  }

  get ismasterAdmin(): boolean {
    return this._ismasterAdmin;
  }

  set ismasterAdmin(value: boolean) {
    this._ismasterAdmin = value;
  }

  get dataEntryExpired(): boolean {
    return this._dataEntryExpired;
  }

  set dataEntryExpired(value: boolean) {
    this._dataEntryExpired = value;
  }

  get existActiveProject(): boolean {
    return this._existActiveProject;
  }

  set existActiveProject(value: boolean) {
    this._existActiveProject = value;
  }

  get projectStatus(): number {
    return this._projectStatus;
  }

  set projectStatus(value: number) {
    this._projectStatus = value;
  }

  get remainingDaysProject(): number {
    return this._remainingDaysProject;
  }

  set remainingDaysProject(value: number) {
    this._remainingDaysProject = value;
  }

  get cadminStatus(): number {
    return this._cadminStatus;
  }

  set cadminStatus(value: number) {
    this._cadminStatus = value;
  }

  get cadminStatusMsg(): string {
    return this._cadminStatusMsg;
  }

  set cadminStatusMsg(value: string) {
    this._cadminStatusMsg = value;
  }

  get adminId(): number {
    return this._adminId;
  }

  set adminId(value: number) {
    this._adminId = value;
  }

  get clientId(): number {
    return this._clientId;
  }

  set clientId(value: number) {
    this._clientId = value;
  }

  get clientAdminId(): number {
    return this._clientAdminId;
  }

  set clientAdminId(value: number) {
    this._clientAdminId = value;
  }

  get branchId(): number {
    return this._branchId;
  }

  set branchId(value: number) {
    this._branchId = value;
  }

  get companyId(): number {
    return this._companyId;
  }

  set companyId(value: number) {
    this._companyId = value;
  }


  get userId(): string[] {
    return this._userId;
  }

  set userId(value: string[]) {
    this._userId = value;
  }

  get isFisttime(): boolean {
    return this._isFisttime;
  }

  set isFisttime(value: boolean) {
    this._isFisttime = value;
  }

  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  private _userId: string[];
  private _isFisttime: boolean;
  private _firstName: string;
  private _lastName: string;


  get sessionId(): string {
    return this._sessionId;
  }

  set sessionId(value: string) {
    this._sessionId = value;
  }

  static get instance(): UserState {
    return this._instance;
  }

  static set instance(value: UserState) {
    this._instance = value;
  }

  get username(): string {
    return this._username;
  }

  set username(value: string) {
    this._username = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  get isAuthenticated(): boolean {
    return this._isAuthenticated;
  }

  set isAuthenticated(value: boolean) {
    this._isAuthenticated = value;
  }

  get isEmailVerified(): boolean {
    return this._isEmailVerified;
  }

  set isEmailVerified(value: boolean) {
    this._isEmailVerified = value;
  }

  get isMobileVerfied(): boolean {
    return this._isMobileVerfied;
  }

  set isMobileVerfied(value: boolean) {
    this._isMobileVerfied = value;
  }

  public static getInstance(): UserState {
    UserState._instance = UserState._instance || new UserState();
    window['ppp'] = UserState._instance;
    return UserState._instance;
  }

  public static setInstace(state: UserState):void {
    this.instance = state;
  }



  public reset() : void {
    this.isAuthenticated = false;
    this.username = "";
    this.password = "";
    this.sessionId = "";
    this.userType = 1;
  }

  constructor() {
    if (UserState._instance) {
      throw new Error('Error: Instantiation failed: Use SingletonClass.getInstance() instead of new.');
    }
  }

}
