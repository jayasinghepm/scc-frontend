import {Injectable} from "@angular/core";
import * as CryptoLib from 'crypto-js';


@Injectable({
  providedIn: 'root',
})

export class PBKDF2PasswordHasher {

  private date: Date;
  private startingTime: number;
  private endedTime: number;
  private timeConsumed: number;

  constructor() {

  }

  /*
     *  Hashing the password using the PBKDF2 hashing algorithm
     * @param password
     */
  public hashPassword(password: string): string {
    if (password) {
      this.date = new Date();
      this.startingTime = this.date.getMilliseconds();
      const key = CryptoLib.PBKDF2(password, 'todohave', {
        keySize: 512 / 32,
        iterations: 10000,
        hasher: CryptoLib.algo.SHA512,
      });
      this.date = new Date();
      this.endedTime = this.date.getMilliseconds();
      this.timeConsumed = this.endedTime - this.startingTime;
      return key.toString();
    } else {
      return '';
    }
  }
}
