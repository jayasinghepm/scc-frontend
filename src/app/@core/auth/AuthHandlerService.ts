import {Injectable, Injector} from "@angular/core";
import {from, Observable} from 'rxjs';
import {PBKDF2PasswordHasher} from "./password-hasher";
import {UserState} from "./UserState";
import {BackendService} from "../rest/bo_service";
import {RequestGroup} from "../enums/request-group-enum";
import {RequestType} from "../enums/request-type-enums";
import "rxjs-compat/add/operator/map";
import {User} from "../data/users";

@Injectable(
  {
    providedIn: "root",
  }
)
export class AuthHandlerService {

  constructor(
    injector: Injector,
    private passwordHasher: PBKDF2PasswordHasher,
    private boService: BackendService,

    ) {
  }

  public authenticateConnection() : Observable<any> {

    const hashed = this.passwordHasher.hashPassword(UserState.getInstance().password);

    return from(this.boService.sendRequestToBackend(
      RequestGroup.Auth, RequestType.LoginNormal,{
        USER_NAME: UserState.getInstance().username,
        PASSWORD: UserState.getInstance().password,
      }
    )).pipe().map( res => {
      console.log(res);
      //AUTHENTICATION_STATUS
      // FAIL(0),
      //   SUCCESS(1),
      //   INVALID_LOGIN_NAME(2),
      //   PASSWORD_EXPIRED(3),
      //   INVALID_PASSWORD(4),
      //   USER_NOT_FOUND(5),
      //   INVALID_LOGIN_STATUS(6),
      //   INVALID_INPUT_PARAMETERS(7),
      //   INACTIVE_ACCOUNT(8),
      //   PENDING(9),
      //   LOCKED(10),
      //   SUSPENDED(11),
      //   NOT_REGISTERED(12),
      //   REGISTRATION_PENDING(13);
      if (res !== undefined) {
        if (res == -23) {
          return "Server is not running."
        }
        if (res.DAT  == undefined) {
          return "Error in Authentication."
        }
        res = res.DAT;
        if (res.authenticationStatus == undefined) {
          return "Error in Authentication.";
        }
        switch (res.authenticationStatus) {
          case 0: return "Authentication Failed."
          case 1: {
            console.log("gggggggggggggggggggggggggggggggggg",res)
            UserState.getInstance().sessionId = res.sessionId;
            UserState.getInstance().userType = res.userType;
            UserState.getInstance().isAuthenticated = true;
            UserState.getInstance().username = res.userName;
            UserState.getInstance().firstName = res.userFirstName;
            UserState.getInstance().lastName = res.userLastName;
            UserState.getInstance().clientId = res.clientId;
            UserState.getInstance().adminId = res.adminId;
            UserState.getInstance().clientAdminId = res.cadminId;
            UserState.getInstance().dataEntryUserId = res.dataEntryUserId;
            UserState.getInstance().isFisttime = res.isFirstime == 0 ? false: true;
            UserState.getInstance().companyId = res.companyId;
            UserState.getInstance().branchId = res.branchId;
            UserState.getInstance().pageRoutes = res.userEntitlementList;
            UserState.getInstance().userId = res.userId;
            UserState.getInstance().cadminStatus = res.cadminStatus;
            UserState.getInstance().cadminStatusMsg = res.cadminStatusMsg;
            UserState.getInstance().existActiveProject = res.existActiveProject === 1 ? true: false;
            UserState.getInstance().dataEntryExpired = res.dataEntryExpired === 1 ? true : false;
            UserState.getInstance().projectStatus = res.projectStatus;
            UserState.getInstance().companyName = res.companyName;
            UserState.getInstance().branchName = res.branchName;
            UserState.getInstance().ismasterAdmin = res.isMasterAdmin === 1? true: false;
            UserState.getInstance().pages = res.pages;
            UserState.getInstance().emSources = res.emSources;
            UserState.getInstance().allowedEmissionSources = res.allowedEmissionSources;
            UserState.getInstance().disableFormulas = res.disableFormulas;

            // console.log(UserState.getInstance())
            return 1
          }
          case 2:
          case 5:
          case 4: return "Invalid Username or Password."
          case 3: return "Password Expired."
          case 6:{
                  return res.narration;
          }
          default: return "Error in Authentication.";
        }
      } else {
        return "Error in Authentication."
      }

      return res;
    });
  }
}
