
export const validateEmail = (email:string): boolean => {
  const regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
  return regexp.test(email);
}

export const validateMobile = (mobileNo: string): boolean => {
  const regexp = /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,3})|(\(?\d{2,3}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/;
  return regexp.test(mobileNo);
}

export const validateTelephone = (telNo: string): boolean => {
  const regexp = /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,3})|(\(?\d{2,3}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/;
  return regexp.test(telNo);
}

export const validateYear = (year:string, isFy:boolean) => {
    if (isFy) {
      if (year.indexOf('/') === -1) {
        return false;

      }
      let i = year.indexOf('/')
      let y1 = year.substring(0, i);
      let y2 = year.substring(i+1, year.length);
      y1= y1.trim();
      y2 = y2.trim();
      if (y1.length !== 4) {
        return false;
      }
      if (y2.length !== 2) {
        return false;
      }
      if (isNaN(+y1) || +y1 <= 0 || (+y1 -2000)<= 0 || digits_count(+y1) !== 4) {
        return false;
      }

      if (isNaN(+y2) || +y2 <= 0  || digits_count(+y2) !== 2) {
        return false;
      }
      // this.emission.fyyearStart = `${+y1}`;
      // this.emission.fyyearEnd =  `${2000 + (+y2)}`;

      // this.comFinInfo.fyMonthStart  = 'April'
      // this.comFinInfo.fyMonthEnd  = 'March'
      return true;

    } else {
      let yearNum = +year;

      if (isNaN(yearNum )) {
        return false;
      }
      if (digits_count(yearNum ) == 4 && yearNum  > 0 && (yearNum  - 2000) > 0) {
        // this.comFinInfo.fyMonthStart  = 'January'
        // this.comFinInfo.fyMonthEnd  = 'December'
        // this.emission.fyyearStart = `${year}`;
        // this.emission.fyyearEnd =  `${year}`;
        return true;
      }
    }
}

const digits_count = (n)=> {
  let count = 0;
  if (n >= 1) ++count;

  while (n / 10 >= 1) {
    n /= 10;
    ++count;
  }

  return count;
}
