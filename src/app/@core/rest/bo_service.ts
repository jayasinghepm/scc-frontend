import {Injectable} from "@angular/core";
import {RequestGroup} from "../enums/request-group-enum";
import {RequestType} from "../enums/request-type-enums";
import {UserState} from "../auth/UserState";
import {config} from "../config/app-config";
import {HttpClient, HttpHeaders, HttpParams, HttpRequest} from "@angular/common/http";
import {Router} from '@angular/router';
import {from} from 'rxjs';


@Injectable(
  {
    providedIn: "root",
  }
)
export class BackendService {
  boService: any;

  constructor(private http: HttpClient, private router: Router) {

  }



  private generateReqHeader(reqGroup: RequestGroup, reqType: RequestType) : any {

    const header = {
      MSG_GRP: reqGroup,
      MSG_TYP: reqType,
      SESN_ID: UserState.getInstance().sessionId,
    };
    console.log(header);
    return header;
  }

  public sendRequestToBackend(reqGroup: RequestGroup, reqType: RequestType, jsonBody: any): null | Promise<any> {
   // console.log(reqType)
    let req;
    switch(reqGroup) {
      case RequestGroup.Auth:
          switch (reqType) {
            case RequestType.LoginNormal: {
              req = {
                HED: this.generateReqHeader(reqGroup, reqType),
                DAT: jsonBody,
              }
              break;
            }
            case RequestType.LogoutNormal: {
              req = {
                HED: this.generateReqHeader(reqGroup, reqType),
                DAT: jsonBody,
              }
              break;
            }

          }
        break;

      default:
        req = {
          HED: this.generateReqHeader(reqGroup, reqType),
          DAT: jsonBody,
        };
        break;
    }


    let respose = this.sendRestRequest(JSON.stringify(req));
    // console.log(respose)
    return respose;

  }

  private async sendRestRequest(body: string): Promise<any> {
    const url = config.bo.url;
    const method = config.bo.method;
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    // headers.set('Access-Control-Allow-Origin', '*');
    // headers.set('Access-Control-Allow-Methods','GET, POST, PATCH, PUT, DELETE, OPTIONS');
    // headers.set('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token');
    const req = new HttpRequest(
      method,
      url,
      body,
      {
        headers: headers,
        params: new HttpParams(),
      }
    );
    return new Promise((resole, reject) => {
      this.http.request(req)
        .toPromise()
        .then( res => {
          // console.log(res['body']);
          if(res['body'].HED){
            if (res['body'].HED.RES_STS === -3) {
              UserState.getInstance().isAuthenticated = false;
              this.router.navigate(['home']); // ##login
            //   this.backgroundAuthenticate();
            }
          }
          resole(res['body']);
        },
        err => {
          // console.log(err);
          try {
            if (err.status != undefined && err.status === 404) {
              // console.log("Not found")
              reject(-23);
            }
          } catch (e) {
            // console.log(e);
          }
        }
        )
    });

  }

  public backgroundAuthenticate():any {
   from(this.sendRequestToBackend(
    RequestGroup.Auth, RequestType.LoginNormal,{
      USER_NAME: UserState.getInstance().username,
      PASSWORD: UserState.getInstance().password,
    }
  ) ) .pipe().map( res => {
      // console.log(res);
      //AUTHENTICATION_STATUS
      // FAIL(0),
      //   SUCCESS(1),
      //   INVALID_LOGIN_NAME(2),
      //   PASSWORD_EXPIRED(3),
      //   INVALID_PASSWORD(4),
      //   USER_NOT_FOUND(5),
      //   INVALID_LOGIN_STATUS(6),
      //   INVALID_INPUT_PARAMETERS(7),
      //   INACTIVE_ACCOUNT(8),
      //   PENDING(9),
      //   LOCKED(10),
      //   SUSPENDED(11),
      //   NOT_REGISTERED(12),
      //   REGISTRATION_PENDING(13);
      if (res !== undefined) {
        if (res == -23) {
          return "Server is not running."
        }
        if (res.DAT  == undefined) {
          return "Error in Authentication."
        }
        res = res.DAT;
        if (res.authenticationStatus == undefined) {
          return "Error in Authentication.";
        }
        switch (res.authenticationStatus) {
          case 0: return "Authentication Failed."
          case 1: {
            UserState.getInstance().sessionId = res.sessionId;
            UserState.getInstance().userType = res.userType;
            UserState.getInstance().isAuthenticated = true;
            UserState.getInstance().username = res.userName;
            UserState.getInstance().firstName = res.userFirstName;
            UserState.getInstance().lastName = res.userLastName;
            UserState.getInstance().clientId = res.clientId;
            UserState.getInstance().adminId = res.adminId;
            UserState.getInstance().clientAdminId = res.cadminId;
            UserState.getInstance().isFisttime = res.isFirstime == 0 ? false: true;
            UserState.getInstance().companyId = res.companyId;
            UserState.getInstance().branchId = res.branchId;
            UserState.getInstance().pageRoutes = res.userEntitlementList;
            // console.log(UserState.getInstance())
            return 1
          }
          case 2:
          case 5:
          case 4: return "Invalid Username or Password."
          case 3: return "Password Expired."
          case 6: return "Account was Locked."
          default: return "Error in Authentication.";
        }
      } else {
        return "Error in Authentication."
      }
      if (!UserState.getInstance().isAuthenticated ) {
          this.router.navigateByUrl('home'); // ##login
      }
      return res;
    });
  }

  public downloadS3Objects(url: string) {
    let headers = new HttpHeaders();
    //.set('Content-Type', 'application/json');
    // headers.set('Access-Control-Allow-Origin', '*');
    // headers.set('Access-Control-Allow-Methods','GET, POST, PATCH, PUT, DELETE, OPTIONS');
    // headers.set('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token');
    window.open(url, "_blank");
    // const req = new HttpRequest(
    //   "GET",
    //   url,
    //   {
    //     headers: headers ,
    //     params: new HttpParams(),
    //   }
    // );
    // this.http.request(req).toPromise().then(data => {
    //   console.log(data);
    // })
    // this.http.get(url).subscribe(d=> {
    //   console.log(d);
    // });
  }

  
}
