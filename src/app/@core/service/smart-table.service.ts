import { Injectable } from '@angular/core';
import { SmartTableData } from '../data/smart-table-completer';
import { of as observableOf, Observable } from 'rxjs';

@Injectable(
  {
    providedIn: 'root'
  }
)
export class SmartTableService extends SmartTableData{

  getAirportCodes(): Observable<any[]> {
    return observableOf(['CMB', 'NZ'])
  }

  getBranches(): Observable<any[]> {
    return observableOf(['Head Office', 'Borrella', 'Colombo']);
  }

  getCountries(): Observable<any[]> {
    return observableOf(['Sri Lanka', 'India']);
  }

  getDisposalMethods(): Observable<any[]> {
    return observableOf(['Recycle']);
  }

  getEmployees(): Observable<any[]> {
    return observableOf(['Mr. So', 'Mrs. So']);
  }

  getFireExtTypes(): Observable<any[]> {
    return observableOf(['W/Gas']);
  }

  getFueltypes(): Observable<any[]> {
    return observableOf(['Petrol', 'Diesel']);
  }

  getMonths(): Observable<any[]> {
    return observableOf(['JAN', 'FEB', 'MAR']);
  }

  getRefrigerantTypes(): Observable<any[]> {
    return observableOf(['R22']);
  }

  getSeatClasses(): Observable<any[]> {
    return observableOf(['Economy Class']);
  }

  getTransportModes(): Observable<any[]> {
    return observableOf(['Walking','Car']);
  }

  getUnits(): Observable<any[]> {
    return observableOf(['kWh', 'm3']);
  }

  getVehicleCategories(): Observable<any[]> {
    return observableOf(['Company Own']);
  }

  getVehicleModels(): Observable<any[]> {
    return observableOf(['Car', 'Lorry']);
  }

  getWasteTypes(): Observable<any[]> {
    return observableOf(['Polythene']);
  }

  getYears(): Observable<any[]> {
    return observableOf(['2018', '2019']);
  }

  getMatrialtypes(): Observable<any[]> {
    return observableOf(['Polythene']);
  }


}
