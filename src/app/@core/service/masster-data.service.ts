import {Injectable} from "@angular/core";
import {of as observableOf,Observable} from "rxjs";
import {BackendService} from "../rest/bo_service";
import {RequestGroup} from "../enums/request-group-enum";
import {RequestType} from "../enums/request-type-enums";
import {UserData} from "../data/users";
import {UserState} from "../auth/UserState";
import {from} from 'rxjs';


@Injectable({
  providedIn: "root",
})
export class MassterDataService {
  //cost years updating
  currenty: number = new Date().getFullYear();
  py:number = this.currenty-1;
  ppy:number = this.currenty-2;
  pppy:number = this.currenty-3;
  ppppy:number = this.currenty-4;

  private countries: {id: number, name: string}[] = [];
  private airports: {id:number, name: string, code: string | undefined}[] = [];
  private freightAirports: {id:number, name: string, code: string | undefined, country: string}[] = [];
  private stations: {name: string}[] = [];
  private seeports: {country: string, name: string, code: string}[] = [];
  private branches: {id:number, name: string,  companyId: number}[] = [];
  private companies: {id: number, name: string, fy: boolean, pages: number[], emSources: number[], allowedEmissionSources : any}[] = [];
  private companiesDataEntry: {id: number, name: string, fy: boolean}[] = [];
  private companiesSummaryInputs: {id: number, name: string, fy: boolean}[] = [];
  private seatClasses: {id: number, name: string}[] = [];
  private airTravelways: {id: number, name: string}[] = [];
  private months: {id: number, name: string}[] = [];
  private units: {id: number, name: string}[] = [];
  private refriTypes: {id: number, name: string}[] = [];
  private fuelTypes: {id: number, name: string}[] = [];
  private vehicleTypes: {id: number, name: string}[] = [];
  private vehicleTypesWaste: {id: number, name: string}[] = [];
  private vehcielTypesOwn : {id: number, name: string}[] = [];
  private disMethods: {id: number, name: string}[] = [];
  private wasteTypes: {method: number, id: number, name: string, msw: boolean, sludge: boolean}[] = [];
  private vehicleCategroies: {id: number, name: string}[] = [];
  private vehicleOffroad: {id: number, name: string}[] = [];
  private vehicleRoad: {id: number, name: string}[] = [];//vehicleRoadTrans

  private vehicleRoadTrans: {id: number, name: string}[] = [];//vehicleRoadTrans
  private vehicleRoadRailTrans: {id: number, name: string}[] = [];//vehicleRoadTrans




  private fireExtTypes: {id: number, name: string}[] = [];
  private employees: {id: number, name: string}[] = [];
  private titles: string [] = [];
  private emissionSources : {id: number,name: string}[] = [];
  private publicVehicleTypes: { id: number, name: string} [] =  [];
  private methods: {id: number, name: string}[] = [];
  private activities: {id: number, name: string}[] = [];
  private types: {activity:number, id: number, name: string}[] = [];
  private seaports: {country:string, id: number, name: string,code:string}[] = [];

  private sizes: {type:number, id: number, name: string}[] = [];




  private unitsElectrictity : {id: number, name: string} [] = [];
  public unitsMunWater : {id: number, name: string} [] = []
  private fuelTypesGenerators: {id: number, name: string} [] = []
  private wasteTransVehcileTypes:{id: number, name: string} [] = []
  private districts :{name: string}[]  = []
  private unitsGenerators: {id: number, name: string} [] = []
  private unitsTransport: {id: number, name: string} [] = [];
  private noEmissionOptions : {id: number, name: string} [] = [];

  private ports: {id: number, name: string} [] = [];
  private freightTypes : {id: number, name: string} [ ] = [];//transModes
  private transModes : {id: number, name: string} [ ] = [];


  private unitsDistance: {id: number, name: string} [] = [];

  private years: {comId:number, years: string []}[] = []

  private fueltTypesVehicle: {id: number, name: string, code: string} [] = [];
  private fueltTypesVehicleY: {id: number, name: string} [] = [];
  private fueltTypesVehicleFreight: {id: number, name: string, code: string} [] = [];

  private stroke: { name: string, code: string} [] = [];



  private roles: {id: number, name: string}[] = [];

  private projects: {id: number, name: string, status: number, comId: number} [] = [];
  private companyDtos : any[] = [];
  private biomassTypes : any[] = [];
  mitigationProjects: {id: number, name: string}[] = [];

  constructor(private boservice: BackendService) {

  }
  public init() {


    this.freightAirports  = [
      {
          "country": "aa",
          "name": "AMS",
          "code": "AMS",
          "id": 1
      },
      {
          "country": "aa",
          "name": "Abu dhabi",
          "code": "AUH",
          "id": 1
      },
      {
          "country": "aa",
          "name": "CDG",
          "code": "CDG",
          "id": 1
      },
      {
          "country": "aa",
          "name": "Doha",
          "code": "DOH",
          "id": 1
      },
      {
          "country": "aa",
          "name": "Dubai",
          "code": "DXB",
          "id": 1
      },
      {
          "country": "aa",
          "name": "Istanbul",
          "code": "IST",
          "id": 1
      },
      {
          "country": "aa",
          "name": "Chennai",
          "code": "MAA",
          "id": 1
      },
      {
          "country": "Bangladesh",
          "name": "DAC",
          "code": "DAC",
          "id": 1
      },
      {
          "country": "China",
          "name": "PVG",
          "code": "PVG",
          "id": 1
      },
      {
          "country": "HONGKONG",
          "name": "HONGKONG",
          "code": "HKG",
          "id": 1
      },
      {
          "country": "JAPAN",
          "name": "OSAKA",
          "code": "KIX",
          "id": 1
      },
      {
          "country": "JAPAN",
          "name": "NAGOYA",
          "code": "NGO",
          "id": 2
      },
      {
          "country": "JAPAN",
          "name": "NARITA",
          "code": "NRT",
          "id": 3
      },
      {
          "country": "Male",
          "name": "MLE",
          "code": "MLE",
          "id": 1
      },
      {
          "country": "Malysia",
          "name": "KUL",
          "code": "KUL",
          "id": 1
      },
      {
          "country": "SINGAPORE",
          "name": "SINGAPORE",
          "code": "SIN",
          "id": 1
      },
      {
          "country": "Sri Lanka",
          "name": "Colombo",
          "code": "CMB",
          "id": 1
      }
  ]


    this.stations = [
      {name: "Abanpola"},
      {name: "Agbopura"},
      {name: "Ahangama"},
      {name: "Badulla"},

      {name: "Balapitiya"},
      {name: "Babarenda"},
      {name: "Bambalapitiya"},
      {name: "Bandarawela"},

      {name: "Chavakachcheri"},
      {name: "Chenkaladi"},
      {name: "Chilaw"},
      {name: "Chunnakam"},

      {name: "Dematagoda"},
      {name: "Daraluwa"},
      {name: "Dehiwala"},
      {name: "Demodara"}
    
    ]
    // this.seeports = [
    //   {
    //     "name": "Aabenraa",
    //     "country": "Denmark",
    //     "code": "DK0001"
    //   },
    //   {
    //       "name": "Aalborg",
    //       "country": "Denmark",
    //       "code": "DK0002"
    //   },
    //   {
    //       "name": "Aalborg Gronlandshavn",
    //       "country": "Denmark",
    //       "code": "DK0167"
    //   },
    //   {
    //       "name": "Aalborg Osthavn",
    //       "country": "Denmark",
    //       "code": "DK0166"
    //   },
    //   {
    //       "name": "Aalborg Portland",
    //       "country": "Denmark",
    //       "code": "DK0165"
    //   },
    // ];

    // todo:
    // this.countries.push(...[{id: 1, name: 'Sri lanka'}, {id: 2, name: 'India'}])
    // this.airports.push(...[{id: 0, name: 'None'},{id: 1, name: 'CMB'}, {id: 2, name: 'NZ'}]);
    // this.branches.push(...[{id: 1, name: 'Head office'}, {id: 2, name: 'Borrella'}]);
    // this.companies.push({id: 1, name: 'Commercial Bank'});
    this.airports = []
    this.branches = []
    this.companies = []
    this.countries = []
    this.employees = []
    this.titles = [];
    this.seatClasses = []
    this.airTravelways = []
    this.months = []
    this.units = []
    this.unitsMunWater = []
    this.unitsElectrictity = []


    this.refriTypes = [];
    this.fuelTypesGenerators = []
    this.fuelTypes = []
    this.vehicleTypes = []
    this.wasteTransVehcileTypes = []
    this.vehicleCategroies = [];
    this .publicVehicleTypes = [];
    this.disMethods = [];
    this.wasteTypes = [];
    this.fireExtTypes = []
    this.emissionSources = []
    this.unitsGenerators = []
    this.districts = []
    this.unitsTransport = [];
    this.fueltTypesVehicle = [];
    this.fueltTypesVehicleY = [];
    this.fueltTypesVehicleFreight = [];
    this.methods = [];
    this.activities = [];
    this.sizes = [];
    this.unitsDistance = [];

    this.vehicleTypesWaste = [];
    this.noEmissionOptions = [];
    this.vehcielTypesOwn = [];
    this.roles = [];
    this.ports = [];
    this.freightTypes = [];
    this.transModes = []
    this.companyDtos = [];
    this.types = [];
    this.vehicleRoadRailTrans = [];







    this.loadAirports()
    // this.loadBranches().subscribe(d => {
    //   this.branches = d;
    // })
    this.loadCompanies().subscribe(d => {
      this.companies = d;
    })
    this.loadCompaninesForDataEnry();
    this.loadCompaninesForSummaryInputs();
    this.loadCountries();
    this.loadEmployees().subscribe(d => {
      this.employees = d;
    })

    this.freightTypes.push(...[{id: 1, name: 'off-road'}, {id: 2, name: 'rail'},{id: 3, name: 'road'},{id: 4, name: 'water'},{id: 5, name: 'air'}]);
    this.transModes.push(...[{id: 1, name: 'off-road'}, {id: 2, name: 'rail'},{id: 3, name: 'road'},{id: 4, name: 'water'}]);
    

    this.ports.push(...[
      {id: 1, name: 'SHANGHAI (CHINA)'},
      {id: 2, name: 'JAKARTA (INDONESIA)'},
      {id: 3, name: 'HOCHIMINH (VIETNAM)'},
      {id: 4, name: 'SINGAPORE'},
      {id: 5, name: 'MUNDRA (INDIA)'},
      {id: 6, name: 'TUTICORIN (INDIA)'},
      {id: 7, name: 'KAOHSIUNG (TAIWAN)'},
      {id: 8, name: 'QINGDAO (CHINA)'},
      {id: 9, name: 'KARACHCHI (PAKISTHAN)'},
      {id: 10, name: 'PIPAVAV (INDIA)'},
      {id: 11, name: 'NINGBO (CHINA)'},
      {id: 12, name: 'GERMAN'},
      {id: 13, name: 'NETHERLAND'},
      {id: 14, name: 'PORT HEDLAND (AUSTRALIA)'},
      {id: 15, name: 'VANCOUVER (CANADA)'},
      {id: 16, name: 'CARTAGENA (COLOMBIA)'},
      {id: 17, name: 'FREDERICIA (DENMARK)'},
      {id: 18, name: 'HELSINKI (FINLAND)'},
      {id: 19, name: 'MARSEILLE (FRANCE)'},
      {id: 20, name: 'ASHDOD (ISRAEL)'},
      {id: 21, name: 'NAGOYA (JAPAN)'},
      {id: 22, name: 'BUSAN (KOREA)'},
      {id: 23, name: 'MANZANILLO (MEXICO)'},
      {id: 24, name: 'ROTTERDAM (NETHERLANDS)'},
      {id: 25, name: 'TAURANGA (NEW ZEALAND)'},
      {id: 26, name: 'DURBAN (SOUTH AFRICA)'},
      {id: 27, name: 'SOUTH LOUISIANA (UNITED STATES)'},

    ])
    this.titles.push(...['Mr', 'Mrs', 'Miss', 'Ms', 'Dr', 'Prof', 'Rev','Eng'])
    this.seatClasses.push(...[{id: 1, name: 'First Class'}, {id: 2, name: 'Business Class'}, {id: 3, name: 'Economy Class'}]);
    this.airTravelways.push(...[{id: 1, name: 'No'}, { id: 2, name: 'Yes'}]);
    this.months.push(...[
      {id: 12, name: 'All Months'},
      {id: 0, name: 'January',},
      { id: 1, name: 'February'},
      {id: 2, name: 'March'},
      {id: 3, name: 'April'},
      {id: 4, name: 'May'},
      {id: 5, name: 'June'},
      {id: 6, name: 'July'},
      {id: 7, name: 'August'},
      {id: 9, name: 'September'},
      {id: 8, name: 'October'},
      {id: 10, name: 'November'},
      {id: 11, name: 'December'},
    ]);

    this.mitigationProjects.push(...[
      { id: 1, name: 'Solar'},
      { id: 2, name: 'Hydro'},
      { id: 3, name: 'Wind'},
      { id: 4, name: 'Tree planting'},
      { id: 5, name: 'Rain water harvesting'},
      { id: 6, name: 'Energy efficiency actions'},
      { id: 7, name: 'Biogas'},
      { id: 8, name: 'Recycling'},
    ])



    this.units.push(...[
      { id: 1, name: 'm3'},
      {id: 2, name: 'kg'},
      {id: 3, name: 'kWh'},
      {id: 4, name: 'liters'},
      {id: 5, name: 'Tons'},
      {id: 6, name: 'LKR'},

    ]);

    this.unitsDistance.push(...[
      { id: 1, name: 'km'},
      {id: 2, name: 'NM'},
    
    ]);
    this.unitsTransport.push(...[
      { id: 1, name: 'm3'},
      {id: 4, name: 'liters'},
      {id: 6, name: 'LKR'},
    ])
    this.unitsGenerators.push(...[{ id: 1, name: 'm3'},{id: 4, name: 'liters'},{id: 6, name: 'LKR'},])
    this.unitsElectrictity.push(...[{id: 3, name: 'kWh'},]);
    this.unitsMunWater.push( { id: 1, name: 'm3'})

    this.refriTypes.push(...[
      {id: 1, name: 'R22'},
      {id: 2, name: 'R407C'},
      {id: 3, name: 'R410A'},
      {id: 4, name: 'R134a'},
    ]);
    this.fuelTypes.push(...[
      {id: 1, name: 'Petrol' },
      {id: 2, name: 'Diesel',},
      {id: 3, name: 'Kerosene'},
      {id: 4, name: 'Solar Electric'},
      {id: 5, name: 'Grid Electric'},
    ]);

    this.stroke = [    
      { name: 'Two Stroke', code: 'Two Stroke'},
      { name: 'Four Stroke', code: 'Four Stroke' },
    ];

    this.fueltTypesVehicle.push(...[
     
      {id: 1, name: 'Petrol', code: 'Petrol'},
      {id: 2, name: 'Diesel', code: 'Diesel' },
    ]);
    
    this.fueltTypesVehicleFreight.push(...[
      {id: 1, name: 'IFO (Intermediate Fuel Oil)', code: 'Marine fuel oil'},
      {id: 2, name: 'Marine Gas Oil', code: 'Marine gas oil' },
    ])

    this.fueltTypesVehicleY.push(...[
     
      {id: 1, name: 'LP95'},
      {id: 3, name: 'LP92' },

      {id: 2, name: 'LAD',},
      {id: 4, name: 'LSD'},
      
    ]);


    

    this.fuelTypesGenerators.push(...[
      {id: 2, name: 'Diesel',},])
    this.vehicleTypes.push(...[
      {id: 1, name: 'Lorry'},
      {id: 2, name: 'Tractor'},
      {id: 3, name: 'Van'},
      {id: 4, name: 'Jeep'},
      {id: 5, name: 'Car'},
      {id: 6, name: 'Prime move'},
      {id: 7, name: 'Bike'},
      {id: 8, name: 'Threewheel'},
      {id: 9, name: 'Bus'},
      {id: 10, name: 'Train'},
      {id: 11, name: 'Walking'},
      {id: 12, name: 'Cycling'},
      {id: 13, name: 'Free Ride'},
      {id: 14, name: 'Agriculture Tractors'},
      {id: 15, name: 'Chain saws'},
      {id: 16, name: 'ForkLifts'},
      {id: 17, name: 'Airport Groud Support Equipment'},
      {id: 18, name: 'Other Offroad'},


      {id: 19, name: 'Other'},
      {id: 20, name: 'None'}
    ]);
    this.vehcielTypesOwn.push(...[
      {id: 3, name: 'Van'},
      {id: 4, name: 'Jeep'},
      {id: 5, name: 'Car'},
      {id: 6, name: 'Prime move'},
      {id: 7, name: 'Bike'},
      {id: 8, name: 'Threewheel'},
      {id: 20, name: 'None'}
    ])
    this.noEmissionOptions.push(...[
      {id: 11, name: 'Walking'},
      {id: 12, name: 'Cycling'},
      {id: 13, name: 'Free Ride'},
      {id: 20, name: 'None'}
    ])
    this.wasteTransVehcileTypes.push(...[  {id: 1, name: 'Lorry'},
      {id: 2, name: 'Tractor'},{id: 8, name: 'Threewheel'}, {id: 3, name: 'Van'},])
    this.publicVehicleTypes.push(...[
      {id: 1, name: 'Van Diesel'},
      {id: 2, name: 'Medium Buse Diesel'},
      {id: 3, name: 'Bus Diesel'},
      {id: 4, name: 'Train'},
      {id: 20, name: 'None'}
    ])
    this.vehicleCategroies.push(...[
      {id: 1, name: 'Company Own'},
      {id: 2, name: 'Rented'},
      {id: 3, name: 'Hired'},
      // {id: 4, name: 'Off-Road'},

    ]);

    this.vehicleOffroad.push(...[
      {id: 1, name: 'Asphalt Pavers/Concreate'},
      {id: 2, name: 'Plate Compactors/Tempers/Rammers'},
      {id: 3, name: 'Rollers'},
      {id: 4, name: 'Trenches/Mini Excavators'},
      {id: 5, name: 'Excavators (Wheel/Crawler Type)'},
      {id: 6, name: 'Cement and Mortar Mixers'},
      {id: 7, name: 'Cranes'},
      {id: 8, name: 'Granders/Scrapers'},
      {id: 9, name: 'Off-highway trucks'},
      {id: 10, name: 'Bulldozers'},
      {id: 11, name: 'Tractors/Loaders/Backhoes'},
      {id: 12, name: 'Skid Steer Loaders'},
      {id: 13, name: 'Dumpers/Tenders'},
      {id: 14, name: 'Aerial lifts'},
      {id: 15, name: 'Fork lifts'},
      {id: 16, name: 'Generator Sets'},

    ]);

    this.vehicleRoad.push(...[
      {id: 1, name: 'Demo Batta'},
      {id: 2, name: 'Lorry'},
      {id: 3, name: 'Prime movers'},
      {id: 4, name: 'Three wheelers'},
      {id: 5, name: 'Trucks'},
      {id: 6, name: 'Van'},
      {id: 7, name: 'Other'},

    ]);

    
    this.vehicleRoadTrans.push(...[
      {id: 1, name: 'Van'},
      {id: 2, name: 'Lorry'},
      {id: 3, name: 'Jeep'},
      {id: 4, name: 'Car'},
      {id: 5, name: 'Threewheel'},
      {id: 6, name: 'Bus'},
      {id: 7, name: 'Other'},

    ]);//vehicleRoadRailTrans

    this.vehicleRoadRailTrans.push(...[
      {id: 1, name: 'Train'},

    ]);//vehicleRoadRailTrans




    this.methods.push(...[
      {id: 1, name: 'fuel_base'},
      {id: 2, name: 'distance_base'},

    ]);

    this.activities.push(...[
      {id: 1, name: 'Sea tanker'},
      {id: 2, name: 'Cargo ship'},

    ]);
    this.disMethods.push(...[
      {id: 1, name: 'Re-use'},
      {id: 2, name: 'Open-loop'},
      {id: 3, name: 'Closed-loop'},
      {id: 4, name: 'Combustion'},
      {id: 5, name: 'Composting'},
      {id: 6, name: 'Landfill'},
      {id: 7, name: 'Anaerobic digestion'},
      {id: 8, name: 'Piggery Feeding'},
      {id: 9, name: 'Incineration'},
    ]);
    this.wasteTypes.push(...[
      {method: 1 ,id: 1, name: 'Average construction', msw: false, sludge: false},
      {method: 1 ,id: 3, name: 'Tyres', msw: false, sludge: false},
      {method: 1 ,id: 4, name: 'Wood', msw: false, sludge: false},
      // {method: 1 ,id: 7, name: 'Average construction', msw: false, sludge: false},

      {method: 2 ,id: 1, name: 'Average construction', msw: false, sludge: false},
      {method: 2 ,id: 3, name: 'Tyres', msw: false, sludge: false},
      {method: 2 ,id: 4, name: 'Wood', msw: false, sludge: false},
      {method: 2 ,id: 6, name: 'Glass', msw: false, sludge: false},
      {method: 2 ,id: 8, name: 'Municipal waste', msw: false, sludge: false},
      {method: 2 ,id: 13, name: 'WEEE', msw: false, sludge: false},
      {method: 2 ,id: 14, name: 'WEEE - fridges and freezers', msw: false, sludge: false},
      {method: 2 ,id: 15, name: 'Batteries', msw: false, sludge: false},
      {method: 2 ,id: 17, name: 'Plastics', msw: false, sludge: false},

      {method: 3 ,id: 1, name: 'Average construction', msw: false, sludge: false},
      {method: 3 ,id: 2, name: 'Soils', msw: false, sludge: false},
      {method: 3 ,id: 3, name: 'Tyres', msw: false, sludge: false},
      {method: 3 ,id: 4, name: 'Wood', msw: false, sludge: false},
      {method: 3 ,id: 5, name: 'Books', msw: false, sludge: false},
      {method: 3 ,id: 6, name: 'Glass', msw: false, sludge: false},
      {method: 3 ,id: 7, name: 'Clothing', msw: false, sludge: false},
      {method: 3 ,id: 8, name: 'Municipal waste', msw: false, sludge: false},
      {method: 3 ,id: 12, name: 'Commercial and industrial waste', msw: false, sludge: false},
      {method: 3 ,id: 16, name: 'Metal', msw: false, sludge: false},
      {method: 3 ,id: 17, name: 'Plastics', msw: false, sludge: false},
      {method: 3 ,id: 18, name: 'Paper and board', msw: false, sludge: false},

      {method: 4 ,id: 4, name: 'Wood', msw: false, sludge: false},
      {method: 4 ,id: 5, name: 'Books', msw: false, sludge: false},
      {method: 4 ,id: 6, name: 'Glass', msw: false, sludge: false},
      {method: 4 ,id: 7, name: 'Clothing', msw: false, sludge: false},
      {method: 4 ,id: 8, name: 'Municipal waste', msw: false, sludge: false},
      {method: 4 ,id: 9, name: 'Organic: food and drink waste', msw: false, sludge: false},
      {method: 4 ,id: 10, name: 'Organic: garden waste', msw: false, sludge: false},
      {method: 4 ,id: 11, name: 'Organic: mixed food and garden waste', msw: false, sludge: false},
      {method: 4 ,id: 12, name: 'Commercial and industrial waste', msw: false, sludge: false},
      {method: 4 ,id: 13, name: 'WEEE', msw: false, sludge: false},
      {method: 4 ,id: 16, name: 'Metal', msw: false, sludge: false},
      {method: 4 ,id: 17, name: 'Plastics', msw: false, sludge: false},
      {method: 4 ,id: 18, name: 'Wood', msw: false, sludge: false},

      {method: 5 ,id: 4, name: 'Wood', msw: false, sludge: false},
      {method: 5 ,id: 5, name: 'Books', msw: false, sludge: false},
      {method: 5 ,id: 9, name: 'Organic: food and drink waste', msw: false, sludge: false},
      {method: 5 ,id: 10, name: 'Organic: garden waste', msw: false, sludge: false},
      {method: 5 ,id: 11, name: 'Organic: mixed food and garden waste', msw: false, sludge: false},
      {method: 5 ,id: 18, name: 'Paper and board', msw: false, sludge: false},

      {method: 6 ,id: 2, name: 'Soils', msw: false, sludge: false},
      {method: 6 ,id: 4, name: 'Wood', msw: false, sludge: false},
      {method: 6 ,id: 5, name: 'Books', msw: false, sludge: false},
      {method: 6 ,id: 6, name: 'Glass', msw: false, sludge: false},
      {method: 6 ,id: 7, name: 'Clothing', msw: false, sludge: false},
      {method: 6 ,id: 8, name: 'Municipal waste', msw: false, sludge: false},
      {method: 6 ,id: 9, name: 'Organic: food and drink waste', msw: false, sludge: false},
      {method: 6 ,id: 10, name: 'Organic: garden waste', msw: false, sludge: false},
      {method: 6 ,id: 11, name: 'Organic: mixed food and garden waste', msw: false, sludge: false},
      {method: 6 ,id: 12, name: 'Commercial and industrial waste', msw: false, sludge: false},
      {method: 6 ,id: 13, name: 'WEEE ', msw: false, sludge: false},
      {method: 6 ,id: 14, name: 'WEEE - fridges and freezers', msw: false, sludge: false},
      {method: 6 ,id: 15, name: 'Batteries', msw: false, sludge: false},
      {method: 6 ,id: 16, name: 'Metal', msw: false, sludge: false},
      {method: 6 ,id: 17, name: 'Plastics', msw: false, sludge: false},
      {method: 6 ,id: 18, name: 'Paper and board', msw: false, sludge: false},

      {method: 7 ,id: 8, name: 'Municipal waste', msw: false, sludge: false},
      {method: 7 ,id: 9, name: 'Organic: food and drink waste', msw: false, sludge: false},
      {method: 7 ,id: 10, name: 'Organic: garden waste', msw: false, sludge: false},
      {method: 7 ,id: 11, name: 'Organic: mixed food and garden waste', msw: false, sludge: false},
      {method: 7 ,id: 12, name: 'Commercial and industrial waste', msw: false, sludge: false},

      {method: 8 ,id: 30, name: 'Waste', msw: false, sludge: false},



      {method: 9 ,id: 19, name: 'Paper/ Cardboard', msw: true, sludge: false},
      {method: 9 ,id: 20, name: 'Textiles', msw: true, sludge: false},
      {method: 9 ,id: 21, name: 'Food waste', msw: true, sludge: false},
      {method: 9 ,id: 22, name: 'Wood', msw: true, sludge: false},
      {method: 9 ,id: 23, name: 'Garden and Park waste', msw: true, sludge: false},
      {method: 9 ,id: 24, name: 'Nappies', msw: true, sludge: false},
      {method: 9 ,id: 25, name: 'Rubber and Leather', msw: true, sludge: false},
      {method: 9 ,id: 26, name: 'Plastics ', msw: true, sludge: false},
      {method: 9 ,id: 27, name: 'Other, inert waste', msw: true, sludge: false},


      {method: 9 ,id: 28, name: 'Domestic', msw: false, sludge: true},
      {method: 9 ,id: 29, name: 'Industrial', msw: false, sludge: true},




    ]);
    this.types.push(...[
      {activity:1, id: 1, name: 'Crude tanker'},
      {activity:1, id: 2, name: 'Products tanker'},
      {activity:1, id: 3, name: 'Chemical tanker'},
      {activity:1, id: 4, name: 'LPG tanker'},
      {activity:1, id: 5, name: 'LNG Tanker'},

      {activity:2, id: 6, name: 'Bulk carrier'},
      {activity:2, id: 7, name: 'General cargo'},
      {activity:2, id: 8, name: 'Container ship'},
      {activity:2, id: 9, name: 'Vehicle transport'},
      {activity:2, id: 10, name: 'RoRo-Ferry'},
      {activity:2, id: 11, name: 'Large RoPax ferry'},
      {activity:2, id: 12, name: 'Refrigerated cargo'},



    ]);

    this.seaports.push(...[
      {
          "country": "aa",
          "name": "damietta",
          "code": "EGDAM",
          "id": 1
      },
      {
          "country": "aa",
          "name": "busan - south korea",
          "code": "KRPUS",
          "id": 1
      },
      {
          "country": "aa",
          "name": "hambantota",
          "code": "LKHBA",
          "id": 1
      },
      {
          "country": "aa",
          "name": "tanjung pelepas - malaysia",
          "code": "MYTPP",
          "id": 1
      },
      {
          "country": "aa",
          "name": "port of tanjung pelepas (malaysia)",
          "code": "MYTTP",
          "id": 1
      },
      {
          "country": "aa",
          "name": "noumea - new caledonia",
          "code": "NCNOU ",
          "id": 1
      },
      {
          "country": "aa",
          "name": "noumea - new caledonia",
          "code": "NCNOU / KRPUS",
          "id": 1
      },
      {
          "country": "aa",
          "name": "united states - newark",
          "code": "USORF",
          "id": 1
      },
      {
          "country": "australia",
          "name": "victoria",
          "code": "MEL",
          "id": 1
      },
      {
          "country": "australia",
          "name": "new south wales",
          "code": "SYD",
          "id": 2
      },
      {
          "country": "australia",
          "name": "south australia",
          "code": "ADL",
          "id": 3
      },
      {
          "country": "australia",
          "name": "queensland - brisbane",
          "code": "BNE",
          "id": 4
      },
      {
          "country": "australia",
          "name": "western australia - fremantle",
          "code": "FRE",
          "id": 5
      },
      {
          "country": "austria",
          "name": "enns",
          "code": "ENS",
          "id": 1
      },
      {
          "country": "bahrain",
          "name": "bahrain",
          "code": "BAH",
          "id": 1
      },
      {
          "country": "bangladesh",
          "name": "chittagong",
          "code": "BD CGP",
          "id": 1
      },
      {
          "country": "bangladesh",
          "name": "chittagong",
          "code": "CGP",
          "id": 2
      },
      {
          "country": "belgium",
          "name": "antwerpen",
          "code": "ANR",
          "id": 1
      },
      {
          "country": "belgium",
          "name": "antwerp ",
          "code": "BEANR",
          "id": 2
      },
      {
          "country": "brazil",
          "name": "parana",
          "code": "PNG",
          "id": 1
      },
      {
          "country": "canada",
          "name": "vancouver-british columbia",
          "code": "VAN",
          "id": 1
      },
      {
          "country": "canada",
          "name": "ontario - toronto",
          "code": "TOR",
          "id": 2
      },
      {
          "country": "canada",
          "name": "calgary",
          "code": "CAL",
          "id": 2
      },
      {
          "country": "chile",
          "name": "san antonio",
          "code": "SAA",
          "id": 1
      },
      {
          "country": "china",
          "name": "shanghai",
          "code": "SHA",
          "id": 1
      },
      {
          "country": "china",
          "name": "fuzhou",
          "code": "FUG",
          "id": 2
      },
      {
          "country": "china",
          "name": "shenzhen- yantian",
          "code": "YTN",
          "id": 3
      },
      {
          "country": "china",
          "name": "guangzhou",
          "code": "NSH",
          "id": 4
      },
      {
          "country": "china",
          "name": "jiangmen",
          "code": "GSH",
          "id": 5
      },
      {
          "country": "china",
          "name": "chongqing",
          "code": "CKG",
          "id": 6
      },
      {
          "country": "china",
          "name": "shenzhen",
          "code": "SHK",
          "id": 7
      },
      {
          "country": "china",
          "name": "changsha",
          "code": "CSX",
          "id": 8
      },
      {
          "country": "china",
          "name": "guangzhou - huangpu",
          "code": "HUA",
          "id": 9
      },
      {
          "country": "china",
          "name": "guangzhou",
          "code": "HPO",
          "id": 10
      },
      {
          "country": "china",
          "name": "tianjin",
          "code": "TSN",
          "id": 11
      },
      {
          "country": "china",
          "name": "qingdao",
          "code": "TAO",
          "id": 12
      },
      {
          "country": "china",
          "name": "yunfu",
          "code": "YNF",
          "id": 13
      },
      {
          "country": "china",
          "name": "xiamen",
          "code": "XMN",
          "id": 14
      },
      {
          "country": "china",
          "name": "nanchang",
          "code": "KHN",
          "id": 15
      },
      {
          "country": "china",
          "name": "ningbo",
          "code": "NGB",
          "id": 16
      },
      {
          "country": "china",
          "name": "dalian",
          "code": "DLC",
          "id": 17
      },
      {
          "country": "china",
          "name": "suzhou - zhangjiagang",
          "code": "ZJG",
          "id": 18
      },
      {
          "country": "china",
          "name": "foshan- shanghai",
          "code": "SHG",
          "id": 19
      },
      {
          "country": "china",
          "name": "ganzhou",
          "code": "GAN",
          "id": 20
      },
      {
          "country": "china",
          "name": "jiangyin(fuqing)",
          "code": "AHL",
          "id": 21
      },
      {
          "country": "china",
          "name": "dalingshan",
          "code": "CNDGG004",
          "id": 22
      },
      {
          "country": "china",
          "name": "hengli",
          "code": "CNDGG009",
          "id": 23
      },
      {
          "country": "china",
          "name": "nanzhuang",
          "code": "CNFOS032",
          "id": 24
      },
      {
          "country": "china",
          "name": "dongfu",
          "code": "CNHCA002",
          "id": 25
      },
      {
          "country": "china",
          "name": "haicang",
          "code": "CNHCA01A",
          "id": 26
      },
      {
          "country": "china",
          "name": "xinyang",
          "code": "CNHCA02A",
          "id": 27
      },
      {
          "country": "china",
          "name": "quxi",
          "code": "CNJIY038",
          "id": 28
      },
      {
          "country": "china",
          "name": "chaolian",
          "code": "CNJMN037",
          "id": 29
      },
      {
          "country": "china",
          "name": "shuibu",
          "code": "CNJMN061",
          "id": 30
      },
      {
          "country": "china",
          "name": "qiaoshe",
          "code": "CNNCG013",
          "id": 31
      },
      {
          "country": "china",
          "name": "xili",
          "code": "CNSZN021",
          "id": 32
      },
      {
          "country": "china",
          "name": "dianqian",
          "code": "CNXIA002",
          "id": 33
      },
      {
          "country": "china",
          "name": "dasha",
          "code": "CNZQG100",
          "id": 34
      },
      {
          "country": "china",
          "name": "dawang",
          "code": "CNZQG105",
          "id": 35
      },
      {
          "country": "china",
          "name": "chengdu",
          "code": "CTU",
          "id": 36
      },
      {
          "country": "china",
          "name": "fuzhou",
          "code": "FOC",
          "id": 37
      },
      {
          "country": "china",
          "name": "zhuhai(gaolan)",
          "code": "GLA",
          "id": 38
      },
      {
          "country": "china",
          "name": "gaoming",
          "code": "GOM",
          "id": 39
      },
      {
          "country": "china",
          "name": "hongwan(xiangzhou)",
          "code": "HGW",
          "id": 40
      },
      {
          "country": "china",
          "name": "jiujiang",
          "code": "JIJ",
          "id": 41
      },
      {
          "country": "china",
          "name": "jiujiang",
          "code": "JIU",
          "id": 42
      },
      {
          "country": "china",
          "name": "shunde(leliu)",
          "code": "LLU",
          "id": 43
      },
      {
          "country": "china",
          "name": "mawei",
          "code": "MAW",
          "id": 44
      },
      {
          "country": "china",
          "name": "ningde",
          "code": "NIN",
          "id": 45
      },
      {
          "country": "china",
          "name": "nanjing",
          "code": "NKG",
          "id": 46
      },
      {
          "country": "china",
          "name": "nantong",
          "code": "NTG",
          "id": 47
      },
      {
          "country": "china",
          "name": "qinzhou",
          "code": "QZH",
          "id": 48
      },
      {
          "country": "china",
          "name": "maoming( shuidong)",
          "code": "SDG",
          "id": 49
      },
      {
          "country": "china",
          "name": "shantou",
          "code": "SWA",
          "id": 50
      },
      {
          "country": "china",
          "name": "taicang",
          "code": "TAG",
          "id": 51
      },
      {
          "country": "china",
          "name": "xingang - txg",
          "code": "TSN - Tianjin",
          "id": 52
      },
      {
          "country": "china",
          "name": "wuhu",
          "code": "WHI",
          "id": 53
      },
      {
          "country": "china",
          "name": "wenzhou",
          "code": "WNZ",
          "id": 54
      },
      {
          "country": "china",
          "name": "huangpu(wuchongkou)",
          "code": "WUC",
          "id": 55
      },
      {
          "country": "china",
          "name": "wuhan",
          "code": "WUH",
          "id": 56
      },
      {
          "country": "china",
          "name": "wuzhou",
          "code": "WUZ",
          "id": 57
      },
      {
          "country": "china",
          "name": "zhongshan(xiaolan)",
          "code": "XLA",
          "id": 58
      },
      {
          "country": "china",
          "name": "yangpugang",
          "code": "YNP",
          "id": 59
      },
      {
          "country": "china",
          "name": "shunde(beijiao)",
          "code": "YQS",
          "id": 60
      },
      {
          "country": "china",
          "name": "yueyang",
          "code": "YUY",
          "id": 61
      },
      {
          "country": "china",
          "name": "zhanjiang",
          "code": "ZHA",
          "id": 62
      },
      {
          "country": "china",
          "name": "zhenjiang",
          "code": "ZHE",
          "id": 63
      },
      {
          "country": "china",
          "name": "zhongshan",
          "code": "ZSN",
          "id": 64
      },
      {
          "country": "china",
          "name": "zhuzhou",
          "code": "ZZH",
          "id": 65
      },
      {
          "country": "china",
          "name": "ningbo ",
          "code": "CNNGB",
          "id": 66
      },
      {
          "country": "china",
          "name": "nansha",
          "code": "CNNSA",
          "id": 67
      },
      {
          "country": "china",
          "name": "shenzhen",
          "code": "CNSZX",
          "id": 68
      },
      {
          "country": "china",
          "name": "qingdao",
          "code": "CNTAO",
          "id": 69
      },
      {
          "country": "china",
          "name": "rongqi",
          "code": "CNROQ",
          "id": 70
      },
      {
          "country": "china",
          "name": "foshan",
          "code": "CNFOS",
          "id": 71
      },
      {
          "country": "china",
          "name": "zhongshan",
          "code": "CNXIL",
          "id": 72
      },
      {
          "country": "china",
          "name": "shanghai ",
          "code": "CNSHA",
          "id": 73
      },
      {
          "country": "china",
          "name": "beijiao",
          "code": "CNBIJ",
          "id": 74
      },
      {
          "country": "colombia",
          "name": "bolivar - cartagena",
          "code": "CTG",
          "id": 1
      },
      {
          "country": "czech republic",
          "name": "zlin",
          "code": "ZLN",
          "id": 1
      },
      {
          "country": "denmark",
          "name": "aarhus",
          "code": "AHS",
          "id": 1
      },
      {
          "country": "denmark",
          "name": "staden koebenhavn -copenhagen",
          "code": "CPH",
          "id": 2
      },
      {
          "country": "denmark",
          "name": "randers",
          "code": "AKP",
          "id": 3
      },
      {
          "country": "denmark",
          "name": "fredericia",
          "code": "FRC",
          "id": 4
      },
      {
          "country": "egypt",
          "name": "dumyat",
          "code": "DAM",
          "id": 1
      },
      {
          "country": "estonia",
          "name": "tallinn",
          "code": "TLL",
          "id": 1
      },
      {
          "country": "fiji",
          "name": "lautoka",
          "code": "FJLTK",
          "id": 1
      },
      {
          "country": "fiji",
          "name": "suva",
          "code": "FJSUV",
          "id": 2
      },
      {
          "country": "finland",
          "name": "uusimaa - helsinki",
          "code": "HEL",
          "id": 1
      },
      {
          "country": "france",
          "name": "marseille",
          "code": "FRMRS ",
          "id": 1
      },
      {
          "country": "france",
          "name": "le harve",
          "code": "FRLEH",
          "id": 2
      },
      {
          "country": "germany",
          "name": "hamburg",
          "code": "HAM",
          "id": 1
      },
      {
          "country": "germany",
          "name": "hamburg ",
          "code": "DEHAM",
          "id": 2
      },
      {
          "country": "germany",
          "name": "neuburg an der donau",
          "code": "CFO",
          "id": 3
      },
      {
          "country": "hong kong",
          "name": "hong kong",
          "code": "HKHKG",
          "id": 1
      },
      {
          "country": "india",
          "name": "maharashtra- jawaharlal neru",
          "code": "NVA",
          "id": 1
      },
      {
          "country": "india",
          "name": "haryana",
          "code": "AQE",
          "id": 2
      },
      {
          "country": "india",
          "name": "gujarat - mundra",
          "code": "MUN",
          "id": 3
      },
      {
          "country": "india",
          "name": "punjab",
          "code": "LUD",
          "id": 4
      },
      {
          "country": "india",
          "name": "uttar pradesh",
          "code": "DDR",
          "id": 5
      },
      {
          "country": "india",
          "name": "chennai ",
          "code": "INMAA",
          "id": 6
      },
      {
          "country": "india",
          "name": "nhavasheva ",
          "code": "INNSA",
          "id": 7
      },
      {
          "country": "india",
          "name": "bangalore ",
          "code": "INBLR",
          "id": 8
      },
      {
          "country": "india",
          "name": "nhava sheva",
          "code": "INSSA",
          "id": 9
      },
      {
          "country": "india",
          "name": "tuticorin",
          "code": "INTUT",
          "id": 10
      },
      {
          "country": "india",
          "name": "mundra",
          "code": "INMUN",
          "id": 11
      },
      {
          "country": "india",
          "name": "cochin",
          "code": "INCOK",
          "id": 12
      },
      {
          "country": "india",
          "name": "bangalore",
          "code": "BNL",
          "id": 13
      },
      {
          "country": "india",
          "name": "chennai",
          "code": "MAA",
          "id": 14
      },
      {
          "country": "india",
          "name": "mumbai",
          "code": "INBOM",
          "id": 15
      },
      {
          "country": "india",
          "name": "nava shiva",
          "code": "INNHV",
          "id": 16
      },
      {
          "country": "indonesia",
          "name": "jakarta raya",
          "code": "JKT",
          "id": 1
      },
      {
          "country": "indonesia",
          "name": "jawa timur - surabaya",
          "code": "SUB",
          "id": 2
      },
      {
          "country": "indonesia",
          "name": "jawa tengah",
          "code": "SRG",
          "id": 3
      },
      {
          "country": "indonesia",
          "name": "sumatera utara",
          "code": "BLW",
          "id": 4
      },
      {
          "country": "iran",
          "name": "bandar abbas",
          "code": "IR BND",
          "id": 1
      },
      {
          "country": "iraq",
          "name": "iraq",
          "code": "ATP",
          "id": 1
      },
      {
          "country": "italy",
          "name": "genova",
          "code": "GOA",
          "id": 1
      },
      {
          "country": "japan",
          "name": "hyogo",
          "code": "UKB",
          "id": 1
      },
      {
          "country": "japan",
          "name": "osaka",
          "code": "OSA",
          "id": 2
      },
      {
          "country": "japan",
          "name": "aichi - nagoya",
          "code": "NGO",
          "id": 3
      },
      {
          "country": "japan",
          "name": "tokyo",
          "code": "TYO",
          "id": 4
      },
      {
          "country": "japan",
          "name": "fukuoka - hakata",
          "code": "HKT",
          "id": 5
      },
      {
          "country": "japan",
          "name": "kanagawa - yokohama",
          "code": "YOK",
          "id": 6
      },
      {
          "country": "japan",
          "name": "hokkaido - tomakomai",
          "code": "TMK",
          "id": 7
      },
      {
          "country": "japan",
          "name": "nagoya",
          "code": "JPNGO",
          "id": 8
      },
      {
          "country": "japan",
          "name": "osaka",
          "code": "JPOSK",
          "id": 9
      },
      {
          "country": "japan",
          "name": "tokyo",
          "code": "JPTYO",
          "id": 10
      },
      {
          "country": "japan",
          "name": "yokohama",
          "code": "JPYOK",
          "id": 11
      },
      {
          "country": "jordan",
          "name": "jordan - aqaba",
          "code": "AQB",
          "id": 1
      },
      {
          "country": "kenya",
          "name": "mombasa",
          "code": "KEMBA",
          "id": 1
      },
      {
          "country": "kiribati",
          "name": "tarawa",
          "code": "KITRW",
          "id": 1
      },
      {
          "country": "latvia",
          "name": "latvia - riga ",
          "code": "RIX",
          "id": 1
      },
      {
          "country": "lithuania",
          "name": "klaipeda",
          "code": "KLJ",
          "id": 1
      },
      {
          "country": "malaysia",
          "name": "johor",
          "code": "PGU",
          "id": 1
      },
      {
          "country": "malaysia",
          "name": "selangor - kelang",
          "code": "PKG",
          "id": 2
      },
      {
          "country": "malaysia",
          "name": "pulau pinang",
          "code": "PEN",
          "id": 3
      },
      {
          "country": "malaysia",
          "name": "sarawak - bintulu",
          "code": "BTU",
          "id": 4
      },
      {
          "country": "malaysia",
          "name": "sarawak - kuching",
          "code": "KCH",
          "id": 5
      },
      {
          "country": "malaysia",
          "name": "port kelang",
          "code": "MYPKG",
          "id": 6
      },
      {
          "country": "malaysia",
          "name": "port klang",
          "code": "MYPKL",
          "id": 7
      },
      {
          "country": "malaysia",
          "name": "petaling jaya",
          "code": "BAD",
          "id": 8
      },
      {
          "country": "maldives",
          "name": "male",
          "code": "MVMLE",
          "id": 9
      },
      {
          "country": "mexico",
          "name": "colima - manzanillo",
          "code": "ZLO",
          "id": 1
      },
      {
          "country": "myanmar",
          "name": "yangon",
          "code": "RGN",
          "id": 1
      },
      {
          "country": "myanmar",
          "name": "yangon",
          "code": "MMRGN",
          "id": 2
      },
      {
          "country": "netherland",
          "name": "rotterdam ",
          "code": "NLRTM",
          "id": 1
      },
      {
          "country": "netherlands",
          "name": "zuid-holland -rotterdam",
          "code": "RTM",
          "id": 2
      },
      {
          "country": "new caledonia",
          "name": "noumea",
          "code": "NCNOU",
          "id": 1
      },
      {
          "country": "new zealand",
          "name": "auckland",
          "code": "AKL",
          "id": 1
      },
      {
          "country": "new zealand",
          "name": "canterbury - lyttelton",
          "code": "LYT",
          "id": 2
      },
      {
          "country": "nicaragua",
          "name": "corinto",
          "code": "CIO",
          "id": 1
      },
      {
          "country": "norway",
          "name": "brevik",
          "code": "BEQ",
          "id": 1
      },
      {
          "country": "norway",
          "name": "bergen",
          "code": "BGO",
          "id": 2
      },
      {
          "country": "oman",
          "name": "sohar",
          "code": "SOH",
          "id": 1
      },
      {
          "country": "pakistan",
          "name": "sindh - karachi",
          "code": "KHI",
          "id": 1
      },
      {
          "country": "pakistan",
          "name": "karachi",
          "code": "PKKHI",
          "id": 2
      },
      {
          "country": "papua new guinea",
          "name": "lae",
          "code": "PGLAE",
          "id": 1
      },
      {
          "country": "papua new guinea",
          "name": "motukea island",
          "code": "PGMTK",
          "id": 2
      },
      {
          "country": "peru",
          "name": "callao",
          "code": "CLL",
          "id": 1
      },
      {
          "country": "philippines",
          "name": "cebu",
          "code": "CEB",
          "id": 1
      },
      {
          "country": "philippines",
          "name": "metro manila",
          "code": "MNL",
          "id": 2
      },
      {
          "country": "poland",
          "name": "pomorskie - gdynia",
          "code": "GDY",
          "id": 1
      },
      {
          "country": "poland",
          "name": "oswiecim",
          "code": "BIN",
          "id": 2
      },
      {
          "country": "poland",
          "name": "gdansk",
          "code": "GDN",
          "id": 3
      },
      {
          "country": "portugal",
          "name": "lisboa",
          "code": "LIS",
          "id": 1
      },
      {
          "country": "portugal",
          "name": "leixoes",
          "code": "LEI",
          "id": 2
      },
      {
          "country": "qatar",
          "name": "hamad",
          "code": "QAHMD",
          "id": 1
      },
      {
          "country": "qatar",
          "name": "hamad",
          "code": "HMD",
          "id": 2
      },
      {
          "country": "samoa",
          "name": "apia",
          "code": "ASAPW",
          "id": 1
      },
      {
          "country": "saudi arabia",
          "name": "ash sharqiyah - qalhat",
          "code": "DMN",
          "id": 1
      },
      {
          "country": "saudi arabia",
          "name": "ar riyad",
          "code": "RUH",
          "id": 2
      },
      {
          "country": "saudi arabia",
          "name": "dammam",
          "code": "SADMM",
          "id": 3
      },
      {
          "country": "saudi arabia",
          "name": "jeddah",
          "code": "JED",
          "id": 4
      },
      {
          "country": "singapore",
          "name": "singapore",
          "code": "SGSIN",
          "id": 1
      },
      {
          "country": "singapore",
          "name": "singapore",
          "code": "SIN",
          "id": 2
      },
      {
          "country": "solomon islands",
          "name": "honiara",
          "code": "SBHIR",
          "id": 1
      },
      {
          "country": "south africa",
          "name": "cape town",
          "code": "CPT",
          "id": 1
      },
      {
          "country": "south korea",
          "name": "busan",
          "code": "PUS",
          "id": 2
      },
      {
          "country": "south korea",
          "name": "inchon",
          "code": "INC",
          "id": 3
      },
      {
          "country": "south korea",
          "name": "chollanam-do - kwangyang",
          "code": "KAN",
          "id": 4
      },
      {
          "country": "spain",
          "name": "barcelona",
          "code": "BCN",
          "id": 1
      },
      {
          "country": "spain",
          "name": "valencia",
          "code": "VLC",
          "id": 2
      },
      {
          "country": "sri lanka",
          "name": "colombo",
          "code": "LKCMB",
          "id": 1
      },
      {
          "country": "sweden",
          "name": "goeteborgs och bohus",
          "code": "GOE",
          "id": 1
      },
      {
          "country": "sweden",
          "name": "stockholms",
          "code": "STO",
          "id": 2
      },
      {
          "country": "taiwan",
          "name": "kaohsiung",
          "code": "KHH",
          "id": 1
      },
      {
          "country": "taiwan",
          "name": "taichung",
          "code": "TCH",
          "id": 1
      },
      {
          "country": "taiwan",
          "name": "taoyuan",
          "code": "TYN",
          "id": 2
      },
      {
          "country": "taiwan",
          "name": "keelung",
          "code": "KEL",
          "id": 3
      },
      {
          "country": "tanzania",
          "name": "tanzania port",
          "code": "TZDAR",
          "id": 1
      },
      {
          "country": "thailand",
          "name": "krung thep mahanakhon",
          "code": "BKK",
          "id": 1
      },
      {
          "country": "thailand",
          "name": "chon buri - laem chabang",
          "code": "LCH",
          "id": 2
      },
      {
          "country": "thailand",
          "name": "lat krabang",
          "code": "LKG",
          "id": 3
      },
      {
          "country": "thailand",
          "name": "leam chabang",
          "code": "THLCH",
          "id": 4
      },
      {
          "country": "the republic of ireland",
          "name": "dublin",
          "code": "DUB",
          "id": 1
      },
      {
          "country": "the republic of ireland",
          "name": "cork",
          "code": "ORK",
          "id": 2
      },
      {
          "country": "the seychelles",
          "name": "port victoria",
          "code": "SCPOV",
          "id": 1
      },
      {
          "country": "tonga",
          "name": "nuku alofa",
          "code": "TOTBU",
          "id": 2
      },
      {
          "country": "trinidad and tobago",
          "name": "port-of-spain",
          "code": "PSP",
          "id": 1
      },
      {
          "country": "turkey",
          "name": "aliaga",
          "code": "ALI",
          "id": 1
      },
      {
          "country": "uae",
          "name": "jebel ali",
          "code": "AE JEA",
          "id": 1
      },
      {
          "country": "united arab emirates",
          "name": "dubai",
          "code": "JEA",
          "id": 1
      },
      {
          "country": "united arab emirates",
          "name": "abu dhabi",
          "code": "AUH",
          "id": 2
      },
      {
          "country": "united kingdom",
          "name": "london gateway",
          "code": "LGP",
          "id": 1
      },
      {
          "country": "united kingdom",
          "name": "haverhill",
          "code": "BQY",
          "id": 1
      },
      {
          "country": "united kingdom",
          "name": "berkeley",
          "code": "BSC",
          "id": 1
      },
      {
          "country": "united states",
          "name": "los angeles",
          "code": "LAS",
          "id": 1
      },
      {
          "country": "united states",
          "name": "king - seattle ",
          "code": "SEA",
          "id": 2
      },
      {
          "country": "united states",
          "name": "los angeles - long beach",
          "code": "LGB",
          "id": 3
      },
      {
          "country": "united states",
          "name": "wyandotte",
          "code": "KCK",
          "id": 4
      },
      {
          "country": "united states",
          "name": "chatham",
          "code": "SAV",
          "id": 5
      },
      {
          "country": "united states",
          "name": "harris",
          "code": "HOU",
          "id": 6
      },
      {
          "country": "united states",
          "name": "new york",
          "code": "USNYC",
          "id": 7
      },
      {
          "country": "united states",
          "name": "minneapolis",
          "code": "USMES",
          "id": 8
      },
      {
          "country": "united states",
          "name": "newark",
          "code": "USNWK",
          "id": 9
      },
      {
          "country": "united states",
          "name": "oakland",
          "code": "USOAK",
          "id": 10
      },
      {
          "country": "united states",
          "name": "tacoma",
          "code": "USTIW",
          "id": 11
      },
      {
          "country": "united states",
          "name": "atlanta",
          "code": "ATL",
          "id": 12
      },
      {
          "country": "united states",
          "name": "nashville",
          "code": "BNA",
          "id": 13
      },
      {
          "country": "united states",
          "name": "chicago",
          "code": "CHI",
          "id": 14
      },
      {
          "country": "united states",
          "name": "columbus",
          "code": "CMH",
          "id": 15
      },
      {
          "country": "united states",
          "name": "cincinnati",
          "code": "CVG",
          "id": 16
      },
      {
          "country": "united states",
          "name": "dallas",
          "code": "DAL",
          "id": 17
      },
      {
          "country": "united states",
          "name": "louisville",
          "code": "LUI",
          "id": 18
      },
      {
          "country": "united states",
          "name": "los angelese",
          "code": "USLAX",
          "id": 19
      },
      {
          "country": "vanuatu",
          "name": "santo",
          "code": "VUSAN",
          "id": 1
      },
      {
          "country": "vanuatu",
          "name": "port vila",
          "code": "VUVLI",
          "id": 2
      },
      {
          "country": "vietnam",
          "name": "hai phong",
          "code": "HPH",
          "id": 3
      },
      {
          "country": "vietnam",
          "name": "ho chi minh",
          "code": "SGN",
          "id": 4
      },
      {
          "country": "vietnam",
          "name": "ho chi minh",
          "code": "CLI",
          "id": 5
      },
      {
          "country": "vietnam",
          "name": "hochimin city",
          "code": "VNSGN",
          "id": 6
      },
      {
          "country": "vietnam",
          "name": "da nang",
          "code": "DAD",
          "id": 7
      }
  ])
    // this.seaports.push(...[
    //   {country:'Sri Lanka', id: 1, name: 'Colombo',code:"LK0003"},
    //   {country:'Sri Lanka', id: 2, name: 'Colombo-South',code:"LK0029"},
    //   {country:'Sri Lanka', id: 3, name: 'Dondra Head - E bound',code:"RP4610"},
    //   {country:'Sri Lanka', id: 4, name: 'Dondra Head - W bound',code:"RP4600"},
    //   {country:'Sri Lanka', id: 5, name: 'Galle',code:"LK0006"},


    //   {country:'India', id: 1, name: 'Alang',code:"IN0150"},
    //   {country:'India', id: 2, name: 'Bedi Anchorage',code:"IN0012"},
    //   {country:'India', id: 3, name: 'Bhavnagar',code:"IN0016"},
    //   {country:'India', id: 4, name: 'BORL-Sikka SBM',code:"IN0194"},
    //   {country:'India', id: 5, name: 'Cochin',code:"IN0033"},


    //   {country:'Indonesia', id: 1, name: 'Belanak Marine Terminal',code:"ID0357"},//Myanmar
    //   {country:'Indonesia', id: 2, name: 'Belawan',code:"ID0038"},
    //   {country:'Indonesia', id: 3, name: 'Belawan - SBM',code:"ID0473"},
    //   {country:'Indonesia', id: 4, name: 'Kabil',code:"ID0099"},
    //   {country:'Indonesia', id: 5, name: 'Tanjung Priok',code:"ID0302"},


    //   {country:'Myanmar', id: 1, name: 'Thilawa',code:"MM0023"},
    //   {country:'Japan', id: 1, name: 'Tokyo',code:"JP0310"},
    //   {country:'France', id: 1, name: 'Marseilles',code:"FR0139"},
    //   {country:'France', id: 2, name: 'Le Havre',code:"FR0284"},

    //   {country:'Myanmar', id: 1, name: 'Thilawa',code:"MM0023"},//Klang

    //   {country:'Malaysia', id: 1, name: 'Klang  ',code:"MY0078"},
    //   {country:'Singapore', id: 1, name: 'Jurong Marine Base',code:"SG0070"},
    //   {country:'Singapore', id: 2, name: 'Keppel Harbour',code:"SG0007"},//Taiwan

    //   {country:'Taiwan', id: 1, name: 'Kaohsiung',code:"TW0006"},//

    //   {country:'Egypt (through Suez canal)', id: 1, name: 'Said  ',code:"EG0115"},
    //   {country:'South Africa', id: 1, name: 'Cape town  ',code:"ZA0006"},




    // ]);




    this.sizes.push(...[
      {type:1, id: 1, name: '200,000+ dwt'},
      {type:1, id: 2, name: '120,000–199,999 dwt'},
      {type:1, id: 3, name: '80,000–119,999 dwt'},
      {type:1, id: 4, name: '60,000–79,999 dwt'},
      {type:1, id: 5, name: '10,000–59,999 dwt'},
      {type:1, id: 6, name: '0–9999 dwt'},
      {type:1, id: 7, name: 'Average'},

      {type:2, id: 1, name: '60,000+ dwt'},
      {type:2, id: 2, name: '20,000–59,999 dwt'},
      {type:2, id: 3, name: '10,000–19,999 dwt'},
      {type:2, id: 4, name: '5000–9999 dwt'},
      {type:2, id: 5, name: '0–4999 dwt'},
      {type:2, id: 6, name: 'Average'},

      {type:3, id: 1, name: '20,000+ dwt'},
      {type:3, id: 2, name: '10,000–19,999 dwt'},
      {type:3, id: 3, name: '5000–9999 dwt'},
      {type:3, id: 4, name: '0–4999 dwt'},
      {type:3, id: 5, name: 'Average'},

      {type:4, id: 1, name: '200,000+ m3'},
      {type:4, id: 2, name: '0–199,999 m3'},
      {type:4, id: 3, name: 'Average'},

      {type:5, id: 1, name: '50,000+ m3'},
      {type:5, id: 2, name: '0–49,999 m3'},
      {type:5, id: 3, name: 'Average'},



      {type:6, id: 1, name: '200,000+ dwt'},
      {type:6, id: 2, name: '100,000–199,999 dwt'},
      {type:6, id: 3, name: '60,000–99,999 dwt'},
      {type:6, id: 4, name: '35,000–59,999 dwt'},
      {type:6, id: 5, name: '10,000–34,999 dwt'},
      {type:6, id: 6, name: '0–9999 dwt'},
      {type:6, id: 7, name: 'Average'},

      {type:7, id: 1, name: '10,000+ dwt'},
      {type:7, id: 2, name: '5000–9999 dwt'},
      {type:7, id: 3, name: '0–4999 dwt'},
      {type:7, id: 4, name: '10,000+ dwt 100+ TEU'},
      {type:7, id: 5, name: '5000–9999 dwt 100+ TEU'},
      {type:7, id: 6, name: '0–4999 dwt 100+ TEU'},
      {type:7, id: 7, name: 'Average'},

      {type:8, id: 1, name: '8000+ TEU'},
      {type:8, id: 2, name: '5000–7999 TEU'},
      {type:8, id: 3, name: '3000–4999 TEU'},
      {type:8, id: 4, name: '2000–2999 TEU'},
      {type:8, id: 5, name: '1000–1999 TEU'},
      {type:8, id: 6, name: '0–999 TEU'},
      {type:8, id: 7, name: 'Average'},

      {type:9, id: 1, name: '4000+ CEU'},
      {type:9, id: 2, name: '0–3999 CEU'},
      {type:9, id: 3, name: 'Average'},

      {type:10, id: 1, name: '2000+ LM'},
      {type:10, id: 2, name: '0–1999 LM'},
      {type:10, id: 3, name: 'Average'},

      {type:11, id: 1, name: 'Average'},

      {type:12, id: 2, name: 'All dwt'},

    ]);
    this.fireExtTypes.push(...[
      // {id: 1, name: 'W/Gas'},
      {id: 2, name: 'CO2'},
      // {id: 3, name: 'D/C/P'},
    ])
    this.emissionSources.push(...[
      {id: 1, name: 'Business Air Travel'},
      {id: 2, name: 'Fire Extinguisher'},
      {id: 3, name: 'Refrigerants'},
      {id: 4, name: 'Generators'},
      {id: 5, name: 'Electricity'},
      {id: 6, name: 'Waste Disposal'},
      {id: 7, name: 'Municipal Water'},
      {id: 8, name: 'Employee Commuting'},
      {id: 9, name: 'Transport'},
      {id: 10, name: 'Waste Transport'},
      {id: 11, name: 'Transport Locally Purchased'},
    ])
    this.districts.push(...[
      {name: 'Ampara'},
      {name: 'Anuradhapura'},
      {name: 'Badulla'},
      {name: 'Batticaloa'},
      {name: 'Colombo'},
      {name: 'Galle'},
      {name: 'Gampaha'},
      {name: 'Hambantota'},
      {name: 'Jaffna'},
      {name: 'Kalutara'},
      {name: 'Kandy'},
      {name: 'Kegalle'},
      {name: 'Kilinochchi'},
      {name: 'Kurunegala'},
      {name: 'Mannar'},
      {name: 'Matale'},
      {name: 'Matara'},
      {name: 'Moneragala'},
      {name: 'Mullaitivu'},
      {name: 'Nuwara Eliya'},
      {name: 'Polonnaruwa'},
      {name: 'Puttalam'},
      {name: 'Ratnapura'},
      {name: 'Trincomalee'},
      {name: 'Vavuniya'},
      {name: 'Not Available'}
    ])
    this.roles.push(...[
      {id: 1, name: 'CEO'},
      {id: 2, name: 'Finance Officer'},
      {id: 3, name: 'Sustainability Officer'},
      {id: 4, name: 'Environmental Engineer'},
      {id: 5, name: 'Head of Low Emissions Initiatives'},
      {id: 6, name: 'Head of Sustainability Strategies/ Head of Finance'},
      {id: 7, name: 'Sustainability Engineer'},

    ])

    this.biomassTypes.push(...[
      {id: 1, name: 'Wood Chips'},
      {id: 2, name: 'Saw Dust'},
      {id: 3, name: 'Other Primary Solid Biomass'}
      ])


  }

  public loadBranches(): Observable<any> {
    let jsonBody ;
    switch (UserState.getInstance().userType) {
      case 1: {
         jsonBody = {
           FILTER_MODEL: {
             id: { value: UserState.getInstance().branchId , type: 1, col: 1}
           },
           PAGE_NUMBER: -1,
         }
        break
      }
      case 2: {
        jsonBody = {
          FILTER_MODEL: {
            companyId: { value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break;
      }
      case 3: {
        jsonBody = {
          PAGE_NUMBER: -1,
        }
        break;
      }
    }
    return from(this.boservice.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListBranch,
      jsonBody
    )).pipe().map(data => {
      // console.log(data);
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          this.branches = [];
          data.DAT.list.forEach(val => {
            if (val != undefined) {
             this.branches.push({id: val.id, name: val.name,  companyId: val.companyId});
            }
          });
          // console.log(this.branches)

        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
      return this.branches;
    })
  }

  public loadCompaninesForDataEnry() :Observable<any> {
    // if (this.companiesDataEntry.length > 0) {
    //   return observableOf(this.companiesDataEntry)
    // }
    let jsonBody ;
    switch (UserState.getInstance().userType) {
      case 1: {
        jsonBody = {
          FILTER_MODEL: {
            id: { value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break
      }
      case 2: {
        jsonBody = {
          FILTER_MODEL: {
            id: {value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break;
      }
      case 3: {
        jsonBody = {
          PAGE_NUMBER: -1,
        }
        break;
      }
    }
    return from(this.boservice.sendRequestToBackend(
      RequestGroup.Institution,
      //todo: change if required
      RequestType.ListCompany,
      jsonBody
    )).pipe().map(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          this.companies = []
          this.years = [];
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.companiesDataEntry = []
              this.companiesDataEntry.push({id: val.id, name: val.name, fy: val.isFinancialYear === 2 ? true: false});

                // console.log(val)
                // this.years = [];
                this.companies.push({
                  id: val.id,
                  name: val.name,
                  fy: val.isFinancialYear === 2 ? true: false,
                  pages: val.pages,
                  emSources: val.emSources,
                  allowedEmissionSources: val.allowedEmissionSources,

                });
                this.years.push({comId: val.id, years: [ val.isFinancialYear === 2 ? `${val.fyCurrentStart}/${(val.fyCurrentEnd % 100)}`: `${val.fyCurrentStart}`]})

            }
          });
          //todo; this.companines
          return this.companies;
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
      return this.companies;
    })
  }


  public loadCompaninesForSummaryInputs() :Observable<any> {
    // if (this.companiesSummaryInputs.length > 0) {
    //   return observableOf(this.companiesSummaryInputs);
    // }
    let jsonBody ;
    switch (UserState.getInstance().userType) {
      case 1: {
        jsonBody = {
          FILTER_MODEL: {
            id: { value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break
      }
      case 2: {
        jsonBody = {
          FILTER_MODEL: {
            id: {value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break;
      }
      case 3: {
        jsonBody = {
          PAGE_NUMBER: -1,
        }
        break;
      }
    }
    return from(this.boservice.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListCompanySummaryInputs,
      jsonBody
    )).pipe().map(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          this.companies = []
          // this.years = [];
          this.companiesSummaryInputs = []
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              // console.log(val)
              this.companiesSummaryInputs.push({id: val.id, name: val.name, fy: val.isFinancialYear === 2 ? true: false});
              // this.years = [];
              // this.years.push({comId: val.id, years: [ val.isFinancialYear === 2 ? `${val.fyCurrentStart}/${(val.fyCurrentEnd % 100)}`: `${val.fyCurrentStart}`]})

            }
          });
          return this.companiesSummaryInputs;
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
      return this.companiesSummaryInputs;
    })
  }

  public loadCompaniesDtos():Observable<any> {
    // if (this.companies.length > 0) {
    //   return observableOf(this.companies);
    // }
    let jsonBody ;
    switch (UserState.getInstance().userType) {
      case 1: {
        jsonBody = {
          FILTER_MODEL: {
            id: { value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break
      }
      case 2: {
        jsonBody = {
          FILTER_MODEL: {
            id: {value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break;
      }
      case 3: {
        jsonBody = {
          PAGE_NUMBER: -1,
        }
        break;
      }
      case 4: {
        jsonBody = {
          FILTER_MODEL: {
            id: {value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break;
      }
    }
    return from(this.boservice.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListCompany,
      jsonBody
    )).pipe().map(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {

          this.companyDtos = []
          // console.log(data);
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.companyDtos.push(val);
            }
          });
          return this.companyDtos;
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
      return this.companyDtos;
    })
  }



  public loadCompanies():Observable<any> {
    // if (this.companies.length > 0) {
    //   return observableOf(this.companies);
    // }
    let jsonBody ;
    switch (UserState.getInstance().userType) {
      case 1: {
        jsonBody = {
          FILTER_MODEL: {
            id: { value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break
      }
      case 2: {
        jsonBody = {
          FILTER_MODEL: {
            id: {value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break;
      }
      case 3: {
        jsonBody = {
          PAGE_NUMBER: -1,
        }
        break;
      }
      case 4: {
        jsonBody = {
          FILTER_MODEL: {
            id: {value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break;
      }
    }
    return from(this.boservice.sendRequestToBackend(
      RequestGroup.Institution,
      RequestType.ListCompany,
      jsonBody
    )).pipe().map(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          this.companies = []
          this.years = [];
          // console.log(data);
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              // this.companyDtos.push(val);
              // console.log(val)
              // this.years = [];
              this.companies.push({id: val.id, name: val.name, fy: val.isFinancialYear === 2 ? true: false, pages: val.pages,
                emSources: val.emSources, allowedEmissionSources: val.allowedEmissionSources});
              this.years.push({comId: val.id, years: [ val.isFinancialYear === 2 ? `${val.fyCurrentStart}/${(val.fyCurrentEnd % 100)}`: `${val.fyCurrentStart}`]})

            }

          });
          return this.companies;
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
      return this.companies;
    })
  }


  getFreightCountries(){
    return this.seaports.map(sp => sp.country);
  }



  private loadCountries() {
    this.boservice.sendRequestToBackend(
      RequestGroup.MasterData,
      RequestType.ListCountries,
      {
        PAGE_NUMBER: -1,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              this.countries.push({id: val.id, name: val.name});
            }
          });
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  private loadAirports() {
    this.boservice.sendRequestToBackend(
      RequestGroup.MasterData,
      RequestType.ListAirports,
      {
        PAGE_NUMBER: 1,
      }
    ).then(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {
          let list = [];
          data.DAT.list.forEach(val => {
            if (val != undefined) {
              var code = val.name.substring(
                val.name.indexOf("(") + 1, 
                val.name.lastIndexOf(")")
            );
              this.airports.push({id: val.id, name: val.name, code: code});
            }
          });
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
    })
  }

  public loadEmployees(): Observable<any> {

    let jsonBody ;
    switch (UserState.getInstance().userType) {
      case 1: {
        jsonBody = {
          FILTER_MODEL: {
            branchId: { value: UserState.getInstance().branchId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break
      }
      case 2: {
        jsonBody = {
          FILTER_MODEL: {
            comId: { value: UserState.getInstance().companyId , type: 1, col: 1}
          },
          PAGE_NUMBER: -1,
        }
        break;
      }
      case 3: {
        jsonBody = {
          PAGE_NUMBER: -1,
        }
        break;
      }
    }
    // todo: user type filet3er
    return from(this.boservice.sendRequestToBackend(
      RequestGroup.BusinessUsers,
      RequestType.ListEmployee,
      jsonBody
    )).pipe().map(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.list != undefined) {

          this.employees = []
          data.DAT.list.forEach(val => {
            if (val != undefined) {

              this.employees.push({id: val.id, name: val.name});
            }
          });
          // console.log(this.employees)
        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
        //  todo: show snack bar
        // console.log('error data 2')
      }
      return this.employees;
    })
  }

  public loadProjects():Observable<any> {

    return from( this.boservice.sendRequestToBackend(
      RequestGroup.GHGProject,
      RequestType.ListProject,
      {
        PAGE_NUMBER: -1,
        FILTER_MODEL: {
          status: { value: 7, type: 2, col: 1}
        }
      }
    )).pipe().map(data => {
      if (data.HED != undefined && data.HED.RES_STS == 1) {
        if (data.DAT != undefined && data.DAT.LIST != undefined) {

          this.projects = [];

          data.DAT.LIST.forEach(val => {
            if (val != undefined && val.status >= 5) {
              this.projects.push({id: val.id, name: val.name, status: val.status, comId: val.companyId});
            }
          });

        } else {
          //  todo: show snack bar
          // console.log('error data 1')
        }
      } else {
    }
      return this.projects;
    }
    );
  }



  public getBranchName(id: number) :Observable<string> {
    if (id == 0 || id == undefined) return observableOf("")
    const branch = this.branches.filter(value=> {return value.id === id})[0];
    return observableOf(branch ? branch.name : "");
  }


  public getEmissionSrcId(name: string): Observable<number> {
    const id  = this.emissionSources.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getEmissionSrcName(id: number) :Observable<string> {
    if (id == 0 || id == undefined) return observableOf("")
    const name = this.emissionSources.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getPublicVehcileId(name: string): Observable<number> {
    const id  = this.publicVehicleTypes.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getPublicVehicleName(id: number) :Observable<string> {
    if (id == 0 || id == undefined) return observableOf("")
    const name = this.publicVehicleTypes.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  


    public getMitigationActionTypeName(id: number) :Observable<string> {
      if (id === undefined) return observableOf("")
      const name = this.mitigationProjects.filter(value=> {return value.id === id})[0].name;
      return observableOf(name);
    }
    public getMitigationActionTypeId(name: string) : Observable<number> {
      const id = this.mitigationProjects.filter(value => value.name === name)[0].id;
      return observableOf(id);
    }

  public getBranchId(name: string): Observable<number> {
    const id  = this.branches.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getEmployeeName(id: number) :Observable<string> {
    if (id == 0 || id == undefined) return observableOf("")
    const name = this.employees.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getEmployeeId(name: string): Observable<number> {
    const id  = this.employees.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getCompanyName(id: number) : Observable<string> {
    if (id == 0 || id == undefined) return observableOf("")
    // console.log(id);
    // console.log(this.companies)
    const name = this.companies.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getCompanyId(name: string) : Observable<number> {
    const id  = this.companies.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public isFy(id: number) : Observable<boolean> {
    return observableOf(this.companies.filter(v => v.id === id)[0].fy);
  }

  public getAirportName(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    // console.log(id);
    const name = this.airports.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getAirportId(name: string) : Observable<number> {
    const id  = this.airports.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getCountryName(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.countries.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getCountryId(name: string) : Observable<number> {
    const id  = this.countries.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getSeatClassName(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.seatClasses.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getSeatClassId(name: string) :Observable<number> {
    const id  = this.seatClasses.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getAirTravelWay(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.airTravelways.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getAirTravelWayId(name: string) : Observable<number> {
    const id  = this.airTravelways.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getMonthName(id: number) :Observable<string> {
    if (id === undefined) return observableOf("")
    const name = this.months.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getMonthId(name: string) : Observable<number> {
    const id = this.months.filter(value => value.name === name)[0].id;
    return observableOf(id);
  }

  public getRefriTrypName(id: number): Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.refriTypes.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getRefriTypeId(name: string) : Observable<number> {
    const id  = this.refriTypes.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getUnitsId(name: string): Observable<number> {
    const id  = this.units.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getUnitsName(id: number): Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.units.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getVehicleTypeId(name: string) :Observable<number> {
    const id  = this.vehicleTypes.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getVehicleTypeName(id :number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.vehicleTypes.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getFuelTypeName(id: number) :Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.fuelTypes.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getFuelTypeId(name: string) : Observable<number> {
    // console.log(name)
    const id  = this.fuelTypes.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getWastTypeId(name: string) : Observable<number> {
    const id  = this.wasteTypes.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getWastTypeName(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.wasteTypes.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getDisposalMethodId(name: string) : Observable<number> {
    const id  = this.disMethods.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getDisposalMethodName(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.disMethods.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getVehicleCategory(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.vehicleCategroies.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getVehicleCatId(name: string) : Observable<number> {
    const id  = this.vehicleCategroies.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getFireExtTypeId(name: string) : Observable<number> {
    const id  = this.fireExtTypes.filter(value => value.name === name)[0].id;
    return observableOf (id);
  }

  public getFireExtTypeName(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    const name = this.fireExtTypes.filter(value=> {return value.id === id})[0].name;
    return observableOf(name);
  }

  public getRoleId(name: string): Observable<number> {
    if (name === undefined || name=== '' ){
      return observableOf(0);
    }
    const id = this.roles.filter(value => value.name === name)[0].id;
    return observableOf(id);
  }

  public getRoleName(id: number): Observable<string> {
    if (id === 0 || id === undefined) return observableOf("")
    const name = this.roles.filter(value => {return value.id === id})[0].name;
    return observableOf(name);
  }

  getAirportCodes(): Observable<any[]> {
    return observableOf(this.airports.map(v => v.name));
  }

  getBranches(): Observable<any[]> {
    // return observableOf(this.branches.map(v => v.name));
    return this.loadBranches().map(v => v.map(d => d.name))
  }



  getBranchesFull(): Observable<any[]> {
    // return observableOf(this.branches)
    return this.loadBranches();
  }

  getCountries(): Observable<any[]> {
    return observableOf(this.countries.map(v => v.name));
  }

  getCountriesFull(): Observable<any[]> {
    return observableOf(this.countries);
  }

  getCompaninesFull(): Observable<any[]> {
    return this.loadCompanies();
    // return observableOf(this.companies)
  }
  getAirportsFull(): Observable<any[]> {
    return observableOf(this.airports)
  }

  getFreightAirportsFull(): Observable<any[]> {
    return observableOf(this.freightAirports)
  }


  getStatinsFull(): Observable<any[]>{
    return observableOf(this.stations)
  }

  getSeeportsFull(): Observable<any[]> {
    return observableOf(this.seaports)
  }

  getWasteDisMethodsFull(): Observable<any[]> {
    return observableOf(this.disMethods);
  }

  getWasteDisTypes(method: number): Observable<any[]> {
    return observableOf(this.wasteTypes.filter(d => { return (d.method=== method);}))
  }


  getSeatClassesFull(): Observable<any[]> {
    return observableOf(this.seatClasses);
  }
  getMonthsFull(): Observable<any[]> {
    return observableOf(this.months);
  }

  getMitigationActionFull(): Observable<any[]> {
    return observableOf(this.mitigationProjects);
  }
  getPortsFull() : Observable<any[]> {
    return observableOf(this.ports);
  }

  getBiomassTypesFull() : Observable<any[]> {
    return observableOf(this.biomassTypes);
  }

  getFreightTypesFull() : Observable<any[]> {
    return observableOf(this.freightTypes);
  }

  getTransModesFull() : Observable<any[]> {
    return observableOf(this.transModes);
  }


  getRolesFull():Observable<any[]> {
    return observableOf(this.roles);
  }
  getBranchesForCompany(comId: number) : Observable<any[]> {
    return observableOf(this.branches.filter(v => v.companyId === comId));
  }
  getCargoTypes() {
    return [
      { code: "AGRICULTURAL_PRODUCTS_AND_LIVE_ANIMALS", name: "Agricultural products and live animals"},
      { name: "Beverage", code: "BEVERAGE"},
      {name: "Groceries", code: "GROCERIES"},
      { name: "Perishable and semi-perishable foodstuff and canned food", code: "PERISHABLE_AND_SEMI_PERISHABLE_FOODSTUFF_AND_CANNED_FOOD"},
      { name: "Other food products and fodder", code: "OTHER_FOOD_PRODUCTS_AND_FODDER"},
      { name: "Solid minerul fuels and petroleum products", code: "SOLID_MINERUL_FUELS_AND_PETROLEUM_PRODUCTS"},
      { name: "Ores and metal waste", code: "ORES_AND_METAL_WASTE"},
      { name: "Metal products", code: "METAL_PRODUCTS"},
      { name: "Mineral products", code: "MINERAL_PRODUCTS"},
      { name: "Other crude and manufactured minerals and building materials", code: "OTHER_CRUDE_AND_MANUFACTURED_MINERALS_AND_BUILDING_MATERIALS"},
      { name: "Fertilizers", code: "FERTILIZERS"},
      { name: "Chemicals", code: "CHEMICALS"},
      { name: "Transport equipment", code: "TRANSPORT_EQUIPMENT"},
      { name: "These images are not clear", code: "THESE_IMAGES_ARE_NOT_CLEAR"},
      { name: "Glass and ceramic and porcelain products", code: "GLASS_AND_CERAMIC_AND_PORCELAIN_PRODUCTS"},
      { name: "Grouped goods", code: "GROUPED_GOODS"},
      { name: "Other manufactured articles", code: "OTHER_MANUFACTURED_ARTICLES"},
    ]
  }
  getYearsForCompany(comId: number): Observable<any[]> {
    // console.log(this.years)
    let list = this.years.filter(v => v.comId === comId);
    // console.log(list);
    return observableOf(list.map(v => {return v.years[0]}));
  }


  getPublicVehicleTypes(): Observable<any[]> {
    return observableOf(this.publicVehicleTypes.map(v => v.name));
  }
  getPublicVehicleTypesFull(): Observable<any[]> {
    return observableOf(this.publicVehicleTypes);
  }

  getDisposalMethods(): Observable<any[]> {
    return observableOf(this.disMethods.map(v => v.name));
  }
  getDisposalMethodsFull(): Observable<any[]> {
    return observableOf(this.disMethods);
  }

  getEmployees(): Observable<any[]> {
    // return observableOf(this.employees.map(v => v.name));
    return this.loadEmployees().map(v => v.map(d => d.name));
  }

  getFireExtTypes(): Observable<any[]> {
    return observableOf(this.fireExtTypes.map(v => v.name));
  }
  getFireExtTypesFull(): Observable<any[]> {
    return observableOf(this.fireExtTypes);
  }

  getFueltypes(): Observable<any[]> {
    return observableOf(this.fuelTypes.map(v => v.name));
  }
  getFueltypesFull(): Observable<any[]> {
    return observableOf(this.fuelTypes);
  }
  public getFuelTypesGenerator(): Observable<any[]> {
    return observableOf(this.fuelTypesGenerators.map(v => v.name));
  }
  public getFuelTypesGeneratorFull(): Observable<any[]> {
    return observableOf(this.fuelTypesGenerators);
  }
  getFuelTypesVehicleFull():Observable<any[]> {
    return observableOf(this.fueltTypesVehicle);
  }
  getStrokeFull():Observable<any[]> {
    return observableOf(this.stroke);
  }

  getFuelTypesVehicleFullFreight():Observable<any[]> {
    return observableOf(this.fueltTypesVehicleFreight);
  }
  getUnitDistance():Observable<any[]> {
    return observableOf(this.unitsDistance);
  }
  getFuelTypesVehicleYFull():Observable<any[]> {
    return observableOf(this.fueltTypesVehicleY);
  }


  getMonths(): Observable<any[]> {
    return observableOf(this.months.map(v => v.name));
  }

  getRefrigerantTypes(): Observable<any[]> {
    return observableOf(this.refriTypes.map(v => v.name));
  }
  getRefrigerantTypesFull(): Observable<any[]> {
    return observableOf(this.refriTypes);
  }

  getSeatClasses(): Observable<any[]> {
    return observableOf(this.seatClasses.map(v => v.name));
  }

  getTransportModes(): Observable<any[]> {
    return observableOf(this.vehicleTypes.map(v => v.name));
  }

  public getWasteTransVehicleTypes():Observable<any[]> {
    return observableOf(this.wasteTransVehcileTypes.map(v => v.name));
  }
  public getWasteTransVehicleTypesFull():Observable<any[]> {
    return observableOf(this.wasteTransVehcileTypes);
  }

  getUnits(): Observable<any[]> {
    return observableOf(this.units.map(v => v.name));
  }
  public getUnitsGenerators(): Observable<any[]> {
    return observableOf(this.unitsGenerators.map(v => v.name));
  }
  public getUnitsGeneratorsFull(): Observable<any[]> {
    return observableOf(this.unitsGenerators);
  }


  getVehicleCategories(): Observable<any[]> {
    return observableOf(this.vehicleCategroies.map(v => v.name));
  }
  getVehicleCategoriesFull(): Observable<any[]> {
    return observableOf(this.vehicleCategroies);
  }
  getOffRoadVehicleFull(): Observable<any[]> {//vehicleRoad
    return observableOf(this.vehicleOffroad);
  }

  getRoadVehiclTranseFull(): Observable<any[]> {//vehicleRoad
    return observableOf(this.vehicleRoadTrans);
  }

  getRailVehiclTranseFull(): Observable<any[]> {//vehicleRoad
    return observableOf(this.vehicleRoadRailTrans);
  }

  getRoadVehicleFull(): Observable<any[]> {//vehicleRoad
    return observableOf(this.vehicleRoad);
  }
  getMethod(): Observable<any[]> {
    return observableOf(this.methods);
  }
  getActivity(): Observable<any[]> {
    return observableOf(this.activities);
  }

  getType(activity: number): Observable<any[]> {
    return observableOf(this.types.filter(d => { return (d.activity=== activity);}))
  }

  getSeaports(country: string): Observable<any[]> {
    return observableOf(this.seaports.filter(d => { return (d.country.toLowerCase() === country.toLowerCase());}))
  }

  getSize(type: number): Observable<any[]> {
    return observableOf(this.sizes.filter(d => { return (d.type=== type);}))
  }
  getVehicleModels(): Observable<any[]> {
    return observableOf(this.vehicleTypes.map(v => v.name));
  }
  getVehicleModelsFull(): Observable<any[]> {
    return observableOf(this.vehicleTypes);
  }

  getWasteTypes(): Observable<any[]> {
    return observableOf(this.wasteTypes.map(v => v.name));
  }
  getWasteTypesFull(): Observable<any[]> {
    return observableOf(this.wasteTypes);
  }

  getYears(): Observable<any[]> {
    return observableOf(['2018', '2019']);
  }

  getMatrialtypes(): Observable<any[]> {
    return observableOf(this.wasteTypes.map(v => v.name));
  }
  getMatrialtypesFull(): Observable<any[]> {
    return observableOf(this.wasteTypes);
  }

  public getTitles(): Observable<string[]> {
    return observableOf(this.titles);
  }


  public getMaterialTypeId(name: string): Observable<number> {
    return observableOf(1);
  }

  public getMaterialTypeName(id: number) : Observable<string> {
    if (id == 0 || id === undefined) return observableOf("")
    return observableOf(this.wasteTypes.filter(v =>  v.id === id)[0].name);
  }

  public getCompanies(): Observable<any[]> {
    // return observableOf(this.companies.map(v => v.map(d => d.name));
    return this.loadCompanies().map(v => v.map(d => d.name));
  }

  public getEmissionSources(): Observable<string[]> {
    return observableOf(this.emissionSources.map(v => v.name));
  }

  public getUnitsElectricity(): Observable<string[]> {
    return observableOf(this.unitsElectrictity.map(v => v.name));
  }

  public getUnitsMunWater():Observable<string[]> {
    return observableOf(this.unitsMunWater.map(v => v.name));
  }

  public getDistricts():Observable<string[]> {
    return observableOf(this.districts.map(v => v.name));
  }

  public getNoEmissionOptionsFull():Observable<any[]> {
    return observableOf(this.noEmissionOptions);
  }

  public getVehiclesOwnFull(): Observable<any[]> {
    return observableOf(this.vehcielTypesOwn);
  }

}
