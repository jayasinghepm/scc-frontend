
export enum RequestType {

  LoginNormal = 1,
  LogoutNormal = 2,


//  Emission
  ManageAirtravelEntry = 1,
  ManageElectricityEntry = 2,
  ManageFireExtEntry = 3,
  ManageGeneratorsEntry  = 4,
  ManageMunWaterEntry = 5,
  ManageRefriEntry = 6,
  ManageTransLocEntry = 7,
  ManageVehicleEntry = 8,
  ManageWasteDisposalEntry = 9,
  ManageWasteTransportEntry = 10,
  ManageFrieghtTransport = 60,
  ListAirtravelEntry = 11,
  ListElectricityEntry = 12,
  ListFireExtEntry = 13,
  ListGeneratorsEntry = 14,
  ListMunWaterEntry = 15,
  ListRefriEntry = 16,
  ListTransLocal = 17,
  ListVehicleEntry = 18,
  ListWasteDisposalEntry = 19,
  ListWasteTransportEntry = 20,
  LisrFrieghtTransport = 61,
  ListWeldingEntry = 66,
  ManageWelding = 65,



  ManageEmpCommuting = 22,
  ListEmpCommuting = 21,
  GHGEmissionSummary = 23,
  GHGEmissionSummaryBranchwise = 24,
  GHGEmissionSummaryExcluded = 25,
  ManageGHGEmissionSummary = 26,
  ExportGHGEmissionSummary = 27,

  ListSeaAirFreightEntry=28 ,
  ManageSeaAirFreightEntry = 29,
  ListLPGEntry = 32 ,
  ListBgasEntry= 58,
  ManageBgasEntry =59,
  ManageLPGEntry = 33,
  ListBioMassEntry= 30 ,
  ManageBioMassEntry = 31,
  ManageAshTransportation = 34,
  ListAshTransportation= 35,
  ManageForklifts = 36,
  ListForklifts = 37,
  ManageFurnaceOil =  38,
  ListFurnaceOil = 39,
  ManageLorryTransportation = 40,
  ListLorryTransportation = 41,
  ManageOilGasTransportation = 42,
  ListOilGasTransportation = 43,
  ManagePaidManagerVehicle = 44,
  ListPaidManagerVehicle = 45,
  ManagePaperWaste = 46,
  ListPaperWaste = 47,
  ManageRawMaterialTransportation = 48,
  ListRawMaterialTransportation = 49,
  ManageSawDustTrasnportation = 50,
  ListSawDustTransportation = 51,
  ManageSludgeTransportation = 52,
  ListSludgeTransportation = 53,
  ManageStaffTransportation = 54,
  ListStaffTransportation = 55,
  ManageVehicleOthers = 56,
  ListVehicleOthers = 57,

  ManageWasteWaterTreatmentEntry=62,
  ListWastWaterTreatmentEntry=63,
  

  // institution
  ListBranch =3,
  ListCompany = 4,
  ManageBranch = 1,
  ManageCompany = 2,
  ListCompanyDataEntry = 5,
  ListCompanySummaryInputs = 6,
  ManageMenuCompany =7,
  ManageEmissionCategory = 8,
  UpdateCompanySuggestionsAndExcludedReasons = 9,


  // business users

  ListEmployee = 5,
  ListAdmin = 6,
  ListClient = 7,
  ListComAdmin = 8,
  ManageEmployee = 1,
  ManageAdmin = 2,
  ManageClient = 3,
  ManageComAdmin =4,
  ManageDataEntryUser = 9,
  ListDataEntryUser = 10,


//  User login

  ManageUserLogin = 1,
  FindUserLogin = 2,
  ListUserActLogs = 3,
  CreateBranchLoginProfile = 4,


//  Master Data
  ListCountries = 1,
  ListAirports = 2,

//  Emission Factors
  ManageEmissionFactors = 1,
  ManagePubTransFactors = 2,
  ManageWasteDisFactors = 3,
  ListEmFactors = 4,
  ListPubTransFactors = 5,
  ListWastDisFactors = 6,

//  GHG Projects
  ListProject = 2,
  ManageProject = 1,

//  Summary inputs

  ElecSummInputs = 1,
  MunWaterSummInputs = 2,
  MunRefriSummInputs = 3,
  FireExtSummInputs = 4,
  WasteTransSummInputs = 5,
  ComOwnedSummInputs = 6,
  EmpCommSummInputs = 7,
  AirtravelSummInputs = 8,
  RentedSummInputs = 9,
  HiredSummInputs = 10,
  GeneratorSummInpust = 11,
  WasteDisSummInputs = 12,
  TransLocPurSummInputs = 13,

  DataEntryStatus =14,
  ManageDataEntryStatus = 15,

  SeaAirFreightSummInputs = 16,

  GetProjectSummary = 19,




//  Report
  ManageReport = 1,
  ListReport = 2,
  ManageReportMeta = 3,
  ListReportMeta = 4,
  GetReportZip = 5,



  //mitigation
  ManageMitigationProject = 1,
  GetMitigationProjects =2,
  ManageMitigationProjectMonthlyActivity=3,
  GetMitigationProjectMonthlyActivity=4,



}
