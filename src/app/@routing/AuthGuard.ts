import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {UserState} from "../@core/auth/UserState";

@Injectable()
export class AuthGuard implements CanActivate {



  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (UserState.getInstance().isAuthenticated) {
      return true;
    }
    // console.log("Auth guard")
    this.router.navigate([''], { queryParams: { returnUrl: state.url }});
    return false;
  }
  constructor(private router: Router) {}

}
