import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CoreModule } from './@core/core.module';
import { AppRoutingModule } from './@routing/app-routing.module';
import { AppComponent } from './app.component';
import { ThemeModule } from './@theme/theme.module';
import {
  NbSidebarModule,
  NbMenuModule,
  NbDatepickerModule,
  NbDialogModule,
  NbWindowModule,
  NbToastrModule,
  NbChatModule,
  NbListModule
} from '@nebular/theme';
import { AuthModule } from './@core/components/auth/auth.module';

import {
  NbActionsModule,
  NbAlertModule,
  NbButtonModule,
  NbCalendarKitModule,
  NbCalendarModule,
  NbCalendarRangeModule,
  NbCardModule,
  NbIconModule,
  NbProgressBarModule,
  NbSelectModule,
  NbSpinnerModule,
  NbTabsetModule,
} from '@nebular/theme';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PagesModule} from "./pages/pages.module";
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {UpdateBranchComponent} from "./pages/cadmin/cadmin-branches/update-branch/update-branch.component";
import {RemoveBranchComponent} from "./pages/cadmin/cadmin-branches/remove-branch/remove-branch.component";
import {AddClientComponent} from "./pages/admin/admin-clients/add-client/add-client.component";
import {RemoveClientComponent} from "./pages/admin/admin-clients/remove-client/remove-client.component";
import {OwlDateTimeModule, OwlNativeDateTimeModule} from "ng-pick-datetime";
import {AddCompanyComponent} from "./pages/admin/admin-companines/add-company/add-company.component";
import {RemoveCompanyComponent} from "./pages/admin/admin-companines/remove-company/remove-company.component";
import {AuthGuard} from "./@routing/AuthGuard";
import {SelectDropDownModule} from "ngx-select-dropdown";
import {AddCadminComponent} from "./pages/admin/com-admins/add-cadmin/add-cadmin.component";
import {ViewClientComponent} from "./pages/admin/admin-clients/view-client/view-client.component";
import {ViewComadminComponent} from "./pages/admin/com-admins/view-comadmin/view-comadmin.component";
import {ElecFormulaComponent} from "./pages/formulas/elec-formula/elec-formula.component";
import {SettingComponent} from "./pages/setting/setting.component";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,

} from "@angular/material";
import {ChangeStatusComponent} from "./pages/admin/ghg-project/change-status/change-status.component";
import {Ng2SmartTableModule} from "./ng2-smart-table/src/lib/ng2-smart-table.module";
import { ActivityLogsComponent } from './pages/admin/activity-logs/activity-logs.component';
import {LoadingScreenComponent} from "./@core/components/loading-component/loading-component.component";

import {LoadingScreenInterceptor} from "./@core/rest/loding-http-interceptor";
import {FireExtFormulaComponent} from "./pages/formulas/fire-ext-formula/fire-ext-formula.component";
import {RefriFormulaComponent} from "./pages/formulas/refri-formula/refri-formula.component";
import {EmpCommFormulaComponent} from "./pages/formulas/emp-comm-formula/emp-comm-formula.component";
import {GeneratorsFormulaComponent} from "./pages/formulas/generators-formula/generators-formula.component";
import {MunWaterFormulaComponent} from "./pages/formulas/mun-water-formula/mun-water-formula.component";
import {WastDisFormulaComponent} from "./pages/formulas/wast-dis-formula/wast-dis-formula.component";
import {WastTransFormulaComponent} from "./pages/formulas/wast-trans-formula/wast-trans-formula.component";
import {TransportFormulaComponent} from "./pages/formulas/transport-formula/transport-formula.component";
import {TransLocPurFormulaComponent} from "./pages/formulas/trans-loc-pur-formula/trans-loc-pur-formula.component";
import { AshTransFormulaComponent } from './pages/formulas/ash-trans-formula/ash-trans-formula.component';
import { FurnaceOilFormulaComponent } from './pages/formulas/furnace-oil-formula/furnace-oil-formula.component';
import { ForkliftsFormulaComponent } from './pages/formulas/forklifts-formula/forklifts-formula.component';
import { LorryTransFormulaComponent } from './pages/formulas/lorry-trans-formula/lorry-trans-formula.component';
import { OilGasTransFormulaComponent } from './pages/formulas/oil-gas-trans-formula/oil-gas-trans-formula.component';
import { PaidManagerVehicleFormulaComponent } from './pages/formulas/paid-manager-vehicle-formula/paid-manager-vehicle-formula.component';
import { PaperWasteFormulaComponent } from './pages/formulas/paper-waste-formula/paper-waste-formula.component';
import { RawMatTransFormulaComponent } from './pages/formulas/raw-mat-trans-formula/raw-mat-trans-formula.component';
import { SawDustTransFormulaComponent } from './pages/formulas/saw-dust-trans-formula/saw-dust-trans-formula.component';
import { SludgeTransFormulaComponent } from './pages/formulas/sludge-trans-formula/sludge-trans-formula.component';
import { OtherVehiclesFormulaComponent } from './pages/formulas/other-vehicles-formula/other-vehicles-formula.component';
import { LpGasFormulaComponent } from './pages/formulas/lp-gas-formula/lp-gas-formula.component';
import { FreightFormulaComponent } from './pages/formulas/freight-formula/freight-formula.component';
import { BiomassFormulaComponent } from './pages/formulas/biomass-formula/biomass-formula.component';
import { ProjectSummaryComponent } from './pages/project-summary/project-summary.component';
// import { LandingComponentComponent } from './@core/components/landing-component/landing-component.component';



// import {EmissionFactorsComponent} from "./pages/admin/emission-factors/emission-factors.component";
// import {PubTransFactorsComponent} from "./pages/admin/pub-trans-factors/pub-trans-factors.component";
// import {WasteDisFactorsComponent} from "./pages/admin/waste-dis-factors/waste-dis-factors.component";


@NgModule({
  declarations: [
    AppComponent,
    UpdateBranchComponent,
    RemoveBranchComponent,
    // AddClientComponent,
    // RemoveClientComponent,
    RemoveCompanyComponent,
    AddCompanyComponent,
    ElecFormulaComponent,
    SettingComponent,
    // AddCadminComponent,
    ChangeStatusComponent,
    LoadingScreenComponent,
    // LandingComponentComponent,
    FireExtFormulaComponent,
    EmpCommFormulaComponent,
    GeneratorsFormulaComponent,
    MunWaterFormulaComponent,
    RefriFormulaComponent,
    TransLocPurFormulaComponent,
    TransportFormulaComponent,
    WastDisFormulaComponent,
    WastTransFormulaComponent,
    AshTransFormulaComponent,
    FurnaceOilFormulaComponent,
    ForkliftsFormulaComponent,
    LorryTransFormulaComponent,
    OilGasTransFormulaComponent,
    PaidManagerVehicleFormulaComponent,
    PaperWasteFormulaComponent,
    RawMatTransFormulaComponent,
    SawDustTransFormulaComponent,
    SludgeTransFormulaComponent,
    OtherVehiclesFormulaComponent,
    LpGasFormulaComponent,
    FreightFormulaComponent,
    BiomassFormulaComponent,
    ProjectSummaryComponent,
    
  
   
    
   


  ],
  imports: [
    // SelectDropDownModule,
    HttpClientModule,
    PagesModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AuthModule,
    NbActionsModule,
    NbAlertModule,
    NbButtonModule,
    NbCalendarKitModule,
    NbCalendarModule,
    NbCalendarRangeModule,
    NbCardModule,
    NbIconModule,
    NbProgressBarModule,
    NbSelectModule,
    NbSpinnerModule,
    NbTabsetModule,
    CoreModule.forRoot(),
    ThemeModule.forRoot(),
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    NbChatModule.forRoot({
      messageGoogleMapKey: 'AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY',
    }),
    NbDatepickerModule.forRoot(),
    ReactiveFormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    NbListModule,
    NgbModule,
    Ng2SmartTableModule,
  ],
  providers: [
AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingScreenInterceptor,
      multi: true
    }
  ],
  entryComponents: [
    UpdateBranchComponent,
    RemoveBranchComponent,
    // AddClientComponent,
    // RemoveClientComponent,
    AddCompanyComponent,
    RemoveCompanyComponent,
    ElecFormulaComponent,
    SettingComponent,
    // AddCadminComponent,
    ChangeStatusComponent,
    FireExtFormulaComponent,
    EmpCommFormulaComponent,
    GeneratorsFormulaComponent,
    MunWaterFormulaComponent,
    RefriFormulaComponent,
    TransLocPurFormulaComponent,
    TransportFormulaComponent,
    WastDisFormulaComponent,
    WastTransFormulaComponent,
    AshTransFormulaComponent,
    ForkliftsFormulaComponent,
    FurnaceOilFormulaComponent,
    LorryTransFormulaComponent,
    PaidManagerVehicleFormulaComponent,
    PaperWasteFormulaComponent,
    RawMatTransFormulaComponent,
    SludgeTransFormulaComponent,
    SawDustTransFormulaComponent,
    OilGasTransFormulaComponent,
    OtherVehiclesFormulaComponent,
    FreightFormulaComponent,
    BiomassFormulaComponent,
    LpGasFormulaComponent

  ],


  bootstrap: [AppComponent]
})
export class AppModule { }
